<?php
include('config/adm_maj_fonctionnalite-etat.php');
$codefonc='pro';
require_once('prepage.php');

require_once('adm_fonc_etat.php');
$objEtat = new etat();

if(!isset($typeaction)) $typeaction="modification";
if(!isset($cle)) $cle="";
//Trace("entrée dans adm_maj_fonctionnalite-etat.php = " . $typeaction);

$objEtat->creEtat($cleparent); // crée la table des objets etat

// réception des paramètres
if($typeaction == "reception")
{
	$f_profil=$_SESSION['f_profil'];
	$objEtat->majEtat($cleparent, $f_profil);
	$majok=true;
//	RedirURL("adm_list_fonctionnalite.php?menu=adm");
//	exit();
}

if ($cle != '') $objPage->addParam('cle', $cle);
$objPage->debPage('center');

// préparation du tableau
$objTab = new tableau('1');
$objFiltre = $objTab->getFiltre();
$objFiltre->setChange();
if (isset($_REQUEST['f_profil'])) $f_profil=$_REQUEST['f_profil'];
else if (isset($_SESSION['f_profil'])) $f_profil=$_SESSION['f_profil'];
$dataliste = "";
$requete = "select PRO_CLE, PRO_NOMPROFIL from profil where PRO_NOMPROFIL !=\"admin\" order by 2";
$statement = $conn->query($requete);
while ($row = $statement->fetch(PDO::FETCH_ASSOC))
{
	if(!isset($f_profil)) $f_profil=$row['PRO_CLE'];
	if ($f_profil == $row['PRO_CLE']) {$opt = " selected ";} else {$opt = "";}
	$dataliste .= "<OPTION value=\"" . $row['PRO_CLE'] . "\"" . $opt . ">" . $row['PRO_NOMPROFIL'] . "\n";
}
$objFiltre->setDataliste('f_profil', $dataliste);

$objEtat->affEtat($objTab, $cleparent, $f_profil);

// affichage des boutons d'enchainement
$objTab->addBouton("button","RETOUR","adm_list_fonctionnalite.php?menu=adm");
$objTab->addBoutonSpe("button","Effacer tout","onclick=\"javascript:razCheck()\"");
$objTab->addBoutonSpe("button","Sel. tout","onclick=\"javascript:selectCheck()\"");
$objTab->addBouton("submit","VALIDER");

// on reste sur le même écran --> affichage d'une boite modal de validation
if (isset($majok) && $majok === true) $objTab->libelle = "Mise à jour effectuée";

$objTab->finTableau();
//Trace("sortie de adm_maj_fonctionnalite-etat.php");

// fin du formulaire et de la page
$objPage->finPage();
