<?php
include('config/cmd_maj_ligne-sansuo.php');
$codefonc='cmd';
require_once('prepage.php');

$objForm = new formulaire('1');

if($typeaction == "creation") 
{
	$objForm->initChamp();
	$montantrev = 0;
	$cle = '';
}

if($typeaction == "modification")
{
	$requete = "select LCMD_CLE, LCMD_IDCMD, LCMD_IDGCMD, LCMD_POSTE, LCMD_FLAGIMPUTATION, LCMD_MARCHANDISE,
				LCMD_TYPEIMPUTATION, LCMD_LIBELLE, LCMD_MONTANTREV, LCMD_COEFREDU, LCMD_QUANTITE, LCMD_TOTALHT,
				DATE_FORMAT(LCMD_DATELIVA, '%d-%m-%Y') AS LCMD_DATELIVA,
				DATE_FORMAT(LCMD_DATEVA, '%d-%m-%Y') AS LCMD_DATEVA,
				DATE_FORMAT(LCMD_DATEVSR, '%d-%m-%Y') AS LCMD_DATEVSR
				from commande_ligne 
				where LCMD_CLE=\"" . $cle . "\"";
	$resultat = $objForm->mapChamp($requete);
}

// réception des paramètres
if($typeaction == "reception")
{
// contrôle des paramètres
	$objForm->recChamp();
	while($objForm->erreur == '')
	{
		if ($quantite == 0) {$objForm->erreur='quantite'; $objForm->libelle="La quantité doit être > 0"; break;}
		break;
	}

	if ($objForm->erreur == '')
	{
		$quantite = str_replace(",", ".", $quantite); // remplacement de la virgule par un point
		//
		// récupération des informations de la commande avant mise à jour
		//
		$objCommande = new commande($conn, 'maj', 'ligne commande', $cleparent, $cle);
		//
		// calcul du montant HT révisé et remisé de la ligne en fonction de la quantité
		//
		$coefredu = str_replace("%", "", $coefredu); // on supprime le % si renseigné
		$coefredu = str_replace(",", ".", $coefredu); // on remplace la virgule par un point
		if ($coefredu == '' || $coefredu == 0) $remise = 1;
		else $remise = (100-$coefredu)/100; // convertir % en coefficient de réduction (25% -> 0,75)
		// pour les lignes sans UO montantrev est pris tel que du formulaire
		$montantHT = ($montantrev * $quantite);
		$montantHT = ($montantHT * $remise);
//		Trace('muo_montant=' . $muo_montant . ' mrev_coefrev=' . $mrev_coefrev . ' quantite=' . $quantite . ' montantHT=' . $montantHT);
		//
		// mise à jour de la ligne de commande en BDD
		//
		$conn->setAttribute(PDO::ATTR_AUTOCOMMIT,0); 
		$conn->beginTransaction();
		$flag = false;
		while(true)
		{
			// mise à jour de la ligne de commande en BDD 
			$totalht = $montantHT; // pour maj bdd uniquement
//			$objForm->addChamp('totalht', 'LCMD_TOTALHT');
			if($objForm->majBdd('commande_ligne', 'LCMD_CLE', $cle) === false) break;
			
			// mise à jour de la commande en BDD si le montant HT ou l'imputation de la ligne ont changé
			if ($objCommande->majLigneCommande($totalht, $flagimputation) === false) break;
			
			// validation des mises à jour
			$conn->commit(); 
			$flag = true;
			break;
		}
		if ($flag == false) 
		{
			$conn->rollBack();
			$objForm->erreur='commande';
			$objForm->libelle=$objCommande->getLibelle();
			if ($objForm->libelle == '') $objForm->libelle="Erreur sur mise à jour de la ligne de commande"; 
		}
		else
		{
			// retour à la liste
			RedirURL("cmd_list_ligne.php");
			exit();
		}
	}
}

// Affichage du début de la page
if ($cle != '') $objPage->addParam('cle', $cle);
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();

$requete = "select GCMD_CLE, GCMD_LIBELLE from commande_groupe_ligne where GCMD_IDCMD=" . $cleparent . " order by 1";
$objForm->setSelect('idgcmd', $idgcmd, $requete, 'GCMD_CLE', 'GCMD_LIBELLE', '');

// si le profil n'autorise pas l'exécution certain champ ne sont pas accessibles
if (!$objProfil->exe)
{
	$objForm->setOpt('marchandise', 'disabled');
//	$objForm->setOpt('flagimputation', 'disabled');
}

$objForm->setOpt('montant', 'disabled');

// affichage des boutons d'enchainement
$objForm->addBouton("reset","RETOUR","cmd_list_ligne.php");
if (($cle == '' && $objProfil->cre) || ($cle != '' && $objProfil->maj))
{
	$objForm->addBouton("submit","ENVOYER");
}
else $objForm->setLecForm();

$objForm->affFormulaire();
$objForm->finFormulaire();

$requete = "select CMD_IDMARCHE, CMD_TVA from commande where CMD_CLE=\"" . $cleparent . "\"";
$statement = $conn->query($requete);
$resultat = $statement->fetch(PDO::FETCH_ASSOC);
$tva = $resultat['CMD_TVA'];

// fin de page
$objPage->finPage();

?>
<script>
<?php
echo "var tva = '" . $tva . "';";
?>
/***********************************************************/
// Exécuté après chargement de la page
/***********************************************************/
var montant = 0;
var remise = 1;
var quantite = 1;
$(document).ready(function() {
	getMontant();
	getQuantite();
	affMontant();
	// calcul du montant si changement UO
	// calcul du montant si changement quantite
	$("#montantrev").change(function() {
		getMontant();
		affMontant();
	});
	$("#quantite").change(function() {
		getQuantite();
		affMontant();
	});
});

function getMontant() 
{
	montant=$("#montantrev").val().replace(',', '.');
	montant=montant.replace(' ', '');
}

function getQuantite() 
{
	quantite=$("#quantite").val().replace(',', '.');
	if (quantite == '' || quantite == 0) quantite = 1;
}

function affMontant() 
{
	if (tva == '' || tva == '0') tva = 20;
	remise=$("#coefredu").val().replace(',', '.');
	if (remise == '' || remise == '0') remise = 1;
	else remise = (100-remise)/100;
	if (montant == '' || montant == '0') montant = 0;
//	console.log('montant=' + montant + ' quantite=' + quantite + ' remise=' + remise);
	var cumul = montant*quantite*remise;
	cumul = Number(Math.round(cumul+'e2')+'e-2');
//	console.log('cumul=' + cumul);
//	cumul = cumul+((cumul*20)/100);
	$("#montant").val(cumul);
}
</script>
