<?php
include('config/prj_list_budget.php');
$codefonc='bud';
include('prepage.php');
if (!$objProfil->ctlDroit($typeaction, 'tableau')) exit();

$objPage->debPage('center');

// suppression d'une entrée
if($typeaction == "suppression")
{
	if(isset($cle))	
	{
		$requete = "delete from projet_budget where PBUD_CLE=\"" . $cle . "\"";
		$statement = $conn->query($requete);
	}
}

// préparation du tableau
$objTab = new tableau('1');

// requête d'accès à la base 
$requete = "select PBUD_CLE, PBUD_LIGNE, MAR_NOMMARCHE, PBUD_LIBELLE, PBUD_REFSIF, 
			REPLACE(FORMAT(PBUD_AEPREVFONCT,2),',',' ') as PBUD_AEPREVFONCT, 
			REPLACE(FORMAT(PBUD_CPPREVFONCT,2),',',' ') as PBUD_CPPREVFONCT, 
			REPLACE(FORMAT(PBUD_AEPREVINVES,2),',',' ') as PBUD_AEPREVINVES, 
			REPLACE(FORMAT(PBUD_CPPREVINVES,2),',',' ') as PBUD_CPPREVINVES
			from projet_budget 
			left join marche on PBUD_IDMARCHE=MAR_CLE
			where PBUD_IDPROJET=\"" . $cleparent . "\" ";
$requete .= $objTab->majRequete('order by PBUD_CLE'); // ajout tri et pagination si besoin

// affichage des boutons d'enchainement
if ($objProfil->cre) $objTab->addBouton("button","AJOUTER","prj_maj_budget.php");

// gestion des paramètres de lien
$objTab->setLien('lignebud','prj_maj_budget.php',"?typeaction=modification&cle=#PBUD_CLE#");
$objTab->setLien('supp','prj_list_budget.php',"?typeaction=suppression&cle=#PBUD_CLE#","Supprimer");

// affichage du tableau
$objTab->affTableau($requete);

// fin de page
$objPage->finPage();
