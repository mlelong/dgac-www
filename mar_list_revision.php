<?php
include('config/mar_list_revision.php');
$codefonc='mar';
include('prepage.php');
if (!$objProfil->ctlDroit($typeaction, 'tableau')) exit();

$objPage->debPage('center');

// suppression d'une entrée
if($typeaction == "suppression")
{
	if(isset($cle))	
	{
		$requete = "delete from marche_revision where MREV_CLE=\"" . $cle . "\"";
		$statement = $conn->query($requete);
	}
}

// préparation du tableau
$objTab = new tableau('1');

// requête d'accès à la base 
$requete = "select MREV_CLE, MREV_DESCRIPTION, 
			REPLACE(FORMAT(MREV_COEFREV,3),',',' ') as MREV_COEFREV,
			DATE_FORMAT(MREV_DATEREV, '%d-%m-%Y') AS MREV_DATEREV
			from marche_revision where MREV_IDMARCHE=\"" . $cleparent . "\"";
$requete .= $objTab->majRequete('order by MREV_CLE'); // ajout tri et pagination si besoin

// affichage des boutons d'enchainement
if ($objProfil->cre)  $objTab->addBouton("button","AJOUTER","mar_maj_revision.php");
	
// gestion des paramètres de lien
$objTab->setLien('daterev','mar_maj_revision.php',"?typeaction=modification&cle=#MREV_CLE#");
$objTab->setLien('supp','mar_list_revision.php',"?typeaction=suppression&cle=#MREV_CLE#","Supprimer");

// affichage du tableau
$objTab->affTableau($requete);

// fin de page
$objPage->finPage();
