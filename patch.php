<?php
$codefonc='pat';
include('prepage.php');

$objPage->debPage('center');

$objPage->tampon .= "<p><h3>Fonctions de mise à jour et de consultation de l'application</h3></p>";
$objPage->tampon .= "<ul>";
$objPage->tampon .= "<li><p><h4><a href='#' " . VersURL('patch_down.php')  . " >Téléchargement des patchs</a></h4></p></li>";
$objPage->tampon .= "<li><p><h4><a href='#' " . VersURL('patch_apply.php')  . " >Application ou suppression des patchs</a></h4></p></li>";
$objPage->tampon .= "<li><p><h4><a href='#' " . VersURL('patch_return.php')  . " >Retour arrière sur les patchs appliqués</a></h4></p></li>";
$objPage->tampon .= "<li><p><h4><a href='#' " . VersURL('patch_delete.php')  . " >Suppression des patchs sauvegardés pour retour arrière</a></h4></p></li>";
$objPage->tampon .= "<li><p><h4><a href='#' " . VersURL('patch_base.php')  . " >Exécution d'une requête sur la base de données</a></h4></p></li>";
$objPage->tampon .= "<li><p><h4><a href='#' " . VersURL('patch_suppfic.php')  . " >Suppression d'un fichier</a></h4></p></li>";
$objPage->tampon .= "<li><p><h4><a href='#' " . VersURL('patch_visufic.php')  . " >Visualisation du contenu d'un fichier</a></h4></p></li>";
$objPage->tampon .= "<li><p><h4><a href='#' " . VersURL('patch_listrep.php')  . "  >Visualisation du contenu d'un répertoire</a></h4></p></li>";
$objPage->tampon .= "<li><p><h4><a href='#' " . VersURL('patch_crerep.php')  . "  >Création d'un répertoire</a></h4></p></li>";
$objPage->tampon .= "<li><p><h4><a href='#' " . VersURL('patch_execscript.php')  . "  >Exécution d'un script batch</a></h4></p></li>";
$objPage->tampon .= "</ul>";
$objPage->tampon .= "<br />";

// fin de page
$objPage->finPage();
