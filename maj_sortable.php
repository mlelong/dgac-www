<?php
// mise � jour des listes sortables
//indique que le type de la reponse renvoyee au client sera du Texte
header("Content-Type: text/html ; charset=utf-8");

//anti Cache pour HTTP/1.1
header("Cache-Control: no-cache , private");

//anti Cache pour HTTP/1.0
header("Pragma: no-cache");

//require_once('fonction.php');

if(isset($_REQUEST['list']))
{
	require_once('connect_base.php');
	$list=$_REQUEST['list'];
	$prefixe=$_REQUEST['prefixe'];
	$table=$_REQUEST['table'];
	
	try 
	{
		$conn = new PDO('mysql:host='.$base.';port='.$port.';dbname='.$dbname.';charset=utf8;', $user, $pass); 
	} 
	catch (PDOException $e) 
	{
		echo " ";
		exit; 
	}

	$tab = explode(';', $list);
	$suffixecle = "_CLE";
	for($i=0; $i<count($tab); $i++)
	{
		if ($tab[$i] == '') continue;
		$requete = "update " . $table . " set " . $prefixe . "_ORDRE=\"" . $i . "\" 
					where " . $prefixe . $suffixecle . "=\"" . $tab[$i] . "\" "; 
		$statement = $conn->query($requete);
	}

	if (isset($conn)) $conn=null; 
	echo "OK";
}
else
{
	echo "KO";
	exit; 
}
