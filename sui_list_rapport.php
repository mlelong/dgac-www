<?php
include('config/sui_list_rapport.php');
$codefonc='rpm';
include('prepage.php');
if (!$objProfil->ctlDroit($typeaction, 'tableau')) exit();

$objPage->debPage('center');

// suppression d'une entrée
if($typeaction == "suppression")
{
	if(isset($cle))	
	{
		$requete = "delete from rapport where RAP_CLE=\"" . $cle . "\"";
		$statement = $conn->query($requete);
	}
}

// préparation du tableau
$objTab = new tableau('1');
if (!$objProfil->maj) $champT1['modifier']['aff'] = false;
if (!$objProfil->sup) $champT1['supp']['aff'] = false;

if(isset($_REQUEST['rapport'])) $rapport=$_REQUEST['rapport'];
else $rapport = '';
if ($rapport != 'Suivi' && $rapport != 'Alerte') $rapport = 'Suivi';
$objPage->addParam('rapport', $rapport);

// requête d'accès à la base 
$requete = "select RAP_CLE, RAP_NOMRAPPORT, RAP_NOMEXE, RAP_DESCRIPTION 
			from rapport where RAP_TYPERAPPORT=\"" . $rapport . "\" ";
$requete .= $objTab->majRequete('order by RAP_NOMRAPPORT'); // ajout tri et pagination si besoin
$statement = $conn->query($requete);

// affichage des boutons d'enchainement
//$objTab->addBouton("button","EXPORTER","sui_csv_rapport.php");
if ($objProfil->cre)  $objTab->addBouton("button","AJOUTER","sui_maj_rapport.php");

while ($row = $statement->fetch(PDO::FETCH_ASSOC))
{
	$param = "?rapport=" . $rapport . "&cle=" . $row['RAP_CLE'];
	if ($row['RAP_NOMEXE'] != '')
		$ligne[0] = "<a href=\"#\" " . VersURL($row['RAP_NOMEXE'] . '.php' . $param) . "\" >" . stripslashes($row['RAP_NOMRAPPORT']) . "</a>";
	else	
		$ligne[0] = "<a href=\"#\" " . VersURL('sui_aff_rapport.php' . $param) . "\" >" . stripslashes($row['RAP_NOMRAPPORT']) . "</a>";
	$ligne[1] = stripslashes($row['RAP_DESCRIPTION']);
	$param = "?typeaction=modification&rapport=" . $rapport . "&cle=" . $row['RAP_CLE'];
	$ligne[2] = "<a " . VersURL('sui_maj_rapport.php' . $param) . " ><i class='fa fa-list-alt'></i></a>";
	$param = "?typeaction=suppression&rapport=" . $rapport . "&cle=" . $row['RAP_CLE'];
	if ($objProfil->sup) $ligne[3] = "<a " . VersURL('sui_list_rapport.php' . $param,'supp') . " ><i class='fa fa-trash'></i></a>";
	else $ligne[3] = "";
	$objTab->affLigne($ligne);
}

// affichage du tableau
$objTab->finTableau();

// fin de page
$objPage->finPage();
