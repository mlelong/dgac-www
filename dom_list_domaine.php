<?php
include('config/dom_list_domaine.php');
$codefonc='dom';
include('prepage.php');
if (!$objProfil->ctlDroit($typeaction, 'tableau')) exit();

$objPage->debPage('center');

// préparation du tableau
$objTab = new tableau('1');

// suppression d'une entrée
if($typeaction == "suppression")
{
	if(isset($cle))	
	{
		$requete = "delete from domaine where DOM_CLE=\"" . $cle . "\"";
		$objTab->supRequete($requete);
	}
}

// requête d'accès à la base 
$requete = "select DOM_CLE, DOM_NOMDOMAINE, DOM_DIRECTION, DOM_LIBELLE from domaine";
$requete .= $objTab->majRequete('order by DOM_CLE'); // ajout tri et pagination si besoin

// affichage des boutons d'enchainement
if ($objProfil->cre) $objTab->addBouton("button","AJOUTER","dom_maj_domaine.php");
	
// gestion des paramètres de lien
$objTab->setLien('nomdomaine','dom_tab_domaine.php',"?typeaction=modification&cle=#DOM_CLE#");
$objTab->setLien('responsable','dom_list_pole-domaine.php',"?typeaction=modification&cle=#DOM_CLE#","Liste");
$objTab->setLien('supp','dom_list_domaine.php',"?typeaction=suppression&cle=#DOM_CLE#","Supprimer");

// affichage du tableau
$objTab->affTableau($requete);

// fin de page
$objPage->finPage();
