<?php
include('config/adm_list_groupe-profil.php');
$codefonc='pro';
include('prepage.php');
if (!$objProfil->ctlDroit($typeaction, 'formulaire',$cle)) exit();

$objForm = new formulaire('1');

$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();
// Affichage des équipes de l'utilisateur
$datalistecle = '';
$datalisteval = '';
$requete = "select PRO_CLE, PRO_NOMPROFIL
			from profil, groupe_profil
			where PRO_CLE=RGP_IDPROFIL and RGP_IDGROUPE=\"" . $cle . "\"
			order by 2";
$statement = $conn->query($requete);
while ($row = $statement->fetch(PDO::FETCH_ASSOC))
{
	$affichage = $row['PRO_NOMPROFIL'];
	if($datalisteval == '') $datalisteval = $affichage;
	else $datalisteval .= ';' . $affichage;
	if($datalistecle == '') $datalistecle = $row['PRO_CLE'];
	else $datalistecle .= ';' . $row['PRO_CLE'];
}
$objForm->champF['idnomlec']['dataliste1'] = $datalistecle . "*;*" . $datalisteval;

// Affichage des autres équipes
$datalistecle = '';
$datalisteval = '';
$requete = "select PRO_CLE, PRO_NOMPROFIL from profil 
			where PRO_CLE NOT IN (select RGP_IDPROFIL from groupe_profil where RGP_IDGROUPE=\"" . $cle . "\") 
			order by 2";
$statement = $conn->query($requete);
while ($row = $statement->fetch(PDO::FETCH_ASSOC))
{
	$affichage = $row['PRO_NOMPROFIL'];
	if($datalisteval == '') $datalisteval = $affichage;
	else $datalisteval .= ';' . $affichage;
	if($datalistecle == '') $datalistecle = $row['PRO_CLE'];
	else $datalistecle .= ';' . $row['PRO_CLE'];
}
$objForm->champF['idnomlec']['dataliste2'] = $datalistecle . "*;*" . $datalisteval;

$objForm->affFormulaire();
// affichage des boutons d'enchainement
$objForm->addBouton("reset","RETOUR","adm_list_groupe.php");
if ($objProfil->maj) 
{
	$objForm->addBoutonSpe("button","VALIDER","onclick=\"majFormSortableDiff('grp-p'," . $cle . ");VersURL('adm_list_groupe.php');\"");
}

// fin de page
$objForm->finFormulaire();
$objPage->finPage();
