<?php
include('config/prj_list_activite-maj.php');
$codefonc='prj';
$codemenu='ama';
include('prepage.php');
if (!$objProfil->ctlDroit($typeaction, 'tableau')) exit();

$objPage->debPage('center');

// préparation du tableau
$objTab = new tableau('1');

// suppression d'une entrée
if($typeaction == "suppression")
{
	if(isset($cle))	
	{
		$requete = "delete from programme where PRG_CLE=\"" . $cle . "\"";
		$objTab->supRequete($requete);
	}
}

// requête d'accès à la base 
$requete = "select PRG_CLE, PRG_NOMPROGRAMME, PRG_LIBELLE from programme where PRG_TYPE=\"Activité majeure\" ";
$requete .= $objTab->majRequete('order by PRG_CLE'); // ajout tri et pagination si besoin

// affichage des boutons d'enchainement
if ($objProfil->cre) $objTab->addBouton("button","AJOUTER","prj_maj_activite-maj.php");
	
// gestion des paramètres de lien
$objTab->setLien('nomprogramme','prj_maj_activite-maj.php',"?typeaction=modification&cle=#PRG_CLE#");
$objTab->setLien('supp','prj_list_activite-maj.php',"?typeaction=suppression&cle=#PRG_CLE#","Supprimer");

// affichage du tableau
$objTab->affTableau($requete);

// fin de page
$objPage->finPage();
