<?php
include('config/adm_list_utilisateur-groupe.php');
$codefonc='gru';
include('prepage.php');
if (!$objProfil->ctlDroit($typeaction, 'formulaire',$cle)) exit();

$objForm = new formulaire('1');

$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();
// Affichage des groupes de l'utilisateur
$datalistecle = '';
$datalisteval = '';
$requete = "select GRU_CLE, GRU_NOMGROUPE
			from groupe, groupe_utilisateur
			where GRU_CLE=RGU_IDGROUPE and RGU_IDUTILISATEUR=\"" . $cle . "\"
			order by 2";
$statement = $conn->query($requete);
while ($row = $statement->fetch(PDO::FETCH_ASSOC))
{
	$affichage = $row['GRU_NOMGROUPE'];
	if($datalisteval == '') $datalisteval = $affichage;
	else $datalisteval .= ';' . $affichage;
	if($datalistecle == '') $datalistecle = $row['GRU_CLE'];
	else $datalistecle .= ';' . $row['GRU_CLE'];
}
$objForm->champF['idnomlec']['dataliste1'] = $datalistecle . "*;*" . $datalisteval;

// Affichage des autres groupes
$datalistecle = '';
$datalisteval = '';
$requete = "select GRU_CLE, GRU_NOMGROUPE from groupe 
			where GRU_CLE NOT IN (select RGU_IDGROUPE from groupe_utilisateur where RGU_IDUTILISATEUR=\"" . $cle . "\") 
			order by 2";
$statement = $conn->query($requete);
while ($row = $statement->fetch(PDO::FETCH_ASSOC))
{
	$affichage = $row['GRU_NOMGROUPE'];
	if($datalisteval == '') $datalisteval = $affichage;
	else $datalisteval .= ';' . $affichage;
	if($datalistecle == '') $datalistecle = $row['GRU_CLE'];
	else $datalistecle .= ';' . $row['GRU_CLE'];
}
$objForm->champF['idnomlec']['dataliste2'] = $datalistecle . "*;*" . $datalisteval;

$objForm->affFormulaire();
// affichage des boutons d'enchainement
$objForm->addBouton("reset","RETOUR","adm_list_utilisateur.php");
if ($objProfil->maj) 
{
	$objForm->addBoutonSpe("button","VALIDER","onclick=\"majFormSortableDiff('anu-g'," . $cle . ");VersURL('adm_list_utilisateur.php');\"");
}

// fin de page
$objForm->finFormulaire();
$objPage->finPage();
