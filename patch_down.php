<?php
include('config/patch_down.php');

$codefonc='pat';
require_once('prepage.php');

$objForm = new formulaire('1');

$retour = '';
if(!isset($typeaction)) $typeaction="creation";

// création d'une entrée
if($typeaction == "creation") $objForm->initChamp();

// réception des paramètres
if($typeaction == "reception")
{
// contrôle des paramètres
	$objForm->recChamp();
	while ($objForm->erreur == '')
	{
		// récupération du fichier
		if ((isset($_FILES['nomfic']['name'][0])&&($_FILES['nomfic']['error'][0] == UPLOAD_ERR_OK)))
		{
			$namefic = ctlNomFichier($_FILES['nomfic']['name'][0]);
			$chemin_destination = 'patch/' . $typefic . '/' . $namefic;   
			move_uploaded_file($_FILES['nomfic']['tmp_name'][0], $chemin_destination); 
			if (!file_exists($chemin_destination)) // vérification pour savoir si le fichier existe
				{$objForm->erreur="nomfic"; $objForm->libelle="Le fichier " . $namefic . " n'a pas pu être chargé"; break;}
		}
		else 
		{
			if ($_FILES['nomfic']['error'][0] == UPLOAD_ERR_NO_FILE)
				{$objForm->erreur="nomfic"; $objForm->libelle="Il faut renseigner un fichier"; break;}
			else
				{$objForm->erreur="nomfic"; $objForm->libelle="Erreur sur chargement du fichier = " . $_FILES['nomfic']['error'][0]; break;}
		}
		break;
	}
	if ($objForm->erreur == '')
	{
		$retour = "<p style='font-size:1.4em;'>Le fichier <B>" . $namefic . "</B> de type <B>" . $typefic . "</B> a été téléchargé<p>";
	}
}

// Affichage du début de la page
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();

// affichage des boutons d'enchainement
$objForm->addBouton("button","RETOUR","patch.php");
if ($objProfil->maj)
{
	$objForm->addBouton("submit","ENVOYER");
}
else $objForm->setLecForm();

$objForm->affFormulaire();
$objForm->finFormulaire();

if ($retour != '') $objPage->tampon .= $retour;

// fin de page
$objPage->finPage();
