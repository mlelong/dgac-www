<?php
include('config/bud_maj_pap.php');
$codefonc='bud';
require_once('prepage.php');

$objForm = new formulaire('1');

if($typeaction == "creation") 
{
	$objForm->initChamp();
	$idbudget = $cleparent;
}

if($typeaction == "modification")
{
	$requete = "select BPAP_CLE, BPAP_IDBUDGET, BPAP_ORDRE, BPAP_LIGNEPAP, BPAP_LIBELLE, BPAP_FLAGIMPUTATION,
				BPAP_FLAGTOTAL,	BPAP_AEPREVFONCT, BPAP_CPPREVFONCT, BPAP_AEPREVINVES, BPAP_CPPREVINVES
				from budget_pap where BPAP_CLE=\"" . $cle . "\"";
	$resultat = $objForm->mapChamp($requete);
}

// réception des paramètres
if($typeaction == "reception")
{
// contrôle des paramètres
	$objForm->recChamp();
	while($objForm->erreur == '')
	{
		break;
	}

	if ($objForm->erreur == '')
	{
		$objForm->majBdd('budget_pap', 'BPAP_CLE', $cle);
		RedirURL("bud_list_pap.php");
		exit();
	}
}

// Affichage du début de la page
if ($cle != '') $objPage->addParam('cle', $cle);
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();

if ($flagtotal == "o") $opt = " checked "; else $opt = "";
$objForm->setOpt('flagtotal', $opt);
$flagtotal = "o";

// affichage des boutons d'enchainement
$objForm->addBouton("reset","RETOUR","bud_list_pap.php");
if (($cle == '' && $objProfil->cre) || ($cle != '' && $objProfil->maj))
{
	$objForm->addBouton("submit","ENVOYER");
}
else $objForm->setLecForm();

$objForm->affFormulaire();
$objForm->finFormulaire();

// fin de page
$objPage->finPage();
