<?php
//indique que le type de la reponse renvoyee au client sera du Texte
header("Content-Type: text/html ; charset=utf-8");

//anti Cache pour HTTP/1.1
header("Cache-Control: no-cache , private");

//anti Cache pour HTTP/1.0
header("Pragma: no-cache");

require_once('connect_base.php');

try 
{
	$conn = new PDO('mysql:host='.$base.';port='.$port.';dbname='.$dbname.';charset=utf8;', $user, $pass); 
} 
catch (PDOException $e) 
{
	echo " ";
	exit; 
}

// r�cup�ration du programme
if(isset($_REQUEST['programme'])) $programme = $_REQUEST['programme'];
else $programme = '';

// affichage des budgets pour un programme
if($programme != '')
{
	// recherche des projets avec une ligne budg�taire pour l'ann�e
	$requete = "select BUD_CLE, BUD_ANNEE from budget where BUD_PROGRAMME=\"".$programme."\" order by 2 desc";
}
$statement = $conn->query($requete);
$res = [];
while ($row = $statement->fetch(PDO::FETCH_ASSOC))
{
	$res['*'.$row['BUD_CLE']] = $row['BUD_ANNEE']; // * pour garder ordre � cause du json
}
// fermeture de la connexion
if (isset($conn)) $conn=null; 
// encodage en json et retour
$retour = json_encode($res);
echo $retour;

