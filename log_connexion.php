<?php
$cookie=false; // true si on veut gérer les cookies sinon false 
include('config/log_connexion.php');
$codefonc='*';

// autoload des fichiers de class lors de l'appel à new
spl_autoload_register(function ($nomclass) {
    require_once('class/' . $nomclass . '.php');
});

// fonctions utilisables dans la page
require_once('fonction.php');

// la classe profil doit être déclarée avant le start session et avant la classe page
require_once('class/profil.php');
session_start(); 

// préparation de la page
$objPage = page::getPage();

// connexion à la base de données
$conn = database::getIntance();

// suppression du profil de la session s'il existe
unset($_SESSION['s_profil']);

// le profil même vide doit être créé avant la construction de la page
$objProfil = profil::getProfil();

// préparation du formulaire
$objForm = new formulaire('1');

if(isset($_REQUEST['typeaction'])) $typeaction=$_REQUEST['typeaction'];
if(!isset($typeaction) || $typeaction == '') $typeaction="creation";

// Initialisation du formulaire 
if($typeaction != "reception") $objForm->initChamp();

// réception des paramètres
if($typeaction == "reception")
{
	// contrôle des paramètres de la connexion utilisateur
	$objForm->recChamp();
	while($objForm->erreur == '')
	{
		if ($identifiant == '') {$objForm->erreur="identifiant"; $objForm->libelle="Il faut renseigner l'identifiant"; break;}
		if ($pwd == '') {$objForm->erreur="pwd"; $objForm->libelle="Il faut renseigner le mot de passe"; break;}
	
		// cas particulier de l'identification en tant qu'administrateur
		if ($identifiant == 'admin')
		{
			if ($objProfil->setProfilAdmin($pwd) === false) {$objForm->erreur="pwd"; $objForm->libelle="Mot de passe incorrect"; break;}
			// on peut aller sur la page d'accueil
			header("Location: index.php");
			exit();
		}
		
		// contrôle si l'identification doit se faire via l'annuaire
		if (file_exists('connect_ldap.php')) 
		{
			require_once('connect_ldap.php');
			$flagldap = true;
			if (!isset($serverName)) $flagldap = false;
			if (!isset($serverPort)) $flagldap = false;
			if (!isset($virtualUserPassword)) $flagldap = false;
			if (!isset($virtualUser)) $flagldap = false;
		}
		else $flagldap = false;
		
		// identification via l'annuaire
		if ($flagldap)
		{
			// recherche dans l'annuaire pour savoir si l'identifiant est reconnu
//			$identifiant="marc.guyot";
//			$pwd="1234";

			//Connexion avec l' utilisateur virtuel + Recherche DN utilisateur
//			Trace($serverName . " " . $serverPort . " " . $virtualUserPassword);
			$connexion=ldap_connect($serverName,$serverPort);
			@$connexion1=ldap_bind($connexion,$virtualUser,$virtualUserPassword);
			if(!$connexion1) {$objForm->erreur="identifiant"; $objForm->libelle="Problème annuaire Angélique"; break;}
			$baseDn1="sn=internes_si,dc=aviation-civile,dc=gouv,dc=fr";
			$filtre1="uid=$identifiant";
			$attributs1=array("dn","sn","givenname","initials","mail");
			$requete1=ldap_search($connexion,$baseDn1,$filtre1,$attributs1);
			$info1=ldap_get_entries($connexion,$requete1);
			//Authentification utilisateur
			// retour print_r($info1) -> Array ( [count] => 1 [0] => Array ( [count] => 0 [dn] => uidnumber=51158,sn=ldap-cedre,sn=n,sn=internes_si,dc=aviation-civile,dc=gouv,dc=fr ) ) 
			$flagconn = false;
			for($i=0;$i<$info1["count"];$i++)
			{
				$dnUser=$info1[$i]["dn"];
				@$connexion2=ldap_bind($connexion,$dnUser,$pwd);
				if(!$connexion2) continue;
				$mail = $info1[$i]["mail"][0];				$nom = $info1[$i]["sn"][0];
				$prenom = $info1[$i]["givenname"][0];
				$trigramme = $info1[$i]["initials"][0];

				$infos = "mail=" . $mail . " nom=" . $nom . " prenom=" . $prenom . " initiales=" . $trigramme;
//				echo $infos;
				$flagconn = true;
				break; // ???
			}
			// l'identifiant n'existe pas dans l'annuaire Angélique
			if ($flagconn === false) 
			{
				ldap_close($connexion);
				$objForm->erreur="identifiant"; 
				$objForm->libelle="Identifiant inconnu dans Angélique"; 
				break;
			}	
			else // l'identifiant existe 
			{
				ldap_close($connexion);
			}
		}
		
		// recherche des informations de l'utilisateur en base de données
		$requete = "select UTI_CLE, UTI_IDENTIFIANT, UTI_NOM, UTI_PRENOM, UTI_PWD
					from utilisateur where UTI_IDENTIFIANT=\"" . $identifiant . "\"";
		$statement = $conn->query($requete);
		$resultat = $statement->fetch(PDO::FETCH_ASSOC);
		if ($resultat['UTI_PWD'] === NULL) $motdepasse = '';
		else $motdepasse = $resultat['UTI_PWD'];
		if ($resultat['UTI_IDENTIFIANT'] == '') {$objForm->erreur="identifiant"; $objForm->libelle="Identifiant non autiorisé"; break;}
		// si pas de LDAP on contrôle le mode de passe interne (identification hors annuaire)
		if (!$flagldap)
		{
			if ($motdepasse != $pwd) {$objForm->erreur="pwd"; $objForm->libelle="Mot de passe incorrect"; break;}
			if ($motdepasse != '' && $pwd == '') {$objForm->erreur="pwd"; $objForm->libelle="Mot de passe obligatoire"; break;}
		}
		break;
	}
	if ($objForm->erreur == '')
	{
		$objProfil->setProfil($resultat);
		if ($cookie === true) setcookie('s_idnom', $resultat['UTI_CLE'], time()+(365*24*3600)); // valable 1 an
		// si url avant connexion redirection vers cette URL
		if (isset($_SESSION['request_uri']))
		{
			$request_uri = $_SESSION['request_uri'];
			unset($_SESSION['request_uri']);
			header("Location: ".$request_uri);
			exit();
		}
		// sinon retour sur la page d'accueil
		header("Location: index.php");
		exit();
	}
}
// Affichage du début de la page
$objPage->debPage('connexion');

// Affichage du formulaire
$objForm->debFormulaire();
$objForm->affFormulaire();
	
// affichage des boutons d'enchainement
$objForm->addBouton("submit","ENVOYER");
$objForm->finFormulaire();

// fin de page
$objPage->finPage();
