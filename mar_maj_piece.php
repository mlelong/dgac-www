<?php
include('config/mar_maj_piece.php');
$codefonc='mar';
require_once('prepage.php');

$objForm = new formulaire('1');

// création
if($typeaction == "creation") $objForm->initChamp();

// modification
if($typeaction == "modification")
{
	$requete = "select PCE_IDTABLE, PCE_NOMFIC, PCE_LIBELLE, PCE_TYPEFIC, PCE_DESCRIPTION from piece
				where PCE_CLE=\"" . $cle . "\"";
	$resultat = $objForm->mapChamp($requete);
}

// réception des paramètres
if($typeaction == "reception")
{
// contrôle des paramètres
	$objForm->recChamp();
	while($objForm->erreur == '')
	{
		if ($cle == '')
		{
			if (!isset($_FILES['nomfic']['name'][0]) || $_FILES['nomfic']['name'][0] == '') 
				{$objForm->erreur="nomfic"; $objForm->libelle="Le nom du fichier à télécharger doit être renseigné"; break;}
		}
		break;
	}

	if ($objForm->erreur == '')
	{
//		$objForm->trtFicAttache('', $cle, 'document', 'PCE_NOMFIC', 'PCE_CLE', 'piece');
//		$objForm->majBdd('piece', 'PCE_CLE', $cle);
		if ($cle == '')
		{
//			print_r($_FILES);
//			exit();
			if (isset($_FILES['nomfic']['name'][0]) && $_FILES['nomfic']['name'][0] != '')
			{
				$nomfic = $_FILES['nomfic']['name'][0];
				$requete = "insert into piece (PCE_NOMTABLE, PCE_IDTABLE, PCE_NOMFIC, PCE_LIBELLE, PCE_TYPEFIC, PCE_DESCRIPTION)
							values (\"marche\",\"" . $cleparent . "\",\"" . addslashes($nomfic) . "\",\"" . addslashes($libelle) 
									. "\",\"" . $typefic . "\",\"" . addslashes($description) . "\")";
				$statement = $conn->query($requete);
				$idfic = $conn->lastInsertId(); // récupération de l'id de la pièce jointe
				if ($idfic > 0)
				{
					$repertoire = "document";
					if ($_FILES['nomfic']['error'][0] == UPLOAD_ERR_OK)
					{						
						$chemin_destination = $repertoire . '/f' . $idfic . '-' . ctlNomFichier($nomfic);   
						move_uploaded_file($_FILES['nomfic']['tmp_name'][0], $chemin_destination); 
					}
				}
			}	
		}
		else 
		{
			$requete = "update piece set PCE_LIBELLE=\"" . addslashes($libelle) 
						. "\", PCE_TYPEFIC=\"" . $typefic
						. "\", PCE_DESCRIPTION=\"" . addslashes($description) 
						. "\" where PCE_CLE=\"" . $cle . "\"";
			$statement = $conn->query($requete);
		}
		RedirURL("mar_list_piece.php");
		exit();
	}
}

// Affichage du début de la page
if ($cle != '') $objPage->addParam('cle', $cle);
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();

$objForm->setRepertoire('nomfic','document');
$objForm->setDataliste('nomfic', $nomfic);

// affichage des boutons d'enchainement
$objForm->addBouton("reset","RETOUR","mar_list_piece.php");
if (($cle == '' && $objProfil->cre) || ($cle != '' && $objProfil->maj))
{
	$objForm->addBouton("submit","ENVOYER");
}
else $objForm->setLecForm();

$objForm->affFormulaire();
$objForm->finFormulaire();

// fin de page
$objPage->finPage();
