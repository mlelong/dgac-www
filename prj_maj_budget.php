<?php
include('config/prj_maj_budget.php');
$codefonc='bud';
require_once('prepage.php');

$objForm = new formulaire('1');

if($typeaction == "creation") $objForm->initChamp();

if($typeaction == "modification")
{
	$requete = "select PBUD_CLE, PBUD_LIGNE, PBUD_IDPROJET, PBUD_IDMARCHE, PBUD_LIBELLE, PBUD_REFSIF, 
				PBUD_AEPREVFONCT, PBUD_CPPREVFONCT, PBUD_AEPREVINVES, PBUD_CPPREVINVES
				from projet_budget where PBUD_CLE=\"" . $cle . "\"";
	$resultat = $objForm->mapChamp($requete);
}

// réception des paramètres
if($typeaction == "reception")
{
// contrôle des paramètres
	$objForm->recChamp();
	while($objForm->erreur == '')
	{
		break;
	}

	if ($objForm->erreur == '')
	{
		$objForm->majBdd('projet_budget', 'PBUD_CLE', $cle);
		RedirURL("prj_list_budget.php");
		exit();
	}
}

// Affichage du début de la page
if ($cle != '') $objPage->addParam('cle', $cle);
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();

$requete = "select PRJ_CLE, PRJ_NOMPROJET from projet order by 1";
$objForm->setSelect('idprojet', $idprojet, $requete, 'PRJ_CLE', 'PRJ_NOMPROJET');
$requete = "select MAR_CLE, MAR_NOMMARCHE from marche order by 1";
$objForm->setSelect('idmarche', $idmarche, $requete, 'MAR_CLE', 'MAR_NOMMARCHE');

// affichage des boutons d'enchainement
$objForm->addBouton("reset","RETOUR","prj_list_budget.php");
if (($cle == '' && $objProfil->cre) || ($cle != '' && $objProfil->maj))
{
	$objForm->addBouton("submit","ENVOYER");
}
else $objForm->setLecForm();

$objForm->affFormulaire();
$objForm->finFormulaire();

// fin de page
$objPage->finPage();
