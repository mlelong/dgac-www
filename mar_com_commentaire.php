<?php
include('config/mar_list_commentaire.php');
$codefonc='mar';
include('prepage.php');

$objPage->debPage('center');

// préparation du commentaire
$objCommentaire = new commentaire();
if ($objProfil->cre) $objCommentaire->addBouton("button","AJOUTER","mar_maj_commentaire.php");

$objCommentaire->debCommentaire();
$requete = "select COM_CLE, UTI_NOM,
			DATE_FORMAT(COM_DATECOM, '%d-%m-%Y %H:%i') AS COM_DATECOM, COM_DESCRIPTION
			from commentaire 
			left join utilisateur on COM_IDNOM=UTI_CLE
			where COM_NOMTABLE=\"marche\" AND COM_IDTABLE=" . $cleparent . " order by 1 desc";
$statement = $conn->query($requete);

// affichage de la liste des commentaires
while ($row = $statement->fetch(PDO::FETCH_ASSOC))
{
	if ($row['COM_DATECOM'] == '00-00-0000 00:00') $datecom = ''; else $datecom = $row['COM_DATECOM'];
	$auteurcom =  $row['UTI_NOM'];
	$contenucom =  stripslashes($row['COM_DESCRIPTION']);
	$objCommentaire->affCommentaire($auteurcom, $datecom, $contenucom);
}

$objCommentaire->finCommentaire();

// fin de page
$objPage->finPage();
