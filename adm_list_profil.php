<?php
include('config/adm_list_profil.php');
$codefonc='pro';
include('prepage.php');
if (!$objProfil->ctlDroit($typeaction, 'tableau')) exit();

$objPage->debPage('center');

// suppression d'une entrée
if($typeaction == "suppression")
{
	if(isset($cle))	
	{
		$requete = "delete from profil where PRO_CLE=\"" . $cle . "\"";
		$statement = $conn->query($requete);
	}
}

// préparation du tableau
$objTab = new tableau('1');

// requête d'accès à la base 
$requete = "select PRO_CLE, PRO_NOMPROFIL, PRO_DESCRIPTION from profil role where 1 ";
$requete .= $objTab->majRequete('order by PRO_CLE'); // ajout tri et pagination si besoin

// affichage des boutons d'enchainement
if ($objProfil->cre) $objTab->addBouton("button","AJOUTER","adm_maj_profil.php");

// gestion des paramètres de lien
$objTab->setLien('nomprofil','adm_maj_profil.php',"?typeaction=modification&cle=#PRO_CLE#");
$objTab->setLien('droit','adm_maj_profil-droit.php',"?typeaction=modification&cle=#PRO_CLE#","Liste");
$objTab->setLien('groupe','adm_list_profil-groupe.php',"?typeaction=modification&cle=#PRO_CLE#","Liste");
$objTab->setLien('utilisateur','adm_list_profil-utilisateur.php',"?typeaction=modification&cle=#PRO_CLE#","Liste");
$objTab->setLien('supp','adm_list_profil.php',"?typeaction=suppression&cle=#PRO_CLE#","Supprimer");

// affichage du tableau
$objTab->affTableau($requete);

// fin de page
$objPage->finPage();
