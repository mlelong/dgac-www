<?php
//indique que le type de la reponse renvoyee au client sera du Texte
header("Content-Type: text/html ; charset=utf-8");

//anti Cache pour HTTP/1.1
header("Cache-Control: no-cache , private");

//anti Cache pour HTTP/1.0
header("Pragma: no-cache");

//require_once('fonction.php');

require_once('class/profil.php');
session_start();
if(!isset($_SESSION['s_profil'])) 
{
	echo "KO";
	exit; 
}
$objProfil = $_SESSION['s_profil'];

if(isset($_REQUEST['url']))
{
	require_once('connect_base.php');
	
	$favori=$_REQUEST['url'];
	try 
	{
		$conn = new PDO('mysql:host='.$base.';port='.$port.';dbname='.$dbname.';charset=utf8;', $user, $pass); 
	} 
	catch (PDOException $e) 
	{
		echo " ";
		exit; 
	}
	
	$requete = "update utilisateur set UTI_FAVORI=\"" . $favori . "\" 
				where UTI_CLE=\"" . $objProfil->idnom . "\" "; 
	$statement = $conn->query($requete);

	if (isset($conn)) $conn=null; 
	echo "OK";
}
else
{
	echo "KO";
	exit; 
}
