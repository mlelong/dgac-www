<?php
$codefonc='fou';
include('prepage.php');

// début de page
$objPage->debPage('center');

$objOnglet = new onglet();

// début de l'onglet
$objOnglet->debOnglet();

// creation des liens
$objOnglet->debLienOnglet();
$objOnglet->addLienOnglet('fou','url','Fournisseur','fou_maj_fournisseur.php?typeaction=' . $typeaction . '&cle=' . $cle);
$objOnglet->addLienOnglet('con','url','Contacts','fou_list_contact.php?cleparent=' . $cle);
$objOnglet->finLienOnglet();

// fin de l'onglet
$objOnglet->finOnglet();

// fin de page
$objPage->finPage();
