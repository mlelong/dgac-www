<?php
include('config/adm_maj_etat.php');
$codefonc='adm';
require_once('prepage.php');

$objForm = new formulaire('1');

// création
if($typeaction == "creation") $objForm->initChamp();

// modification
if($typeaction == "modification")
{
	$requete = "select ETA_IDFONC, ETA_ORDRE, ETA_CODEETAT, ETA_NOMETAT, ETA_IDPHASE, ETA_TYPENOTIF, ETA_IDNOTIF, ETA_MAIL
				from fonc_etat where ETA_CLE=\"" . $cle . "\"";
	$resultat = $objForm->mapChamp($requete);
}

// réception des paramètres
if($typeaction == "reception")
{
// contrôle des paramètres
	$objForm->recChamp();
	while($objForm->erreur == '')
	{
		break;
	}
	if ($objForm->erreur == '')
	{
		if ($cle == '') 
		{
			$objForm->setAff('ordre', true); // pour prise en compte dans la requête
			$requete = "select MAX(ETA_ORDRE) from fonc_etat where ETA_IDFONC=\"" . $cleparent . "\"";
			$statement = $conn->query($requete);
			$resultat = $statement->fetch(PDO::FETCH_ASSOC);
			if ($resultat['MAX(ETA_ORDRE)'] != '') $ordre = ($resultat['MAX(ETA_ORDRE)']+1);
			else $ordre = 1;
			$objForm->setAff('ordre', false); 
		}	
		$objForm->majBdd('fonc_etat', 'ETA_CLE', $cle);
		RedirURL("adm_list_etat.php");
		exit();
	}
}

// Affichage du début de la page
if ($cle != '') $objPage->addParam('cle', $cle);
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();

$requete = "select PHA_CLE, PHA_NOMPHASE from fonc_phase order by 1";
$objForm->setSelect('idphase', $idphase, $requete, 'PHA_CLE', 'PHA_NOMPHASE');

$requete = "select NOT_CLE, NOT_NOMNOTIF from notification order by 1";
$objForm->setSelect('idnotif', $idnotif, $requete, 'NOT_CLE', 'NOT_NOMNOTIF');

// affichage des boutons d'enchainement
$objForm->addBouton("reset","RETOUR","adm_list_etat.php");
if (($cle == '' && $objProfil->cre) || ($cle != '' && $objProfil->maj))
{
	$objForm->addBouton("submit","ENVOYER");
}
else $objForm->setLecForm();

$objForm->affFormulaire();
$objForm->finFormulaire();

// fin de page
$objPage->finPage();
