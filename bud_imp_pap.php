<?php
include('config/bud_imp_pap.php');
$codefonc='bud';
require_once('prepage.php');

$objForm = new formulaire('1');

// création
if (!isset($typeaction) || $typeaction == '') $typeaction = 'creation';
if($typeaction == "creation") $objForm->initChamp();

// réception des paramètres
if($typeaction == "reception")
{
// contrôle des paramètres
	$objForm->recChamp();
	while ($objForm->erreur == '')
	{
		break;
	}
	if ($objForm->erreur == '')
	{
		$rapport = "";
		// récupération du fichier
		if ((isset($_FILES['nomfic']['name'][0])&&($_FILES['nomfic']['error'][0] == UPLOAD_ERR_OK)))
		{
			$namefic = ctlNomFichier($_FILES['nomfic']['name'][0]);
			$chemin_destination = "document/" . $namefic;   
			move_uploaded_file($_FILES['nomfic']['tmp_name'][0], $chemin_destination); 
		}
		if (!file_exists($chemin_destination)) // vérification pour savoir si le fichier existe
			{$objForm->erreur="nomfic"; $objForm->libelle="Le fichier " . $namefic . " n'a pas pu être chargé"; break;}
		//
		// création ou ajout des lignes pour le budget
		//
		$conn->setAttribute(PDO::ATTR_AUTOCOMMIT,0); 
		$conn->beginTransaction();
		$flag = false;
		while(true)
		{
			$fichier = "document/" . $namefic;
//			Trace("Traitement du fichier " . $fichier);
			$handle = fopen ($fichier,"r");
			// entete
			$ligne = fgetcsv($handle, 1000, ";");
			// test si utf8
//			echo 'encoding = ' . mb_detect_encoding($ligne[0]);
			if (mb_detect_encoding($ligne[0] != 'UTF-8')) $flagutf8 = true;
			else $flagutf8 = false;
//			if ($flagutf8) echo "pas UTF-8";
			if ($flagutf8) $ligne = array_map("utf8_encode",$ligne);
			$idbudget = $cleparent;
			// remplace
			if ($flagsup == 'o')
			{
//				Trace("Suppression des lignes existantes");
				$requete = "TRUNCATE TABLE budget_pap";
				$statement = $conn->query($requete);
			}
			// 
			// traitement du fichier
			//
			$libelle = '';
			// lignes des uos
//			Trace("Début de la boucle des lignes");
			while ($ligne = fgetcsv($handle, 1000, ";")) 
			{
		//		print_r($ligne);
				if ($flagutf8) $ligne = array_map("utf8_encode",$ligne);
				// informations lignes
				$clepap = $ligne[0];
				$idbudget = $ligne[1];
				$ordre = $ligne[2];
				if ($ordre == '') continue;
				$lignepap = $ligne[3];
				$flagimputation = $ligne[4];
				$libelle = $ligne[5];
				$flagtotal = $ligne[6];
				$aeprevfonct = str_replace(" ", "", $ligne[7]);
				$aeconsfonct = str_replace(" ", "", $ligne[8]);
				$cpprevfonct = str_replace(" ", "", $ligne[9]);
				$cpconsfonct = str_replace(" ", "", $ligne[10]);
				$aeprevinves = str_replace(" ", "", $ligne[11]);
				$aeconsinves = str_replace(" ", "", $ligne[12]);
				$cpprevinves = str_replace(" ", "", $ligne[13]);
				$cpconsinves = str_replace(" ", "", $ligne[14]);
				$requete = "insert into budget_pap (BPAP_IDBUDGET, BPAP_ORDRE, BPAP_LIGNEPAP, BPAP_LIBELLE, BPAP_FLAGIMPUTATION, 
													BPAP_AEPREVFONCT, BPAP_CPPREVFONCT, BPAP_AEPREVINVES, BPAP_CPPREVINVES)
							values (\"" . $idbudget . "\",\"" . $ordre . "\",\"" . $lignepap . "\",\"" . $libelle . "\",\"" . $flagimputation 
									. "\",\"" . $aeprevfonct . "\",\"" . $cpprevfonct . "\",\"" . $aeprevinves . "\",\"" . $cpprevinves . "\")";
				$statement = $conn->query($requete);
			}
			fclose ($handle);
			$conn->commit(); 
			$flag = true;
//			Trace("COMMIT");
			break;
		}
		if ($flag == false) 
		{
//			Trace("ROLLBACK");
			$conn->rollBack();
			$objForm->erreur="nomfic";
			if ($objForm->libelle == '') $objForm->libelle = "Erreur sur chargement des lignes du PAP"; 
		}
	}
	$result= unlink($fichier); // suppression du fichier
	if ($objForm->libelle == '')
	{
		RedirURL("bud_list_pap.php");
		exit();
	}
}

// Affichage du début de la page
if ($cle != '') $objPage->addParam('cle', $cle);
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();

// affichage des boutons d'enchainement
$objForm->addBouton("reset","RETOUR","bud_list_pap.php");
if (($cle == '' && $objProfil->cre) || ($cle != '' && $objProfil->maj))
{
	$objForm->addBouton("submit","ENVOYER");
}
else $objForm->setLecForm();

$objForm->affFormulaire();
$objForm->finFormulaire();

// fin de page
$objPage->finPage();
