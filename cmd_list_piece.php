<?php
include('config/cmd_list_piece.php');
$codefonc='*';
include('prepage.php');
//if (!$objProfil->ctlDroit($typeaction, 'tableau')) exit();

$objPage->debPage('center');

// suppression d'une entrée
if($typeaction == "suppression")
{
	if(isset($cle))	
	{
		$requete = "select PCE_NOMFIC from piece where PCE_CLE=\"" . $cle . "\"";
		$statement = $conn->query($requete);
		$resultat = $statement->fetch(PDO::FETCH_ASSOC);
		$fic = $resultat["PCE_NOMFIC"];
		$requete = "delete from piece where PCE_CLE=\"" . $cle . "\"";
		$statement = $conn->query($requete);
		$nomfic='document/f' . $cle . '-' . ctlNomFichier($fic);
		if (file_exists($nomfic)) unlink($nomfic); 
	}
}

// préparation du tableau
$objTab = new tableau('1');

// requête d'accès à la base 
$requete = "select PCE_CLE, PCE_NOMFIC, PCE_LIBELLE, PCE_TYPEFIC from piece 
			where PCE_NOMTABLE=\"commande\" AND PCE_IDTABLE=" . $cleparent . " ";
$requete .= $objTab->majRequete('order by PCE_CLE'); // ajout tri et pagination si besoin

// affichage des boutons d'enchainement
if ($objProfil->cre)  $objTab->addBouton("button","AJOUTER","cmd_maj_piece.php");
	
// gestion des paramètres de lien
$objTab->setLien('nomfic','cmd_maj_piece.php',"?typeaction=modification&cle=#PCE_CLE#");
$objTab->setLien('telecharge','download.php',"?cle=#PCE_CLE#","Téléchargement");
$objTab->setLien('supp','cmd_list_piece.php',"?typeaction=suppression&cle=#PCE_CLE#","Supprimer");

// affichage du tableau
$objTab->affTableau($requete);

// fin de page
$objPage->finPage();
