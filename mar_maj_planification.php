<?php
include('config/mar_maj_planification.php');
$codefonc='mar';
require_once('prepage.php');

$objForm = new formulaire('1');

// création
if($typeaction == "creation") $objForm->initChamp();
$flagrma = '';
$flagcb = '';

// modification
if($typeaction == "modification")
{
	$requete = "select MAR_CLE, MAR_ETAT, MAR_FLAGRMA, MAR_FLAGCB, 
				DATE_FORMAT(MAR_DATECCTP, '%d-%m-%Y') AS MAR_DATECCTP,
				DATE_FORMAT(MAR_DATECCAP, '%d-%m-%Y') AS MAR_DATECCAP,
				DATE_FORMAT(MAR_DATERMA, '%d-%m-%Y') AS MAR_DATERMA,
				DATE_FORMAT(MAR_DATEPUB, '%d-%m-%Y') AS MAR_DATEPUB,
				DATE_FORMAT(MAR_DATERET, '%d-%m-%Y') AS MAR_DATERET,
				DATE_FORMAT(MAR_DATECB, '%d-%m-%Y') AS MAR_DATECB,
				DATE_FORMAT(MAR_DATENOT, '%d-%m-%Y') AS MAR_DATENOT,
				DATE_FORMAT(MAR_DATEFIN, '%d-%m-%Y') AS MAR_DATEFIN
				from marche where MAR_CLE=\"" . $cle . "\"";
	$resultat = $objForm->mapChamp($requete);
	$flagrma = $resultat['MAR_FLAGRMA'];
	$flagcb = $resultat['MAR_FLAGCB'];
}

// réception des paramètres
if($typeaction == "reception")
{
// contrôle des paramètres
	$objForm->recChamp();
	while($objForm->erreur == '')
	{
		if ($datecctp != "") $INVdatecctp = invDate($datecctp);
		if ($dateccap != "") $INVdateccap = invDate($dateccap);
		if ($daterma != "")	$INVdaterma = invDate($daterma);
		if ($datepub != "")	$INVdatepub = invDate($datepub);
		if ($dateret != "")	$INVdateret = invDate($dateret);
		if ($datecb != "")	$INVdatecb = invDate($datecb);
		if ($datenot != "")	$INVdatenot = invDate($datenot);
		if ($datefin != "")	$INVdatefin = invDate($datefin);
		// contrôle de cohérence de la date du cctp
		if ($datecctp != "")
		{
			if ($datepub != "" && $datecctp > $datepub) {$objForm->erreur="datecctp"; $objForm->libelle="Date du CCTP > date de publication"; break;}
			if ($dateret != "" && $datecctp > $dateret) {$objForm->erreur="datecctp"; $objForm->libelle="Date du CCTP > date de retour des offres"; break;}
			if ($datecb != "" && $datecctp > $datecb) {$objForm->erreur="datecctp"; $objForm->libelle="Date du CCTP > date de validation CB"; break;}
			if ($datenot != "" && $datecctp > $datenot) {$objForm->erreur="datecctp"; $objForm->libelle="Date du CCTP > date de notification"; break;}
			if ($datefin != "" && $datecctp > $datefin) {$objForm->erreur="datecctp"; $objForm->libelle="Date du CCTP > date de fin"; break;}
		}
		// contrôle de cohérence de la date du ccap
		if ($dateccap != "")
		{
			if ($datepub != "" && $dateccap > $datepub) {$objForm->erreur="dateccap"; $objForm->libelle="Date du CCAP > date de publication"; break;}
			if ($dateret != "" && $dateccap > $dateret) {$objForm->erreur="dateccap"; $objForm->libelle="Date du CCAP > date de retour des offres"; break;}
			if ($datecb != "" && $dateccap > $datecb) {$objForm->erreur="dateccap"; $objForm->libelle="Date du CCAP > date de validation CB"; break;}
			if ($datenot != "" && $dateccap > $datenot) {$objForm->erreur="dateccap"; $objForm->libelle="Date du CCAP > date de notification"; break;}
			if ($datefin != "" && $dateccap > $datefin) {$objForm->erreur="dateccap"; $objForm->libelle="Date du CCAP > date de fin"; break;}
		}
		// contrôle de cohérence de la date du ccap
		if ($daterma != "")
		{
			if ($datepub != "" && $daterma > $datepub) {$objForm->erreur="daterma"; $objForm->libelle="Date de validation RMA > date de publication"; break;}
			if ($dateret != "" && $daterma > $dateret) {$objForm->erreur="daterma"; $objForm->libelle="Date de validation RMA > date de retour des offres"; break;}
			if ($datecb != "" && $daterma > $datecb) {$objForm->erreur="daterma"; $objForm->libelle="Date de validation RMA > date de validation CB"; break;}
			if ($datenot != "" && $daterma > $datenot) {$objForm->erreur="daterma"; $objForm->libelle="Date de validation RMA > date de notification"; break;}
			if ($datefin != "" && $daterma > $datefin) {$objForm->erreur="daterma"; $objForm->libelle="Date de validation RMA > date de fin"; break;}
		}
		// contrôle de cohérence de la date de pubication
		if ($datepub != "")
		{
			if ($dateret != "" && $datepub > $dateret) {$objForm->erreur="datepub"; $objForm->libelle="Date de publication > date de retour des offres"; break;}
			if ($datecb != "" && $datepub > $datecb) {$objForm->erreur="datepub"; $objForm->libelle="Date de publication > date de validation CB"; break;}
			if ($datenot != "" && $datepub > $datenot) {$objForm->erreur="datepub"; $objForm->libelle="Date de publication > date de notification"; break;}
			if ($datefin != "" && $datepub > $datefin) {$objForm->erreur="datepub"; $objForm->libelle="Date de publication > date de fin"; break;}
		}
		// contrôle de cohérence de la date de retour des offres
		if ($dateret != "")
		{
			if ($datecb != "" && $dateret > $datecb) {$objForm->erreur="dateret"; $objForm->libelle="Date de retour des offres > date de validation CB"; break;}
			if ($datenot != "" && $dateret > $datenot) {$objForm->erreur="dateret"; $objForm->libelle="Date de retour des offres > date de notification"; break;}
			if ($datefin != "" && $dateret > $datefin) {$objForm->erreur="dateret"; $objForm->libelle="Date de retour des offres > date de fin"; break;}
		}
		// contrôle de cohérence de la date de validation du CB
		if ($datecb != "")
		{
			if ($datenot != "" && $datecb > $datenot) {$objForm->erreur="datecb"; $objForm->libelle="Date de validation CB > date de notification"; break;}
			if ($datefin != "" && $datecb > $datefin) {$objForm->erreur="datecb"; $objForm->libelle="Date de validation CB > date de fin"; break;}
		}
		// contrôle de cohérence de la date de notification
		if ($datenot != "")
		{
			if ($datefin != "" && $datecb > $datenot) {$objForm->erreur="datenot"; $objForm->libelle="Date de notification > date de fin"; break;}
		}
		break;
	}
	if ($objForm->erreur == '')
	{
		$objForm->majBdd('marche', 'MAR_CLE', $cle);
		$objForm->libelle="Mise à jour effectuée";
	}
}

// Affichage du début de la page
if ($cle != '') $objPage->addParam('cle', $cle);
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();

if ($flagrma == '')	$objForm->setAff('daterma', false);
if ($flagcb == '')	$objForm->setAff('datecb', false);

// affichage des boutons d'enchainement
if (($cle == '' && $objProfil->cre) || ($cle != '' && $objProfil->maj))
{
	$objForm->addBouton("submit","ENVOYER");
}
else $objForm->setLecForm();

$objForm->affFormulaire();
$objForm->finFormulaire();

// fin de page
$objPage->finPage();
