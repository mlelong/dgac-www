<?php
//indique que le type de la reponse renvoyee au client sera du Texte
header("Content-Type: text/html ; charset=utf-8");

//anti Cache pour HTTP/1.1
header("Cache-Control: no-cache , private");

//anti Cache pour HTTP/1.0
header("Pragma: no-cache");

require_once('connect_base.php');

try 
{
	$conn = new PDO('mysql:host='.$base.';port='.$port.';dbname='.$dbname.';charset=utf8;', $user, $pass); 
} 
catch (PDOException $e) 
{
	echo " ";
	exit; 
}

// récupération de l'id du projet
if(isset($_REQUEST['nomprojet'])) $nomprojet = $_REQUEST['nomprojet'];
else $nomprojet = "";
// récupération du programme du projet
$requete = "select PRG_NOMPROGRAMME from projet left join programme on PRJ_IDPROGRAMME=PRG_CLE	where PRJ_NOMPROJET=\"".$nomprojet."\"";
$statement = $conn->query($requete);
$resultat = $statement->fetch(PDO::FETCH_ASSOC);
$retour = $resultat['PRG_NOMPROGRAMME'];
if (isset($conn)) $conn=null; 
echo $retour;

