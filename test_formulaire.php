<?php
include('config/test_formulaire.php');
$codefonc='dem';
$codemenu='for';
require_once('prepage.php');

$objProfil = new profil();
$objProfil->maj = true;

$opt="disabled";

$objForm = new formulaire('1');
if($typeaction == "") $typeaction = 'creation';

if($typeaction == "creation") $objForm->initChamp();
if($typeaction == "modification") 
{
	$objForm->initChamp();
	$numreg='001';
	$nomreg="alsace";
	$datedeb="01-01-2017";
	$dateheure="31-12-2017 14:30";
	$libelle="Région de picardie";
	$deuxchamps1="champ1";
	$deuxchamps2="champ2";
	$flagperm="o";
	$casecol1='';
	$casecol2="o";
	$casecol3='';
	$caselig1='';
	$caselig2="o";
	$caselig3='';
	$radiocol=2;
	$radiolig=2;
	$description="Description de la région";
//	$motpasse="***";
}	

if($typeaction == "reception")
{
	// contrôle des paramètres
	$objForm->recChamp();
//	$description="nomreg=" . $nomreg . " libelle=" .$libelle;
}

// Affichage du début de la page
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();

if ($opt != '')
{
	$objForm->setOpt("numreg",$opt);
	$objForm->setOpt("nomreg",$opt);
	$objForm->setOpt("datedeb",$opt);
	$objForm->setOpt("dateheure",$opt);
	$objForm->setOpt("libelle",$opt);
	$objForm->setOpt("deuxchamps1",$opt);
	$objForm->setOpt("deuxchamps2",$opt);
	$objForm->setOpt("flagperm",$opt);
	$objForm->setOpt("casecol1",$opt);
	$objForm->setOpt("casecol2",$opt);
	$objForm->setOpt("casecol3",$opt);
	$objForm->setOpt("caselig1",$opt);
	$objForm->setOpt("caselig2",$opt);
	$objForm->setOpt("caselig3",$opt);
	$objForm->setOpt("radiocol",$opt);
	$objForm->setOpt("radiolig",$opt);
	$objForm->setOpt("description",$opt);
	$objForm->setOpt("fichier",$opt);
	$objForm->setOpt("fichiers",$opt);
	$objForm->setOpt("motpasse",$opt);
	$objForm->setOpt("groupe2",$opt);
	$objForm->setOpt("libre",$opt);
	$objForm->setOpt("sortable",$opt);
}

if ($flagperm == "o") $opt = " checked "; else $opt = "";
$objForm->setOpt('flagperm', $opt);
if ($casecol1 == "o") $opt = " checked "; else $opt = "";
$objForm->setOpt('casecol1', $opt);
if ($casecol2 == "o") $opt = " checked "; else $opt = "";
$objForm->setOpt('casecol2', $opt);
if ($casecol3 == "o") $opt = " checked "; else $opt = "";
$objForm->setOpt('casecol3', $opt);
if ($caselig1 == "o") $opt = " checked "; else $opt = "";
$objForm->setOpt('caselig1', $opt);
if ($caselig2 == "o") $opt = " checked "; else $opt = "";
$objForm->setOpt('caselig2', $opt);
if ($caselig3 == "o") $opt = " checked "; else $opt = "";
$objForm->setOpt('caselig3', $opt);

$requete = "select 'zone1' as CLE, 'liste 1' as DONNEE union select 'zone2' as CLE, 'liste 2' as DONNEE union select 'zone3' as CLE, 'liste 3' as DONNEE";
$objForm->setSelect('libre', 'zone2', $requete, 'CLE', 'DONNEE');

/*
$dataliste = "<span id='libre'>\n"; // bug IE
$dataliste .= "<SELECT disabled='disabled' class='form-control input-sm' NAME='libre'>\n";
$dataliste .= "<OPTION value=''>\n";
	$dataliste .= "<OPTION value=\"zone1\">liste 1\n";
	$dataliste .= "<OPTION selected value=\"zone2\">liste 2\n";
	$dataliste .= "<OPTION value=\"zone3\">liste 3\n";
$dataliste .= "</SELECT>\n";
$dataliste .= "</span>\n";
$objForm->setDataliste('libre', $dataliste);
*/

$datalisteval = "Groupe 1;Groupe 2; Groupe 3";
$datalistecle = "cle1;cle2;cle3";
$objForm->champF['sortable']['dataliste1'] = $datalistecle . "*;*" . $datalisteval;
$datalisteval = "Groupe 4;Groupe 5; Groupe 6";
$datalistecle = "cle4;cle5;cle6";
$objForm->champF['sortable']['dataliste2'] = $datalistecle . "*;*" . $datalisteval;

$objForm->affFormulaire();
	
// affichage des boutons d'enchainement
$objForm->addBouton("reset","CREATION","test_formulaire.php?typeaction=creation");
$objForm->addBouton("reset","MODIFICATION","test_formulaire.php?typeaction=modification");
$objForm->addBouton("submit","ENVOYER");
$objForm->finFormulaire();

// fin de page
$objPage->finPage();
