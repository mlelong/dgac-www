<?php
include('config/mar_maj_fournisseur.php');
$codefonc='mar';
require_once('prepage.php');

$objForm = new formulaire('1');

// création
if($typeaction == "creation") $objForm->initChamp();

// modification
if($typeaction == "modification")
{
	$requete = "select REL_IDMARCHE, REL_ETAT, REL_IDFOURNISSEUR from marche_fournisseur 
				where REL_CLE=\"" . $cle . "\"";
	$resultat = $objForm->mapChamp($requete);
}

// réception des paramètres
if($typeaction == "reception")
{
// contrôle des paramètres
	$objForm->recChamp();
	while($objForm->erreur == '')
	{
		break;
	}

	if ($objForm->erreur == '')
	{
		$objForm->majBdd('marche_fournisseur', 'REL_CLE', $cle);
		RedirURL("mar_list_fournisseur.php");
		exit();
	}
}

// Affichage du début de la page
if ($cle != '') $objPage->addParam('cle', $cle);
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();

$requete = "select FOU_CLE, FOU_NOMFOU from fournisseur order by 1";
$objForm->setSelect('idfournisseur', $idfournisseur, $requete, 'FOU_CLE', 'FOU_NOMFOU');

// affichage des boutons d'enchainement
$objForm->addBouton("reset","RETOUR","mar_list_fournisseur.php");
if (($cle == '' && $objProfil->cre) || ($cle != '' && $objProfil->maj))
{
	$objForm->addBouton("submit","ENVOYER");
}
else $objForm->setLecForm();

$objForm->affFormulaire();
$objForm->finFormulaire();

// fin de page
$objPage->finPage();
