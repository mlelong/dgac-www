<?php
include('config/cmd_maj_groupe-ligne.php');
$codefonc='cmd';
require_once('prepage.php');

$objForm = new formulaire('1');

if($typeaction == "creation") $objForm->initChamp();

if($typeaction == "modification")
{
	$requete = "select GCMD_CLE, GCMD_IDCMD, GCMD_TYPEIMPUTATION, GCMD_LIBELLE,
				DATE_FORMAT(GCMD_DATELIVA, '%d-%m-%Y') AS GCMD_DATELIVA,
				DATE_FORMAT(GCMD_DATEVA, '%d-%m-%Y') AS GCMD_DATEVA,
				DATE_FORMAT(GCMD_DATEVSR, '%d-%m-%Y') AS GCMD_DATEVSR
				from commande_groupe_ligne where GCMD_CLE=\"" . $cle . "\"";
	$resultat = $objForm->mapChamp($requete);
}

// réception des paramètres
if($typeaction == "reception")
{
// contrôle des paramètres
	$objForm->recChamp();
	while($objForm->erreur == '')
	{
		break;
	}

	if ($objForm->erreur == '')
	{
		$objForm->majBdd('commande_groupe_ligne', 'GCMD_CLE', $cle);
		$objForm->libelle="Mise à jour effectuée";
		RedirURL("cmd_list_groupe-ligne.php");
		exit();
	}
}

// Affichage du début de la page
if ($cle != '') $objPage->addParam('cle', $cle);
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();
	
// affichage des boutons d'enchainement
$objForm->addBouton("reset","RETOUR","cmd_list_groupe-ligne.php");
if (($cle == '' && $objProfil->cre) || ($cle != '' && $objProfil->maj))
{
	$objForm->addBouton("submit","ENVOYER");
}
else $objForm->setLecForm();

$objForm->affFormulaire();
$objForm->finFormulaire();

// fin de page
$objPage->finPage();
