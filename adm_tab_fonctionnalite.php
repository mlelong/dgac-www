<?php
$codefonc='adm';
include('prepage.php');

// début de page
$objPage->debPage('center');

$objOnglet = new onglet();

// début de l'onglet
$objOnglet->debOnglet();

// creation des liens
$objOnglet->debLienOnglet();
$objOnglet->addLienOnglet('wor','url','Workflow','adm_maj_fonctionnalite-etat.php?cleparent=' . $cle);
$objOnglet->addLienOnglet('eta','url','Etat','adm_list_etat.php?cleparent=' . $cle);
$objOnglet->addLienOnglet('pha','url','Phase','adm_list_phase.php?cleparent=' . $cle);
$objOnglet->finLienOnglet();

// fin de l'onglet
$objOnglet->finOnglet();

// fin de page
$objPage->finPage();
