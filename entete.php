<?php
	$this->tampon .= affEntete();
	// Affichage de l'entête
	function affEntete() 
	{
		$objProfil = profil::getProfil();
		$conn = database::getIntance();
		if (is_object($conn)) // si connexion bdd OK
		{
			if (basename($_SERVER['SCRIPT_NAME']) != 'log_connexion.php') 
			{	
				$requete = "select CFG_NBVISITE	from config where CFG_CLE=1";
				$statement = $conn->query($requete);
				$resultat = $statement->fetch(PDO::FETCH_ASSOC);
				$nbvisite = $resultat['CFG_NBVISITE'];
			}
		}
		
		/************************************************************************/
		// Bandeau avec logo
		/************************************************************************/
                $objMenu = menu::getMenu();
		$retour = '
			    <!-- Fixed top navbar -->
                            <nav class="navbar navbar-default navbar-fixed-top">
                                <div class="container-fluid">
                                    <div class="navbar-header">
                                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                        <a class="navbar-brand" href="#">Themis</a>
                                    </div>
                                    <div id="navbar" class="navbar-collapse collapse ">
                        ';
                                        
		if (is_object($objProfil) && $objProfil->nom != '') // Si session OK
		{
                    $retour .= '<ul class="nav navbar-nav navbar-right visible-xs">';
                    $retour .= $objMenu->getItemsHtmlContent();
                    $retour .= '</ul>';
                    
                    $retour .= '
                                    <hr class="visible-xs">
                                    <ul class="nav navbar-nav navbar-right">
                                        <li><a href="#"><i class="fa fa-user"></i> '.$objProfil->prenom.' '.$objProfil->nom.'</a></li>
                                        <li class="hidden-xs hidden-sm"><a href="#" onClick="setFavori()"><i class="fa fa-star"></i></a></li>
                                        <li class="hidden-xs hidden-sm"><a href="adm_list_favori.php"><i class="fa fa-home"></i></a></li>
                                        <li><a href="log_deconnexion.php">Déconnexion</a></li>
                                    </ul>
				';
		}
		
		$retour .= '	
                                    </div>    
                                </div>
                            </nav>
			';
		return $retour;	
	}
