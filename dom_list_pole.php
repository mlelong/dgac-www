<?php
include('config/dom_list_pole.php');
$codefonc='dom';
include('prepage.php');
if (!$objProfil->ctlDroit($typeaction, 'tableau')) exit();

$objPage->debPage('center');

// suppression d'une entrée
if($typeaction == "suppression")
{
	if(isset($cle))	
	{
		$requete = "delete from pole where POL_CLE=\"" . $cle . "\"";
		$statement = $conn->query($requete);
	}
}

// préparation du tableau
$objTab = new tableau('1');

// requête d'accès à la base 
$requete = "select POL_CLE, POL_NOMPOLE, DOM_NOMDOMAINE, POL_LIBELLE from pole
			left join domaine on POL_IDDOMAINE=DOM_CLE
			where POL_IDDOMAINE=\"" . $cleparent . "\"";
$requete .= $objTab->majRequete('order by POL_CLE'); // ajout tri et pagination si besoin

// affichage des boutons d'enchainement
if ($objProfil->cre) $objTab->addBouton("button","AJOUTER","dom_maj_pole.php");
	
// gestion des paramètres de lien
$objTab->setLien('nompole','dom_maj_pole.php',"?typeaction=modification&cle=#POL_CLE#");
$objTab->setLien('membre','dom_list_pole-utilisateur.php',"?typeaction=modification&cle=#POL_CLE#","Liste");
$objTab->setLien('responsable','dom_list_pole-responsable.php',"?typeaction=modification&cle=#POL_CLE#","Liste");
$objTab->setLien('supp','dom_list_pole.php',"?typeaction=suppression&cle=#POL_CLE#","Supprimer");

// affichage du tableau
$objTab->affTableau($requete);

// fin de page
$objPage->finPage();
