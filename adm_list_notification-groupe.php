<?php
include('config/adm_list_notification-groupe.php');
$codefonc='adm';
$codemenu='not';
include('prepage.php');
if (!$objProfil->ctlDroit($typeaction, 'tableau')) exit();

$objPage->debPage('center');

// suppression d'une entrée
if($typeaction == "suppression")
{
	if(isset($cle))	
	{
		$requete = "delete from notification_groupe where GNOT_CLE=\"" . $cle . "\"";
		$statement = $conn->query($requete);
	}
}

// préparation du tableau
$objTab = new tableau('1');

// requête d'accès à la base 
$requete = "select GNOT_CLE, GNOT_NOMGROUPE, T1.GRU_NOMGROUPE AS GNOT_GROUPEDEM, 
			T2.GRU_NOMGROUPE AS GNOT_GROUPEVAL, GNOT_DESCRIPTION 
			from notification_groupe
			left join groupe T1 on GNOT_IDGROUPEDEM=T1.GRU_CLE
			left join groupe T2 on GNOT_IDGROUPEVAL=T2.GRU_CLE
			where GNOT_IDNOTIF=\"" . $cleparent . "\"";
$requete .= $objTab->majRequete('order by GNOT_CLE'); // ajout tri et pagination si besoin

// affichage des boutons d'enchainement
if ($objProfil->cre) $objTab->addBouton("button","AJOUTER","adm_maj_notification-groupe.php");
	
// gestion des paramètres de lien
$objTab->setLien('nomgroupe','adm_maj_notification-groupe.php',"?typeaction=modification&cle=#GNOT_CLE#");
$objTab->setLien('supp','adm_list_notification-groupe.php',"?typeaction=suppression&cle=#GNOT_CLE#","Supprimer");

// affichage du tableau
$objTab->affTableau($requete);

// fin de page
$objPage->finPage();
