<?php
//indique que le type de la reponse renvoyee au client sera du Texte
header("Content-Type: text/html ; charset=utf-8");

//anti Cache pour HTTP/1.1
header("Cache-Control: no-cache , private");

//anti Cache pour HTTP/1.0
header("Pragma: no-cache");

require_once('connect_base.php');

try 
{
	$conn = new PDO('mysql:host='.$base.';port='.$port.';dbname='.$dbname.';charset=utf8;', $user, $pass); 
} 
catch (PDOException $e) 
{
	echo " ";
	exit; 
}

// r�cup�ration de l'id du p�le
if(isset($_REQUEST['idpole'])) $idpole = $_REQUEST['idpole'];
else $idpole = 0;
// r�cup�ration de l'id du budget
if(isset($_REQUEST['idbudget'])) $idbudget = $_REQUEST['idbudget'];
else $idbudget = 0;

// affichage des lignes budg�taires pour une ann�e en fonction du p�le
if($idpole > 0 && $idbudget > 0)
{
	// recherche des projets avec une ligne budg�taire pour l'ann�e
	$requete = "select BUDL_CLE, CONCAT(PRJ_NOMPROJET,\" ->\t\",IFNULL(BUDL_LIBELLE,'')) as PRJ_NOMPROJET,
				PRJ_CLE from budget_ligne 
				left join projet on BUDL_IDPROJET=PRJ_CLE
				left join budget on BUDL_IDBUDGET=BUD_CLE
				where PRJ_IDPOLE IN (select POL_CLE from pole where POL_IDDOMAINE=(select POL_IDDOMAINE from pole where POL_CLE=".$idpole.")) 
				and BUD_CLE=\"".$idbudget."\"";
}
// ou affichage de l'ensemble des projets
$statement = $conn->query($requete);
$res = [];
while ($row = $statement->fetch(PDO::FETCH_ASSOC))
{
	$res['*'.$row['BUDL_CLE']."*;*".$row['PRJ_CLE']] = $row['PRJ_NOMPROJET']; // * pour garder ordre � cause du json
}
// fermeture de la connexion
if (isset($conn)) $conn=null; 
// encodage en json et retour
$retour = json_encode($res);
echo $retour;

