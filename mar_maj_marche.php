<?php
include('config/mar_maj_marche.php');
$codefonc='mar';
require_once('prepage.php');
if (!$objProfil->ctlDroit($typeaction, 'formulaire',$cle)) exit();

$objForm = new formulaire('1');

// création
if($typeaction == "creation") $objForm->initChamp();

// modification
if($typeaction == "modification")
{
	$requete = "select MAR_CLE, MAR_NUMERODGR, MAR_NUMEROSIF, MAR_NUMEROOFFICIEL, 
				MAR_IDDEMANDEUR, MAR_DEMANDEUR, MAR_IDINTERVENANT, MAR_INTERVENANT, 
				MAR_URGENCE, MAR_PRIORITE, MAR_NOMMARCHE, MAR_IDPROJET, MAR_ETAT, MAR_NBLOT, 
				MAR_IDTYPEMARCHE, MAR_IDTYPEPROC, MAR_TYPEUO, MAR_IDPOLE, MAR_LIBELLE,
				MAR_TYPERECONDUC, MAR_DUREEINITIALE, MAR_NBRECONDUC, MAR_DUREERECONDUC,
				MAR_PREAVISRECONDUC, MAR_FLAGRMA,MAR_FLAGCB, 
				MAR_AEPREVTOTAL, MAR_AECONSTOTAL, MAR_FICHIER, MAR_DESCRIPTION,
				MAR_DATENOT, MAR_DATEFIN, MAR_DATERECCUR, MAR_DATERECSUI
				from marche where MAR_CLE=\"" . $cle . "\"";
	$resultat = $objForm->mapChamp($requete);
}

// réception des paramètres
if($typeaction == "reception")
{
	// contrôle des paramètres
	$objForm->recChamp();
	// gestion du demandeur et de l'intervenant
	if ($iddemandeur == 0 || $iddemandeur == '')
	{
		$demandeur = $objProfil->nom;
		$iddemandeur = $objProfil->idnom;
	}
	if ($intervenant != '')
	{
		$tab = explode('*-*', $intervenant);
		$idintervenant = $tab[0];
		$intervenant = $tab[1];
	}
	else
	{
		$idintervenant = 0;
		$intervenant = '';
	}
	while($objForm->erreur == '')
	{
/*	
		// un marché sans UO ne peut pas avoir d'UO
		if ($typeuo == "Sans UO" && $cle != '')
		{
			$requete = "select count(*) as NB from marche_uo where MUO_IDMARCHE=\"" . $cle . "\"";
			$statement = $conn->query($requete);
			$resultat = $statement->fetch(PDO::FETCH_ASSOC);
			if ($resultat['NB'] > 0) {$objForm->erreur='typeuo'; $objForm->libelle="Ce marché a des UO, il ne peut pas être 'sans UO'";}
			break;
		}
		// un marché sans UO avec des commandes ne pas pas passer avec UO
		if ($cle != '')
		{
			$requete = "select MAR_TYPEUO from marche where MAR_CLE=\"" . $cle . "\"";
			$statement = $conn->query($requete);
			$resultat = $statement->fetch(PDO::FETCH_ASSOC);
			if ($typeuo == "Sans UO" && $resultat['MAR_TYPEUO'] != 'Sans UO')
				{$objForm->erreur='typeuo'; $objForm->libelle="Changement impossible, il existe des commandes avec UO";}
			else if ($typeuo != "Sans UO" && $resultat['MAR_TYPEUO'] == 'Sans UO')
				{$objForm->erreur='typeuo'; $objForm->libelle="Changement impossible, il existe des commandes sans UO";}
			break;
		}
*/		
		break;
	}
	if ($objForm->erreur == '')
	{
		//
		// calcule des dates de reconduction et de fin de marché si le marché est notifié
		//
		if ($datenot != '' && $datenot != '0000-00-00' && ctlDate($datenot)) 
		{
			// durée initiale 1,2,3,,6 ans (dureeinitiale)
			// durée de reconduction 12,24,36,48 mois (dureereconduc)
			// nombre de reconduction 1,,6 (nbreconduc)
			// préavis de reconduction 1,2,3,4 mois (preavisreconduc)
			//
			// la date de notification = date de début du marché
			// les dates suivantes suivantes sont calculées
			// date de reconduction courante (dernière)
			// date de prochaine reconduction (suivante)
			// date de la prochaine reconduction 
			// date de fin de marché
			$curdate = date("Y-m-d");
			//
			// calcul de la date de fin du marché
			//
	//		$datefin = date($datedeb,strtotime("+" . . " month", strtotime($datedeb)));			
			$tab = explode('-', $datenot);
			$AA = $tab[0];
			$MM = $tab[1];
			$JJ = $tab[2];
			$nbmois = $dureeinitiale*12;
			if ($nbreconduc > 0) $nbmois += $nbreconduc*$dureereconduc;
			$nbannee = (integer)(($nbmois+$MM)/12); 
			$nbmois = ($nbmois+$MM)%12;
			$newAA = (integer)($AA+$nbannee);
			$newMM = (integer)($MM+$nbmois);
			$datefin = $newAA . "-" . $newMM . "-" . $JJ;
			//
			// calcul des dates de reconduction
			//
			if ($nbreconduc > 0)
			{
				if ($datereccur != '' && $datereccur != '0000-00-00')
				{
					if (ctlDate($datereccur)) $datedeb = $datereccur;
					else $datedeb = $datenot;
				}
				else $datedeb = $datenot;
				// calcul de la prochaine date de reconduction
				$tab = explode('-', $datedeb);
				$AA = $tab[0];
				$MM = $tab[1];
				$JJ = $tab[2];
				$nbmois = $dureereconduc;
				$nbannee = (integer)(($nbmois+$MM)/12); 
				$nbmois = ($nbmois+$MM)%12;
				$newAA = (integer)($AA+$nbannee);
				$newMM = (integer)($MM+$nbmois);
				$daterec = $newAA . "-" . $newMM . "-" . $JJ;
				// la date de reconduction ne doit pas dépasser la date de fin
				if ($daterec < $datefin) 
				{
					while(true)
					{
						if ($daterecsui == '' || $daterecsui == '0000-00-00') $daterecsui = $daterec;
						if ($curdate < $daterecsui) break;
						$datereccur = $daterecsui;
						$daterecsui = $daterec;
						$newAA = (integer)($newAA+$nbannee);
						$newMM = (integer)($newMM+$nbmois);
						$daterec = $newAA . "-" . $newMM . "-" . $JJ;
					}
				}
//				$description = "datedeb=" . $datedeb . " nbmois=" . $nbmois . " nbannee=" . $nbannee . " newAA=" . $newAA . " datereccur=" . $datereccur . " daterecsui=" . $daterecsui;
			}
		}
		// suppression des zéros dans les dates
		$datenot = preg_replace('#0000-00-00#', '', $datenot);
		$datefin = preg_replace('#0000-00-00#', '', $datefin);
		$datereccur = preg_replace('#0000-00-00#', '', $datereccur);
		$daterecsui = preg_replace('#0000-00-00#', '', $daterecsui);
		// mise à jour du marché
		if (!$objForm->majBdd('marche', 'MAR_CLE', $cle)) 
		{
			if ($objForm->libelle == '1062') {$objForm->erreur='nommarche'; $objForm->libelle="le nom du marché existe déjà";}
		}	
		else 
		{
			if ($cle == '') RedirURL("mar_tab_marche.php?typeaction=modification&cle=".$conn->lastInsertId());
		}
	}
}

// Affichage du début de la page
if ($cle != '') $objPage->addParam('cle', $cle);
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();

//$requete = "select PRJ_CLE, PRJ_NOMPROJET from projet order by 1";
//$objForm->setSelect('idprojet', $idprojet, $requete, 'PRJ_CLE', 'PRJ_NOMPROJET');
//$requete = "select POL_CLE, POL_NOMPOLE from pole order by 1";
//$objForm->setSelect('idpole', $idpole, $requete, 'POL_CLE', 'POL_NOMPOLE');
$requete = "select TMAR_CLE, TMAR_TYPE from type_marche order by 1";
$objForm->setSelect('idtypemarche', $idtypemarche, $requete, 'TMAR_CLE', 'TMAR_TYPE');
$requete = "select TPRO_CLE, CONCAT(TPRO_TYPE,\" - \t\",TPRO_LIBELLE) as TEXTE from type_procedure order by 1";
$objForm->setSelect('idtypeproc', $idtypeproc, $requete, 'TPRO_CLE', 'TEXTE');
$requete = "select UTI_NOM, CONCAT(UTI_CLE,\"*-*\",UTI_NOM) as CLE from utilisateur order by 1";
$objForm->setSelect('intervenant', $idintervenant . "*-*" . $intervenant, $requete, 'CLE', 'UTI_NOM');

//$objForm->setOpt('annee', 'readonly');
//if ($cle != '')	$objForm->setAff('flagrma', false);
if ($flagrma == "o") $opt = " checked "; else $opt = "";
$objForm->setOpt('flagrma', $opt);
$flagrma = "o";
if ($flagcb == "o") $opt = " checked "; else $opt = "";
$objForm->setOpt('flagcb', $opt);
$flagcb = "o";

// affichage des boutons d'enchainement
$objForm->addBouton("reset","RETOUR","mar_list_marche.php?menu=mar");
if (($cle == '' && $objProfil->cre) || ($cle != '' && $objProfil->maj))
{
	$objForm->addBouton("submit","ENVOYER");
}
else $objForm->setLecForm();

$objForm->affFormulaire();
$objForm->finFormulaire();

// fin de page
$objPage->finPage();
