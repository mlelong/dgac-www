<?php
// mise � jour des champs sortable d'un formulaire
//indique que le type de la reponse renvoyee au client sera du Texte
header("Content-Type: text/html ; charset=utf-8");

//anti Cache pour HTTP/1.1
header("Cache-Control: no-cache , private");

//anti Cache pour HTTP/1.0
header("Pragma: no-cache");

require_once('fonction.php');

if(isset($_REQUEST['cle']))
{
	require_once('connect_base.php');
	$cle=$_REQUEST['cle'];
	$type=$_REQUEST['type'];
	$list=$_REQUEST['list'];
	
	try 
	{
		$conn = new PDO('mysql:host='.$base.';port='.$port.';dbname='.$dbname.';charset=utf8;', $user, $pass); 
	} 
	catch (PDOException $e) 
	{
		echo " ";
		exit; 
	}
	
	// liste des groupes pour un profil
	if ($type == 'pro-g') 
	{
		$relation = 'a';
		$tab = explode(';', $list);
		for ($i=0; $i<count($tab); $i++)
		{
			if ($tab[$i] == '') continue;
			$groupe=explode('*-*', $tab[$i]);
			if ($groupe[0] == 'A')
			{
				$requete = "insert into groupe_profil (RGP_IDPROFIL, RGP_IDGROUPE, RGP_RELATION)
							values (\"" . $cle . "\",\"" . $groupe[1] . "\",\"" . $relation . "\")";
				$statement = $conn->query($requete);
			}
			if ($groupe[0] == 'S')
			{
				$requete = "delete from groupe_profil where RGP_IDPROFIL=\"" . $cle . "\" 
							and RGP_IDGROUPE=\"" . $groupe[1] . "\" and RGP_RELATION=\"" . $relation . "\" ";
				$statement = $conn->query($requete);
			}
		}
	}

	// liste des profils pour un groupe
	if ($type == 'grp-p') 
	{
		$relation = 'a';
		$tab = explode(';', $list);
		for ($i=0; $i<count($tab); $i++)
		{
			if ($tab[$i] == '') continue;
			$profil=explode('*-*', $tab[$i]);
			if ($profil[0] == 'A')
			{
				$requete = "insert into groupe_profil (RGP_IDPROFIL, RGP_IDGROUPE, RGP_RELATION)
							values (\"" . $profil[1] . "\",\"" . $cle . "\",\"" . $relation . "\")";
				$statement = $conn->query($requete);
			}
			if ($profil[0] == 'S')
			{
				$requete = "delete from groupe_profil where RGP_IDPROFIL=\"" . $profil[1] . "\" 
							and RGP_IDGROUPE=\"" . $cle . "\" and RGP_RELATION=\"" . $relation . "\" ";
				$statement = $conn->query($requete);
			}
		}
	}
	
	// liste des utilisateur pour un profil
	if ($type == 'pro-u') 
	{
		$relation = 'a';
		$tab = explode(';', $list);
		for ($i=0; $i<count($tab); $i++)
		{
			if ($tab[$i] == '') continue;
			$utilisateur=explode('*-*', $tab[$i]);
			if ($utilisateur[0] == 'A')
			{
				$requete = "insert into utilisateur_profil (RUP_IDPROFIL, RUP_IDUTILISATEUR, RUP_RELATION)
							values (\"" . $cle . "\",\"" . $utilisateur[1] . "\",\"" . $relation . "\")";
				$statement = $conn->query($requete);
			}
			if ($utilisateur[0] == 'S')
			{
				$requete = "delete from utilisateur_profil where RUP_IDPROFIL=\"" . $cle . "\" 
							and RUP_IDUTILISATEUR=\"" . $utilisateur[1] . "\" and RUP_RELATION=\"" . $relation . "\" ";
				$statement = $conn->query($requete);
			}
		}
	}

	// liste des profils pour un utilisateur
	if ($type == 'anu-p') 
	{
		$relation = 'a';
		$tab = explode(';', $list);
		for ($i=0; $i<count($tab); $i++)
		{
			if ($tab[$i] == '') continue;
			$profil=explode('*-*', $tab[$i]);
			if ($profil[0] == 'A')
			{
				$requete = "insert into utilisateur_profil (RUP_IDPROFIL, RUP_IDUTILISATEUR, RUP_RELATION)
							values (\"" . $profil[1] . "\",\"" . $cle . "\",\"" . $relation . "\")";
				$statement = $conn->query($requete);
			}
			if ($profil[0] == 'S')
			{
				$requete = "delete from utilisateur_profil where RUP_IDPROFIL=\"" . $profil[1] . "\" 
							and RUP_IDUTILISATEUR=\"" . $cle . "\" and RUP_RELATION=\"" . $relation . "\" ";
				$statement = $conn->query($requete);
			}
		}
	}

	// liste des utilisateurs pour un groupe
	if ($type == 'grp-u') 
	{
		$relation = 'a';
		$tab = explode(';', $list);
		for ($i=0; $i<count($tab); $i++)
		{
			if ($tab[$i] == '') continue;
			$utilisateur=explode('*-*', $tab[$i]);
			if ($utilisateur[0] == 'A')
			{
				$requete = "insert into groupe_utilisateur (RGU_IDGROUPE, RGU_IDUTILISATEUR, RGU_RELATION)
							values (\"" . $cle . "\",\"" . $utilisateur[1] . "\",\"" . $relation . "\")";
				$statement = $conn->query($requete);
			}
			if ($utilisateur[0] == 'S')
			{
				$requete = "delete from groupe_utilisateur where RGU_IDGROUPE=\"" . $cle . "\" 
							and RGU_IDUTILISATEUR=\"" . $utilisateur[1] . "\" and RGU_RELATION=\"" . $relation . "\" ";
				$statement = $conn->query($requete);
			}
		}
	}

	// liste des groupes pour un utilisateur
	if ($type == 'anu-g') 
	{
		$relation = 'a';
		$tab = explode(';', $list);
		for ($i=0; $i<count($tab); $i++)
		{
			if ($tab[$i] == '') continue;
			$groupe=explode('*-*', $tab[$i]);
			if ($groupe[0] == 'A')
			{
				$requete = "insert into groupe_utilisateur (RGU_IDGROUPE, RGU_IDUTILISATEUR, RGU_RELATION)
							values (\"" . $groupe[1] . "\",\"" . $cle . "\",\"" . $relation . "\")";
				$statement = $conn->query($requete);
			}
			if ($groupe[0] == 'S')
			{
				$requete = "delete from groupe_utilisateur where RGU_IDGROUPE=\"" . $groupe[1] . "\" 
							and RGU_IDUTILISATEUR=\"" . $cle . "\" and RGU_RELATION=\"" . $relation . "\" ";
				$statement = $conn->query($requete);
			}
		}
	}
	
	// liste des utilisateurs pour un pole
	if ($type == 'pol-u') $relation = 'u';
	if ($type == 'pol-p') $relation = 'p';
	if ($type == 'pol-u' || $type == 'pol-p') 
	{
		$tab = explode(';', $list);
		for ($i=0; $i<count($tab); $i++)
		{
			if ($tab[$i] == '') continue;
			$utilisateur=explode('*-*', $tab[$i]);
			if ($utilisateur[0] == 'A')
			{
				$requete = "insert into pole_utilisateur (RPU_IDPOLE, RPU_IDUTILISATEUR, RPU_RELATION)
							values (\"" . $cle . "\",\"" . $utilisateur[1] . "\",\"" . $relation . "\")";
				$statement = $conn->query($requete);
			}
			if ($utilisateur[0] == 'S')
			{
				$requete = "delete from pole_utilisateur where RPU_IDPOLE=\"" . $cle . "\" 
							and RPU_IDUTILISATEUR=\"" . $utilisateur[1] . "\" and RPU_RELATION=\"" . $relation . "\" ";
				$statement = $conn->query($requete);
			}
		}
	}

	if ($type == 'dom-d') 
	{
		$relation = 'd';
		$tab = explode(';', $list);
		for ($i=0; $i<count($tab); $i++)
		{
			if ($tab[$i] == '') continue;
			$utilisateur=explode('*-*', $tab[$i]);
			if ($utilisateur[0] == 'A')
			{
				$requete = "insert into domaine_utilisateur (RDU_IDDOMAINE, RDU_IDUTILISATEUR, RDU_RELATION)
							values (\"" . $cle . "\",\"" . $utilisateur[1] . "\",\"" . $relation . "\")";
				$statement = $conn->query($requete);
			}
			if ($utilisateur[0] == 'S')
			{
				$requete = "delete from domaine_utilisateur where RDU_IDDOMAINE=\"" . $cle . "\" 
							and RDU_IDUTILISATEUR=\"" . $utilisateur[1] . "\" and RDU_RELATION=\"" . $relation . "\" ";
				$statement = $conn->query($requete);
			}
		}
	}

	if (isset($conn)) $conn=null; 
	echo "OK";
}
else
{
	echo "KO";
	exit; 
}
