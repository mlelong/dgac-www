<?php
include('config/mar_list_uo.php');
$codefonc='mar';
include('prepage.php');
if (!$objProfil->ctlDroit($typeaction, 'tableau')) exit();

$objPage->debPage('center');

// suppression d'une entrée
if($typeaction == "suppression")
{
	if(isset($cle))	
	{
		$requete = "delete from marche_uo where MUO_CLE=\"" . $cle . "\"";
		$statement = $conn->query($requete);
	}
}

// préparation du tableau
$objTab = new tableau('1');

// requête d'accès à la base 
$requete = "select MUO_CLE, MUO_NOMUO, MUO_MARCHANDISE, MUO_FLAGIMPUTATION, MUO_FLAGREVISION, MUO_FLAGDSI,  
			REPLACE(FORMAT(MUO_MONTANT,2),',',' ') as MUO_MONTANT 
			from marche_uo where MUO_IDMARCHE=\"" . $cleparent . "\" ";
$requete .= $objTab->majRequete('order by MUO_CLE'); // ajout tri et pagination si besoin

// affichage des boutons d'enchainement
if ($objProfil->cre)  
{
	$objTab->addBouton("button","IMPORTER","mar_imp_uo.php");
//	if ($objProfil->nom == 'GUYOT') $objTab->addBouton("button","IMPORTER UGAP","mar_imp_uo-ugap.php");
	$objTab->addBouton("button","AJOUTER","mar_maj_uo.php");
}
	
// gestion des paramètres de lien
$objTab->setLien('nomuo','mar_maj_uo.php',"?typeaction=modification&cle=#MUO_CLE#");
$objTab->setLien('supp','mar_list_uo.php',"?typeaction=suppression&cle=#MUO_CLE#","Supprimer");

// affichage du tableau
$objTab->affTableau($requete);

// fin de page
$objPage->finPage();
