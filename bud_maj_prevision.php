<?php
include('config/bud_maj_prevision.php');
$codefonc='bud';
require_once('prepage.php');

$objForm = new formulaire('1');

if($typeaction == "creation") $objForm->initChamp();

if($typeaction == "modification")
{
	$requete = "select BUDL_CLE, BUDL_LIGNE, BUDL_IDBUDGET, BUDL_IDPAPFONCT, BUDL_IDPAPINVES, BUDL_IDPROJET, BUDL_TYPELIG, BUDL_LIBELLE,  
				BUDL_AEDOTAFONCT, BUDL_AEDEBLFONCT, BUDL_CPDOTAFONCT, BUDL_CPDEBLFONCT,
				BUDL_AEDOTAINVES, BUDL_AEDEBLINVES, BUDL_CPDOTAINVES, BUDL_CPDEBLINVES,
				BUDL_AEPREVT1, BUDL_AEPREVT2, BUDL_AEPREVT3, BUDL_AEPREVT4, 
				BUDL_CPPREVT1, BUDL_CPPREVT2, BUDL_CPPREVT3, BUDL_CPPREVT4, BUDL_COMMENTAIRE
				from budget_ligne where BUDL_CLE=\"" . $cle . "\"";
	$resultat = $objForm->mapChamp($requete);
}

// réception des paramètres
if($typeaction == "reception")
{
// contrôle des paramètres
	$objForm->recChamp();
	while($objForm->erreur == '')
	{
		break;
	}

	if ($objForm->erreur == '')
	{
		if ($aeprevt1 == '') $aeprevt1=0;
		if ($aeprevt2 == '') $aeprevt2=0;
		if ($aeprevt3 == '') $aeprevt3=0;
		if ($aeprevt4 == '') $aeprevt4=0;
		if ($cpprevt1 == '') $cpprevt1=0;
		if ($cpprevt2 == '') $cpprevt2=0;
		if ($cpprevt3 == '') $cpprevt3=0;
		if ($cpprevt4 == '') $cpprevt4=0;
		
		$objForm->majBdd('budget_ligne', 'BUDL_CLE', $cle);
		$objForm->libelle="Mise à jour effectuée";
		RedirURL("bud_list_prevision.php");
		exit();
	}
}

// Affichage du début de la page
if ($cle != '') $objPage->addParam('cle', $cle);
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();

// affichage des boutons d'enchainement
$objForm->addBouton("reset","RETOUR","bud_list_prevision.php");
if (($cle == '' && $objProfil->cre) || ($cle != '' && $objProfil->maj))
{
	$objForm->addBouton("submit","ENVOYER");
}
else $objForm->setLecForm();

$objForm->affFormulaire();
$objForm->finFormulaire();

// fin de page
$objPage->finPage();
