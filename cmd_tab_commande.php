<?php
$codefonc='cmd';
include('prepage.php');

// début de page
$objPage->debPage('center');

$objOnglet = new onglet();

// début de l'onglet
$objOnglet->debOnglet();

// creation des liens
$objOnglet->debLienOnglet();
$objOnglet->addLienOnglet('cmd','url','Commande','cmd_maj_commande.php?typeaction=' . $typeaction . '&cle=' . $cle);
$objOnglet->addLienOnglet('lgr','url','Groupes Postes','cmd_list_groupe-ligne.php?cleparent=' . $cle);
$objOnglet->addLienOnglet('lig','url','Postes','cmd_list_ligne.php?cleparent=' . $cle);
$objOnglet->addLienOnglet('syn','url','Paiements','cmd_list_paiement.php?cleparent=' . $cle);
$objOnglet->addLienOnglet('pce','url','Pièces jointes','cmd_list_piece.php?cleparent=' . $cle);
$objOnglet->finLienOnglet();

// fin de l'onglet
$objOnglet->finOnglet();

// trace d'accès aux commandes
TraceAcces($conn, $objProfil->idnom, "commande", $cle);

// fin de page
$objPage->finPage();
