<?php
include('config/adm_list_etat.php');
$codefonc='adm';
include('prepage.php');
if (!$objProfil->ctlDroit($typeaction, 'tableau')) exit();

$objPage->debPage('center');

// suppression d'une entrée
if($typeaction == "suppression")
{
	if(isset($cle))	
	{
		$requete = "delete from fonc_etat where ETA_CLE=\"" . $cle . "\"";
		$statement = $conn->query($requete);
	}
}

// préparation du tableau
$objTab = new tableau('1');

// requête d'accès à la base 
$requete = "select ETA_CLE, ETA_ORDRE, ETA_CODEETAT, ETA_NOMETAT, PHA_NOMPHASE  
			from fonc_etat left join fonc_phase on  ETA_IDPHASE=PHA_CLE 
			where ETA_IDFONC=\"" . $cleparent . "\" ";
$requete .= $objTab->majRequete('order by ETA_ORDRE, ETA_CLE'); // ajout tri et pagination si besoin

if ($objProfil->maj) $objPage->tampon .= $objTab->Sortable('sortable', 'ETA', 'fonc_etat');
	
// affichage des boutons d'enchainement
if ($objProfil->cre) $objTab->addBouton("button","AJOUTER","adm_maj_etat.php");

$objTab->setLigneAttr(" id='#ETA_CLE#'");

// gestion des paramètres de lien
$objTab->setLien('codeetat','adm_maj_etat.php',"?typeaction=modification&cle=#ETA_CLE#");
$objTab->setLien('supp','adm_list_etat.php',"?typeaction=suppression&cle=#ETA_CLE#","Supprimer");

// affichage du tableau
$objTab->affTableau($requete);

// fin de page
$objPage->finPage();
