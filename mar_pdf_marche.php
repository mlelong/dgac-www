<?php
require('./fpdf/fpdf.php');
include("./config/mar_pdf_marche.php");
class PDF extends FPDF
{

function FancyTable($header, $data)
{
    include("./config/mar_pdf_marche.php");
    // Couleurs, épaisseur du trait et police grasse
    $this->SetFillColor(44,62,80);
    $this->SetTextColor(255);
    $this->SetDrawColor(0,0,0);
    $this->SetLineWidth(.3);
    $this->SetFont('','B');
    // En-tête
    $w = array(25, 30, 100, 15, 20);
    for($i=0;$i<count($header);$i++)
        $this->Cell($w[$i],7,$header[$i],1,0,'C',true);
    $this->Ln();
    // Restauration des couleurs et de la police
    $this->SetFillColor(224,235,255);
    $this->SetTextColor(0);
    $this->SetFont('');
    // Données
    $fill = false;
    foreach($data as $row)
    {
        $this->Cell($w[0],6,$row[0],'LR',0,'L',$fill);
        $this->Cell($w[1],6,$row[1],'LR',0,'L',$fill);
        $this->Cell($w[2],6,$row[2],'LR',0,'R',$fill);
        $this->Cell($w[3],6,$row[3],'LR',0,'R',$fill);
        $this->Cell($w[4],6,$row[4],'LR',0,'R',$fill);

        $this->Ln();
        $fill = !$fill;
    }
    // Trait de terminaison
    $this->Cell(array_sum($w),0,'','T');
}

function Header()
{
    $this->Image('./images/fondentete.png',10,6,180,17);
	$this->SetTextColor(255,255,255);
    $this->SetFont('Arial', 'B', 18);
    $this->Text(15, 16, 'ITScore for Enterprise');
	$this->line(11,26,189,26);
    $this->Ln(20);
}

// Pied de page
function Footer()
{
    // Positionnement à 1,5 cm du bas
    $this->SetY(-15);
    // Police Arial italique 8
    $this->SetFont('Arial','I',8);
    // Numéro de page
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}

function Titre($libelle)
{
	$this->SetMargins(15,15);
   // Arial 9
    $this->SetFont('Arial','',9);
    // Couleur de fond
    $this->SetFillColor(200,220,255);
    // Titre
    $this->Cell(0,6,$libelle,0,1,'L',true);
    // Saut de ligne
    $this->Ln();
}

function Normal($libelle)
{
    // Arial 8
    $this->SetFont('Arial','',8);
    // Couleur de fond
    $this->SetFillColor(0,0,0);
    // Titre
	$this->MultiCell(0,5,$libelle);
	// Saut de ligne
    $this->Ln();
}

}

// Instanciation de la classe dérivée
$pdf = new PDF('P','mm','A4');
$pdf->AliasNbPages();
$pdf->AddPage();

// titre
$pdf->SetLineWidth(0.1);
$pdf->Titre('Level Summary - Executive View Maturity');
$pdf->Titre('Level 2: Enabling');
$pdf->Normal('The IT organization is cast in the role of supporter, providing on-demand operational support and projet services. The CIO is focused on supply side challenges that can compromise IT performance, operational integrity and costs.');

/*
$header = array('Country', 'Capital', 'Area (sq km)', 'Pop. (thousands)');
$pdf->SetFont('Arial','',8);
$data = [
    ["11_00_877","DGAC Oracle DB","Commande des serveurs de base de donnees Oracle","11/01/01","1000000"],
    ["131_300_877_002","DSAC Nord Oracle DB plus long",substr("Commande des serveurs de base de donnees Oracle, nom plus long azertyuiop azertyuiop",0,70),"11/01/01","1640"]
        ];

$pdf->FancyTable($header,$data);
*/
$pdf->Output();
