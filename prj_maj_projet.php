<?php
include('config/prj_maj_projet.php');
$codefonc='prj';
require_once('prepage.php');

$objForm = new formulaire('1');

// création
if($typeaction == "creation") $objForm->initChamp();

// modification
if($typeaction == "modification")
{
	$requete = "select PRJ_CLE, PRJ_TYPE, PRJ_CODE, PRJ_NOMPROJET, 
				PRJ_IDPOLE, PRJ_IDPROGRAMME, PRJ_LIBELLE
				from projet where PRJ_CLE=\"" . $cle . "\"";
	$resultat = $objForm->mapChamp($requete);
}

// réception des paramètres
if($typeaction == "reception")
{
// contrôle des paramètres
	$objForm->recChamp();
	while($objForm->erreur == '')
	{
		break;
	}
	if ($objForm->erreur == '')
	{
		$type = "Projet";
		if (!$objForm->majBdd('projet', 'PRJ_CLE', $cle))
		{
			if ($objForm->libelle == '1062') {$objForm->erreur='nomprojet'; $objForm->libelle="le nom du projet existe déjà";}
		}	
		else 
		{
			RedirURL("prj_list_projet.php");
			exit();
		}
	}
}

// Affichage du début de la page
if ($cle != '') $objPage->addParam('cle', $cle);
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();

$requete = "select PRG_CLE, PRG_NOMPROGRAMME from programme where PRG_TYPE=\"programme\" order by 1";
$objForm->setSelect('idprogramme', $idprogramme, $requete, 'PRG_CLE', 'PRG_NOMPROGRAMME', '');

$requete = "select POL_CLE, POL_NOMPOLE from pole order by 1";
$objForm->setSelect('idpole', $idpole, $requete, 'POL_CLE', 'POL_NOMPOLE');

// affichage des boutons d'enchainement
$objForm->addBouton("reset","RETOUR","prj_list_projet.php");
if (($cle == '' && $objProfil->cre) || ($cle != '' && $objProfil->maj))
{
	$objForm->addBouton("submit","ENVOYER");
}
else $objForm->setLecForm();

$objForm->affFormulaire();
$objForm->finFormulaire();

// fin de page
$objPage->finPage();
