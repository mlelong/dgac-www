<?php
include('config/prj_list_commentaire.php');
$codefonc='prj';
include('prepage.php');
if (!$objProfil->ctlDroit($typeaction, 'tableau')) exit();

$objPage->debPage('center');

// préparation du tableau
$objTab = new tableau('1');

// requête d'accès à la base 
$requete = "select COM_CLE, UTI_NOM,
			DATE_FORMAT(COM_DATECOM, '%d-%m-%Y %H:%i') AS COM_DATECOM, COM_DESCRIPTION
			from commentaire 
			left join utilisateur on COM_IDNOM=UTI_CLE
			where COM_NOMTABLE=\"projet\" AND COM_IDTABLE=" . $cleparent . " ";
$requete .= $objTab->majRequete('order by 1 desc'); // ajout tri et pagination si besoin

// affichage des boutons d'enchainement
if ($objProfil->cre)  $objTab->addBouton("button","AJOUTER","prj_maj_commentaire.php");

// affichage du tableau
$objTab->affTableau($requete);

// fin de page
$objPage->finPage();
