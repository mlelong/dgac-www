<?php
include('config/cfg_maj_parametre.php');
$codefonc='adm';
$codemenu='par';
require_once('prepage.php');

$objForm = new formulaire('1');

$cle = 1;
$requete = "select CFG_FLAGMAIL, CFG_MAIL_OBJET, CFG_MAIL_CORPS
			from config where CFG_CLE=\"" . $cle . "\"";
$resultat = $objForm->mapChamp($requete);

// réception des paramètres
if($typeaction == "reception")
{
// contrôle des paramètres
	$objForm->recChamp();
	while($objForm->erreur == '')
	{
		break;
	}

	if ($objForm->erreur == '')
	{
		$objForm->majBdd('config', 'CFG_CLE', $cle);
		$objForm->libelle="Mise à jour effectuée";
	}
}

// Affichage du début de la page
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();

if ($flagmail == "o") $opt = " checked "; else $opt = "";
$objForm->setOpt('flagmail', $opt);
$flagmail = "o";
if ($flagmailobj == "o") $opt = " checked "; else $opt = "";
$objForm->setOpt('flagmailobj', $opt);
$flagmailobj = "o";
if ($flagmailcor == "o") $opt = " checked "; else $opt = "";
$objForm->setOpt('flagmailcor', $opt);
$flagmailcor = "o";

// affichage des boutons d'enchainement
if (($cle == '' && $objProfil->cre) || ($cle != '' && $objProfil->maj))
{
	$objForm->addBouton("submit","ENVOYER");
}
else $objForm->setLecForm();

$objForm->affFormulaire();
$objForm->finFormulaire();

// fin de page
$objPage->finPage();
