<?php
//indique que le type de la reponse renvoyee au client sera du Texte
header("Content-Type: text/html ; charset=utf-8");

//anti Cache pour HTTP/1.1
header("Cache-Control: no-cache , private");

//anti Cache pour HTTP/1.0
header("Pragma: no-cache");

require_once('connect_base.php');

try 
{
	$conn = new PDO('mysql:host='.$base.';port='.$port.';dbname='.$dbname.';charset=utf8;', $user, $pass); 
} 
catch (PDOException $e) 
{
	echo " ";
	exit; 
}

$retour = '';
if(isset($_REQUEST['idmarche']))
{
	$idmarche=$_REQUEST['idmarche'];

	$requete = "select MUO_CLE, CONCAT(IFNULL(MUO_REFEXTERNE,''),' ', IFNULL(MUO_NOMUO,''),\"\t\",IFNULL(MUO_MONTANT,''),\" Euros\") as TEXTE 
				from marche_uo where MUO_FLAGDSI=\"o\" 
				and	MUO_IDMARCHE=(select CMD_IDMARCHE from commande where CMD_CLE=" . $idmarche . ") order by 1";
	$statement = $conn->query($requete);
	$retour = '';
	$json = array();
	
	while ($row = $statement->fetch(PDO::FETCH_ASSOC))
	{
		$json[] = array('value'=> $row['TEXTE'], 'id'=> $row['MUO_CLE']); 
	}
	echo json_encode($json);
}
else echo $retour;
if (isset($conn)) $conn=null;
