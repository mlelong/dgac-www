<?php
require_once('../connect_base.php');

//*******************************************************************
// détection du type d'OS
//*******************************************************************
$extention = "mysqli";
if (!extension_loaded($extention)) 
{
	echo "load extention mysql";
   if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') dl('php_'.$extention.'.dll'); 
   else dl($extention.'.so');
}

//*******************************************************************
// connexion a la base
//*******************************************************************

// connexion mysql
try {
	$conn = mysqli_connect($base, $user, $pass, $dbname, $port);
	mysqli_select_db($conn, $dbname);
}
catch (Exception $e) {
	die ("Erreur connexion : " . $e->getMessage());
}

// connexion PDO
/*
try 
{
	$conn = new PDO('mysql:host='.$base.';port='.$port.';dbname='.$dbname., $user, $pass); 
} 
catch (PDOException $e) 
{
	die ("Erreur connexion : " . $e->getMessage());
}
*/
