<?php
include('config/adm_list_phase.php');
$codefonc='adm';
include('prepage.php');
if (!$objProfil->ctlDroit($typeaction, 'tableau')) exit();

$objPage->debPage('center');

// suppression d'une entrée
if($typeaction == "suppression")
{
	if(isset($cle))	
	{
		$requete = "delete from fonc_phase where PHA_CLE=\"" . $cle . "\"";
		$statement = $conn->query($requete);
	}
}

// préparation du tableau
$objTab = new tableau('1');

// requête d'accès à la base 
$requete = "select PHA_CLE, PHA_ORDRE, PHA_CODEPHASE, PHA_NOMPHASE  from fonc_phase 
			where PHA_IDFONC=\"" . $cleparent . "\" ";
$requete .= $objTab->majRequete('order by PHA_ORDRE, PHA_CLE'); // ajout tri et pagination si besoin

if ($objProfil->maj) $objPage->tampon .= $objTab->Sortable('sortable', 'PHA', 'fonc_phase');

// affichage des boutons d'enchainement
if ($objProfil->cre) $objTab->addBouton("button","AJOUTER","adm_maj_phase.php");

// gestion des paramètres de lien
$objTab->setLien('codephase','adm_maj_phase.php',"?typeaction=modification&cle=#PHA_CLE#");
$objTab->setLien('supp','adm_list_phase.php',"?typeaction=suppression&cle=#PHA_CLE#","Supprimer");

// affichage du tableau
$objTab->affTableau($requete);

// fin de page
$objPage->finPage();
