<?php
$codefonc='rpm';
include('prepage.php');

// début de page
$objPage->debPage('center');

$objOnglet = new onglet();

// début de l'onglet
$objOnglet->debOnglet();

// creation des liens
$objOnglet->debLienOnglet();
$objOnglet->addLienOnglet('rps','url','Suivi','sui_list_rapport.php?rapport=Suivi');
$objOnglet->addLienOnglet('rpa','url','Alerte','sui_list_rapport.php?rapport=Alerte');
$objOnglet->finLienOnglet();

// fin de l'onglet
$objOnglet->finOnglet();

// fin de page
$objPage->finPage();
