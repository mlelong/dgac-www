<?php
include('config/cmd_list_groupe-ligne.php');
$codefonc='cmd';
include('prepage.php');
//if (!$objProfil->ctlDroit($typeaction, 'tableau')) exit();

$objPage->debPage('center');

// suppression d'une entrée
if($typeaction == "suppression")
{
	if(isset($cle))	
	{
		$requete = "delete from commande_groupe_ligne where GCMD_CLE=\"" . $cle . "\"";
		$statement = $conn->query($requete);
	}
}

// préparation du tableau
$objTab = new tableau('1');

// requête d'accès à la base 
$requete = "select GCMD_CLE, GCMD_LIBELLE, 
			DATE_FORMAT(GCMD_DATELIVA, '%d-%m-%Y') AS GCMD_DATELIVA,
			DATE_FORMAT(GCMD_DATEVA, '%d-%m-%Y') AS GCMD_DATEVA,
			DATE_FORMAT(GCMD_DATEVSR, '%d-%m-%Y') AS GCMD_DATEVSR
			from commande_groupe_ligne where GCMD_IDCMD=\"" . $cleparent . "\"";
$requete .= $objTab->majRequete('order by GCMD_CLE'); // ajout tri et pagination si besoin

// affichage des boutons d'enchainement
if ($objProfil->cre) $objTab->addBouton("button","AJOUTER","cmd_maj_groupe-ligne.php");
	
// gestion des paramètres de lien
$objTab->setLien('libelle','cmd_maj_groupe-ligne.php',"?typeaction=modification&cle=#GCMD_CLE#");
$objTab->setLien('supp','cmd_list_groupe-ligne.php',"?typeaction=suppression&cle=#GCMD_CLE#","Supprimer");

// affichage du tableau
$objTab->affTableau($requete);

// fin de page
$objPage->finPage();
