<?php
class uo 
{
	public $cle;  			/* clé de l'uo */
	public $nomuo;  		/* nom uo */
	public $idprestation;  	/* id de la prestation */
	public $nomprestation;  /* nom de la prestation */
	public $libprestation;  /* libellé de la prestation */
	public $idmarche;  		/* id du marché */
	public $refexterne;		/* référence extrene de l'uo */
	public $frequence;    	/* fréquence achat (forfaitaire ou unitaire) */
	public $libposte;  		/* libellé du poste */
	public $montant;    	/* montant uo */
	public $mcosla;    		/* liste des montants de support, MCO ou SLA */
	public $marchandise;  	/* groupe de marchandise */
	public $flagdsi;    	/* uo utilisée par la DSI ou non */
	public $flagimputation; /* type imputation (Fonct ou invest) */
	public $description;    /* description */
}
$objuo = new uo();
include('config/mar_imp_uo.php');
$codefonc='mar';
require_once('prepage.php');

$objForm = new formulaire('1');

if (!isset($typeaction) || $typeaction == '') $typeaction = 'creation';
if($typeaction == "creation") $objForm->initChamp();

// réception des paramètres
if($typeaction == "reception")
{
// contrôle des paramètres
	$objForm->recChamp();
	while ($objForm->erreur == '')
	{
		// récupération du fichier et sauvegarde dans le répertoire document
		if ((isset($_FILES['nomfic']['name'][0])&&($_FILES['nomfic']['error'][0] == UPLOAD_ERR_OK)))
		{
			$namefic = ctlNomFichier($_FILES['nomfic']['name'][0]);
			$chemin_destination = "document/" . $namefic;   
			move_uploaded_file($_FILES['nomfic']['tmp_name'][0], $chemin_destination); 
			if (!file_exists($chemin_destination)) // vérification pour savoir si le fichier existe
				{$objForm->erreur="nomfic"; $objForm->libelle="Le fichier " . $namefic . " n'a pas pu être chargé"; break;}
		}
		else 
		{
			if ($_FILES['nomfic']['error'][0] == UPLOAD_ERR_NO_FILE)
				{$objForm->erreur="nomfic"; $objForm->libelle="Il faut renseigner un fichier"; break;}
			else
				{$objForm->erreur="nomfic"; $objForm->libelle="Erreur sur chargement du fichier = " . $_FILES['nomfic']['error'][0]; break;}
		}
		break;
	}
	if ($objForm->erreur == '')
	{
		$rapport = "";
		//
		// création ou ajout des UO pour le marché	
		//
		$conn->setAttribute(PDO::ATTR_AUTOCOMMIT,0); 
		$conn->beginTransaction();
		$flag = false;
		$flagdsi = false;
		while(true)
		{
			$fichier = "document/" . $namefic;
//			Trace("Traitement du fichier " . $fichier);
			$handle = fopen ($fichier,"r");
			// numéro du marché sif
			$ligne = fgetcsv($handle, 1000, ";");
			// test si utf8
//			echo 'encoding = ' . mb_detect_encoding($ligne[1]);
			if (mb_detect_encoding($ligne[1] != 'UTF-8')) $flagutf8 = true;
			else $flagutf8 = false;
//			if ($flagutf8) echo "pas UTF-8";
			if ($flagutf8) $ligne = array_map("utf8_encode",$ligne);
			// traitement de l'entête
			$numerosif = $ligne[2];
			if ($numerosif != '')
			{
//				Trace("Traitement du marché " . $numerosif);
				$requete = "select MAR_CLE from marche where MAR_NUMEROSIF=\"" . $numerosif . "\"";
				$statement = $conn->query($requete);
				$resultat = $statement->fetch(PDO::FETCH_ASSOC);
				$idmarche  = $resultat['MAR_CLE'];
				if ($idmarche != $cleparent)
				{
//					Trace("Différence de clé idmarche=" . $idmarche . " cleparent=" . $cleparent);
					$objForm->erreur="nomfic";
					$objForm->libelle = "Le fichier ne correspond pas au marché " . $numerosif; 
					break;
				}
			}
			$objuo->idmarche = $cleparent;
			// remplace
			if ($flagsup == 'o')
			{
//				Trace("Suppression des UO existantes");
				$requete = "delete from marche_revision where MREV_IDMARCHE=\"" . $objuo->idmarche . "\"";
				$statement = $conn->query($requete);
				$requete = "delete from marche_uo where MUO_IDMARCHE=\"" . $objuo->idmarche . "\"";
				$statement = $conn->query($requete);
			}
			// 
			// traitement de l'entête
			//
			$libposte = '';
			$description = '';
			// nom fournisseur
			$ligne = getLigne($handle,$flagutf8);
			$titulaire = $ligne[2];
			// numéro SSIM
			$ligne = getLigne($handle,$flagutf8);
			$numerodgr = $ligne[2];
			//
			// cas particulier en fonction du tag d'import
			//
			$ligne = getLigne($handle,$flagutf8);
			$tagimport = $ligne[2];
			
			$versionbpu = ''; 
			//********************************************************************
			// cas particulier ANTEMETA
			//********************************************************************
			if ($tagimport == "ANTEMETA")
			{
				$objuo->flagdsi = 'o';
				$objuo->frequence = 'unitaire';
				$objuo->nomuocourt = '';
				// nom du marché
				$ligne = getLigne($handle,$flagutf8);
				// lot
				$ligne = getLigne($handle,$flagutf8);
				// prestation
				$ligne = getLigne($handle,$flagutf8);
				$objuo->nomprestation = $ligne[1] . " " . $ligne[2];
				$objuo->libprestation = '';
				// type bpu
				$ligne = getLigne($handle,$flagutf8);
				// version bpu
				$ligne = getLigne($handle,$flagutf8);
				$versionbpu = $ligne[2];
				// boucle sur toutes les lignes
				while ($ligne = fgetcsv($handle, 1000, ";")) 
				{
				/*
					if ($ligne[0] == 'AM')
					{
						$string = $ligne[5];
						echo "string=".$string."=";
						for ($i = 0; $i < strlen($string); $i++) {
							echo str_pad(dechex(ord($string[$i])), 2, '0', STR_PAD_LEFT)."=";
						}
					}
				*/	
					if ($ligne[0] == 'AM')
					{
						$ligne[5] = str_replace(chr(128), "", $ligne[5]); // 128 = €
						$ligne[7] = str_replace(chr(128), "", $ligne[7]);
						$ligne[8] = str_replace(chr(128), "", $ligne[8]);
						$ligne[9] = str_replace(chr(128), "", $ligne[9]);
						$ligne[10] = str_replace(chr(128), "", $ligne[10]);
					}	
					if ($flagutf8) $ligne = array_map("utf8_encode",$ligne);
					if ($ligne[0] != 'AM' && $ligne[0] != 'A' && $ligne[0] != 'M') continue;
					$objuo->refexterne = $ligne[1];
					/***********************************************************************/
					// on ajoute BPU'versionBPU'- devant la ligne                          
					/***********************************************************************/
					if ($versionbpu != '') $objuo->nomuo = "BPU-".$versionbpu."-".$ligne[1]."-".$ligne[2]."-".$ligne[5];
					else if ($ligne[0] == 'M') $objuo->nomuo = $ligne[1]."-".$ligne[2];
					else $objuo->nomuo = $ligne[2];
					if ($objuo->nomuo == '') continue;
					$objuo->montant = str_replace(",", ".", $ligne[5]);
					$objuo->montant = str_replace(" ", "", $objuo->montant);
					$objuo->montant = str_replace(chr(128), "", $objuo->montant);
					// traitement des mco et sla associés à l'UO
					if ($ligne[0] == 'AM')
					{
						if (isset($ligne[7])) $objuo->mcosla = $ligne[7];
						else $objuo->mcosla = '';
						if (isset($ligne[8])) $objuo->mcosla .= ";".$ligne[8];
						else $objuo->mcosla .= ";";
						if (isset($ligne[9])) $objuo->mcosla .= ";".$ligne[9];
						else $objuo->mcosla .= ";";
						if (isset($ligne[10])) $objuo->mcosla .= ";".$ligne[10];
						else $objuo->mcosla .= ";";
						$objuo->mcosla = str_replace(",", ".", $objuo->mcosla);
						$objuo->mcosla = str_replace(" ", "", $objuo->mcosla);
						$objuo->mcosla = str_replace(chr(128), "", $objuo->mcosla);
					}
					else if ($ligne[0] == 'M')
					{
						$objuo->montant = 0;
						if (isset($ligne[3])) $objuo->mcosla = $ligne[3];
						else $objuo->mcosla = '';
						if (isset($ligne[4])) $objuo->mcosla .= ";".$ligne[4];
						else $objuo->mcosla .= ";";
						if (isset($ligne[5])) $objuo->mcosla .= ";".$ligne[5];
						else $objuo->mcosla .= ";";
						if (isset($ligne[6])) $objuo->mcosla .= ";".$ligne[6];
						else $objuo->mcosla .= ";";
						$objuo->mcosla = str_replace(",", ".", $objuo->mcosla);
						$objuo->mcosla = str_replace(" ", "", $objuo->mcosla);
						$objuo->mcosla = str_replace(chr(128), "", $objuo->mcosla);
					}
					else $objuo->mcosla = '';
					// traitement de l'imputation
					$objuo->flagimputation = 'F';
					// traitement du groupe de marchandise
					$objuo->marchandise = '51.02.07;51.04.09;51.04.16';
					// traitement prestation
					$objuo->idprestation = setPrestation($objuo);
					/***********************************************************************/
					// suppression du flag dsi la première fois
					/***********************************************************************/
					if (!$flagdsi) 
					{
						$requete = "update marche_uo set MUO_FLAGDSI=\"\" where MUO_IDMARCHE=\"".$objuo->idmarche."\""; 
//						$statement = $conn->query($requete);
						$flagdsi = true;
					}
					// traitement uo
					setUO($objuo, $versionbpu);
				}
			}
			//********************************************************************
			// cas standard
			//********************************************************************
			if ($tagimport == "")
			{
				$objuo->mcosla = '';
				// TVA 
				$tva = str_replace("%", "", $ligne[10]); // on supprime le % si renseigné 
				// entête du tableau des lignes
				$ligne = getLigne($handle,$flagutf8);
				// lignes des uos
	//			Trace("Début de la boucle des UO");
				while ($ligne = fgetcsv($handle, 1000, ";")) 
				{
			//		print_r($ligne);
					if ($flagutf8) $ligne = array_map("utf8_encode",$ligne);
					// informations UO
					$objuo->flagdsi = $ligne[0];
					if ($objuo->flagdsi == 'x') $objuo->flagdsi = 'o';
					else $objuo->flagdsi = '';
					$prestation = $ligne[1];
					if ($prestation != '')
					{	
						$tab = explode(':', $prestation);
						if (isset($tab[0])) $objuo->nomprestation = $tab[0]; 
							else $objuo->nomprestation = '';
						if (isset($tab[1])) $objuo->libprestation = $tab[1]; 
							else $objuo->libprestation = '';
					}
					else
					{
						$objuo->nomprestation = '';
						$objuo->libprestation = '';
					}	
					$objuo->nomuo = $ligne[2];
					if ($objuo->nomuo == '') continue;
					$objuo->nomuocourt = $ligne[3];
					$objuo->frequence = $ligne[4];
					$objuo->montant = str_replace(",", ".", $ligne[5]);
					$objuo->flagimputation = $ligne[6];
					if ($objuo->flagimputation != 'F' && $objuo->flagimputation != 'I') $objuo->flagimputation = '';
					$objuo->marchandise = $ligne[7];
					$vide = $ligne[8];
					// informations révision de prix
					$daterevision = $ligne[9]; // JJ/MM/AAAA
					$revision = str_replace(",", ".", $ligne[10]); 
					//	
					// traitement si révision renseignée
					//	
					if ($daterevision != '' && $daterevision != 'NC')
					{
						// inversion du format de la date
						$tab = explode('-', $daterevision);
						if (count($tab) != 3) $tab = explode('/', $daterevision);
						if (count($tab) != 3) $tab = explode('.', $daterevision);
						$daterevision = $tab[2] . "-" . $tab[1] . "-" . $tab[0];
						// création ou mise à jour de la révision de prix
						$requete = "select MREV_CLE, MREV_COEFREV, MREV_DATEREV from marche_revision 
									where MREV_IDMARCHE=\"" . $objuo->idmarche . "\" and MREV_DATEREV=\"" . $daterevision . "\"";
						$statement = $conn->query($requete);
						$resultat = $statement->fetch(PDO::FETCH_ASSOC);
						$mrev_cle = $resultat['MREV_CLE'];
						$mrev_coefrev = $resultat['MREV_COEFREV'];
						$mrev_daterev = $resultat['MREV_DATEREV'];
						if ($mrev_cle > 0) // la révision existe déjà
						{
							$requete = "update marche_revision set MREV_COEFREV=\"" . $mrev_coefrev . "\"
										where MREV_CLE=\"" . $mrev_cle. "\"";
							$statement = $conn->query($requete);
						}
						else
						{
							$requete = "insert into marche_revision (MREV_IDMARCHE, MREV_COEFREV, MREV_DATEREV)
										values (\"" . $objuo->idmarche . "\",\"" . $revision . "\",\"" . $daterevision . "\")";
							$statement = $conn->query($requete);
						}
					}
					// traitement prestation
					$objuo->idprestation = setPrestation($objuo);
					// traitement uo
					setUO($objuo);
				}	
			}
			fclose ($handle);
			$conn->commit(); 
			$flag = true;
//			Trace("COMMIT");
			break;
		}
		if ($flag == false) 
		{
//			Trace("ROLLBACK");
			$conn->rollBack();
			$objForm->erreur="nomfic";
			if ($objForm->libelle == '') $objForm->libelle = "Erreur sur chargement des UO"; 
		}
		if (isset($fichier)) 
		{
			error_reporting(0);
			$result= unlink($fichier); // suppression du fichier
			error_reporting(E_ALL);
		}	
	}
	if ($objForm->libelle == '')
	{
		RedirURL("mar_list_uo.php");
		exit();
	}
}

// Affichage du début de la page
if ($cle != '') $objPage->addParam('cle', $cle);
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();

// affichage des boutons d'enchainement
$objForm->addBouton("reset","RETOUR","mar_list_uo.php");
if (($cle == '' && $objProfil->cre) || ($cle != '' && $objProfil->maj))
{
	$objForm->addBouton("submit","ENVOYER");
}
else $objForm->setLecForm();

$objForm->affFormulaire();
$objForm->finFormulaire();

// fin de page
$objPage->finPage();

//********************************************************************
// fonction
//********************************************************************

// récupération d'une ligne
function getLigne($handle, $flagutf8)
{
	$ligne = fgetcsv($handle, 1000, ";"); 
	if ($flagutf8) $ligne = array_map("utf8_encode",$ligne);
	return $ligne;
}

// traitement prestation
function setPrestation($objuo)
{
	$conn = database::getIntance();
	$requete = "select MPRE_CLE, MPRE_NOMPRESTATION from marche_prestation 
				where MPRE_IDMARCHE=\"" . $objuo->idmarche . "\" and MPRE_NOMPRESTATION=\"" . addslashes($objuo->nomprestation) . "\"";
	$statement = $conn->query($requete);
	$resultat = $statement->fetch(PDO::FETCH_ASSOC);
	$mpre_cle = $resultat['MPRE_CLE'];
	if ($mpre_cle > 0) // la prestation existe déjà
	{
		$requete = "update marche_prestation set MPRE_LIBELLE=\"" . addslashes($objuo->libprestation) . "\" where MPRE_CLE=\"" . $mpre_cle. "\"";
		$statement = $conn->query($requete);
	}
	else
	{
		$requete = "insert into marche_prestation (MPRE_NOMPRESTATION, MPRE_IDMARCHE, MPRE_LIBELLE)
					values (\"" . addslashes($objuo->nomprestation) . "\",\"" . $objuo->idmarche . "\",\"" . addslashes($objuo->libprestation) . "\")";
		$statement = $conn->query($requete);
		$mpre_cle = $conn->lastInsertId();
	}
	return $mpre_cle;
}

// traitement de l'uo
function setUo($objuo, $versionbpu='')
{
	$conn = database::getIntance();
	// selection sur nom uo ou reference externe
	if ($objuo->refexterne == '')
	{
		$requete = "select MUO_CLE, MUO_NOMUO, MUO_IDMARCHE from marche_uo 
					where MUO_IDMARCHE=\"" . $objuo->idmarche . "\" and MUO_NOMUO=\"" . addslashes($objuo->nomuo) . "\"";
		$statement = $conn->query($requete);
		$resultat = $statement->fetch(PDO::FETCH_ASSOC);
		$muo_cle = $resultat['MUO_CLE'];
		$muo_nomuo = $resultat['MUO_NOMUO'];
	}
	else
	{
		$requete = "select MUO_CLE, MUO_NOMUO, MUO_IDMARCHE from marche_uo 
					where MUO_IDMARCHE=\"" . $objuo->idmarche . "\" and MUO_REFEXTERNE=\"" . addslashes($objuo->refexterne) . "\"";
		$statement = $conn->query($requete);
		$resultat = $statement->fetch(PDO::FETCH_ASSOC);
		$muo_cle = $resultat['MUO_CLE'];
		$muo_nomuo = $resultat['MUO_NOMUO'];
	}
//	Trace($requete);
	if ($versionbpu != '') $refexterne = "BPU".$versionbpu."-".$objuo->refexterne;
	else $refexterne = $objuo->refexterne;
	if ($muo_cle > 0) // l'uo existe déjà
	{
//	Trace("Mise à jour " . $nomuo);
		$requete = "update marche_uo set MUO_NOMUO=\"" . addslashes($objuo->nomuo) 
					. "\", MUO_REFEXTERNE=\"" . $refexterne
					. "\", MUO_IDPRESTATION=\"" . $objuo->idprestation
					. "\", MUO_IDMARCHE=\"" . $objuo->idmarche
					. "\", MUO_FREQUENCE=\"" . $objuo->frequence
					. "\", MUO_LIBPOSTE=\"" . addslashes($objuo->libposte)
					. "\", MUO_MONTANT=\"" . $objuo->montant
					. "\", MUO_MARCHANDISE=\"" . $objuo->marchandise
					. "\", MUO_FLAGDSI=\"" . $objuo->flagdsi
					. "\", MUO_FLAGIMPUTATION=\"" . $objuo->flagimputation
					. "\", MUO_MCOSLA=\"" . $objuo->mcosla
					. "\", MUO_DESCRIPTION=\"" . $objuo->description
					. "\" where MUO_CLE=\"" . $muo_cle. "\"";
		$statement = $conn->query($requete);
	}
	else
	{
//	Trace("Insertion " . $nomuo);
		$requete = "insert into marche_uo (MUO_NOMUO, MUO_REFEXTERNE, MUO_IDPRESTATION, 
					MUO_IDMARCHE, MUO_FREQUENCE, MUO_LIBPOSTE, 
					MUO_MONTANT, MUO_MARCHANDISE, MUO_FLAGDSI, 
					MUO_FLAGIMPUTATION, MUO_MCOSLA, MUO_DESCRIPTION)
					values (\"" . addslashes($objuo->nomuo) . "\",\"" . $refexterne . "\",\"" . $objuo->idprestation
					. "\",\"" . $objuo->idmarche . "\",\"" . $objuo->frequence	. "\",\"" . addslashes($objuo->libposte)
					. "\",\"" . $objuo->montant . "\",\"" . $objuo->marchandise	. "\",\"" . $objuo->flagdsi
					. "\",\"" . $objuo->flagimputation . "\",\"" . $objuo->mcosla . "\",\"" . $objuo->description . "\")";
		$statement = $conn->query($requete);
	}
}
