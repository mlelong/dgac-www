<?php
include('config/mar_list_fournisseur.php');
$codefonc='mar';
include('prepage.php');
if (!$objProfil->ctlDroit($typeaction, 'tableau')) exit();

$objPage->debPage('center');

// suppression d'une entrée
if($typeaction == "suppression")
{
	if(isset($cle))	
	{
		$requete = "delete from marche_fournisseur where REL_CLE=\"" . $cle . "\"";
		$statement = $conn->query($requete);
	}
}

// préparation du tableau
$objTab = new tableau('1');

// requête d'accès à la base 
$requete = "select REL_CLE, FOU_NUMERO, FOU_NOMFOU, REL_ETAT from marche_fournisseur 
			left join fournisseur on REL_IDFOURNISSEUR=FOU_CLE
			where REL_IDMARCHE=\"" . $cleparent . "\" order by 1";

// affichage des boutons d'enchainement
if ($objProfil->cre)  $objTab->addBouton("button","AJOUTER","mar_maj_fournisseur.php");

// gestion des paramètres de lien
$objTab->setLien('numero','mar_maj_fournisseur.php',"?typeaction=modification&cle=#REL_CLE#");
$objTab->setLien('supp','mar_list_fournisseur.php',"?typeaction=suppression&cle=#REL_CLE#","Supprimer");

// affichage du tableau
$objTab->affTableau($requete);

// fin de page
$objPage->finPage();
