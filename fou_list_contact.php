<?php
include('config/fou_list_contact.php');
$codefonc='fou';
include('prepage.php');
if (!$objProfil->ctlDroit($typeaction, 'tableau')) exit();

$objPage->debPage('center');

// suppression d'une entrée
if($typeaction == "suppression")
{
	if(isset($cle))	
	{
		$requete = "delete from contact where CTC_CLE=\"" . $cle . "\"";
		$statement = $conn->query($requete);
	}
}

// préparation du tableau
$objTab = new tableau('1');

// requête d'accès à la base 
$requete = "select CTC_CLE, CTC_NOM, CTC_PRENOM, CTC_TELEPHONE
			from contact where CTC_IDFOURNISSEUR=\"" . $cleparent . "\" ";
$requete .= $objTab->majRequete('order by CTC_CLE'); // ajout tri et pagination si besoin

// affichage des boutons d'enchainement
if ($objProfil->cre) $objTab->addBouton("button","AJOUTER","fou_maj_contact.php");

// gestion des paramètres de lien
$objTab->setLien('nom','fou_maj_contact.php',"?typeaction=modification&cle=#CTC_CLE#");
$objTab->setLien('supp','fou_list_contact.php',"?typeaction=suppression&cle=#CTC_CLE#","Supprimer");

// affichage du tableau
$objTab->affTableau($requete);

// fin de page
$objPage->finPage();
