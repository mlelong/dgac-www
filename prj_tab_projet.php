<?php
$codefonc='prj';
include('prepage.php');

// début de page
$objPage->debPage('center');

$objOnglet = new onglet();

// début de l'onglet
$objOnglet->debOnglet();

// creation des liens
$objOnglet->debLienOnglet();
$objOnglet->addLienOnglet('prj','url','Projets','prj_maj_projet.php?typeaction=' . $typeaction . '&cle=' . $cle);
$objOnglet->addLienOnglet('bud','url','Budget','prj_list_ligne.php?cleparent=' . $cle);
$objOnglet->addLienOnglet('com','url','Commentaires','prj_list_commentaire.php?cleparent=' . $cle);
$objOnglet->finLienOnglet();

// fin de l'onglet
$objOnglet->finOnglet();

// trace d'accès aux projets
TraceAcces($conn, $objProfil->idnom, "projet", $cle)

// fin de page
$objPage->finPage();
