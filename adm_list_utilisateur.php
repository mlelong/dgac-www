<?php
include('config/adm_list_utilisateur.php');
$codefonc='uti';
include('prepage.php');
if (!$objProfil->ctlDroit($typeaction, 'tableau')) exit();

$objPage->debPage('center');

// suppression d'une entrée
if($typeaction == "suppression")
{
	if(isset($cle))	
	{
		$requete = "delete from utilisateur where UTI_CLE=\"" . $cle . "\"";
		$statement = $conn->query($requete);
	}
}

// préparation du tableau
$objTab = new tableau('1');

// requête d'accès à la base 
$requete = "select UTI_CLE, UTI_NOM, UTI_PRENOM, UTI_ETAT
			from utilisateur where 1 ";
if ($f_groupe != '') 
{
	$requete .= "	and UTI_CLE IN
	(select RGU_IDUTILISATEUR from groupe_utilisateur, groupe 
	where RGU_IDGROUPE=GRU_CLE and RGU_IDUTILISATEUR=UTI_CLE and GRU_NOMGROUPE LIKE \"%" . $f_groupe . "%\") ";
}	
$requete .= $objTab->majRequete('order by 2'); // ajout tri et pagination si besoin

// affichage des boutons d'enchainement
if ($objProfil->cre) $objTab->addBouton("button","AJOUTER","adm_maj_utilisateur.php");

// gestion des paramètres de lien
$objTab->setLien('nom','adm_maj_utilisateur.php',"?typeaction=modification&cle=#UTI_CLE#");
$objTab->setLien('groupe','adm_list_utilisateur-groupe.php',"?typeaction=modification&cle=#UTI_CLE#","Liste");
$objTab->setLien('profil','adm_list_utilisateur-profil.php',"?typeaction=modification&cle=#UTI_CLE#","Liste");
$objTab->setLien('supp','adm_list_utilisateur.php',"?typeaction=suppression&cle=#UTI_CLE#","Supprimer");

// affichage du tableau
$objTab->affTableau($requete);

// fin de page
$objPage->finPage();
