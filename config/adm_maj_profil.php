<?php
/* config fonction adm_maj_profil.php*/
$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Caractéristiques d'un profil","type"=>"Groupe"],
	"profil"=>["aff"=>true,"label"=>"Nom du profil","bdd"=>"PRO_NOMPROFIL","type"=>"Texte","taille"=>50,"ctl"=>"o"],
	"description"=>["aff"=>true,"label"=>"Description","bdd"=>"PRO_DESCRIPTION","type"=>"Zone texte","taille"=>500]
];
$descF1=["titre"=>"Administration des profils"];
?>
