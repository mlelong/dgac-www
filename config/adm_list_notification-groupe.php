<?php
/* config fonction adm_list_notification-groupe.php*/
$champT1=[
	"nomgroupe"=>["aff"=>true,"label"=>"Nom du groupe","bdd"=>"GNOT_NOMGROUPE","type"=>"Lien"],
	"groupedem"=>["aff"=>true,"label"=>"Groupe demandeur","bdd"=>"GNOT_GROUPEDEM","type"=>"Texte"],
	"groupeval"=>["aff"=>true,"label"=>"Groupe valideur","bdd"=>"GNOT_GROUPEVAL","type"=>"Texte"],
	"description"=>["aff"=>true,"label"=>"Description","bdd"=>"GNOT_DESCRIPTION"],
	"supp"=>["aff"=>true,"label"=>"Supprimer","type"=>"Lien"]
];
$descT1=["titre"=>"Gestion des notifications","pagination"=>false,"largeur"=>"","nombre"=>20,"filtre"=>false,"deforder"=>""];
?>
