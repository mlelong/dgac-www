<?php
/* config fonction prj_list_activite-maj.php*/
$champT1=[
	"nomprogramme"=>["aff"=>true,"label"=>"Nom de l'activité majeure","bdd"=>"PRG_NOMPROGRAMME","type"=>"Lien"],
	"libelle"=>["aff"=>true,"label"=>"Libellé","bdd"=>"PRG_LIBELLE"],
	"supp"=>["aff"=>true,"label"=>"Supprimer","type"=>"Lien"]
];
$descT1=["titre"=>"Gestion des activités majeures","pagination"=>false,"largeur"=>"","nombre"=>20,"filtre"=>false,"deforder"=>""];
?>
