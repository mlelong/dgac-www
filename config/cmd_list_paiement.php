<?php
/* config fonction cmd_list_paiement.php*/
$champT1=[
	"datefact"=>["aff"=>true,"label"=>"Réception facture","bdd"=>"PCMD_DATEFACT"],
	"dateok"=>["aff"=>true,"label"=>"Facture ok","bdd"=>"PCMD_DATEOK"],
	"dateac"=>["aff"=>true,"label"=>"Prise en compte AC","bdd"=>"PCMD_DATEAC"],
	"dateprev"=>["aff"=>true,"label"=>"Prévision paiement","bdd"=>"PCMD_DATEPREV"],
	"datepaie"=>["aff"=>true,"label"=>"Décaissée","bdd"=>"PCMD_DATEPAIE"],
	"totalttc"=>["aff"=>true,"label"=>"Montant TTC","bdd"=>"PCMD_TOTALTTC","type"=>"Montant"]
];
$descT1=["titre"=>"Gestion des paiements d'une commande","titresuite"=>true,"pagination"=>false,"largeur"=>"","nombre"=>20,"filtre"=>false,"deforder"=>""];

