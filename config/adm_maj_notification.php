<?php
/* config fonction adm_maj_notification.php*/
$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Caractéristiques d'une notification","type"=>"Groupe"],
	"nomnotif"=>["aff"=>true,"label"=>"Nom de la notification","bdd"=>"NOT_NOMNOTIF","type"=>"Texte","taille"=>50,"ctl"=>"o"],
	"description"=>["aff"=>true,"label"=>"Description","bdd"=>"NOT_DESCRIPTION","type"=>"Zone texte","taille"=>500]
];
$descF1=["titre"=>"Administration des notifications"];
?>
