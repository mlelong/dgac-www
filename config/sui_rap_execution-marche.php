<?php
/* config fonction mar_suivi_execution-marche.php*/
$champT1=[
	"numero"=>["aff"=>true,"label"=>"Numéro","bdd"=>"MAR_NUMERODGR","tri"=>true],
	"nommarche"=>["aff"=>true,"label"=>"Nom","bdd"=>"MAR_NOMMARCHE","tri"=>true],
	"nbuo"=>["aff"=>true,"label"=>"Nb UO","bdd"=>"MAR_NBUO","tri"=>true],
	"datenot"=>["aff"=>true,"label"=>"Date notification","bdd"=>"MAR_DATENOT"],
	"datereccur"=>["aff"=>true,"label"=>"Date reconduction","bdd"=>"MAR_DATERECCUR"],
	"daterecsui"=>["aff"=>true,"label"=>"Date prochaine reconduction","bdd"=>"MAR_DATERECSUI"],
	"datefin"=>["aff"=>true,"label"=>"Date fin","bdd"=>"MAR_DATEFIN"]
];
$descT1=["titre"=>"Suivi de l'exécution des marchés","pagination"=>false,"largeur"=>"","tri"=>true,"nombre"=>20,"filtre"=>false,"deforder"=>""];
$reqT1="select MAR_NUMERODGR, MAR_NOMMARCHE, MAR_NBUO,
				DATE_FORMAT(MAR_DATENOT, '%d-%m-%Y') AS MAR_DATENOT,
				DATE_FORMAT(MAR_DATERECCUR, '%d-%m-%Y') AS MAR_DATERECCUR,
				DATE_FORMAT(MAR_DATERECSUI, '%d-%m-%Y') AS MAR_DATERECSUI,
				DATE_FORMAT(MAR_DATEFIN, '%d-%m-%Y') AS MAR_DATEFIN
			from marche where 1";
$orderT1="order by MAR_NUMERODGR";
?>