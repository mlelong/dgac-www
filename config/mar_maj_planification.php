<?php
/* config fonction mar_maj_planification.php*/
$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Planification d'un marché","type"=>"Groupe"],
	"datecctp"=>["aff"=>true,"label"=>"Date rédaction du CCTP","bdd"=>"MAR_DATECCTP","type"=>"Date"],
	"dateccap"=>["aff"=>true,"label"=>"Date rédaction du CCAP","bdd"=>"MAR_DATECCAP","type"=>"Date"],
	"daterma"=>["aff"=>true,"label"=>"Date validation du RMA","bdd"=>"MAR_DATERMA","type"=>"Date"],
	"datepub"=>["aff"=>true,"label"=>"Date de publication","bdd"=>"MAR_DATEPUB","type"=>"Date"],
	"dateret"=>["aff"=>true,"label"=>"Date de retour des offres","bdd"=>"MAR_DATERET","type"=>"Date"],
	"datecb"=>["aff"=>true,"label"=>"Date validation du CB","bdd"=>"MAR_DATECB","type"=>"Date"],
	"datenot"=>["aff"=>true,"label"=>"Date de notification","bdd"=>"MAR_DATENOT","type"=>"Date"],
	"datefin"=>["aff"=>true,"label"=>"Date de fin","bdd"=>"MAR_DATEFIN","type"=>"Date"]
];
$descF1=["titre"=>"Gestion des marchés","titresuite"=>true];
?>
