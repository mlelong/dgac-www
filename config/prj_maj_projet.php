<?php
/* config fonction prj_maj_projet.php*/
$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Caractéristiques d'un projet","type"=>"Groupe"],
	"type"=>["aff"=>false,"label"=>"","bdd"=>"PRJ_TYPE","type"=>"Texte"],
	"nomprojet"=>["aff"=>true,"label"=>"Nom","bdd"=>"PRJ_NOMPROJET","type"=>"Texte","taille"=>250,"ctl"=>"o"],
	"code"=>["aff"=>true,"label"=>"Code du projet","bdd"=>"PRJ_CODE","type"=>"Texte","taille"=>50],
	"idprogramme"=>["aff"=>true,"label"=>"Programme","bdd"=>"PRJ_IDPROGRAMME","type"=>"Libre"],
	"idpole"=>["aff"=>true,"label"=>"Domaine/Pôle","bdd"=>"PRJ_IDPOLE","type"=>"Libre"],
	"libelle"=>["aff"=>true,"label"=>"Description","bdd"=>"PRJ_LIBELLE","type"=>"Texte","taille"=>250]
];
$descF1=["titre"=>"Administration des projets","titresuite"=>"nomprojet"];
?>
