<?php
/* config fonction bud_list_ligne.php*/
$champFL1=[
	"f_nomprogramme"=>["aff"=>true,"label"=>"Programme","bdd"=>"PRG_NOMPROGRAMME","type"=>"Texte"],
	"f_nomprojet"=>["aff"=>true,"label"=>"Projet","bdd"=>"PRJ_NOMPROJET","type"=>"Texte"],
	"f_nompole"=>["aff"=>true,"label"=>"Pôle","bdd"=>"POL_NOMPOLE","type"=>"Texte"],
	"f_nomdomaine"=>["aff"=>true,"label"=>"Domaine","bdd"=>"DOM_NOMDOMAINE","type"=>"Texte"]
];
$champT1=[
	"nomprogramme"=>["aff"=>true,"label"=>"Programme/activité","bdd"=>"PRG_NOMPROGRAMME","type"=>"Lien","tri"=>true],
	"nomprojet"=>["aff"=>true,"label"=>"Projet/activité","bdd"=>"PRJ_NOMPROJET","tri"=>true],
	"libelle"=>["aff"=>true,"label"=>"Libellé","bdd"=>"BUDL_LIBELLE","tri"=>true],
	"idpapfonct"=>["aff"=>true,"label"=>"PAP Fon.","bdd"=>"BUDL_IDPAPFONCT","tri"=>true],
	"idpapinves"=>["aff"=>true,"label"=>"PAP Inv.","bdd"=>"BUDL_IDPAPINVES","tri"=>true],
	"nompole"=>["aff"=>true,"label"=>"Nom pôle","bdd"=>"POL_NOMPOLE","tri"=>true],
	"typelig"=>["aff"=>true,"label"=>"Type","bdd"=>"BUDL_TYPELIG","tri"=>true],
	"typeimputation"=>["aff"=>true,"label"=>"Nature dépense","bdd"=>"BUDL_TYPEIMPUTATION","tri"=>true],
	"priorite"=>["aff"=>true,"label"=>"Priorité","bdd"=>"BUDL_PRIORITE","tri"=>true],
	"aedotafonct"=>["aff"=>true,"label"=>"AE fonctionnement","bdd"=>"BUDL_AEDOTAFONCT","type"=>"Montant","total"=>true],
	"aedotainves"=>["aff"=>true,"label"=>"AE investissement","bdd"=>"BUDL_AEDOTAINVES","type"=>"Montant","total"=>true],
	"cpdotafonct"=>["aff"=>true,"label"=>"CP fonctionnement","bdd"=>"BUDL_CPDOTAFONCT","type"=>"Montant","total"=>true],
	"cpdotainves"=>["aff"=>true,"label"=>"CP investissement","bdd"=>"BUDL_CPDOTAINVES","type"=>"Montant","total"=>true],
	"aedeblfonct"=>["aff"=>false,"label"=>"Débloqué AE FON","bdd"=>"BUDL_AEDEBLFONCT","type"=>"Montant","total"=>true],
	"aedeblinves"=>["aff"=>false,"label"=>"Débloqué AE INV","bdd"=>"BUDL_AEDEBLINVES","type"=>"Montant","total"=>true],
	"cpdeblfonct"=>["aff"=>false,"label"=>"Débloqué CP FON","bdd"=>"BUDL_CPDEBLFONCT","type"=>"Montant","total"=>true],
	"cpdeblinves"=>["aff"=>false,"label"=>"Débloqué CP INV","bdd"=>"BUDL_CPDEBLINVES","type"=>"Montant","total"=>true],
	"supp"=>["aff"=>true,"label"=>"Supprimer","type"=>"Lien"]
];
$descT1=["titre"=>"Gestion des lignes de budget","titresuite"=>true,"decimale"=>0,"pagination"=>false,"largeur"=>"","tri"=>true,"nombre"=>20,"filtre"=>true,"deforder"=>""];
?>