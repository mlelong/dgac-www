<?php
/* config fonction mar_list_marche.php*/
$champFL1=[
	"f_numerodgr"=>["aff"=>true,"label"=>"Numéro DGR","bdd"=>"MAR_NUMERODGR","type"=>"Texte"],
	"f_numerosif"=>["aff"=>true,"label"=>"Numéro SIF","bdd"=>"MAR_NUMEROSIF","type"=>"Texte"],
	"f_nommarche"=>["aff"=>true,"label"=>"Nom","bdd"=>"MAR_NOMMARCHE","type"=>"Texte"],
	"f_etat"=>["aff"=>true,"label"=>"Etat","bdd"=>"MAR_ETAT","type"=>"Texte"],
	"f_typeproc"=>["aff"=>false,"label"=>"Type procédure","bdd"=>"TPRO_TYPE","type"=>"Texte"],
	"f_nomprojet"=>["aff"=>false,"label"=>"Projet/activité","bdd"=>"PRJ_NOMPROJET","type"=>"Texte"],
	"f_nompole"=>["aff"=>false,"label"=>"Nom pôle","bdd"=>"POL_NOMPOLE","type"=>"Texte"],
	"f_datenot"=>["aff"=>false,"label"=>"Date","bdd"=>"MAR_DATENOT","type"=>"Texte"]
];
//	"f_nomprojet"=>["aff"=>true,"label"=>"Projet/activité","bdd"=>"PRJ_NOMPROJET","type"=>"Texte"],
//	"f_nompole"=>["aff"=>true,"label"=>"Nom pôle","bdd"=>"POL_NOMPOLE","type"=>"Texte"],
$champT1=[
	"numerodgr"=>["aff"=>true,"label"=>"Numéro DGR","bdd"=>"MAR_NUMERODGR","type"=>"Lien","tri"=>true],
	"numerosif"=>["aff"=>true,"label"=>"Numéro SIF","bdd"=>"MAR_NUMEROSIF","type"=>"Lien","tri"=>true],
	"nommarche"=>["aff"=>true,"label"=>"Nom","bdd"=>"MAR_NOMMARCHE","tri"=>true],
	"typeuo"=>["aff"=>false,"label"=>"Type UO","bdd"=>"MAR_TYPEUO","tri"=>true],
	"nbuo"=>["aff"=>false,"label"=>"Nb UO","bdd"=>"MAR_NBUO","tri"=>true],
	"intervenant"=>["aff"=>true,"label"=>"Rédacteur","bdd"=>"MAR_INTERVENANT","tri"=>true],
	"etat"=>["aff"=>true,"label"=>"Etat","bdd"=>"MAR_ETAT","tri"=>true],
	"typeproc"=>["aff"=>true,"label"=>"Procédure","bdd"=>"TPRO_LIBELLE"],
	"datepub"=>["aff"=>false,"label"=>"Date publication","bdd"=>"MAR_DATEPUB","tri"=>true],
	"dateret"=>["aff"=>false,"label"=>"Date offre","bdd"=>"MAR_DATERET","tri"=>true],
	"datenot"=>["aff"=>true,"label"=>"Date notification","bdd"=>"MAR_DATENOT","tri"=>true],
	"datefin"=>["aff"=>false,"label"=>"Date fin","bdd"=>"MAR_DATEFIN","tri"=>true],
	"montant"=>["aff"=>true,"label"=>"Montant","bdd"=>"MAR_AEPREVTOTAL","type"=>"Montant","tri"=>true],
	"supp"=>["aff"=>true,"label"=>"Supprimer","type"=>"Lien"]
];
$descT1=["titre"=>"Marchés et devis","pagination"=>true,"largeur"=>"","tri"=>true,"nombre"=>20,"filtre"=>true,"deforder"=>""];
//	"nomprojet"=>["aff"=>true,"label"=>"Projet/activité","bdd"=>"PRJ_NOMPROJET","type"=>"Texte","tri"=>true],
//	"nompole"=>["aff"=>true,"label"=>"Nom pôle","bdd"=>"POL_NOMPOLE","type"=>"Texte","tri"=>true],
?>
