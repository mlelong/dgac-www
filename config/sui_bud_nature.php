<?php
/* config fonction sui_bud_nature.php*/

$champFL1=[
	"f_annee"=>["aff"=>true,"label"=>"Année","bdd"=>"BUD_ANNEE","type"=>"Texte"],
	"f_programme"=>["aff"=>true,"label"=>"Programme LOLF","bdd"=>"BUD_PROGRAMME","type"=>"Liste","dataliste"=>array("P612"=>"P612","P613"=>"P613","P614-01"=>"P614-01","P614-02"=>"P614-02")],
];
$champT1=[
	"nature"=>["aff"=>true,"label"=>"Ligne","bdd"=>"BPAP_LIGNEPAP","type"=>"Lien"],
	"total"=>["aff"=>true,"label"=>"AE TOTAL","bdd"=>"","type"=>"Montant"],
	"t1"=>["aff"=>true,"label"=>"AE T1","bdd"=>"","type"=>"Montant"],
	"t2"=>["aff"=>true,"label"=>"AE T2","bdd"=>"","type"=>"Montant"],
	"t3"=>["aff"=>true,"label"=>"AE T3","bdd"=>"","type"=>"Montant"],
	"t4"=>["aff"=>true,"label"=>"AE T4","bdd"=>"","type"=>"Montant"]
];
$descT1=["titre"=>"Budget AE par nature de dépense","titresuite"=>true,"pagination"=>false,"largeur"=>"","nombre"=>20,"filtre"=>true,"deforder"=>""];
?>
