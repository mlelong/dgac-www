<?php
/* config fonction mar_list_revision.php*/
$champT1=[
	"daterev"=>["aff"=>true,"label"=>"Date révision","bdd"=>"MREV_DATEREV","type"=>"Lien","tri"=>true],
	"coefrev"=>["aff"=>true,"label"=>"Coefficient révision","bdd"=>"MREV_COEFREV","type"=>"Montant","tri"=>true],
	"supp"=>["aff"=>true,"label"=>"Supprimer","type"=>"Lien"]
];
$descT1=["titre"=>"Révision de prix pour un marché","titresuite"=>true,"pagination"=>false,"largeur"=>"","tri"=>true,"nombre"=>20,"filtre"=>false,"deforder"=>""];
?>
