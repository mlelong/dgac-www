<?php
/* config fonction dom_maj_domaine.php*/
$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Caractéristiques d'un pôle","type"=>"Groupe"],
	"cleparent"=>["aff"=>false,"label"=>"cleparent","bdd"=>"POL_IDDOMAINE","type"=>"Texte"],
	"nompole"=>["aff"=>true,"label"=>"Code du pôle","bdd"=>"POL_NOMPOLE","type"=>"Texte","taille"=>50,"ctl"=>"o"],
	"libelle"=>["aff"=>true,"label"=>"Nom du pôle","bdd"=>"POL_LIBELLE","type"=>"Texte","taille"=>250]
];
$descF1=["titre"=>"Administration des pôles","titresuite"=>true];
?>
