<?php
/* config fonction patch.php */

$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Téléchargement des patchs","type"=>"Groupe"],
	"typefic"=>["aff"=>true,"label"=>"Type de fichier","type"=>"Liste","dataliste"=>array("page"=>"page","aide"=>"aide","batch"=>"batch","class"=>"class","config"=>"config","css"=>"css","images"=>"images","js"=>"js","SQL"=>"SQL","fonts"=>"fonts")],
	"nomfic"=>["aff"=>true,"label"=>"Nom du fichier à attacher (8M maxi)","type"=>"Fichier"]
];
$descF1=["titre"=>"Gestion des patchs"];
?>
