<?php
/* config fonction mar_imp_uo.php */

$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Import des UO d'un marché","type"=>"Groupe"],
	"nomfic"=>["aff"=>true,"label"=>"Nom du fichier à attacher (8M maxi)","type"=>"Fichier"],
	"flagsup"=>["aff"=>true,"label"=>"Suppression avant insertion","type"=>"Case à cocher"]
];
$descF1=["titre"=>"Import unités d'oeuvre"];
?>
