<?php
/* config fonction prj_maj_programme.php*/
$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Caractéristiques d'un programme","type"=>"Groupe"],
	"type"=>["aff"=>false,"label"=>"","bdd"=>"PRG_TYPE","type"=>"Texte"],
	"nomprogramme"=>["aff"=>true,"label"=>"Nom du programme","bdd"=>"PRG_NOMPROGRAMME","type"=>"Texte","taille"=>250,"ctl"=>"o"],
	"libelle"=>["aff"=>true,"label"=>"Libellé","bdd"=>"PRG_LIBELLE","type"=>"Texte","taille"=>250,"ctl"=>""]
];
$descF1=["titre"=>"Administration des programmes","titresuite"=>true];
?>
