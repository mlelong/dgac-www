<?php
/* config fonction adm_maj_profil-droit.php*/
$champT1=[
	["aff"=>true,"label"=>"Code Fonction","largeur_col"=>40],
	["aff"=>true,"label"=>"Nom Fonction","largeur_col"=>200],
	["aff"=>true,"label"=>"Lecture","largeur_col"=>50],
	["aff"=>true,"label"=>"Création","largeur_col"=>50],
	["aff"=>true,"label"=>"Modification","largeur_col"=>50],
	["aff"=>true,"label"=>"Suppression","largeur_col"=>50],
	["aff"=>true,"label"=>"Exécution","largeur_col"=>50],
	["aff"=>true,"label"=>"Lecture hors périmètre","largeur_col"=>50],
	["aff"=>true,"label"=>"Modification hors périmètre","largeur_col"=>50],
	["aff"=>true,"label"=>"Admin hors périmètre","largeur_col"=>50]
];
$descT1=["titre"=>"Gestion des Droits d'un profil","pagination"=>false,"droit"=>true,"largeur"=>"","nombre"=>15,"filtre"=>false,"deforder"=>""];
?>
