<?php
/* config fonction mar_suivi_planification.php*/
$champFL1=[
	"f_nompole"=>["aff"=>true,"label"=>"Nom du pôle","bdd"=>"POL_NOMPOLE","type"=>"Texte","taille"=>false],
	"f_nommarche"=>["aff"=>true,"label"=>"Nom du marché","bdd"=>"MAR_NOMMARCHE","type"=>"Texte","taille"=>false]
];
$champT1=[
	"numero"=>["aff"=>true,"label"=>"Numéro","bdd"=>"CMD_NUMERO","type"=>"Lien","taille"=>false,"tri"=>true],
	"nompole"=>["aff"=>true,"label"=>"Nom pôle","bdd"=>"POL_NOMPOLE","type"=>"Texte","taille"=>false,"tri"=>true],
	"nommarche"=>["aff"=>true,"label"=>"Nom marché","bdd"=>"MAR_NOMMARCHE","type"=>"Texte","taille"=>false,"tri"=>true],
	"nomprojet"=>["aff"=>true,"label"=>"Nom projet","bdd"=>"PRJ_NOMPROJET","type"=>"Texte","taille"=>false,"tri"=>true],
	"etat"=>["aff"=>true,"label"=>"Etat","bdd"=>"ETA_NOMETAT","type"=>"Texte","taille"=>false,"tri"=>true],
	"datecmd"=>["aff"=>true,"label"=>"Date EJ SIF","bdd"=>"CMD_DATECMD","type"=>"Texte","taille"=>false,"tri"=>true],
	"totalttc"=>["aff"=>true,"label"=>"Total TTC","bdd"=>"CMD_TOTALTTC","type"=>"Montant","taille"=>false,"tri"=>true,"total"=>true]
];
$descT1=["titre"=>"Commandes en cours","pagination"=>false,"largeur"=>"","tri"=>true,"nombre"=>20,"filtre"=>true,"deforder"=>""];
$reqT1="select CMD_CLE, CMD_NUMERO, POL_NOMPOLE, MAR_NOMMARCHE, PRJ_NOMPROJET, 
			DATE_FORMAT(CMD_DATECMD, '%d-%m-%Y') AS CMD_DATECMD,
			ETA_NOMETAT, CMD_TOTALTTC
			from commande 
			left join pole on CMD_IDPOLE=POL_CLE
			left join marche on CMD_IDMARCHE=MAR_CLE
			left join projet on CMD_IDPROJET=PRJ_CLE
			left join fonc_etat on CMD_IDETAT=ETA_CLE
			where 1 ";
$orderT1="order by CMD_NUMERO desc";
$lienT1=array(
	array("numero","cmd_tab_commande.php","?typeaction=modification&cle=#CMD_CLE#",'')
);
?>