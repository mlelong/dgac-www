<?php
/* config fonction cfg_maj_parametre.php*/
$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Configuration des paramètres","type"=>"Groupe"],
	"flagmail"=>["aff"=>true,"label"=>"Envoi des mails de notification","bdd"=>"CFG_FLAGMAIL","type"=>"Case à cocher"],
	"flagmailobj"=>["aff"=>true,"label"=>"Objet du mail en utf8","bdd"=>"CFG_MAIL_OBJET","type"=>"Case à cocher"],
	"flagmailcor"=>["aff"=>true,"label"=>"Corps du mail en utf8","bdd"=>"CFG_MAIL_CORPS","type"=>"Case à cocher"]
];
$descF1=["titre"=>"Gestion des paramètres"];
?>
