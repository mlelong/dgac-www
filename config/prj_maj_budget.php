<?php
/* config fonction prj_maj_budget.php*/
$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Caractéristiques d'un projet","type"=>"Groupe"],
	"lignebud"=>["aff"=>true,"label"=>"Ligne","bdd"=>"PBUD_LIGNE","type"=>"Texte","taille"=>250],
	"idprojet"=>["aff"=>true,"label"=>"Nom du projet","bdd"=>"PBUD_IDPROJET","type"=>"Libre","taille"=>250,"ctl"=>"o"],
	"idmarche"=>["aff"=>true,"label"=>"Nom du marché","bdd"=>"PBUD_IDMARCHE","type"=>"Libre","taille"=>250],
	"libelle"=>["aff"=>true,"label"=>"Libellé","bdd"=>"PBUD_LIBELLE","type"=>"Texte","taille"=>250],
	"refsif"=>["aff"=>true,"label"=>"Référence SIF","bdd"=>"PBUD_REFSIF","type"=>"Texte","taille"=>50],
	"aefonct"=>["aff"=>true,"label"=>"AE fonctionnement (Euros TTC)","bdd"=>"PBUD_AEPREVFONCT","type"=>"Montant","taille"=>25],
	"cpfonct"=>["aff"=>true,"label"=>"CP fonctionnement (Euros TTC)","bdd"=>"PBUD_CPPREVFONCT","type"=>"Montant","taille"=>25],
	"aeinves"=>["aff"=>true,"label"=>"AE investissement (Euros TTC)","bdd"=>"PBUD_AEPREVINVES","type"=>"Montant","taille"=>25],
	"cpinves"=>["aff"=>true,"label"=>"CP investissement (Euros TTC)","bdd"=>"PBUD_CPPREVINVES","type"=>"Montant","taille"=>25]
];
$descF1=["titre"=>"Administration du budget des projets","titresuite"=>true];
?>
