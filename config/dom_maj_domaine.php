<?php
/* config fonction dom_maj_domaine.php*/
$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Caractéristiques d'un domaine","type"=>"Groupe"],
	"nomdomaine"=>["aff"=>true,"label"=>"Code du domaine","bdd"=>"DOM_NOMDOMAINE","type"=>"Texte","taille"=>50,"ctl"=>"o"],
	"direction"=>["aff"=>true,"label"=>"Code de la direction","bdd"=>"DOM_DIRECTION","type"=>"Liste","taille"=>50,"ctl"=>"o","dataliste"=>array("DSNA"=>"DSNA","DSAC"=>"DSAC","DTA"=>"DTA","SG"=>"SG")],
	"libelle"=>["aff"=>true,"label"=>"Nom du domaine","bdd"=>"DOM_LIBELLE","type"=>"Texte","taille"=>250]
];
$descF1=["titre"=>"Administration des domaines","titresuite"=>"nomdomaine"];
?>