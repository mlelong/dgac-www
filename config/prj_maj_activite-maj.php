<?php
/* config fonction prj_maj_activite-maj.php*/
// 	"type"=>["aff"=>false,"label"=>"","bdd"=>"PRG_TYPE","type"=>"Texte"],
$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Caractéristiques d'une activité majeure","type"=>"Groupe"],
	"type"=>["aff"=>true,"label"=>"Type","bdd"=>"PRG_TYPE","type"=>"Liste","dataliste"=>array("Programme"=>"Programme","Activité majeure"=>"Activité majeure")],
	"nomprogramme"=>["aff"=>true,"label"=>"Nom de l'activité majeure","bdd"=>"PRG_NOMPROGRAMME","type"=>"Texte","taille"=>250,"ctl"=>"o"],
	"libelle"=>["aff"=>true,"label"=>"Libellé","bdd"=>"PRG_LIBELLE","type"=>"Texte","taille"=>250,"ctl"=>""]
];
$descF1=["titre"=>"Administration des activités majeures","titresuite"=>true];
?>
