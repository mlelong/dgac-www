<?php
/* config fonction cmd_list_commande.php*/
$champFL1=[
	"f_numero"=>["aff"=>true,"label"=>"Numéro","bdd"=>"CMD_NUMERO","type"=>"Texte","compare"=>"egal"],
	"f_numerosif"=>["aff"=>true,"label"=>"Numéro EJ","bdd"=>"CMD_NUMEROSIF","type"=>"Texte","cacher-mobile"=>true],
	"f_datecmd"=>["aff"=>true,"label"=>"Date EJ","bdd"=>"CMD_DATECMD","type"=>"Texte"],
	"f_objet"=>["aff"=>true,"label"=>"Objet","bdd"=>"CMD_OBJET","type"=>"Texte","cacher-mobile"=>true],
	"f_nompole"=>["aff"=>true,"label"=>"Pôle","bdd"=>"POL_NOMPOLE","type"=>"Texte"],
	"f_demandeur"=>["aff"=>true,"label"=>"Demandeur","bdd"=>"CMD_DEMANDEUR","type"=>"Texte","cacher-mobile"=>true],
	"f_nommarche"=>["aff"=>true,"label"=>"Marché","bdd"=>"MAR_NOMMARCHE","type"=>"Texte","cacher-mobile"=>true],
	"f_nomprojet"=>["aff"=>true,"label"=>"Projet","bdd"=>"PRJ_NOMPROJET","type"=>"Texte","cacher-mobile"=>true],
	"f_etat"=>["aff"=>false,"label"=>"Etat","bdd"=>"ETA_NOMETAT","type"=>"Texte"]
];
$champT1=[
	"numero"=>["aff"=>true,"label"=>"Numéro","bdd"=>"CMD_NUMERO","type"=>"Lien","tri"=>true,"cacher-mobile"=>true],
	"objet"=>["aff"=>true,"label"=>"Objet","bdd"=>"CMD_OBJET","type"=>"Lien"],
	"nompole"=>["aff"=>true,"label"=>"Nom pôle","bdd"=>"POL_NOMPOLE","tri"=>true,"cacher-mobile"=>true],
	"demandeur"=>["aff"=>true,"label"=>"Demandeur","bdd"=>"CMD_DEMANDEUR","tri"=>true,"cacher-mobile"=>true],
	"nommarche"=>["aff"=>true,"label"=>"Nom marché","bdd"=>"MAR_NOMMARCHE","tri"=>true,"cacher-mobile"=>true],
	"nomprojet"=>["aff"=>true,"label"=>"Nom projet","bdd"=>"PRJ_NOMPROJET","tri"=>true,"cacher-mobile"=>true],
	"nomprogramme"=>["aff"=>false,"label"=>"Nom programme","bdd"=>"PRG_NOMPROGRAMME","tri"=>true,"cacher-mobile"=>true],
	"intervenant"=>["aff"=>true,"label"=>"Gestionnaire","bdd"=>"CMD_INTERVENANT","tri"=>true,"cacher-mobile"=>true,"cacher-mobile"=>true],
	"etat"=>["aff"=>true,"label"=>"Etat","bdd"=>"ETA_NOMETAT","tri"=>true,"cacher-mobile"=>true],
	"typeimputation"=>["aff"=>false,"label"=>"Type imputation","bdd"=>"CMD_TYPEIMPUTATION","tri"=>true,"cacher-mobile"=>true],
	"numerosif"=>["aff"=>true,"label"=>"EJ","bdd"=>"CMD_NUMEROSIF","tri"=>true,"cacher-mobile"=>true],
	"datecmd"=>["aff"=>true,"label"=>"Commande","bdd"=>"CMD_DATECMD","tri"=>true,"cacher-mobile"=>true],
	"dateliva"=>["aff"=>false,"label"=>"Livraison ","bdd"=>"CMD_DATELIVA","tri"=>true,"cacher-mobile"=>true],
	"dateva"=>["aff"=>false,"label"=>"VA","bdd"=>"CMD_DATEVA","tri"=>true,"cacher-mobile"=>true],
	"dateserv"=>["aff"=>false,"label"=>"Service fait","bdd"=>"CMD_DATESERV","tri"=>true,"cacher-mobile"=>true],
	"totalttc"=>["aff"=>true,"label"=>"Total TTC","bdd"=>"CMD_TOTALTTC","type"=>"Montant","largeur_col"=>125,"tri"=>true],
	"totalttcserv"=>["aff"=>false,"label"=>"Livré TTC","bdd"=>"CMD_TOTALTTCSERV","type"=>"Montant","largeur_col"=>125,"tri"=>true,"cacher-mobile"=>true],
	"totalttcfact"=>["aff"=>true,"label"=>"Facturé TTC","bdd"=>"CMD_TOTALTTCFACT","type"=>"Montant","largeur_col"=>125,"tri"=>true,"cacher-mobile"=>true],
	"supp"=>["aff"=>true,"label"=>"Supprimer","type"=>"Lien","cacher-mobile"=>true]
];
$descT1=["titre"=>"Gestion des commandes","pagination"=>true,"defil"=>true,"largeur"=>"","tri"=>true,"nombre"=>20,"filtre"=>true,"deforder"=>""];
?>
