<?php
/* config fonction mar_maj_piece.php*/
$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Caractéristiques d'une pièce jointe","type"=>"Groupe"],
	"cleparent"=>["aff"=>false,"label"=>"cleparent","bdd"=>"PCE_IDMARCHE","type"=>"Texte"],
	"nomfic"=> ["aff"=>true,"label"=>"Nom du fichier à attacher (8M maxi)","bdd"=>"PCE_NOMFIC","type"=>"Fichier"],
	"libelle"=>["aff"=>true,"label"=>"Libellé","bdd"=>"PCE_LIBELLE","type"=>"Texte","taille"=>250,"ctl"=>"o"],
	"typefic"=>["aff"=>true,"label"=>"Type de fichier","bdd"=>"PCE_TYPEFIC","type"=>"Liste","taille"=>50,"dataliste"=>array("cctp"=>"CCTP","reponse"=>"Réponse")],
	"description"=>["aff"=>true,"label"=>"Description","bdd"=>"PCE_DESCRIPTION","type"=>"Zone texte","taille"=>2000]
];
$descF1=["titre"=>"Gestion des pièces jointes","titresuite"=>true];
?>
