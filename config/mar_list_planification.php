<?php
/* config fonction mar_list_planification.php*/
$champT1=[
	"numero"=>["aff"=>true,"label"=>"Numéro","bdd"=>"MAR_NUMERODGR","type"=>"Lien","tri"=>true],
	"nommarche"=>["aff"=>true,"label"=>"Nom","bdd"=>"MAR_NOMMARCHE","tri"=>true],
	"datecctp"=>["aff"=>true,"label"=>"Date rédaction CCTP","bdd"=>"MAR_DATECCTP"],
	"dateccap"=>["aff"=>true,"label"=>"Date rédaction CCAP","bdd"=>"MAR_DATECCAP"],
	"daterma"=>["aff"=>true,"label"=>"Date validation RMA","bdd"=>"MAR_DATERMA"],
	"datepub"=>["aff"=>true,"label"=>"Date publication","bdd"=>"MAR_DATEPUB"],
	"dateret"=>["aff"=>true,"label"=>"Date retour offres","bdd"=>"MAR_DATERET"],
	"datecb"=>["aff"=>true,"label"=>"Date validation CB","bdd"=>"MAR_DATECB"],
	"datenot"=>["aff"=>true,"label"=>"Date notification","bdd"=>"MAR_DATENOT"],
	"datefin"=>["aff"=>true,"label"=>"Date fin","bdd"=>"MAR_DATEFIN"]
];
$descT1=["titre"=>"Planification des marchés","selectable"=>true,"pagination"=>true,"largeur"=>"","tri"=>true,"nombre"=>20,"filtre"=>false,"deforder"=>""];
?>
