<?php
/* config fonction cmd_maj_ligne-sansuo.php*/
$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Caractéristiques d'un poste de commande","type"=>"Groupe"],
	"cleparent"=>["aff"=>false,"label"=>"cleparent","bdd"=>"LCMD_IDCMD","type"=>"Texte"],
	"poste"=>["aff"=>true,"label"=>"Poste","bdd"=>"LCMD_POSTE","type"=>"Texte","taille"=>250,"ctl"=>"o"],
	"idgcmd"=>["aff"=>true,"label"=>"Groupe de postes","bdd"=>"LCMD_IDGCMD","type"=>"Libre"],
	"marchandise"=>["aff"=>true,"label"=>"Groupe de marchandise","bdd"=>"LCMD_MARCHANDISE","type"=>"Texte","taille"=>100],
	"flagimputation"=>["aff"=>true,"label"=>"Type d'imputation","bdd"=>"LCMD_FLAGIMPUTATION","type"=>"Radio bouton","inline"=>true,"dataliste"=>array("Fonctionnement","Investissement"),"val"=>"F;I"],
	"typeimputation"=>["aff"=>true,"label"=>"Nature de dépense (PAP/RAP)","bdd"=>"LCMD_TYPEIMPUTATION","type"=>"Liste","taille"=>250,
		"dataliste"=>array("AMOA"=>"AMOA","AMOE"=>"AMOE","Prestation de service"=>"Prestation de service","Prestation intellectuelle"=>"Prestation intellectuelle",
		"Achat licence technique"=>"Achat licence technique","Maintenance licence technique"=>"Maintenance licence technique","Achat matériel"=>"Achat matériel","Renouvellement matériel"=>"Renouvellement matériel",
		"Maintenance matériel"=>"Maintenance matériel","Achat licence applicative"=>"Achat licence applicative","Maintenance licence applicative"=>"Maintenance licence applicative",
		"Achat développement"=>"Achat développement","Maintenance développement"=>"Maintenance développement","Abonnement réseau"=>"Abonnement réseau","Logistique"=>"Logistique","RH"=>"RH")],
	"libelle"=>["aff"=>true,"label"=>"Libellé","bdd"=>"LCMD_LIBELLE","type"=>"Texte","taille"=>250],
	"coefredu"=>["aff"=>true,"label"=>"Remise (%)","bdd"=>"LCMD_COEFREDU","type"=>"Montant","taille"=>25],
	"montantrev"=>["aff"=>true,"label"=>"Montant unitaire","bdd"=>"LCMD_MONTANTREV","type"=>"Montant","taille"=>50,"ctl"=>"o"],
	"quantite"=>["aff"=>true,"label"=>"Quantité","bdd"=>"LCMD_QUANTITE","type"=>"Texte","taille"=>50,"ctl"=>"o"],
	"montant"=>["aff"=>true,"label"=>"Montant total HT"],
	"dateliva"=>["aff"=>true,"label"=>"Date de livraison attendue","bdd"=>"LCMD_DATELIVA","type"=>"Date"],
	"dateva"=>["aff"=>true,"label"=>"Date de fin de VA","bdd"=>"LCMD_DATEVA","type"=>"Date"],
	"datevsr"=>["aff"=>true,"label"=>"Date de fin de VSR","bdd"=>"LCMD_DATEVSR","type"=>"Date"],
	"totalht"=>["aff"=>false,"label"=>"","bdd"=>"LCMD_TOTALHT","type"=>"Montant","taille"=>50],
];
$descF1=["titre"=>"Administration des postes de commande","titresuite"=>true];
?>
