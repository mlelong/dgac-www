<?php
/* config fonction adm_maj_notification-groupe.php*/
$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Caractéristiques d'une notification","type"=>"Groupe"],
	"idnotif"=>["aff"=>false,"label"=>"","bdd"=>"GNOT_IDNOTIF","type"=>"Texte"],
	"nomgroupe"=>["aff"=>true,"label"=>"Nom du groupe","bdd"=>"GNOT_NOMGROUPE","type"=>"Texte","taille"=>50,"ctl"=>"o"],
	"idgroupedem"=>["aff"=>true,"label"=>"Groupe demandeur","bdd"=>"GNOT_IDGROUPEDEM","type"=>"Libre","ctl"=>"o"],
	"idgroupeval"=>["aff"=>true,"label"=>"Groupe valideur","bdd"=>"GNOT_IDGROUPEVAL","type"=>"Libre","ctl"=>"o"],
	"description"=>["aff"=>true,"label"=>"Description","bdd"=>"GNOT_DESCRIPTION","type"=>"Zone texte","taille"=>500]
];
$descF1=["titre"=>"Administration des notifications"];
?>
