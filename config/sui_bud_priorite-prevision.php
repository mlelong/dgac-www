<?php
/* config fonction sui_bud_priorite-prevision.php*/

$champFL1=[
	"f_annee"=>["aff"=>true,"label"=>"Année","bdd"=>"BUD_ANNEE","type"=>"Texte"],
	"f_programme"=>["aff"=>true,"label"=>"Programme LOLF","bdd"=>"BUD_PROGRAMME","type"=>"Liste","dataliste"=>array("P612"=>"P612","P613"=>"P613","P614-01"=>"P614-01","P614-02"=>"P614-02")],
];
$champT1=[
	"nature"=>["aff"=>true,"label"=>"Ligne","bdd"=>"BPAP_LIGNEPAP","type"=>"Lien"],
	"total"=>["aff"=>true,"label"=>"AE TOTAL","bdd"=>"","type"=>"Montant"],
	"cumul"=>["aff"=>true,"label"=>"CUMUL","bdd"=>"","type"=>"Montant"]
];
$descT1=["titre"=>"Budget prévisionnel AE par priorité de dépense","titresuite"=>true,"pagination"=>false,"largeur"=>"","nombre"=>20,"filtre"=>true,"deforder"=>""];
?>
