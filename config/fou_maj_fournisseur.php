<?php
/* config fonction fou_maj_fournisseur.php*/
$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Caractéristiques d'un fournisseur","type"=>"Groupe"],
	"numero"=>["aff"=>true,"label"=>"Numéro du fournisseur","bdd"=>"FOU_NUMERO","type"=>"Texte","taille"=>50,"ctl"=>"o"],
	"nomfou"=>["aff"=>true,"label"=>"Nom du fournisseur","bdd"=>"FOU_NOMFOU","type"=>"Texte","taille"=>150,"ctl"=>"o"]
];
$descF1=["titre"=>"Administration des fournisseurs","titresuite"=>"nomfou"];
?>
