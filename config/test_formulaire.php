<?php
/* config fonction test_formulaire.php*/
$champF1=[
	"groupe1"=>["aff"=>true,"label"=>"Test première partie formulaire","type"=>"Groupe"],
	"numreg"=>["aff"=>true,"label"=>"Numéro de la région","type"=>"Texte","taille"=>10,"ctl"=>"o"],
	"nomreg"=>["aff"=>true,"label"=>"Nom de la région","type"=>"Liste","taille"=>50,"dataliste"=>array("picardie"=>"Picardie","alsace"=>"Alsace","bretagne"=>"Bretagne")],
	"datedeb"=>["aff"=>true,"label"=>"Date de début","type"=>"Date","taille"=>50,"ctl"=>"o"],
	"dateheure"=>["aff"=>true,"label"=>"date et heure de début","type"=>"Date/heure","taille"=>50,"ctl"=>"o"],
	"libelle"=>["aff"=>true,"label"=>"Libellé de la région","type"=>"Texte","taille"=>50,"ctl"=>"o"],
	"deuxchamps1"=>["aff"=>true,"label"=>"Deux champ sur la même ligne","type"=>"Texte","taille"=>50,"debinline"=>true],
	"deuxchamps2"=>["aff"=>true,"label"=>"","type"=>"Texte","taille"=>50,"fininline"=>true],
	"flagperm"=>["aff"=>true,"label"=>"Controleur","type"=>"Case à cocher"],
	"casecol1"=>["aff"=>true,"label"=>"Type de contrôle multiple","type"=>"Case à cocher","labelinline"=>"Controleur"],
	"casecol2"=>["aff"=>true,"label"=>"","type"=>"Case à cocher","labelinline"=>"Instructeur"],
	"casecol3"=>["aff"=>true,"label"=>"","type"=>"Case à cocher","labelinline"=>"Elève"],
	"caselig1"=>["aff"=>true,"label"=>"Type de contrôle en ligne","type"=>"Case à cocher","debinline"=>true,"labelinline"=>"Controleur"],
	"caselig2"=>["aff"=>true,"label"=>"","type"=>"Case à cocher","inline"=>true,"labelinline"=>"Instructeur"],
	"caselig3"=>["aff"=>true,"label"=>"","type"=>"Case à cocher","fininline"=>true,"labelinline"=>"Elève"],
	"radiocol"=>["aff"=>true,"label"=>"Type d'export multiple","type"=>"Radio bouton","dataliste"=>array("Excel5","Excel7","csv")],
	"radiolig"=>["aff"=>true,"label"=>"Type d'export en ligne","type"=>"Radio bouton","inline"=>true,"dataliste"=>array("Excel5","Excel7","csv")],
	"description"=>["aff"=>true,"label"=>"Description","type"=>"Zone texte","taille"=>2000],
	"fichier"=>["aff"=>true,"label"=>"Pièces jointes","type"=>"Fichier","taille"=>50,"repertoire"=>"mg","dataliste"=>"Excel5.xls;Excel7.xls;csv.csv;fichier3.doc;fichier4.doc;fichier5.doc"],
	"fichiers"=>["aff"=>true,"label"=>"Pièces jointes","type"=>"Fichiers","taille"=>50,"repertoire"=>"mg","dataliste"=>"Excel5.xls;Excel7.xls;csv.csv;fichier3.doc;fichier4.doc;fichier5.doc"],
	"motpasse"=>["aff"=>true,"label"=>"Mot de passe","type"=>"Password","taille"=>50],
	"groupe2"=>["aff"=>true,"label"=>"Test deuxième partie formulaire","type"=>"Groupe"],
	"libre"=>["aff"=>true,"label"=>"Liste libre","type"=>"Libre","taille"=>50],
	"sortable"=>["aff"=>true,"label"=>"Liste des groupes","type"=>"Sortable","taille"=>20,"largeur"=>"form"]
];
$descF1=["titre"=>"Test formulaire"];
?>
