<?php
/* config fonction cmd_list_groupe-ligne.php*/
$champT1=[
	"libelle"=>["aff"=>true,"label"=>"Libellé du groupe de poste","bdd"=>"GCMD_LIBELLE","type"=>"Lien"],
	"dateliva"=>["aff"=>true,"label"=>"Livraison attendue","bdd"=>"GCMD_DATELIVA"],
	"dateva"=>["aff"=>true,"label"=>"Fin VA","bdd"=>"GCMD_DATEVA"],
	"datevsr"=>["aff"=>true,"label"=>"Fin VSR","bdd"=>"GCMD_DATEVSR"],
	"supp"=>["aff"=>true,"label"=>"Supprimer","type"=>"Lien"]
];
$descT1=["titre"=>"Gestion des groupes de poste","titresuite"=>"titresuite","pagination"=>false,"largeur"=>"","nombre"=>20,"filtre"=>false,"deforder"=>""];
?>
