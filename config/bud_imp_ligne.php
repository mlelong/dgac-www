<?php
/* config fonction bud_imp_ligne.php */

$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Import des lignes budgétaires","type"=>"Groupe"],
	"nomfic"=>["aff"=>true,"label"=>"Nom du fichier à attacher (8M maxi)","type"=>"Fichier"],
	"flagsup"=>["aff"=>true,"label"=>"Suppression avant insertion","type"=>"Case à cocher"]
];
$descF1=["titre"=>"Import lignes budgétaires"];
?>
