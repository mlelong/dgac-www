<?php
/* config fonction cmd_maj_groupe-ligne.php*/
$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Caractéristiques d'un groupe de poste","type"=>"Groupe"],
	"cleparent"=>["aff"=>false,"label"=>"cleparent","bdd"=>"GCMD_IDCMD","type"=>"Texte"],
	"libelle"=>["aff"=>true,"label"=>"Libellé","bdd"=>"GCMD_LIBELLE","type"=>"Texte","taille"=>250,"ctl"=>"o"],
	"dateliva"=>["aff"=>true,"label"=>"Date de livraison attendue","bdd"=>"GCMD_DATELIVA","type"=>"Date"],
	"dateva"=>["aff"=>true,"label"=>"Date de fin de VA","bdd"=>"GCMD_DATEVA","type"=>"Date"],
	"datevsr"=>["aff"=>true,"label"=>"Date de fin de VSR","bdd"=>"GCMD_DATEVSR","type"=>"Date"],
	"typeimputation"=>["aff"=>true,"label"=>"Nature de dépense (PAP/RAP)","bdd"=>"GCMD_TYPEIMPUTATION","type"=>"Liste","taille"=>250,
		"dataliste"=>array("AMOA"=>"AMOA","AMOE"=>"AMOE","Prestation de service"=>"Prestation de service","Prestation intellectuelle"=>"Prestation intellectuelle",
		"Achat licence technique"=>"Achat licence technique","Maintenance licence technique"=>"Maintenance licence technique","Achat matériel"=>"Achat matériel","Renouvellement matériel"=>"Renouvellement matériel",
		"Maintenance matériel"=>"Maintenance matériel","Achat licence applicative"=>"Achat licence applicative","Maintenance licence applicative"=>"Maintenance licence applicative",
		"Achat développement"=>"Achat développement","Maintenance développement"=>"Maintenance développement","Abonnement réseau"=>"Abonnement réseau","Logistique"=>"Logistique","RH"=>"RH")],
];
$descF1=["titre"=>"Administration des groupes de postes","titresuite"=>true];
?>
