<?php
/* config fonction prj_maj_activite-min.php*/
$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Caractéristiques d'une activité mineure","type"=>"Groupe"],
	"type"=>["aff"=>false,"label"=>"","bdd"=>"PRJ_TYPE","type"=>"Texte"],
	"nomprojet"=>["aff"=>true,"label"=>"Nom","bdd"=>"PRJ_NOMPROJET","type"=>"Texte","taille"=>250,"ctl"=>"o"],
	"code"=>["aff"=>true,"label"=>"Code de l'activité","bdd"=>"PRJ_CODE","type"=>"Texte","taille"=>50],
	"idprogramme"=>["aff"=>true,"label"=>"Activité majeure","bdd"=>"PRJ_IDPROGRAMME","type"=>"Libre"],
	"idpole"=>["aff"=>true,"label"=>"Domaine/Pôle","bdd"=>"PRJ_IDPOLE","type"=>"Libre"],
	"libelle"=>["aff"=>true,"label"=>"Description","bdd"=>"PRJ_LIBELLE","type"=>"Texte","taille"=>250]
];
$descF1=["titre"=>"Administration des activités mineures","titresuite"=>"nomprojet"];
?>
