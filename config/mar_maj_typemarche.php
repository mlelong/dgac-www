<?php
/* config fonction mar_maj_typemarche.php*/
$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Caractéristiques d'un type de marché","type"=>"Groupe"],
	"typemarche"=>["aff"=>true,"label"=>"Nom du type de marché","bdd"=>"TMAR_TYPE","type"=>"Texte","taille"=>100,"ctl"=>"o"],
	"libelle"=>["aff"=>true,"label"=>"Libellé","bdd"=>"TMAR_LIBELLE","type"=>"Texte","taille"=>100]
];
$descF1=["titre"=>"Gestion des types de marché"];
?>
