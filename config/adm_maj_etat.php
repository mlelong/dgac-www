<?php
/* config fonction adm_maj_etat.php*/
$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Caractéristiques d'un état","type"=>"Groupe"],
	"cleparent"=>["aff"=>false,"label"=>"cleparent","bdd"=>"ETA_IDFONC","type"=>"Texte"],
	"ordre"=>["aff"=>false,"label"=>"Ordre","bdd"=>"ETA_ORDRE","type"=>"Texte","taille"=>5],
	"codeetat"=>["aff"=>true,"label"=>"Code de l'état","bdd"=>"ETA_CODEETAT","type"=>"Texte","taille"=>10,"ctl"=>"o"],
	"nometat"=>["aff"=>true,"label"=>"Nom de l'état","bdd"=>"ETA_NOMETAT","type"=>"Texte","taille"=>50,"ctl"=>"o"],
	"idphase"=>["aff"=>true,"label"=>"Nom de la phase","bdd"=>"ETA_IDPHASE","type"=>"Libre"],
	"typenotif"=>["aff"=>true,"label"=>"Type d'évènement","bdd"=>"ETA_TYPENOTIF","type"=>"Liste",
	              "dataliste"=>array("Notification"=>"Notification par mail","Validation"=>"Validation")],
	"idnotif"=>["aff"=>true,"label"=>"Nom évenement","bdd"=>"ETA_IDNOTIF","type"=>"Libre"],
	"mail"=>["aff"=>true,"label"=>"Mail pour notification","bdd"=>"ETA_MAIL","type"=>"Zone texte","taille"=>1000]
];
$descF1=["titre"=>"Administration des états"];
?>
