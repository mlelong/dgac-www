<?php
/* config fonction bud_maj_ligne.php*/
$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Caractéristiques d'une ligne de budget","type"=>"Groupe"],
	"cleparent"=>["aff"=>false,"label"=>"cleparent","bdd"=>"BUDL_IDBUDGET","type"=>"Texte"],
	"aeprevt1"=>["aff"=>true,"label"=>"Prévision AE / CP du T1","bdd"=>"BUDL_AEPREVT1","type"=>"Montant","taille"=>25,"debinline"=>true],
	"cpprevt1"=>["aff"=>true,"label"=>"","bdd"=>"BUDL_CPPREVT1","type"=>"Montant","taille"=>25,"fininline"=>true],
	"aeprevt2"=>["aff"=>true,"label"=>"Prévision AE / CP du T2","bdd"=>"BUDL_AEPREVT2","type"=>"Montant","taille"=>25,"debinline"=>true],
	"cpprevt2"=>["aff"=>true,"label"=>"","bdd"=>"BUDL_CPPREVT2","type"=>"Montant","taille"=>25,"fininline"=>true],
	"aeprevt3"=>["aff"=>true,"label"=>"Prévision AE / CP du T3","bdd"=>"BUDL_AEPREVT3","type"=>"Montant","taille"=>25,"debinline"=>true],
	"cpprevt3"=>["aff"=>true,"label"=>"","bdd"=>"BUDL_CPPREVT3","type"=>"Montant","taille"=>25,"fininline"=>true],
	"aeprevt4"=>["aff"=>true,"label"=>"Prévision AE / CP du T4","bdd"=>"BUDL_AEPREVT4","type"=>"Montant","taille"=>25,"debinline"=>true],
	"cpprevt4"=>["aff"=>true,"label"=>"","bdd"=>"BUDL_CPPREVT4","type"=>"Montant","taille"=>25,"fininline"=>true],
	"commentaire"=>["aff"=>true,"label"=>"Commentaire","bdd"=>"BUDL_COMMENTAIRE","type"=>"Zone texte","taille"=>3000]
];
$descF1=["titre"=>"Administration des lignes de budget","titresuite"=>true];
?>
