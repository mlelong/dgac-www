<?php
/* config fonction adm_maj_groupe.php*/
$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Caractéristiques d'un groupe","type"=>"Groupe"],
	"groupe"=>["aff"=>true,"label"=>"Nom du groupe","bdd"=>"GRU_NOMGROUPE","type"=>"Texte","taille"=>50,"ctl"=>"o"],
	"description"=>["aff"=>true,"label"=>"Description","bdd"=>"GRU_DESCRIPTION","type"=>"Zone texte","taille"=>500]
];
$descF1=["titre"=>"Administration des groupes"];
?>
