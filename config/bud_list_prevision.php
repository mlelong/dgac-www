<?php
/* config fonction bud_list_prevision.php*/
$champFL1=[
	"f_nomprogramme"=>["aff"=>true,"label"=>"Programme","bdd"=>"PRG_NOMPROGRAMME","type"=>"Texte"],
	"f_nomprojet"=>["aff"=>true,"label"=>"Projet","bdd"=>"PRJ_NOMPROJET","type"=>"Texte"],
	"f_nompole"=>["aff"=>true,"label"=>"Pôle","bdd"=>"POL_NOMPOLE","type"=>"Texte"],
	"f_nomdomaine"=>["aff"=>true,"label"=>"Domaine","bdd"=>"DOM_NOMDOMAINE","type"=>"Texte"]
];
$champT1=[
	"nomprogramme"=>["aff"=>true,"label"=>"Programme/activité","bdd"=>"PRG_NOMPROGRAMME","type"=>"Lien","tri"=>true],
	"nomprojet"=>["aff"=>true,"label"=>"Projet/activité","bdd"=>"PRJ_NOMPROJET","tri"=>true],
	"libelle"=>["aff"=>true,"label"=>"Libellé","bdd"=>"BUDL_LIBELLE","tri"=>true],
	"nompole"=>["aff"=>true,"label"=>"Nom pôle","bdd"=>"POL_NOMPOLE","tri"=>true],
	"ae"=>["aff"=>true,"label"=>"AE","bdd"=>"BUDL_AE","type"=>"Montant","decimale"=>0,"total"=>true,"cesure"=>false],
	"cumulae"=>["aff"=>true,"label"=>"AE T1-4","bdd"=>"BUDL_CUMULAE","type"=>"Montant","decimale"=>0,"cesure"=>false],
	"cp"=>["aff"=>true,"label"=>"CP","bdd"=>"BUDL_CP","type"=>"Montant","decimale"=>0,"total"=>true,"cesure"=>false],
	"cumulcp"=>["aff"=>true,"label"=>"CP T1-4","bdd"=>"BUDL_CUMULCP","type"=>"Montant","decimale"=>0,"cesure"=>false],
	"aeprevt1"=>["aff"=>true,"label"=>"AE T1","bdd"=>"BUDL_AEPREVT1","type"=>"Montant","total"=>true,"cesure"=>false],
	"cpprevt1"=>["aff"=>true,"label"=>"CP T1","bdd"=>"BUDL_CPPREVT1","type"=>"Montant","total"=>true,"cesure"=>false],
	"aeprevt2"=>["aff"=>true,"label"=>"AE T2","bdd"=>"BUDL_AEPREVT2","type"=>"Montant","total"=>true,"cesure"=>false],
	"cpprevt2"=>["aff"=>true,"label"=>"CP T2","bdd"=>"BUDL_CPPREVT2","type"=>"Montant","total"=>true,"cesure"=>false],
	"aeprevt3"=>["aff"=>true,"label"=>"AE T3","bdd"=>"BUDL_AEPREVT3","type"=>"Montant","total"=>true,"cesure"=>false],
	"cpprevt3"=>["aff"=>true,"label"=>"CP T3","bdd"=>"BUDL_CPPREVT3","type"=>"Montant","total"=>true,"cesure"=>false],
	"aeprevt4"=>["aff"=>true,"label"=>"AE T4","bdd"=>"BUDL_AEPREVT4","type"=>"Montant","total"=>true,"cesure"=>false],
	"cpprevt4"=>["aff"=>true,"label"=>"CP T4","bdd"=>"BUDL_CPPREVT4","type"=>"Montant","total"=>true,"cesure"=>false]
];
$descT1=["titre"=>"Gestion des lignes de budget","titresuite"=>true,"decimale"=>0,"pagination"=>false,"largeur"=>"","tri"=>true,"nombre"=>20,"filtre"=>true,"deforder"=>""];
?>