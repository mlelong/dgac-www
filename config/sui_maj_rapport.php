<?php
/* config fonction sui_maj_rapport.php*/
$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Caractéristiques d'un rapport","type"=>"Groupe"],
	"nomrapport"=>["aff"=>true,"label"=>"Nom du rapport","bdd"=>"RAP_NOMRAPPORT","type"=>"Texte","taille"=>250,"ctl"=>"o"],
	"nomexe"=>["aff"=>true,"label"=>"Nom du fichier à exécuter","bdd"=>"RAP_NOMEXE","type"=>"Texte","taille"=>250,"ctl"=>""],
	"typerapport"=>["aff"=>true,"label"=>"Type de rapport","bdd"=>"RAP_TYPERAPPORT","type"=>"Liste","dataliste"=>array("Suivi"=>"Suivi","Alerte"=>"Alerte")],
	"description"=>["aff"=>true,"label"=>"Libellé du rapport","bdd"=>"RAP_DESCRIPTION","type"=>"Zone texte","taille"=>500],
	"cfgfilter"=>["aff"=>true,"label"=>"Configuration du filtre","bdd"=>"RAP_CFGFILTER","type"=>"Zone texte","taille"=>1000],
	"cfgtableau"=>["aff"=>true,"label"=>"Configuration des colonnes du tableau","bdd"=>"RAP_CFGTABLEAU","type"=>"Zone texte","taille"=>3000],
	"cfgdesc"=>["aff"=>true,"label"=>"Configuration de la description du tableau","bdd"=>"RAP_CFGDESC","type"=>"Zone texte","taille"=>250],
	"requete"=>["aff"=>true,"label"=>"Requête","bdd"=>"RAP_REQUETE","type"=>"Zone texte","taille"=>1000],
	"lien"=>["aff"=>true,"label"=>"Configuration des liens","bdd"=>"RAP_LIEN","type"=>"Zone texte","taille"=>500]
];
$descF1=["titre"=>"Administration des rapports"];
?>