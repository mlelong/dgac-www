<?php
/* config fonction mar_list_uo.php*/
$champT1=[
	"nomuo"=>["aff"=>true,"label"=>"N°UO/Poste","bdd"=>"MUO_NOMUO","type"=>"Lien","tri"=>true],
	"montant"=>["aff"=>true,"label"=>"Montant HT","bdd"=>"MUO_MONTANT","type"=>"Montant","tri"=>true,"cesure"=>false],
	"flagdsi"=>["aff"=>true,"label"=>"Utilisé par la DSI","bdd"=>"MUO_FLAGDSI","tri"=>true],
	"flagrevision"=>["aff"=>true,"label"=>"Révision non applicable","bdd"=>"MUO_FLAGREVISION","tri"=>true],
	"marchandise"=>["aff"=>true,"label"=>"Groupe Marchandise","bdd"=>"MUO_MARCHANDISE","tri"=>true],
	"flagimputation"=>["aff"=>true,"label"=>"Imputation","bdd"=>"MUO_FLAGIMPUTATION","tri"=>true],
	"supp"=>["aff"=>true,"label"=>"Supprimer","type"=>"Lien"]
];
$descT1=["titre"=>"Unités d'oeuvre pour un marché","titresuite"=>true,"pagination"=>false,"largeur"=>"","tri"=>true,"nombre"=>20,"filtre"=>false,"deforder"=>""];
?>
