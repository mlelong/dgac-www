<?php
/* config fonction adm_list_utilisateur.php*/
$champFL1=[
	"f_groupe"=>["aff"=>true,"label"=>"Nom du groupe","type"=>"Texte","requete"=>false],
	"f_nom"=>["aff"=>true,"label"=>"Nom de l'utilisateur","bdd"=>"UTI_NOM","type"=>"Texte"]
];
$champT1=[
	"nom"=>["aff"=>true,"label"=>"Nom","bdd"=>"UTI_NOM","type"=>"Lien"],
	"prenom"=>["aff"=>true,"label"=>"Prénom","bdd"=>"UTI_PRENOM"],
	"etat"=>["aff"=>true,"label"=>"Etat","bdd"=>"UTI_ETAT"],
	"groupe"=>["aff"=>true,"label"=>"Groupes de l'utilisateur","type"=>"Lien"],
	"profil"=>["aff"=>true,"label"=>"Profils de l'utilisateur","type"=>"Lien"],
	"supp"=>["aff"=>true,"label"=>"Supprimer","type"=>"Lien"]
];
$descT1=["titre"=>"Gestion des utilisateurs","pagination"=>false,"largeur"=>"","nombre"=>20,"filtre"=>true,"deforder"=>""];
?>
