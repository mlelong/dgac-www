<?php
/* config fonction sui_alert_budget.php */
 
$champT1=[
	"Date"=>["aff"=>true,"label"=>"Date du contrôle","bdd"=>"BAT_DATEBATCH"],
	"Message"=>["aff"=>true,"label"=>"Message","bdd"=>"BAT_MESSAGE"]
];
$descT1=["titre"=>"Contrôle de cohérence budget","pagination"=>false,"largeur"=>"","tri"=>false,"nombre"=>20,"filtre"=>false,"deforder"=>""];
$reqT1="select DATE_FORMAT(BAT_DATEBATCH, '%Y-%m-%d %H:%i') as BAT_DATEBATCH, BAT_MESSAGE 	
		from trace_batch where BAT_NOMBATCH=\"contrôle budget\" 
		and DATE_FORMAT(BAT_DATEBATCH, '%Y-%m-%d')=(select max(DATE_FORMAT(BAT_DATEBATCH, '%Y-%m-%d')) from trace_batch where BAT_NOMBATCH=\"contrôle budget\")
		";
$orderT1="";
?>