<?php
/* config fonction sui_bud_programme.php*/
$champFL1=[
	"f_annee"=>["aff"=>true,"label"=>"Année","bdd"=>"BUD_ANNEE","type"=>"Texte"],
	"f_programme"=>["aff"=>true,"label"=>"Programme LOLF","bdd"=>"BUD_PROGRAMME","type"=>"Liste","dataliste"=>array("P612"=>"P612","P613"=>"P613","P614-01"=>"P614-01","P614-02"=>"P614-02")]
];
$champT1=[
	"libelle"=>["aff"=>true,"label"=>"Répartition","bdd"=>"BUD_LIBELLE","type"=>"Texte","taille"=>false,"tri"=>false],
	"aedota"=>["aff"=>true,"label"=>"AE alloués","bdd"=>"BUD_AEDOTA","type"=>"Montant","taille"=>false,"tri"=>false,"total"=>true],
	"aedebl"=>["aff"=>true,"label"=>"AE débloqués","bdd"=>"BUD_AEDEBL","type"=>"Montant","taille"=>false,"tri"=>false,"total"=>true],
	"aeprev"=>["aff"=>true,"label"=>"AE prévisionnels","bdd"=>"BUD_AEPREV","type"=>"Montant","taille"=>false,"tri"=>false,"total"=>true],
	"aecons"=>["aff"=>true,"label"=>"AE consommés","bdd"=>"BUD_AECONS","type"=>"Montant","taille"=>false,"tri"=>false,"total"=>true],
	"cpdota"=>["aff"=>true,"label"=>"CP alloués","bdd"=>"BUD_CPDOTA","type"=>"Montant","taille"=>false,"tri"=>false,"total"=>true],
	"cpdebl"=>["aff"=>true,"label"=>"CP débloqués","bdd"=>"BUD_CPDEBL","type"=>"Montant","taille"=>false,"tri"=>false,"total"=>true],
	"cpprev"=>["aff"=>true,"label"=>"CP prévisionnels","bdd"=>"BUD_CPPREV","type"=>"Montant","taille"=>false,"tri"=>false,"total"=>true],
	"cpcons"=>["aff"=>true,"label"=>"CP consommés","bdd"=>"BUD_CPCONS","type"=>"Montant","taille"=>false,"tri"=>false,"total"=>true]
];
$descT1=["titre"=>"Budget de la DSI","pagination"=>false,"largeur"=>"","tri"=>true,"nombre"=>20,"filtre"=>true,"deforder"=>""];
