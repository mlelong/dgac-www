<?php
/* config fonction dom_list_domaine.php*/
$champT1=[
	"nomdomaine"=>["aff"=>true,"label"=>"Code du domaine","bdd"=>"DOM_NOMDOMAINE","type"=>"Lien"],
	"direction"=>["aff"=>true,"label"=>"Code de la direction","bdd"=>"DOM_DIRECTION"],
	"responsable"=>["aff"=>true,"label"=>"Responsables du domaine","type"=>"Lien"],
	"libelle"=>["aff"=>true,"label"=>"Nom du domaine","bdd"=>"DOM_LIBELLE"],
	"supp"=>["aff"=>true,"label"=>"Supprimer","type"=>"Lien"]
];
$descT1=["titre"=>"Gestion des domaines","pagination"=>false,"largeur"=>"","nombre"=>20,"filtre"=>false,"deforder"=>""];
?>
