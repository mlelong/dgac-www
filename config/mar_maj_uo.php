<?php
/* config fonction mar_maj_uo.php*/
$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Caractéristiques d'une unité d'oeuvre","type"=>"Groupe"],
	"cleparent"=>["aff"=>false,"label"=>"cleparent","bdd"=>"MUO_IDMARCHE","type"=>"Texte"],
	"idprestation"=>["aff"=>true,"label"=>"Prestation","bdd"=>"MUO_IDPRESTATION","type"=>"Libre"],
	"nomuo"=>["aff"=>true,"label"=>"N°UO/Poste","bdd"=>"MUO_NOMUO","type"=>"Texte","taille"=>250,"ctl"=>"o"],
	"refexterne"=>["aff"=>true,"label"=>"Référence externe","bdd"=>"MUO_REFEXTERNE","type"=>"Texte","taille"=>50,"ctl"=>""],
	"libposte"=>["aff"=>true,"label"=>"Libellé du poste SIF (40 car. maxi)","bdd"=>"MUO_LIBPOSTE","type"=>"Texte","taille"=>40],
	"frequence"=>["aff"=>true,"label"=>"Fréquence de la dépense","bdd"=>"MUO_FREQUENCE","type"=>"Liste","taille"=>50,"dataliste"=>array("forfaitaire"=>"forfaitaire","unitaire"=>"unitaire","gratuite"=>"gratuite")],
	"montant"=>["aff"=>true,"label"=>"Montant HT","bdd"=>"MUO_MONTANT","type"=>"Montant","taille"=>50],
	"mcosla1"=>["aff"=>true,"label"=>"Support/SLA associé (niveau 1 2 3 4)","type"=>"Montant","taille"=>25,"debinline"=>true],
	"mcosla2"=>["aff"=>true,"label"=>"","type"=>"Montant","taille"=>25,"fininline"=>true],
	"mcosla3"=>["aff"=>true,"label"=>"","type"=>"Montant","taille"=>25,"debinline"=>true],
	"mcosla4"=>["aff"=>true,"label"=>"","type"=>"Montant","taille"=>25,"fininline"=>true],
	"marchandise"=>["aff"=>true,"label"=>"Groupe de marchandise","bdd"=>"MUO_MARCHANDISE","type"=>"Texte","taille"=>100],
	"flagimputation"=>["aff"=>true,"label"=>"Type d'imputation","bdd"=>"MUO_FLAGIMPUTATION","type"=>"Radio bouton","inline"=>true,"dataliste"=>array("Fonctionnement","Investissement"),"val"=>"F;I"],
	"flagrevision"=>["aff"=>true,"label"=>"Révision de prix non applicable","bdd"=>"MUO_FLAGREVISION","type"=>"Case à cocher"],
	"flagdsi"=>["aff"=>true,"label"=>"Utilisé par la DSI","bdd"=>"MUO_FLAGDSI","type"=>"Case à cocher"],
	"description"=>["aff"=>true,"label"=>"Description","bdd"=>"MUO_DESCRIPTION","type"=>"Zone texte","taille"=>1000],
];
$descF1=["titre"=>"Gestion des unités d'oeuvre","titresuite"=>true];
?>
