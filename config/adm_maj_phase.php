<?php
/* config fonction adm_maj_phase.php*/
$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Caractéristiques d'une phase","type"=>"Groupe"],
	"cleparent"=>["aff"=>false,"label"=>"cleparent","bdd"=>"PHA_IDFONC","type"=>"Texte"],
	"ordre"=>["aff"=>false,"label"=>"Ordre","bdd"=>"PHA_ORDRE","type"=>"Texte","taille"=>5],
	"codephase"=>["aff"=>true,"label"=>"Code de la phase","bdd"=>"PHA_CODEPHASE","type"=>"Texte","taille"=>5,"ctl"=>"o"],
	"nomphase"=>["aff"=>true,"label"=>"Nom de la phase","bdd"=>"PHA_NOMPHASE","type"=>"Texte","taille"=>50,"ctl"=>"o"]
];
$descF1=["titre"=>"Administration des phases"];
?>
