<?php
/* config fonction sui_rap_paiements-en-cours.php */
$champFL1=[
	"f_nompole"=>["aff"=>true,"label"=>"Nom du pôle","bdd"=>"POL_NOMPOLE","type"=>"Texte"],
	"f_nommarche"=>["aff"=>true,"label"=>"Nom du pôle","bdd"=>"MAR_NOMMARCHE","type"=>"Texte"]
];
$champT1=[
	"numero"=>["aff"=>true,"label"=>"Numéro","bdd"=>"CMD_NUMERO","type"=>"Lien","tri"=>true],
	"nompole"=>["aff"=>true,"label"=>"Nom pôle","bdd"=>"POL_NOMPOLE","tri"=>true],
	"nommarche"=>["aff"=>true,"label"=>"Nom marché","bdd"=>"MAR_NOMMARCHE","tri"=>true],
	"nomprojet"=>["aff"=>true,"label"=>"Nom projet","bdd"=>"PRJ_NOMPROJET","tri"=>true],
	"etat"=>["aff"=>true,"label"=>"Etat","bdd"=>"PCMD_ETAT","tri"=>true],
	"datepaie"=>["aff"=>true,"label"=>"Date DP SIF","bdd"=>"PCMD_DATEPAIE","tri"=>true],
	"totalttc"=>["aff"=>true,"label"=>"Total TTC","bdd"=>"PCMD_TOTALTTC","type"=>"Montant","tri"=>true,"total"=>true]
];
$descT1=["titre"=>"Commandes en cours","pagination"=>false,"largeur"=>"","tri"=>true,"nombre"=>20,"filtre"=>true,"deforder"=>""];
$reqT1="select CMD_CLE, CMD_NUMERO, POL_NOMPOLE, MAR_NOMMARCHE, PRJ_NOMPROJET, 
			DATE_FORMAT(PCMD_DATEPAIE, '%d-%m-%Y') AS PCMD_DATEPAIE,
			PCMD_ETAT, PCMD_TOTALTTC
			from commande_paiement 
			left join commande on PCMD_IDCMD=CMD_CLE
			left join pole on CMD_IDPOLE=POL_CLE
			left join marche on CMD_IDMARCHE=MAR_CLE
			left join projet on CMD_IDPROJET=PRJ_CLE
			where 1 ";
$orderT1="order by CMD_NUMERO desc";
$lienT1=array(
	array("numero","cmd_tab_commande.php","?typeaction=modification&cle=#CMD_CLE#",'')
);
?>