<?php
/* config fonction prj_list_projet.php*/
$champFL1=[
	"f_nomprojet"=>["aff"=>true,"label"=>"Projet","bdd"=>"PRJ_NOMPROJET","type"=>"Texte"],
	"f_nomprogramme"=>["aff"=>true,"label"=>"Programme","bdd"=>"PRG_NOMPROGRAMME","type"=>"Texte"],
	"f_code"=>["aff"=>true,"label"=>"Code projet","bdd"=>"PRJ_CODE","type"=>"Texte"],
	"f_nompole"=>["aff"=>true,"label"=>"Pôle","bdd"=>"POL_NOMPOLE","type"=>"Texte"]
];
$champT1=[
	"nomprojet"=>["aff"=>true,"label"=>"Nom projet","bdd"=>"PRJ_NOMPROJET","type"=>"Lien","tri"=>true],
	"nomprogramme"=>["aff"=>true,"label"=>"Nom programme","bdd"=>"PRG_NOMPROGRAMME","tri"=>true],
	"code"=>["aff"=>true,"label"=>"Code","bdd"=>"PRJ_CODE","tri"=>true],
	"nompole"=>["aff"=>true,"label"=>"Domaine/pôle","bdd"=>"POL_NOMPOLE","tri"=>true],
	"nb_cmd"=>["aff"=>true,"label"=>"Nb commandes","bdd"=>"NB_CMD"],
	"libelle"=>["aff"=>true,"label"=>"Description","bdd"=>"PRJ_LIBELLE"],
	"supp"=>["aff"=>true,"label"=>"Supprimer","type"=>"Lien"]
];
$descT1=["titre"=>"Gestion des projets","pagination"=>false,"largeur"=>"","tri"=>true,"nombre"=>20,"filtre"=>true,"deforder"=>""];
?>
