<?php
/* config fonction sui_bud_commande.php*/
$champFL1=[
	"f_annee"=>["aff"=>true,"label"=>"Année","type"=>"Texte"],
	"f_programme"=>["aff"=>true,"label"=>"Programme LOLF","bdd"=>"CMD_PROGRAMME","type"=>"Liste","dataliste"=>array("P612"=>"P612","P613"=>"P613","P614-01"=>"P614-01","P614-02"=>"P614-02")],
	"f_nomprogramme"=>["aff"=>true,"label"=>"Programme","bdd"=>"PRG_NOMPROGRAMME","type"=>"Texte"],
	"f_nomprojet"=>["aff"=>true,"label"=>"Projet","bdd"=>"PRJ_NOMPROJET","type"=>"Texte"],
	"f_nomdomaine"=>["aff"=>true,"label"=>"Domaine","bdd"=>"DOM_NOMDOMAINE","type"=>"Texte"],
	"f_nompole"=>["aff"=>true,"label"=>"Pôle","bdd"=>"POL_NOMPOLE","type"=>"Texte"]
];
$champT1=[
	"numero"=>["aff"=>true,"label"=>"Numéro","bdd"=>"CMD_NUMERO","tri"=>true],
	"datesou"=>["aff"=>true,"label"=>"Soumission","bdd"=>"CMD_DATESOU","tri"=>true],
	"nomprogramme"=>["aff"=>true,"label"=>"Programme/activité","bdd"=>"PRG_NOMPROGRAMME","tri"=>true],
	"nomprojet"=>["aff"=>true,"label"=>"Projet/activité","bdd"=>"PRJ_NOMPROJET","tri"=>true],
	"nomdomaine"=>["aff"=>true,"label"=>"Nom domaine","bdd"=>"DOM_NOMDOMAINE","tri"=>true],
	"nompole"=>["aff"=>true,"label"=>"Nom pôle","bdd"=>"POL_NOMPOLE","tri"=>true],
	"objet"=>["aff"=>true,"label"=>"Objet","bdd"=>"CMD_OBJET","tri"=>true,"largeur_col"=>450],
	"totalcmdae"=>["aff"=>true,"label"=>"AE","bdd"=>"CMD_TOTALTTC","type"=>"Montant","total"=>true,"largeur_col"=>100],
	"totalcmdcp"=>["aff"=>true,"label"=>"CP","bdd"=>"CMD_TOTALTTCFACT","type"=>"Montant","total"=>true,"largeur_col"=>100]
];
$descT1=["titre"=>"Budget par commande","titresuite"=>true,"decimale"=>0,"pagination"=>false,"largeur"=>"","tri"=>true,"nombre"=>20,"filtre"=>true,"deforder"=>""];
