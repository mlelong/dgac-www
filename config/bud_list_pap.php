<?php
/* config fonction bud_list_pap.php*/
$champFL1=[
	"f_lignepap"=>["aff"=>true,"label"=>"Ligne","bdd"=>"BPAP_LIGNEPAP","type"=>"Texte"]
];
$champT1=[
	"lignepap"=>["aff"=>true,"label"=>"Ligne","bdd"=>"BPAP_LIGNEPAP","type"=>"Lien"],
	"aepap"=>["aff"=>true,"label"=>"AE PAP","bdd"=>"","ligne_style"=>"text-align:right"],
	"cppap"=>["aff"=>true,"label"=>"CP PAP","bdd"=>"","ligne_style"=>"text-align:right"],
	"aebudget"=>["aff"=>true,"label"=>"AE BUDGET","bdd"=>"","ligne_style"=>"text-align:right"],
	"cpbudget"=>["aff"=>true,"label"=>"CP BUDGET","bdd"=>"","ligne_style"=>"text-align:right"],
	"supp"=>["aff"=>true,"label"=>"Supprimer","type"=>"Lien"]
];
$descT1=["titre"=>"Gestion des lignes du PAP","titresuite"=>true,"pagination"=>false,"largeur"=>"","nombre"=>20,"filtre"=>true,"deforder"=>""];
?>
