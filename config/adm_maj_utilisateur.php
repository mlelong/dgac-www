<?php
/* config fonction adm_maj_utilisateur.php*/
$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Caractéristiques d'un utilisateur","type"=>"Groupe"],
	"nom"=>["aff"=>true,"label"=>"Nom de l'agent","bdd"=>"UTI_NOM","type"=>"Texte","taille"=>50,"ctl"=>"o"],
	"prenom"=>["aff"=>true,"label"=>"Prénom de l'agent","bdd"=>"UTI_PRENOM","type"=>"Texte","taille"=>50,"ctl"=>"o"],
	"civilite"=>["aff"=>true,"label"=>"Civilité","bdd"=>"UTI_CIVILITE","type"=>"Liste","dataliste"=>array("Monsieur"=>"Monsieur","Madame"=>"Madame")],
	"identifiant"=>["aff"=>true,"label"=>"Identifiant de connexion","bdd"=>"UTI_IDENTIFIANT","type"=>"Texte","taille"=>50,"ctl"=>"o"],
	"pwd"=>["aff"=>true,"label"=>"Mot de passe","bdd"=>"UTI_PWD","type"=>"Password","taille"=>50,"ctl"=>"o"],
	"etat"=>["aff"=>true,"label"=>"Etat","bdd"=>"UTI_ETAT","type"=>"Liste","ctl"=>"o","dataliste"=>array("Nouveau"=>"Nouveau","Actif"=>"Actif")],
	"adresse"=>["aff"=>true,"label"=>"Adresse","bdd"=>"UTI_ADRESSE","type"=>"Texte","taille"=>250],
	"commune"=>["aff"=>true,"label"=>"Commune","bdd"=>"UTI_COMMUNE","type"=>"Texte","taille"=>50],
	"departement"=>["aff"=>true,"label"=>"Département","bdd"=>"UTI_DEPARTEMENT","type"=>"Texte","taille"=>50],
	"codepostal"=>["aff"=>true,"label"=>"Code postal","bdd"=>"UTI_CODEPOSTAL","type"=>"Texte","taille"=>10],
	"telephone"=>["aff"=>true,"label"=>"Téléphone","bdd"=>"UTI_TELEPHONE","type"=>"Texte","taille"=>50],
	"portable"=>["aff"=>true,"label"=>"Portable","bdd"=>"UTI_PORTABLE","type"=>"Texte","taille"=>50],
	"fax"=>["aff"=>true,"label"=>"Fax","bdd"=>"UTI_FAX","type"=>"Texte","taille"=>50],
	"mail"=>["aff"=>true,"label"=>"Mail","bdd"=>"UTI_MAIL","type"=>"Texte","taille"=>250],
	"dateinscription"=>["aff"=>true,"label"=>"Date d'inscription","bdd"=>"UTI_DATEINSCRIPTION","type"=>"Date"],
	"datesuspension"=>["aff"=>true,"label"=>"Date de suspension","bdd"=>"UTI_DATESUSPENSION","type"=>"Date"],
	"datefin"=>["aff"=>true,"label"=>"Date de fin","bdd"=>"UTI_DATEFIN","type"=>"Date"],
	"description"=>["aff"=>true,"label"=>"Description","bdd"=>"UTI_DESCRIPTION","type"=>"Zone texte","taille"=>500]
];
$descF1=["titre"=>"Administration des utilisateurs"];
?>
