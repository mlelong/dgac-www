<?php
/* config fonction mar_list_prestation.php*/
$champT1=[
	"nomprestation"=>["aff"=>true,"label"=>"Nom de la prestation","bdd"=>"MPRE_NOMPRESTATION","type"=>"Lien"],
	"libelle"=>["aff"=>true,"label"=>"Libellé de la prestation","bdd"=>"MPRE_LIBELLE","type"=>"Texte"],
	"supp"=>["aff"=>true,"label"=>"Supprimer","type"=>"Lien"]
];
$descT1=["titre"=>"Gestion des prestations de marché","titresuite"=>"titresuite","pagination"=>false,"largeur"=>"","nombre"=>20,"filtre"=>false,"deforder"=>""];
?>
