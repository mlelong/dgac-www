<?php
/* config fonction sui_rap_budget-projet.php */
$champFL1=[
	"f_annee"=>["aff"=>true,"label"=>"Année","bdd"=>"BUD_ANNEE","type"=>"Texte"],
	"f_programme"=>["aff"=>true,"label"=>"Programme LOLF","bdd"=>"BUD_PROGRAMME","type"=>"Liste","dataliste"=>array("P612"=>"P612","P613"=>"P613","P614-01"=>"P614-01","P614-02"=>"P614-02")],
	"f_nomprojet"=>["aff"=>true,"label"=>"Projet","bdd"=>"PRJ_NOMPROJET","type"=>"Texte"],
	"f_nompole"=>["aff"=>true,"label"=>"Pôle","bdd"=>"POL_NOMPOLE","type"=>"Texte"],
	"f_nomdomaine"=>["aff"=>true,"label"=>"Domaine","bdd"=>"DOM_NOMDOMAINE","type"=>"Texte"]
];
$champT1=[
	"nomdomaine"=>["aff"=>true,"label"=>"Nom marché","bdd"=>"DOM_NOMDOMAINE","tri"=>true],
	"nompole"=>["aff"=>true,"label"=>"Nom pôle","bdd"=>"POL_NOMPOLE","tri"=>true],
	"nomprojet"=>["aff"=>true,"label"=>"Nom projet","bdd"=>"PRJ_NOMPROJET","tri"=>true],
	"aeprevfonct"=>["aff"=>true,"label"=>"Prévisionnel AE FON","bdd"=>"BUDL_AEPREVFONCT","type"=>"Montant","total"=>true],
	"aeprevinves"=>["aff"=>true,"label"=>"Prévisionnel AE INV","bdd"=>"BUDL_AEPREVINVES","type"=>"Montant","total"=>true],
	"cpprevfonct"=>["aff"=>true,"label"=>"Prévisionnel CP FON","bdd"=>"BUDL_CPPREVFONCT","type"=>"Montant","total"=>true],
	"cpprevinves"=>["aff"=>true,"label"=>"Prévisionnel CP INV","bdd"=>"BUDL_CPPREVINVES","type"=>"Montant","total"=>true],
	"aeconsfonct"=>["aff"=>true,"label"=>"Consommé AE FON","bdd"=>"BUDL_AECONSFONCT","type"=>"Montant","total"=>true],
	"aeconsinves"=>["aff"=>true,"label"=>"Consommé AE INV","bdd"=>"BUDL_AECONSINVES","type"=>"Montant","total"=>true],
	"cpconsfonct"=>["aff"=>true,"label"=>"Consommé CP FON","bdd"=>"BUDL_CPCONSFONCT","type"=>"Montant","total"=>true],
	"cpconsinves"=>["aff"=>true,"label"=>"Consommé CP INV","bdd"=>"BUDL_CPCONSINVES","type"=>"Montant","total"=>true],
];
$descT1=["titre"=>"Commandes en cours","pagination"=>false,"largeur"=>"","tri"=>true,"nombre"=>20,"filtre"=>true,"deforder"=>""];
$reqT1="select BUDL_CLE, PRJ_NOMPROJET,  
		BUDL_AEPREVFONCT, BUDL_AEPREVINVES, BUDL_CPPREVFONCT, BUDL_CPPREVINVES,
		BUDL_AECONSFONCT, BUDL_AECONSINVES,	BUDL_CPCONSFONCT, BUDL_CPCONSINVES,
		POL_NOMPOLE, DOM_NOMDOMAINE
		from budget_ligne 
		left join budget on BUDL_IDBUDGET=BUD_CLE
		left join projet on BUDL_IDPROJET=PRJ_CLE
		left join pole on PRJ_IDPOLE=POL_CLE 
		left join domaine on POL_IDDOMAINE=DOM_CLE 
		where BUD_ANNEE=\"f_annee\" and BUD_PROGRAMME=\"f_programme\" order by PRJ_NOMPROJET";
$orderT1="";
?>