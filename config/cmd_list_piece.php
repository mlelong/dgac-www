<?php
/* config fonction cmd_list_piece.php*/
$champT1=[
	"nomfic"=>["aff"=>true,"label"=>"Document","bdd"=>"PCE_NOMFIC","type"=>"Lien"],
	"typefic"=>["aff"=>true,"label"=>"Type de document","bdd"=>"PCE_TYPEFIC"],
	"libelle"=>["aff"=>true,"label"=>"Libellé","bdd"=>"PCE_LIBELLE"],
	"telecharge"=>["aff"=>true,"label"=>"Télécharger","type"=>"Lien"],
	"supp"=>["aff"=>true,"label"=>"Supprimer","type"=>"Lien"]
];
$descT1=["titre"=>"Pièces jointes pour une commande","titresuite"=>true,"pagination"=>false,"largeur"=>"","nombre"=>20,"filtre"=>false,"deforder"=>""];
?>
