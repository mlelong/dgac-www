<?php
/* config fonction cmd_maj_groupe-ligne.php*/
$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Caractéristiques d'une prestation de marché","type"=>"Groupe"],
	"cleparent"=>["aff"=>false,"label"=>"cleparent","bdd"=>"MPRE_IDMARCHE","type"=>"Texte"],
	"nomprestation"=>["aff"=>true,"label"=>"Nom de la prestation","bdd"=>"MPRE_NOMPRESTATION","type"=>"Texte","taille"=>50,"ctl"=>"o"],
	"libelle"=>["aff"=>true,"label"=>"Libellé","bdd"=>"MPRE_LIBELLE","type"=>"Texte","taille"=>250,"ctl"=>""],
];
$descF1=["titre"=>"Administration des prestations de marché","titresuite"=>true];
?>
