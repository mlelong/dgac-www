<?php
/* config fonction prj_imp_projet.php */

$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Import des projets/activités","type"=>"Groupe"],
	"nomfic"=>["aff"=>true,"label"=>"Nom du fichier à attacher (8M maxi)","type"=>"Fichier"],
	"flagsup"=>["aff"=>true,"label"=>"Suppression avant insertion","type"=>"Case à cocher"]
];
$descF1=["titre"=>"Import projets/activités"];
?>
