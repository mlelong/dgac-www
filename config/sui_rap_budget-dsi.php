<?php
/* config fonction sui_rap_budget-dsi.php */
$champFL1=[
	"f_annee"=>["aff"=>true,"label"=>"Année","bdd"=>"BUD_ANNEE","type"=>"Texte"]
];
 
$champT1=[
	"libelle"=>["aff"=>true,"label"=>"Répartition","bdd"=>"BUD_LIBELLE"],
	"aedota"=>["aff"=>true,"label"=>"AE alloués","bdd"=>"BUD_AEDOTA","type"=>"Montant","total"=>true],
	"aedebl"=>["aff"=>true,"label"=>"AE débloqués","bdd"=>"BUD_AEDEBL","type"=>"Montant","total"=>true],
	"aeprev"=>["aff"=>true,"label"=>"AE prévisionnels","bdd"=>"BUD_AEPREV","type"=>"Montant","total"=>true],
	"aecons"=>["aff"=>true,"label"=>"AE consommés","bdd"=>"BUD_AECONS","type"=>"Montant","total"=>true],
	"cpdota"=>["aff"=>true,"label"=>"CP alloués","bdd"=>"BUD_CPDOTA","type"=>"Montant","total"=>true],
	"cpdebl"=>["aff"=>true,"label"=>"CP débloqués","bdd"=>"BUD_CPDEBL","type"=>"Montant","total"=>true],
	"cpprev"=>["aff"=>true,"label"=>"CP prévisionnels","bdd"=>"BUD_CPPREV","type"=>"Montant","total"=>true],
	"cpcons"=>["aff"=>true,"label"=>"CP consommés","bdd"=>"BUD_CPCONS","type"=>"Montant","total"=>true]
];
$descT1=["titre"=>"Budget de la DSI","pagination"=>false,"largeur"=>"","tri"=>true,"nombre"=>20,"filtre"=>true,"deforder"=>""];
$reqT1="select \"Fonctionnement\" as  BUD_LIBELLE,
		BUD_AEDOTAFONCT as BUD_AEDOTA, BUD_AEDEBLFONCT as BUD_AEDEBL, BUD_AEPREVFONCT as BUD_AEPREV, BUD_AECONSFONCT as BUD_AECONS,
		BUD_CPDOTAFONCT as BUD_CPDOTA, BUD_CPDEBLFONCT as BUD_CPDEBL, BUD_CPPREVFONCT as BUD_CPPREV, BUD_CPCONSFONCT as BUD_CPCONS
		from budget where BUD_ANNEE=\"f_annee\"
		union
		select \"Investissement\" as  BUD_LIBELLE,	
		BUD_AEDOTAINVES as BUD_AEDOTA, BUD_AEDEBLINVES as BUD_AEDEBL, BUD_AEPREVINVES as BUD_AEPREV, BUD_AECONSINVES as BUD_AECONS,
		BUD_CPDOTAINVES as BUD_CPDOTA, BUD_CPDEBLINVES as BUD_CPDEBL, BUD_CPPREVINVES as BUD_CPPREV, BUD_CPCONSINVES as BUD_CPCONS
		from budget where BUD_ANNEE=\"f_annee\"";
$orderT1="";
?>