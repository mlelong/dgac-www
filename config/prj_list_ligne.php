<?php
/* config fonction prj_list_ligne.php*/
$champFL1=[
	"f_lignebud"=>["aff"=>true,"label"=>"Ligne","bdd"=>"BUDL_LIGNE","type"=>"Texte"]
];
$champT1=[
	"lignebud"=>["aff"=>true,"label"=>"Ligne","bdd"=>"BUDL_LIGNE","type"=>"Lien"],
	"aefonct"=>["aff"=>true,"label"=>"AE fonctionnement","bdd"=>"BUDL_AEPREVFONCT"],
	"cpfonct"=>["aff"=>true,"label"=>"CP fonctionnement","bdd"=>"BUDL_CPPREVFONCT"],
	"aeinves"=>["aff"=>true,"label"=>"AE investissement","bdd"=>"BUDL_AEPREVINVES"],
	"cpinves"=>["aff"=>true,"label"=>"CP investissement","bdd"=>"BUDL_CPPREVINVES"],
	"supp"=>["aff"=>true,"label"=>"Supprimer","type"=>"Lien"]
];
$descT1=["titre"=>"Gestion des lignes de budget","titresuite"=>true,"pagination"=>true,"largeur"=>"","nombre"=>20,"filtre"=>true,"deforder"=>""];
?>
