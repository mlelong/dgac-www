<?php
/* config fonction cmd_maj_commande-dgr.php*/
$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Caractéristiques d'une commande","type"=>"Groupe"],
	"numero"=>["aff"=>true,"label"=>"Numéro commande","bdd"=>"CMD_NUMERO","type"=>"Texte","taille"=>50,"ctl"=>"o"],
	"reffournisseur"=>["aff"=>true,"label"=>"Référence fournisseur","bdd"=>"CMD_REFFOURNISSEUR","type"=>"Texte","taille"=>50],
	"idetat"=>["aff"=>true,"label"=>"Etat","bdd"=>"CMD_IDETAT","type"=>"Libre","taille"=>15],
	"lastidetat"=>["aff"=>false,"label"=>"","bdd"=>"CMD_IDETAT","type"=>"Texte","taille"=>15],
	"idmarchandise"=>["aff"=>true,"label"=>"Groupe de marchandise","bdd"=>"CMD_IDMARCHANDISE","type"=>"Libre"],
	"montacc"=>["aff"=>true,"label"=>"Montant accompte","bdd"=>"CMD_MONTACC","type"=>"Montant","taille"=>50],
	"dateacc"=>["aff"=>true,"label"=>"Date accompte","bdd"=>"CMD_DATEACC","type"=>"Date","taille"=>50],
	"montintm"=>["aff"=>true,"label"=>"Montant intérêts moratoires","bdd"=>"CMD_MONTINTM","type"=>"Montant","taille"=>50],
	"dateintm"=>["aff"=>true,"label"=>"Date intérêts moratoires","bdd"=>"CMD_DATEINTM","type"=>"Date","taille"=>50],
	"commentaire"=>["aff"=>true,"label"=>"Commentaire","bdd"=>"CMD_COMMENTAIRE","type"=>"Zone texte","taille"=>500]
];
$descF1=["titre"=>"Administration des commandes","titresuite"=>"numero"];
?>
