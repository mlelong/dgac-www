<?php
/* config fonction cmd_list_marchandise.php*/
$champFL1=[
	"f_numero"=>["aff"=>true,"label"=>"Numéro","bdd"=>"GRM_NUMERO","type"=>"Texte"],
	"f_libelle"=>["aff"=>true,"label"=>"Libellé","bdd"=>"GRM_LIBELLE","type"=>"Texte"]
];
$champT1=[
	"numero"=>["aff"=>true,"label"=>"Numéro du groupe de marchandise","bdd"=>"GRM_NUMERO","type"=>"Lien","tri"=>true],
	"libelle"=>["aff"=>true,"label"=>"Libellé","bdd"=>"GRM_LIBELLE","tri"=>true],
	"description"=>["aff"=>true,"label"=>"Libellé","bdd"=>"GRM_DESCRIPTION"],
	"flagdsi"=>["aff"=>true,"label"=>"Utilisé par la DSI","bdd"=>"GRM_FLAGDSI","tri"=>true],
	"supp"=>["aff"=>true,"label"=>"Supprimer","type"=>"Lien"]
];
$descT1=["titre"=>"Groupe de marchandise","pagination"=>true,"largeur"=>"","tri"=>true,"nombre"=>20,"filtre"=>true,"deforder"=>""];
?>
