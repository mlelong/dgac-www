<?php
/* config fonction cmd_list_commande.php*/
$champT1=[
	"nomuo"=>["aff"=>true,"label"=>"Poste","bdd"=>"MUO_NOMUO","type"=>"Lien"],
	"marchandise"=>["aff"=>true,"label"=>"GM","bdd"=>"LCMD_MARCHANDISE"],
	"flagimputation"=>["aff"=>true,"label"=>"F/I","bdd"=>"MUO_FLAGIMPUTATION"],
	"montant"=>["aff"=>true,"label"=>"Montant révisé HT","bdd"=>"LCMD_MONTANTREV","type"=>"Montant","entete_style"=>"text-align:right","ligne_style"=>"text-align:right"],
	"quantite"=>["aff"=>true,"label"=>"Quantité","bdd"=>"LCMD_QUANTITE"],
	"coefredu"=>["aff"=>true,"label"=>"Remise","bdd"=>"LCMD_COEFREDU"],
	"totalht"=>["aff"=>true,"label"=>"Montant total HT","bdd"=>"LCMD_TOTALHT","type"=>"Montant","entete_style"=>"text-align:right","ligne_style"=>"text-align:right"],
	"dateliva"=>["aff"=>true,"label"=>"Livraison attendue","bdd"=>"LCMD_DATELIVA"],
	"dateva"=>["aff"=>true,"label"=>"Fin VA","bdd"=>"LCMD_DATEVA"],
	"supp"=>["aff"=>true,"label"=>"Supprimer","type"=>"Lien"]
];
$descT1=["titre"=>"Gestion des postes de commande projets","titresuite"=>"titresuite","pagination"=>false,"largeur"=>"","nombre"=>20,"filtre"=>false,"deforder"=>""];
?>
