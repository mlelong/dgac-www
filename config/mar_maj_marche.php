<?php
/* config fonction mar_maj_marche.php*/
$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Caractéristiques d'un marché ou devis","type"=>"Groupe"],
	"numero"=>["aff"=>true,"label"=>"Numéro DGR","bdd"=>"MAR_NUMERODGR","type"=>"Texte","taille"=>50,"ctl"=>"o"],
	"nommarche"=>["aff"=>true,"label"=>"Nom","bdd"=>"MAR_NOMMARCHE","type"=>"Texte","taille"=>250,"ctl"=>"o"],
	"numerosif"=>["aff"=>true,"label"=>"Numéro sif","bdd"=>"MAR_NUMEROSIF","type"=>"Texte","taille"=>50],
	"intervenant"=>["aff"=>true,"label"=>"Rédacteur","bdd"=>"MAR_INTERVENANT","type"=>"Libre"],
	"urgence"=>["aff"=>true,"label"=>"Urgence","bdd"=>"MAR_URGENCE","type"=>"Liste","taille"=>50,"dataliste"=>array("Faible"=>"Faible","Moyenne"=>"Moyenne","Elevée"=>"Elevée")],
	"priorité"=>["aff"=>true,"label"=>"Priorité","bdd"=>"MAR_PRIORITE","type"=>"Liste","taille"=>50,"dataliste"=>array("Haute"=>"Haute","Moyenne"=>"Moyenne","Basse"=>"Basse")],
	"etat"=>["aff"=>true,"label"=>"Etat d'avancement","bdd"=>"MAR_ETAT","type"=>"Liste","taille"=>50,"ctl"=>"o","dataliste"=>array("Créé"=>"Créé","Rédaction"=>"Rédaction","Validation DSI"=>"Validation DSI","Publication"=>"Publication","Analyse"=>"Analyse","Validation CB"=>"Validation CB","Notification"=>"Notification","Exécution"=>"Exécution","Terminé"=>"Terminé")],
	"nblot"=>["aff"=>true,"label"=>"Nombre de lots","bdd"=>"MAR_NBLOT","type"=>"Liste","taille"=>50,"dataliste"=>array("1"=>"1","2"=>"2","3"=>"3","4"=>"4","5"=>"5")],
	"idtypemarche"=>["aff"=>true,"label"=>"Type de marché (CCAG)","bdd"=>"MAR_IDTYPEMARCHE","type"=>"Libre"],
	"idtypeproc"=>["aff"=>true,"label"=>"Type de procédure","bdd"=>"MAR_IDTYPEPROC","type"=>"Libre"],
	"typeuo"=>["aff"=>true,"label"=>"Type d'uo/bpu","bdd"=>"MAR_TYPEUO","type"=>"Liste","taille"=>50,"dataliste"=>array("UO"=>"UO","BPU sans SLA"=>"BPU sans SLA","BPU avec SLA"=>"BPU avec SLA","Sans UO"=>"Sans UO")],
	"libelle"=>["aff"=>true,"label"=>"Libellé","bdd"=>"MAR_LIBELLE","type"=>"Texte","taille"=>250],
	"typereconduc"=>["aff"=>true,"label"=>"Type de reconduction","bdd"=>"MAR_TYPERECONDUC","type"=>"Liste","taille"=>50,"dataliste"=>array("Aucune"=>"Aucune","Expresse"=>"Expresse","Tacite"=>"Tacite")],
	"dureeinitiale"=>["aff"=>true,"label"=>"Durée initiale","bdd"=>"MAR_DUREEINITIALE","type"=>"Liste","taille"=>50,"dataliste"=>array("1"=>"1 an","2"=>"2 ans","3"=>"3 ans","4"=>"4 ans","5"=>"5 ans","6"=>"6 ans")],
	"nbreconduc"=>["aff"=>true,"label"=>"Nombre de reconductions","bdd"=>"MAR_NBRECONDUC","type"=>"Liste","taille"=>50,"dataliste"=>array("1"=>"1","2"=>"2","3"=>"3","4"=>"4","5"=>"5","6"=>"6")],
	"dureereconduc"=>["aff"=>true,"label"=>"Durée reconduction","bdd"=>"MAR_DUREERECONDUC","type"=>"Liste","taille"=>50,"dataliste"=>array("12"=>"12 mois","24"=>"24 mois","36"=>"36 mois","48"=>"48 mois")],
	"preavisreconduc"=>["aff"=>true,"label"=>"Préavis de reconduction","bdd"=>"MAR_PREAVISRECONDUC","type"=>"Liste","taille"=>50,"dataliste"=>array("1"=>"1 mois","2"=>"2 mois","3"=>"3 mois","4"=>"4 mois")],
	"montant"=>["aff"=>true,"label"=>"Montant TTC estimatif","bdd"=>"MAR_AEPREVTOTAL","type"=>"Montant","taille"=>50],
	"flagrma"=>["aff"=>true,"label"=>"Validation par","bdd"=>"MAR_FLAGRMA","type"=>"Case à cocher","debinline"=>true,"labelinline"=>"RMA"],
	"flagcb"=>["aff"=>true,"label"=>"","bdd"=>"MAR_FLAGCB","type"=>"Case à cocher","fininline"=>true,"labelinline"=>"CB"],
	"fichier"=>["aff"=>true,"label"=>"Répertoire de stockage des fichiers","bdd"=>"MAR_FICHIER","type"=>"Texte","taille"=>250],
	"description"=>["aff"=>true,"label"=>"Description","bdd"=>"MAR_DESCRIPTION","type"=>"Zone texte","taille"=>1000],
	"demandeur"=>["aff"=>false,"label"=>"","bdd"=>"MAR_DEMANDEUR","type"=>"Libre"],
	"iddemandeur"=>["aff"=>false,"label"=>"","bdd"=>"MAR_IDDEMANDEUR","type"=>"Libre"],
	"idintervenant"=>["aff"=>false,"label"=>"","bdd"=>"MAR_IDINTERVENANT","type"=>"Libre"],
	"datenot"=>["aff"=>false,"label"=>"Date de notification","bdd"=>"MAR_DATENOT","type"=>"Texte","taille"=>50],
	"datefin"=>["aff"=>false,"label"=>"Date de fin","bdd"=>"MAR_DATEFIN","type"=>"Texte","taille"=>50],
	"datereccur"=>["aff"=>false,"label"=>"Date de reconduction","bdd"=>"MAR_DATERECCUR","type"=>"Texte","taille"=>50],
	"daterecsui"=>["aff"=>false,"label"=>"Date de reconduction","bdd"=>"MAR_DATERECSUI","type"=>"Texte","taille"=>50]
];
$descF1=["titre"=>"Gestion  des marchés et des devis","titresuite"=>"nommarche"];
//	"idprojet"=>["aff"=>true,"label"=>"Projet/activité","bdd"=>"MAR_IDPROJET","type"=>"Libre"],
//	"numeroofficiel"=>["aff"=>true,"label"=>"Numéro sif officiel","bdd"=>"MAR_NUMEROOFFICIEL","type"=>"Texte","taille"=>50],
//	"idpole"=>["aff"=>true,"label"=>"Pôle","bdd"=>"MAR_IDPOLE","type"=>"Libre"],
?>
