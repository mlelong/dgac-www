<?php
/* config fonction cmd_maj_piece.php*/
$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Caractéristiques d'une pièce jointe","type"=>"Groupe"],
	"cleparent"=>["aff"=>false,"label"=>"cleparent","bdd"=>"PCE_IDTABLE","type"=>"Texte"],
	"nomfic"=>["aff"=>true,"label"=>"Nom du fichier à attacher (8M maxi)","bdd"=>"PCE_NOMFIC","type"=>"Fichier"],
	"libelle"=>["aff"=>true,"label"=>"Libellé","bdd"=>"PCE_LIBELLE","type"=>"Texte","taille"=>250,"ctl"=>"o"],
	"typefic"=>["aff"=>true,"label"=>"Type de fichier","bdd"=>"PCE_TYPEFIC","type"=>"Liste","taille"=>50,"dataliste"=>array("Devis"=>"Devis","Expression besoin"=>"Expression besoin","Bon de commande"=>"Bon de commande","Service fait"=>"Service fait")],
	"description"=>["aff"=>true,"label"=>"Description","bdd"=>"PCE_DESCRIPTION","type"=>"Zone texte","taille"=>2000]
];
$descF1=["titre"=>"Gestion des pièces jointes","titresuite"=>true];
?>
