<?php
/* config fonction mar_maj_typeproc.php*/
$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Caractéristiques d'un type de procédure","type"=>"Groupe"],
	"typeproc"=>["aff"=>true,"label"=>"Nom du type de procédure","bdd"=>"TPRO_TYPE","type"=>"Texte","taille"=>100,"ctl"=>"o"],
	"libelle"=>["aff"=>true,"label"=>"Libellé","bdd"=>"TPRO_LIBELLE","type"=>"Texte","taille"=>100]
];
$descF1=["titre"=>"Gestion des types de procédure"];
?>
