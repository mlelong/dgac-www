<?php
/* config fonction adm_list_groupe.php*/
$champFL1=[
	"f_nomgroupe"=>["aff"=>true,"label"=>"Nom du groupe","bdd"=>"GRU_NOMGROUPE","type"=>"Texte"]
];
$champT1=[
	"nomgroupe"=>["aff"=>true,"label"=>"Nom du groupe","bdd"=>"GRU_NOMGROUPE","type"=>"Lien"],
	"membre"=>["aff"=>true,"label"=>"Membres du groupe","type"=>"Lien"],
	"profil"=>["aff"=>true,"label"=>"Profils du groupe","type"=>"Lien"],
	"description"=>["aff"=>true,"label"=>"Description","bdd"=>"GRU_DESCRIPTION"],
	"supp"=>["aff"=>true,"label"=>"Supprimer","type"=>"Lien"]
];
$descT1=["titre"=>"Gestion des groupes d'utilisateur","pagination"=>false,"largeur"=>"","nombre"=>20,"filtre"=>true,"deforder"=>""];
?>
