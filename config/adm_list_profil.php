<?php
/* config fonction adm_list_profil.php*/
$champT1=[
	"nomprofil"=>["aff"=>true,"label"=>"Nom du profil","bdd"=>"PRO_NOMPROFIL","type"=>"Lien"],
	"droit"=>["aff"=>true,"label"=>"Droits","type"=>"Lien"],
	"groupe"=>["aff"=>true,"label"=>"Groupes","type"=>"Lien"],
	"utilisateur"=>["aff"=>true,"label"=>"Utilisateurs","type"=>"Lien"],
	"description"=>["aff"=>true,"label"=>"Description","bdd"=>"PRO_DESCRIPTION"],
	"supp"=>["aff"=>true,"label"=>"Supprimer","type"=>"Lien"]
];
$descT1=["titre"=>"Gestion des profils","pagination"=>false,"largeur"=>"","nombre"=>20,"filtre"=>false,"deforder"=>""];
?>
