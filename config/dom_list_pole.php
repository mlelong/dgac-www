<?php
/* config fonction dom_list_pole.php*/
$champT1=[
	"nompole"=>["aff"=>true,"label"=>"Code du pôle","bdd"=>"POL_NOMPOLE","type"=>"Lien"],
	"membre"=>["aff"=>true,"label"=>"Membres du pôle","type"=>"Lien"],
	"responsable"=>["aff"=>true,"label"=>"Responsables du pôle","type"=>"Lien"],
	"libelle"=>["aff"=>true,"label"=>"Nom du pôle","bdd"=>"POL_LIBELLE"],
	"supp"=>["aff"=>true,"label"=>"Supprimer","type"=>"Lien"]
];
$descT1=["titre"=>"Gestion des pôles","titresuite"=>true,"pagination"=>false,"largeur"=>"","nombre"=>20,"filtre"=>false,"deforder"=>""];
?>
