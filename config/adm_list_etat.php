<?php
/* config fonction adm_list_etat.php*/
$champT1=[
	"cleetat"=>["aff"=>false,"label"=>"","bdd"=>"ETA_CLE"],
	"codeetat"=>["aff"=>true,"label"=>"Code état","bdd"=>"ETA_CODEETAT","type"=>"Lien"],
	"nometat"=>["aff"=>true,"label"=>"Nom de l'état","bdd"=>"ETA_NOMETAT"],
	"nomphase"=>["aff"=>true,"label"=>"Nom de la phase","bdd"=>"PHA_NOMPHASE"],
	"supp"=>["aff"=>true,"label"=>"Supprimer","type"=>"Lien"]
];
$descT1=["titre"=>"Gestion des états","pagination"=>false,"largeur"=>"","nombre"=>20,"filtre"=>false,"deforder"=>""];
?>
