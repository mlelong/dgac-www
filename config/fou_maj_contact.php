<?php
/* config fonction fou_maj_contact.php */
$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Caractéristiques d'un contact","type"=>"Groupe"],
	"cleparent"=>["aff"=>false,"label"=>"cleparent","bdd"=>"CTC_IDFOURNISSEUR","type"=>"Texte"],
	"nom"=>["aff"=>true,"label"=>"Nom du contact","bdd"=>"CTC_NOM","type"=>"Texte","taille"=>50,"ctl"=>"o"],
	"prenom"=>["aff"=>true,"label"=>"Prénom du contact","bdd"=>"CTC_PRENOM","type"=>"Texte","taille"=>50,"ctl"=>"o"],
	"civilite"=>["aff"=>true,"label"=>"Civilité","bdd"=>"CTC_CIVILITE","type"=>"Liste","dataliste"=>array("Monsieur"=>"Monsieur","Madame"=>"Madame")],
	"etat"=>["aff"=>true,"label"=>"Etat","bdd"=>"CTC_ETAT","type"=>"Liste","ctl"=>"o","dataliste"=>array("Actif"=>"Actif","Inactif"=>"Inactif")],
	"adresse"=>["aff"=>true,"label"=>"Adresse","bdd"=>"CTC_ADRESSE","type"=>"Texte","taille"=>250],
	"commune"=>["aff"=>true,"label"=>"Commune","bdd"=>"CTC_COMMUNE","type"=>"Texte","taille"=>50],
	"departement"=>["aff"=>true,"label"=>"Département","bdd"=>"CTC_DEPARTEMENT","type"=>"Texte","taille"=>50],
	"codepostal"=>["aff"=>true,"label"=>"Code postal","bdd"=>"CTC_CODEPOSTAL","type"=>"Texte","taille"=>10],
	"telephone"=>["aff"=>true,"label"=>"Téléphone","bdd"=>"CTC_TELEPHONE","type"=>"Texte","taille"=>50],
	"portable"=>["aff"=>true,"label"=>"Portable","bdd"=>"CTC_PORTABLE","type"=>"Texte","taille"=>50],
	"fax"=>["aff"=>true,"label"=>"Fax","bdd"=>"CTC_FAX","type"=>"Texte","taille"=>50],
	"mail"=>["aff"=>true,"label"=>"Mail","bdd"=>"CTC_MAIL","type"=>"Texte","taille"=>250],
	"description"=>["aff"=>true,"label"=>"Description","bdd"=>"CTC_DESCRIPTION","type"=>"Zone texte","taille"=>500]
];
$descF1=["titre"=>"Administration des contacts","titresuite"=>true];
?>
