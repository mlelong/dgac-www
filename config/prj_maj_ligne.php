<?php
/* config fonction prj_maj_ligne.php*/
$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Caractéristiques d'une ligne de budget","type"=>"Groupe"],
	"idbudget"=>["aff"=>true,"label"=>"Année","bdd"=>"BUDL_IDBUDGET","type"=>"Libre","taille"=>10,"ctl"=>"o"],
	"lignebud"=>["aff"=>true,"label"=>"Ligne","bdd"=>"BUDL_LIGNE","type"=>"Texte","taille"=>250],
	"idprojet"=>["aff"=>false,"label"=>"Nom du projet","bdd"=>"BUDL_IDPROJET","type"=>"Libre","taille"=>"250"],
	"idmarche"=>["aff"=>true,"label"=>"Nom du marché","bdd"=>"BUDL_IDMARCHE","type"=>"Libre","taille"=>"250"],
	"libelle"=>["aff"=>true,"label"=>"Libellé","bdd"=>"BUDL_LIBELLE","type"=>"Texte","taille"=>250],
	"refsif"=>["aff"=>true,"label"=>"Référence SIF","bdd"=>"BUDL_REFSIF","type"=>"Texte","taille"=>50],
	"aefonct"=>["aff"=>true,"label"=>"AE fonctionnement (Euros TTC)","bdd"=>"BUDL_AEPREVFONCT","type"=>"Montant","taille"=>25],
	"cpfonct"=>["aff"=>true,"label"=>"CP fonctionnement (Euros TTC)","bdd"=>"BUDL_CPPREVFONCT","type"=>"Montant","taille"=>25],
	"aeinves"=>["aff"=>true,"label"=>"AE investissement (Euros TTC)","bdd"=>"BUDL_AEPREVINVES","type"=>"Montant","taille"=>25],
	"cpinves"=>["aff"=>true,"label"=>"CP investissement (Euros TTC)","bdd"=>"BUDL_CPPREVINVES","type"=>"Montant","taille"=>25]
];
$descF1=["titre"=>"Administration des lignes de budget","titresuite"=>true];
?>
