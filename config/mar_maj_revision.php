<?php
/* config fonction mar_maj_revision.php*/
$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Caractéristiques d'une révision de prix","type"=>"Groupe"],
	"cleparent"=>["aff"=>false,"label"=>"cleparent","bdd"=>"MREV_IDMARCHE","type"=>"Texte"],
	"daterev"=>["aff"=>true,"label"=>"Date d'application de la révision","bdd"=>"MREV_DATEREV","type"=>"Date","taille"=>50,"ctl"=>"o"],
	"coefrev"=>["aff"=>true,"label"=>"Coefficient de la révision","bdd"=>"MREV_COEFREV","type"=>"Montant","taille"=>50,"decimale"=>3,"ctl"=>"o"],
	"description"=>["aff"=>true,"label"=>"Libellé","bdd"=>"MREV_DESCRIPTION","type"=>"Zone texte","taille"=>500]
];
$descF1=["titre"=>"Gestion des révisions de prix","titresuite"=>true];
?>
