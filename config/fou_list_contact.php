<?php
/* config fonction fou_list_contact.php */
$champFL1=[
	"f_nom"=>["aff"=>true,"label"=>"Nom du contact","bdd"=>"CTC_NOM","type"=>"Texte"]
];
$champT1=[
	"nom"=>["aff"=>true,"label"=>"Nom","bdd"=>"CTC_NOM","type"=>"Lien"],
	"prenom"=>["aff"=>true,"label"=>"Prénom","bdd"=>"CTC_PRENOM"],
	"telephone"=>["aff"=>true,"label"=>"Téléphone","bdd"=>"CTC_TELEPHONE"],
	"supp"=>["aff"=>true,"label"=>"Supprimer","type"=>"Lien"]
];
$descT1=["titre"=>"Gestion des contacts","titresuite"=>true,"pagination"=>false,"largeur"=>"","nombre"=>20,"filtre"=>true,"deforder"=>""];
?>
