<?php
/* config fonction mar_suivi_planification.php*/
$champT1=[
	"numero"=>["aff"=>true,"label"=>"Numéro","bdd"=>"CMD_NUMERO","type"=>"Lien","tri"=>true],
	"nompole"=>["aff"=>true,"label"=>"Nom pôle","bdd"=>"POL_NOMPOLE","tri"=>true],
	"nommarche"=>["aff"=>true,"label"=>"Nom marché","bdd"=>"MAR_NOMMARCHE","tri"=>true],
	"nomprojet"=>["aff"=>true,"label"=>"Nom projet","bdd"=>"PRJ_NOMPROJET","tri"=>true],
	"etat"=>["aff"=>true,"label"=>"Etat","bdd"=>"ETA_NOMETAT","tri"=>true],
	"datecmd"=>["aff"=>true,"label"=>"Date EJ SIF","bdd"=>"CMD_DATECMD","tri"=>true],
	"totalttc"=>["aff"=>true,"label"=>"Total TTC","bdd"=>"CMD_TOTALTTC","tri"=>true]
];
$descT1=["titre"=>"Commandes en cours","pagination"=>false,"largeur"=>"","tri"=>true,"nombre"=>20,"filtre"=>false,"deforder"=>""];
$reqT1="select CMD_CLE, CMD_NUMERO, POL_NOMPOLE, MAR_NOMMARCHE, PRJ_NOMPROJET, 
			DATE_FORMAT(CMD_DATECMD, '%d-%m-%Y') AS CMD_DATECMD,
			ETA_NOMETAT, CMD_TOTALTTC
			from commande 
			left join pole on CMD_IDPOLE=POL_CLE
			left join marche on CMD_IDMARCHE=MAR_CLE
			left join projet on CMD_IDPROJET=PRJ_CLE
			left join fonc_etat on CMD_IDETAT=ETA_CLE
			where 1 ";
$orderT1="order by CMD_NUMERO desc";
$lienT1=array(
	array("numero","cmd_tab_commande.php","?typeaction=modification&cle=#CMD_CLE#",'')
);
?>