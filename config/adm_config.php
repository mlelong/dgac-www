<?php
/* config fonction adm_config.php*/
$champC1=[
	["aff"=>true,"type"=>"fonctionnalite","code"=>"adm","ordre"=>1,"nom"=>"Administration",
		"modifiable"=>false,"droit"=>["lec"=>"lec","cre"=>"cre","maj"=>"maj","sup"=>"sup"]],
		
	["aff"=>true,"type"=>"fonctionnalite","code"=>"cfg","ordre"=>2,"nom"=>"Gestion de la configuration",
		"modifiable"=>false,"droit"=>["lec"=>"lec","cre"=>"cre","maj"=>"maj","sup"=>"sup"]],
		
	["aff"=>true,"type"=>"fonctionnalite","code"=>"prj","ordre"=>3,"nom"=>"Gestion des projets",
		"modifiable"=>false,"droit"=>["lec"=>"lec","cre"=>"cre","maj"=>"maj","sup"=>"sup"]],
		
	["aff"=>true,"type"=>"fonctionnalite","code"=>"bud","ordre"=>4,"nom"=>"Gestion des budgets",
		"modifiable"=>false,"droit"=>["lec"=>"lec","cre"=>"cre","maj"=>"maj","sup"=>"sup"]],
		
	["aff"=>true,"type"=>"fonctionnalite","code"=>"rpb","ordre"=>5,"nom"=>"Gestion rapports sur les budgets",
		"modifiable"=>false,"droit"=>["lec"=>"lec","cre"=>"cre","maj"=>"maj","sup"=>"sup"]],
		
	["aff"=>true,"type"=>"fonctionnalite","code"=>"cmd","ordre"=>6,"nom"=>"Gestion des commandes",
		"modifiable"=>false,"droit"=>["lec"=>"lec","cre"=>"cre","maj"=>"maj","sup"=>"sup","exe"=>"exe","adm"=>"adm"]],
		
	["aff"=>true,"type"=>"fonctionnalite","code"=>"mar","ordre"=>7,"nom"=>"Gestion des marchés",
		"modifiable"=>false,"droit"=>["lec"=>"lec","cre"=>"cre","maj"=>"maj","sup"=>"sup"],"table"=>["nom"=>"region","cle"=>"MAR_CLE","visibilite"=>"MAR_VISIBILITE","etat"=>"MAR_ETAT"]],
		
	["aff"=>true,"type"=>"fonctionnalite","code"=>"rpm","ordre"=>8,"nom"=>"Gestion des rapport sur les marchés",
		"modifiable"=>false,"droit"=>["lec"=>"lec","cre"=>"cre","maj"=>"maj","sup"=>"sup"]],
		
	["aff"=>true,"type"=>"fonctionnalite","code"=>"uos","ordre"=>9,"nom"=>"Gestion des unités d'oeuvre",
		"modifiable"=>false,"droit"=>["lec"=>"lec","cre"=>"cre","maj"=>"maj","sup"=>"sup"]],
		
	["aff"=>true,"type"=>"fonctionnalite","code"=>"fou","ordre"=>10,"nom"=>"Gestion des fournisseurs",
		"modifiable"=>false,"droit"=>["lec"=>"lec","cre"=>"cre","maj"=>"maj","sup"=>"sup"]],
		
	["aff"=>true,"type"=>"fonctionnalite","code"=>"con","ordre"=>11,"nom"=>"Gestion des contacts",
		"modifiable"=>false,"droit"=>["lec"=>"lec","cre"=>"cre","maj"=>"maj","sup"=>"sup"]],
		
	["aff"=>true,"type"=>"fonctionnalite","code"=>"dom","ordre"=>12,"nom"=>"Gestion des domaines",
		"modifiable"=>false,"droit"=>["lec"=>"lec","cre"=>"cre","maj"=>"maj","sup"=>"sup"]],
		
	["aff"=>true,"type"=>"fonctionnalite","code"=>"gru","ordre"=>13,"nom"=>"Gestion des groupes d'utilisateur",
		"modifiable"=>false,"droit"=>["lec"=>"lec","cre"=>"cre","maj"=>"maj","sup"=>"sup"]],
		
	["aff"=>true,"type"=>"fonctionnalite","code"=>"uti","ordre"=>14,"nom"=>"Gestion des utilisateurs",
		"modifiable"=>false,"droit"=>["lec"=>"lec","cre"=>"cre","maj"=>"maj","sup"=>"sup"]],
		
	["aff"=>true,"type"=>"fonctionnalite","code"=>"pro","ordre"=>15,"nom"=>"Gestion des profils",
		"modifiable"=>false,"droit"=>["lec"=>"lec","cre"=>"cre","maj"=>"maj","sup"=>"sup"]],

	["aff"=>true,"type"=>"fonctionnalite","code"=>"pat","ordre"=>16,"nom"=>"Gestion des patchs",
	"modifiable"=>false,"droit"=>["lec"=>"lec","cre"=>"cre","maj"=>"maj","sup"=>"sup"]]
		
];
$descC1=["titre"=>"Configuration des tables liées aux droits"];
?>
