<?php
/* config fonction bud_maj_budget.php*/
$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Caractéristiques d'un budget","type"=>"Groupe"],
	"annee"=>["aff"=>true,"label"=>"Année","bdd"=>"BUD_ANNEE","type"=>"Texte","taille"=>10,"ctl"=>"o"],
	"programme"=>["aff"=>true,"label"=>"Programme LOLF","bdd"=>"BUD_PROGRAMME","type"=>"Liste","taille"=>50,"ctl"=>"o","dataliste"=>array("P612"=>"P612","P613"=>"P613","P614-01"=>"P614-01","P614-02"=>"P614-02")],
	"libelle"=>["aff"=>true,"label"=>"Description","bdd"=>"BUD_LIBELLE","type"=>"Texte","taille"=>250],
	"aedotafonct"=>["aff"=>true,"label"=>"AE fonctionnement (Euros TTC)","bdd"=>"BUD_AEDOTAFONCT","type"=>"Montant","taille"=>25],
	"aedotainves"=>["aff"=>true,"label"=>"AE investissement (Euros TTC)","bdd"=>"BUD_AEDOTAINVES","type"=>"Montant","taille"=>25],
	"cpdotafonct"=>["aff"=>true,"label"=>"CP fonctionnement (Euros TTC)","bdd"=>"BUD_CPDOTAFONCT","type"=>"Montant","taille"=>25],
	"cpdotainves"=>["aff"=>true,"label"=>"CP investissement (Euros TTC)","bdd"=>"BUD_CPDOTAINVES","type"=>"Montant","taille"=>25],
	"aedeblfonct"=>["aff"=>false,"label"=>"Débloqué AE fonctionnement (Euros TTC)","bdd"=>"BUD_AEDEBLFONCT","type"=>"Montant","taille"=>25],
	"aedeblinves"=>["aff"=>false,"label"=>"Débloqué AE investissement (Euros TTC)","bdd"=>"BUD_AEDEBLINVES","type"=>"Montant","taille"=>25],
	"cpdeblfonct"=>["aff"=>false,"label"=>"Débloqué CP fonctionnement (Euros TTC)","bdd"=>"BUD_CPDEBLFONCT","type"=>"Montant","taille"=>25],
	"cpdeblinves"=>["aff"=>false,"label"=>"Débloqué CP investissement (Euros TTC)","bdd"=>"BUD_CPDEBLINVES","type"=>"Montant","taille"=>25],
	"flagcopie"=>["aff"=>true,"label"=>"Copier lignes année précédante","type"=>"Case à cocher"],
	"commentaire"=>["aff"=>true,"label"=>"Commentaire","bdd"=>"BUD_COMMENTAIRE","type"=>"Zone texte","taille"=>3000]
];
$descF1=["titre"=>"Administration des budgets","titresuite"=>"annee"];
?>