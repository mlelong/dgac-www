<?php
/* config fonction fou_list_fournisseur.php*/
$champFL1=[
	"f_nomfou"=>["aff"=>true,"label"=>"Nom","bdd"=>"FOU_NOMFOU","type"=>"Texte"]
];
$champT1=[
	"numero"=>["aff"=>true,"label"=>"Numéro","bdd"=>"FOU_NUMERO","type"=>"Lien","tri"=>true],
	"nomfou"=>["aff"=>true,"label"=>"Nom","bdd"=>"FOU_NOMFOU","tri"=>true],
	"Localite"=>["aff"=>true,"label"=>"Localité","bdd"=>"FOU_LOCALITE","tri"=>true],
	"codepostal"=>["aff"=>true,"label"=>"Code postal","bdd"=>"FOU_CODEPOSTAL","tri"=>true],
	"supp"=>["aff"=>true,"label"=>"Supprimer","type"=>"Lien"]
];
$descT1=["titre"=>"Gestion des fournisseurs","pagination"=>true,"largeur"=>"","tri"=>true,"nombre"=>20,"filtre"=>true,"deforder"=>""];
?>
