<?php
/* config fonction cmd_maj_marchandise.php*/
$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Caractéristiques d'un groupe de marchandise","type"=>"Groupe"],
	"numero"=>["aff"=>true,"label"=>"Numéro groupe de marchandise","bdd"=>"GRM_NUMERO","type"=>"Texte","taille"=>10,"ctl"=>"o"],
	"libelle"=>["aff"=>true,"label"=>"Libellé","bdd"=>"GRM_LIBELLE","type"=>"Texte","taille"=>50],
	"description"=>["aff"=>true,"label"=>"Description","bdd"=>"GRM_DESCRIPTION","type"=>"Texte","taille"=>250],
	"flagdsi"=>["aff"=>true,"label"=>"Utilisé par la DSI","bdd"=>"GRM_FLAGDSI","type"=>"Case à cocher"]
];
$descF1=["titre"=>"Gestion des groupes de marchandise"];
?>
