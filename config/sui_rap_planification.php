<?php
/* config fonction mar_suivi_planification.php*/
$champT1=[
	"numerodgr"=>["aff"=>true,"label"=>"Numéro","bdd"=>"MAR_NUMERODGR","type"=>"Lien","tri"=>true],
	"nommarche"=>["aff"=>true,"label"=>"Nom","bdd"=>"MAR_NOMMARCHE","tri"=>true],
	"datecctp"=>["aff"=>true,"label"=>"Date rédaction CCTP","bdd"=>"MAR_DATECCTP"],
	"dateccap"=>["aff"=>true,"label"=>"Date rédaction CCAP","bdd"=>"MAR_DATECCAP"],
	"daterma"=>["aff"=>true,"label"=>"Date validation RMA","bdd"=>"MAR_DATERMA"],
	"datepub"=>["aff"=>true,"label"=>"Date publication","bdd"=>"MAR_DATEPUB"],
	"dateret"=>["aff"=>true,"label"=>"Date retour offres","bdd"=>"MAR_DATERET"],
	"datecb"=>["aff"=>true,"label"=>"Date validation CB","bdd"=>"MAR_DATECB"],
	"datenot"=>["aff"=>true,"label"=>"Date notification","bdd"=>"MAR_DATENOT"],
	"datefin"=>["aff"=>true,"label"=>"Date fin","bdd"=>"MAR_DATEFIN"]
];
$descT1=["titre"=>"Planification des marchés","pagination"=>false,"largeur"=>"","tri"=>true,"nombre"=>20,"filtre"=>false,"deforder"=>""];
$reqT1="select MAR_CLE, MAR_NUMERODGR, MAR_NOMMARCHE, 
				DATE_FORMAT(MAR_DATECCTP, '%d-%m-%Y') AS MAR_DATECCTP,
				DATE_FORMAT(MAR_DATECCAP, '%d-%m-%Y') AS MAR_DATECCAP,
				DATE_FORMAT(MAR_DATERMA, '%d-%m-%Y') AS MAR_DATERMA,
				DATE_FORMAT(MAR_DATEPUB, '%d-%m-%Y') AS MAR_DATEPUB,
				DATE_FORMAT(MAR_DATERET, '%d-%m-%Y') AS MAR_DATERET,
				DATE_FORMAT(MAR_DATECB, '%d-%m-%Y') AS MAR_DATECB,
				DATE_FORMAT(MAR_DATENOT, '%d-%m-%Y') AS MAR_DATENOT,
				DATE_FORMAT(MAR_DATEFIN, '%d-%m-%Y') AS MAR_DATEFIN
			from marche where 1";
$orderT1="order by MAR_NUMERODGR";
$lienT1=array(
	array("numerodgr","mar_tab_marche.php","?typeaction=modification&cle=#MAR_CLE#",'')
);
?>