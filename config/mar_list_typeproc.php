<?php
/* config fonction mar_list_typeproc.php*/
$champT1=[
	"typeproc"=>["aff"=>true,"label"=>"Nom du type de procédure","bdd"=>"TPRO_TYPE","type"=>"Lien"],
	"libelle"=>["aff"=>true,"label"=>"Libellé","bdd"=>"TPRO_LIBELLE"],
	"supp"=>["aff"=>true,"label"=>"Supprimer","type"=>"Lien"]
];
$descT1=["titre"=>"Types de procédure de marché","pagination"=>false,"largeur"=>"","nombre"=>20,"filtre"=>false,"deforder"=>""];
?>
