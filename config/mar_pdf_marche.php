<?php
/* config fonction mar_pdf_marche.php*/

$descT1=["titre"=>"Gestion des marchés"];

$champT1=[
	["aff"=>true,"label"=>"Numéro","taille"=>25,"aliaspdf"=>false],
	["aff"=>true,"label"=>"Nom","taille"=>30,"aliaspdf"=>false],
	["aff"=>false,"label"=>"Type","aliaspdf"=>false],
	["aff"=>true,"label"=>"Libellé","taille"=>100,"aliaspdf"=>false],
	["aff"=>true,"label"=>"Date publication","taille"=>15,"aliaspdf"=>"Date publi"],
	["aff"=>false,"label"=>"Date retour","aliaspdf"=>false],
	["aff"=>false,"label"=>"Date notification","aliaspdf"=>false],
	["aff"=>true,"label"=>"Montant","taille"=>20,"aliaspdf"=>false],
	["aff"=>false,"label"=>"Supprimer","aliaspdf"=>false]
];
