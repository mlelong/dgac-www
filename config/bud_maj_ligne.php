<?php
/* config fonction bud_maj_ligne.php*/
$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Caractéristiques d'une ligne de budget","type"=>"Groupe"],
	"cleparent"=>["aff"=>false,"label"=>"cleparent","bdd"=>"BUDL_IDBUDGET","type"=>"Texte"],
	"idprojet"=>["aff"=>true,"label"=>"Nom du projet","bdd"=>"BUDL_IDPROJET","type"=>"Libre","taille"=>"250","ctl"=>"o"],
	"idpapfonct"=>["aff"=>true,"label"=>"Référence PAP Fonctionnement","bdd"=>"BUDL_IDPAPFONCT","type"=>"Libre","taille"=>"250"],
	"idpapinves"=>["aff"=>true,"label"=>"Référence PAP Investissement","bdd"=>"BUDL_IDPAPINVES","type"=>"Libre","taille"=>"250"],
	"typelig"=>["aff"=>true,"label"=>"Type de dépense","bdd"=>"BUDL_TYPELIG","type"=>"Liste","dataliste"=>array("Obligatoire"=>"Obligatoire","Essentiel"=>"Essentiel","Projet"=>"Projet","Autre"=>"Autre")],
	"libelle"=>["aff"=>true,"label"=>"Libellé","bdd"=>"BUDL_LIBELLE","type"=>"Texte","taille"=>250],
	"typeimputation"=>["aff"=>true,"label"=>"Nature de dépense (PAP/RAP)","bdd"=>"BUDL_TYPEIMPUTATION","type"=>"Liste","taille"=>50,"ctl"=>"o",
		"dataliste"=>array("AMOA"=>"AMOA","AMOE"=>"AMOE","Prestation de service"=>"Prestation de service","Prestation intellectuelle"=>"Prestation intellectuelle",
		"Achat licence technique"=>"Achat licence technique","Maintenance licence technique"=>"Maintenance licence technique","Achat matériel"=>"Achat matériel","Renouvellement matériel"=>"Renouvellement matériel",
		"Maintenance matériel"=>"Maintenance matériel","Achat licence applicative"=>"Achat licence applicative","Maintenance licence applicative"=>"Maintenance licence applicative",
		"Achat développement"=>"Achat développement","Maintenance développement"=>"Maintenance développement","Abonnement réseau"=>"Abonnement réseau","Logistique"=>"Logistique","RH"=>"RH")],
	"priorite"=>["aff"=>true,"label"=>"Priorité","bdd"=>"BUDL_PRIORITE","type"=>"Liste","dataliste"=>array("P0"=>"P0","P1"=>"P1","P2"=>"P2","P3"=>"P3")],
	"aedotafonct"=>["aff"=>true,"label"=>"AE / CP fonctionnement (Euros TTC)","bdd"=>"BUDL_AEDOTAFONCT","type"=>"Montant","taille"=>25,"debinline"=>true],
	"cpdotafonct"=>["aff"=>true,"label"=>"","bdd"=>"BUDL_CPDOTAFONCT","type"=>"Montant","taille"=>25,"fininline"=>true],
	"aedotainves"=>["aff"=>true,"label"=>"AE / CP investissement (Euros TTC)","bdd"=>"BUDL_AEDOTAINVES","type"=>"Montant","taille"=>25,"debinline"=>true],
	"cpdotainves"=>["aff"=>true,"label"=>"","bdd"=>"BUDL_CPDOTAINVES","type"=>"Montant","taille"=>25,"fininline"=>true],
	"commentaire"=>["aff"=>true,"label"=>"Commentaire","bdd"=>"BUDL_COMMENTAIRE","type"=>"Zone texte","taille"=>3000]
];
$descF1=["titre"=>"Administration des lignes de budget","titresuite"=>true];
?>
