<?php
/* config fonction dom_resume_domaine.php*/
$champT1=[
	"numero"=>["aff"=>true,"label"=>"Numéro","bdd"=>"CMD_NUMERO","type"=>"Lien","tri"=>true],
	"nomprojet"=>["aff"=>true,"label"=>"Nom projet","bdd"=>"PRJ_NOMPROJET","tri"=>true],
	"etat"=>["aff"=>true,"label"=>"Etat","bdd"=>"ETA_NOMETAT","tri"=>true]
];
$descT1=["titre"=>"Dernières commandes","pagination"=>false,"colonne"=>false,"largeur-decal"=>"2","largeur"=>"8","nombre"=>5,"filtre"=>false,"deforder"=>""];

$champT2=[
	"nommarche"=>["aff"=>true,"label"=>"Nom","bdd"=>"MAR_NOMMARCHE","type"=>"Lien","tri"=>true],
	"numero"=>["aff"=>true,"label"=>"Numéro","bdd"=>"MAR_NUMERODGR","tri"=>true],
	"libelle"=>["aff"=>true,"label"=>"Libellé","bdd"=>"MAR_LIBELLE"]
];
$descT2=["titre"=>"Derniers marchés","pagination"=>false,"colonne"=>false,"largeur-decal"=>"2","largeur"=>"8","tri"=>true,"nombre"=>5,"filtre"=>false,"deforder"=>""];
?>
