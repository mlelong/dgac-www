<?php
/* config fonction test_tableau.php*/
$champFL1=[
	"f_nommarche"=>["aff"=>true,"label"=>"Nom","bdd"=>"MAR_NOMMARCHE","type"=>"Texte"],
	"f_nomprojet"=>["aff"=>true,"label"=>"Projet/activité","bdd"=>"PRJ_NOMPROJET","type"=>"Texte"],
	"f_nompole"=>["aff"=>true,"label"=>"Nom pôle","bdd"=>"POL_NOMPOLE","type"=>"Texte"],
	"f_typeproc"=>["aff"=>true,"label"=>"Type procédure","bdd"=>"TPRO_TYPE","type"=>"Texte"],
	"f_datenot"=>["aff"=>true,"label"=>"Date","bdd"=>"MAR_DATENOT","type"=>"Texte"]
];
$champT1=[
	"numerodgr"=>["aff"=>true,"label"=>"Numéro","bdd"=>"MAR_NUMERODGR","type"=>"Lien","tri"=>true],
	"numerosif"=>["aff"=>true,"label"=>"Référence SIF","bdd"=>"MAR_NUMEROSIF","type"=>"Lien","tri"=>true],
	"nommarche"=>["aff"=>true,"label"=>"Nom","bdd"=>"MAR_NOMMARCHE","type"=>"Texte","taille"=>200,"tri"=>true],
	"nomprojet"=>["aff"=>true,"label"=>"Projet/activité","bdd"=>"PRJ_NOMPROJET","type"=>"Texte","tri"=>true],
	"nompole"=>["aff"=>true,"label"=>"Nom pôle","bdd"=>"POL_NOMPOLE","type"=>"Texte","tri"=>true],
	"flagrma"=>["aff"=>true,"label"=>"RMA","bdd"=>"MAR_FLAGRMA","type"=>"Case à cocher"],
	"flagcb"=>["aff"=>true,"label"=>"CB","bdd"=>"MAR_FLAGCB","type"=>"Case à cocher"],
	"etat"=>["aff"=>true,"label"=>"Etat","bdd"=>"MAR_ETAT","type"=>"Texte","tri"=>true],
	"montant"=>["aff"=>true,"label"=>"Montant","bdd"=>"MAR_AEPREVTOTAL","type"=>"Texte","tri"=>true],
	"supp"=>["aff"=>true,"label"=>"Supprimer","type"=>"Lien"]
];
$descT1=["titre"=>"Marchés","pagination"=>true,"largeur"=>"","tri"=>true,"nombre"=>20,"filtre"=>true,"deforder"=>""];
?>
