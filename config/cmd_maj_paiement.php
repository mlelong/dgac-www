<?php
/* config fonction cmd_maj_paiement.php*/
$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Caractéristiques d'un paiement de commande","type"=>"Groupe"],
	"hiddenMUO"=>["aff"=>false,"label"=>"","type"=>"Texte"],
	"hiddenLCMD"=>["aff"=>false,"label"=>"","type"=>"Texte"],
	"hiddenRPL"=>["aff"=>false,"label"=>"","type"=>"Texte"],
	"hiddenqte"=>["aff"=>false,"label"=>"","type"=>"Texte"],
	"hiddenval"=>["aff"=>false,"label"=>"","type"=>"Texte"],
	"cleparent"=>["aff"=>false,"label"=>"cleparent","bdd"=>"PCMD_IDCMD","type"=>"Texte"],
	"idbudget"=>["aff"=>false,"label"=>"","bdd"=>"PCMD_IDBUDGET","type"=>"Libre"],
	"idlignebudget"=>["aff"=>false,"label"=>"","bdd"=>"PCMD_IDLIGNEBUDGET","type"=>"Libre"],
	"datecre"=>["aff"=>true,"label"=>"Date de création","bdd"=>"PCMD_DATECRE","type"=>"Date"],
	"datepre"=>["aff"=>true,"label"=>"Date prévisionnelle","bdd"=>"PCMD_DATEPRE","type"=>"Date"],
	"demandeur"=>["aff"=>false,"label"=>"","bdd"=>"PCMD_DEMANDEUR","type"=>"Libre"],
	"iddemandeur"=>["aff"=>false,"label"=>"","bdd"=>"PCMD_IDDEMANDEUR","type"=>"Libre"],
	"idintervenant"=>["aff"=>false,"label"=>"","bdd"=>"PCMD_IDINTERVENANT","type"=>"Libre"],
	"intervenant"=>["aff"=>true,"label"=>"Rédacteur","bdd"=>"PCMD_INTERVENANT","type"=>"Libre"],
	"urgence"=>["aff"=>true,"label"=>"Urgence","bdd"=>"PCMD_URGENCE","type"=>"Liste","taille"=>50,"dataliste"=>array("Faible"=>"Faible","Moyenne"=>"Moyenne","Elevée"=>"Elevée")],
	"priorité"=>["aff"=>true,"label"=>"Priorité","bdd"=>"PCMD_PRIORITE","type"=>"Liste","taille"=>50,"dataliste"=>array("Haute"=>"Haute","Moyenne"=>"Moyenne","Basse"=>"Basse")],
	"typepaie"=>["aff"=>true,"label"=>"Type de paiement","bdd"=>"PCMD_TYPEPAIE","type"=>"Liste","taille"=>25,"ctl"=>"o","dataliste"=>array("Avance"=>"Avance","Récupération Avance"=>"Récupération Avance","Acompte (avec Service Fait)"=>"Acompte (avec Service Fait)","Acompte sur avancement"=>"Acompte sur avancement","Solde (avec Service Fait)"=>"Solde (avec Service Fait)","Paiement unique"=>"Paiement unique","Intérêts moratoires"=>"Intérêts moratoires","Pénalités"=>"Pénalités","Avoir total"=>"Avoir total","Avoir partiel"=>"Avoir partiel","Retenue de garanties"=>"Retenue de garanties","Levée de garanties"=>"Levée de garanties")],
	"totalttc"=>["aff"=>true,"label"=>"Montant(Euros TTC)","bdd"=>"PCMD_TOTALTTC","type"=>"Montant","taille"=>25],
	"datepre"=>["aff"=>true,"label"=>"Date prévisionnelle","bdd"=>"PCMD_DATEPRE","type"=>"Date"],
	"dateserv"=>["aff"=>true,"label"=>"Date du service fait","bdd"=>"PCMD_DATESERV","type"=>"Date"],
	"numerosif"=>["aff"=>true,"label"=>"Numéro de DP SIF","bdd"=>"PCMD_NUMEROSIF","type"=>"Texte","taille"=>50],
	"datepaie"=>["aff"=>true,"label"=>"Date de DP SIF","bdd"=>"PCMD_DATEPAIE","type"=>"Date"],
	"etat"=>["aff"=>true,"label"=>"Etat du paiement","bdd"=>"PCMD_ETAT","type"=>"Liste","taille"=>50,"ctl"=>"o","dataliste"=>array("Brouillon"=>"Brouillon","Soumise"=>"Soumise","PV signé"=>"PV signé","PV retourné"=>"PV retourné","DP réalisée dans le SIF"=>"DP réalisée dans le SIF","DP décaissée"=>"DP décaissée")], 
	"libelle"=>["aff"=>true,"label"=>"Libellé","bdd"=>"PCMD_LIBELLE","type"=>"Texte","taille"=>250]
];
$descF1=["titre"=>"Administration des paiements de commande","titresuite"=>true];

$champT1=[
	"nomuo"=>["aff"=>true,"label"=>"UO","bdd"=>"MUO_NOMUO"],
	"datelivr"=>["aff"=>true,"label"=>"Date livraison réelle","bdd"=>"RPL_DATELIVR","type"=>"Date","saisie"=>true],
	"totalht"=>["aff"=>true,"label"=>"Montant révisé HT","bdd"=>"LCMD_TOTALHT","type"=>"Montant"],
	"quantite"=>["aff"=>true,"label"=>"Quantité commandée","bdd"=>"LCMD_QUANTITE"],
	"qtereste"=>["aff"=>true,"label"=>"Quantité restante","bdd"=>"LCMD_QTEPAYER"],
	"qtepayer"=>["aff"=>true,"label"=>"Quantité à payer","bdd"=>"RPL_QUANTITE","type"=>"Nombre","saisie"=>true,"largeur_col"=>150]
];
$descT1=["titre"=>"Sélection des UO à prendre en compte","titresuite"=>false,"affform"=>true,"pagination"=>false,"colonne"=>false,"largeur"=>"","nombre"=>20,"filtre"=>false,"deforder"=>""];

?>