<?php
/* config fonction bud_maj_pap.php*/
$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Caractéristiques d'une ligne du PAP","type"=>"Groupe"],
	"lignepap"=>["aff"=>true,"label"=>"Ligne","bdd"=>"BPAP_LIGNEPAP","type"=>"Texte","taille"=>250],
	"idbudget"=>["aff"=>false,"label"=>"","bdd"=>"BPAP_IDBUDGET","type"=>"Texte"],
	"ordre"=>["aff"=>true,"label"=>"Identifiant d'ordre (spérateur -)","bdd"=>"BPAP_ORDRE","type"=>"Texte","taille"=>250],
	"libelle"=>["aff"=>true,"label"=>"Libellé","bdd"=>"BPAP_LIBELLE","type"=>"Texte","taille"=>250],
	"flagtotal"=>["aff"=>true,"label"=>"Ajouter total","bdd"=>"BPAP_FLAGTOTAL","type"=>"Case à cocher"],
	"flagimputation"=>["aff"=>true,"label"=>"Imputation","bdd"=>"BPAP_FLAGIMPUTATION","type"=>"Liste","dataliste"=>array("F"=>"Fonctionnement","I"=>"Investissement")],
	"aeprevfonct"=>["aff"=>true,"label"=>"AE fonctionnement (Euros TTC)","bdd"=>"BPAP_AEPREVFONCT","type"=>"Montant","taille"=>25],
	"cpprevfonct"=>["aff"=>true,"label"=>"CP fonctionnement (Euros TTC)","bdd"=>"BPAP_CPPREVFONCT","type"=>"Montant","taille"=>25],
	"aeprevinves"=>["aff"=>true,"label"=>"AE investissement (Euros TTC)","bdd"=>"BPAP_AEPREVINVES","type"=>"Montant","taille"=>25],
	"cpprevinves"=>["aff"=>true,"label"=>"CP investissement (Euros TTC)","bdd"=>"BPAP_CPPREVINVES","type"=>"Montant","taille"=>25]
];
$descF1=["titre"=>"Administration des lignes du PAP","titresuite"=>true];
?>
