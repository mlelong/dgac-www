<?php
/* config fonction mar_maj_fournisseur.php*/
$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Caractéristiques d'un fournisseur","type"=>"Groupe"],
	"cleparent"=>["aff"=>false,"label"=>"cleparent","bdd"=>"REL_IDMARCHE","type"=>"Texte"],
	"idfournisseur"=>["aff"=>true,"label"=>"Nom du fournisseur","bdd"=>"REL_IDFOURNISSEUR","type"=>"Libre","taille"=>150,"ctl"=>"o"],
	"etat"=>["aff"=>true,"label"=>"Etat","bdd"=>"REL_ETAT","type"=>"Liste","taille"=>150,"ctl"=>"o","dataliste"=>array("candidat"=>"Candidat","titulaire"=>"Titulaire")]
];
$descF1=["titre"=>"Gestion des fournisseurs","titresuite"=>true];
?>
