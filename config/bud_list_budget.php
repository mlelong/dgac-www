<?php
/* config fonction bud_list_budget.php*/
$champT1=[
	"annee"=>["aff"=>true,"label"=>"Année","bdd"=>"BUD_ANNEE","type"=>"Lien","tri"=>true],
	"programme"=>["aff"=>true,"label"=>"Programme","bdd"=>"BUD_PROGRAMME"],
	"libelle"=>["aff"=>true,"label"=>"Description","bdd"=>"BUD_LIBELLE"],
	"aedotafonct"=>["aff"=>true,"label"=>"Dotation AE FON","bdd"=>"BUD_AEDOTAFONCT","type"=>"Montant"],
	"aedotainves"=>["aff"=>true,"label"=>"Dotation AE INV","bdd"=>"BUD_AEDOTAINVES","type"=>"Montant"],
	"cpdotafonct"=>["aff"=>true,"label"=>"Dotation CP FON","bdd"=>"BUD_CPDOTAFONCT","type"=>"Montant"],
	"cpdotainves"=>["aff"=>true,"label"=>"Dotation CP INV","bdd"=>"BUD_CPDOTAINVES","type"=>"Montant"],
	"supp"=>["aff"=>true,"label"=>"Supprimer","type"=>"Lien"]
];
$descT1=["titre"=>"Gestion des budgets","pagination"=>true,"largeur"=>"","nombre"=>20,"filtre"=>false,"deforder"=>""];
?>
