<?php
/* config fonction mar_maj_execution.php*/
$champF1=[
	"fieldset"=>["aff"=>true,"label"=>"Exécution d'un marché","type"=>"Groupe"],
	"coefrevc"=>["aff"=>true,"label"=>"Coefficient courant","bdd"=>"MAR_COEFREVC","type"=>"Montant","taille"=>50],
	"coefrev1"=>["aff"=>true,"label"=>"Coefficient/Date révision 1","bdd"=>"MAR_COEFREV1","type"=>"Montant","debinline"=>true,"taille"=>50],
	"daterev1"=>["aff"=>true,"label"=>"","bdd"=>"MAR_DATEREV1","type"=>"Date","fininline"=>true,"taille"=>50],
	"coefrev2"=>["aff"=>true,"label"=>"Coefficient/Date révision 2","bdd"=>"MAR_COEFREV2","type"=>"Montant","debinline"=>true,"taille"=>50],
	"daterev2"=>["aff"=>true,"label"=>"","bdd"=>"MAR_DATEREV2","type"=>"Date","fininline"=>true,"taille"=>50],
	"coefrev3"=>["aff"=>true,"label"=>"Coefficient/Date révision 3","bdd"=>"MAR_COEFREV3","type"=>"Montant","debinline"=>true,"taille"=>50],
	"daterev3"=>["aff"=>true,"label"=>"","bdd"=>"MAR_DATEREV3","type"=>"Date","fininline"=>true,"taille"=>50],
	"coefrev4"=>["aff"=>true,"label"=>"Coefficient/Date révision 4","bdd"=>"MAR_COEFREV4","type"=>"Montant","debinline"=>true,"taille"=>50],
	"daterev4"=>["aff"=>true,"label"=>"","bdd"=>"MAR_DATEREV4","type"=>"Date","fininline"=>true,"taille"=>50],
	"coefrev5"=>["aff"=>true,"label"=>"Coefficient/Date révision 5","bdd"=>"MAR_COEFREV5","type"=>"Montant","debinline"=>true,"taille"=>50],
	"daterev5"=>["aff"=>true,"label"=>"","bdd"=>"MAR_DATEREV5","type"=>"Date","fininline"=>true,"taille"=>50]
];
$descF1=["titre"=>"Gestion des marchés","titresuite"=>true];
?>
