<?php
require_once 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Alignment;

require_once('class/database.php');
$conn = database::getIntance();

// Cr�ation du excel
$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();

// �criture de l'ent�te
$entete = array("NUMERO DGR","NUMERO SIF","NUMERO SIF OFFICIEL","NOM MARCHE","PROJET/ACTIVITE","NB LOTS","TYPE MARCHE","TYPE PROCEDURE","ETAT",
					"NOM POLE","LIBELLE","PUBLICATION","RETOUR","NOTIFICATION","FIN","DATE RECONDUCTION",
					"DATE REC SUIV","DATE CCTP","DATE CCAP","FLAG RMA","DATE RMA","FLAG CB","DATE CB","TYPE RECONDUC","DUREE INITIALE",
					"NB RECONDUC","DUREE RECONDUC","PREAVIS RECONDUC","MONTANT ESTIME","DESCRIPTION");
$sheet->fromArray($entete, NULL, 'A1');    // Premi�re ligne

// Ex�cution de la requete
$requete = "select MAR_CLE, MAR_NUMERODGR, MAR_NUMEROSIF, MAR_NUMEROOFFICIEL, MAR_NOMMARCHE, 
			PRJ_NOMPROJET, MAR_NBLOT, TMAR_TYPE, TPRO_TYPE, MAR_ETAT, POL_NOMPOLE,  
			MAR_LIBELLE, MAR_DATEPUB, MAR_DATERET, MAR_DATENOT, MAR_DATEFIN, 
			MAR_DATERECCUR, MAR_DATERECSUI, MAR_DATECCTP, MAR_DATECCAP, MAR_FLAGRMA, MAR_DATERMA,
			MAR_FLAGCB, MAR_DATECB, MAR_TYPERECONDUC, MAR_DUREEINITIALE, MAR_NBRECONDUC, MAR_DUREERECONDUC,
			MAR_PREAVISRECONDUC, MAR_AEPREVTOTAL, MAR_DESCRIPTION
			from marche 
			left join projet on MAR_IDPROJET=PRJ_CLE
			left join type_marche on MAR_IDTYPEMARCHE=TMAR_CLE
			left join type_procedure on MAR_IDTYPEPROC=TPRO_CLE
			left join pole on MAR_IDPOLE=POL_CLE
			order by 2 desc";
$statement = $conn->query($requete);
$out = [];  // Tableau avec toutes les lignes
while ($row = $statement->fetch(PDO::FETCH_ASSOC))
{
    $ligne = array(
    	$row['MAR_NUMERODGR'],
    	$row['MAR_NUMEROSIF'],
    	$row['MAR_NUMEROOFFICIEL'],
    	stripslashes($row['MAR_NOMMARCHE']),
    	stripslashes($row['PRJ_NOMPROJET']),
    	$row['MAR_NBLOT'],
    	$row['TMAR_TYPE'],
    	$row['TPRO_TYPE'],
    	$row['MAR_ETAT'],
    	$row['POL_NOMPOLE'],
    	stripslashes($row['MAR_LIBELLE']),
    	$row['MAR_DATEPUB'],
    	$row['MAR_DATERET'],
    	$row['MAR_DATENOT'],
    	$row['MAR_DATEFIN'],
    	$row['MAR_DATERECCUR'],
    	$row['MAR_DATERECSUI'],
    	$row['MAR_DATECCTP'],
    	$row['MAR_DATECCAP'],
    	$row['MAR_FLAGRMA'],
    	$row['MAR_DATERMA'],
    	$row['MAR_FLAGCB'],
    	$row['MAR_DATECB'],
    	$row['MAR_TYPERECONDUC'],
    	$row['MAR_DUREEINITIALE'],
    	$row['MAR_NBRECONDUC'],
    	$row['MAR_DUREERECONDUC'],
    	$row['MAR_PREAVISRECONDUC'],
    	$row['MAR_AEPREVTOTAL'],
    	stripslashes($row['MAR_DESCRIPTION']),
		);
    array_push($out, $ligne);
}
$sheet->fromArray($out, NULL, 'A2'); // Ecrit le tableau dans la feuille

$writer = new Xlsx($spreadsheet);

// Propose de t�l�charger le fichier
$filename = 'marche';
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"');
header('Cache-Control: max-age=0');
$writer->save('php://output');
exit;

