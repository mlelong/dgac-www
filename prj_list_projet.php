<?php
include('config/prj_list_projet.php');
$codefonc='prj';
include('prepage.php');
if (!$objProfil->ctlDroit($typeaction, 'tableau')) exit();

$objPage->debPage('center');

// préparation du tableau
$objTab = new tableau('1');

// suppression d'une entrée
if($typeaction == "suppression")
{
	if(isset($cle))	
	{
		$requete = "delete from projet where PRJ_CLE=\"" . $cle . "\"";
		$objTab->supRequete($requete);
	}
}

// requête d'accès à la base 
$requete = "select PRJ_CLE, PRJ_CODE, PRJ_NOMPROJET, PRG_NOMPROGRAMME, POL_NOMPOLE, PRJ_LIBELLE,
			(select count(*) from commande where CMD_IDPROJET=PRJ_CLE) AS NB_CMD
			from projet	
			left join programme on PRJ_IDPROGRAMME=PRG_CLE 
			left join pole on PRJ_IDPOLE=POL_CLE 
			where PRJ_TYPE=\"Projet\" ";
$requete .= $objTab->majRequete('order by PRJ_NOMPROJET'); // ajout tri et pagination si besoin

// préparation des boutons d'enchainement
$objTab->addBouton("button","EXPORTER","prj_csv_projet.php");
if ($objProfil->cre)  
{
//	if ($objProfil->nom == 'GUYOT') $objTab->addBouton("button","IMPORTER","prj_imp_projet.php");
	$objTab->addBouton("button","AJOUTER","prj_maj_projet.php");
}	

// gestion des paramètres de lien
$objTab->setLien('nomprojet','prj_maj_projet.php',"?typeaction=modification&cle=#PRJ_CLE#");
$objTab->setLien('supp','prj_list_projet.php',"?typeaction=suppression&cle=#PRJ_CLE#","Supprimer");

// affichage du tableau
$objTab->affTableau($requete);

// fin de page
$objPage->finPage();
