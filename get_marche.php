<?php
//indique que le type de la reponse renvoyee au client sera du Texte
header("Content-Type: text/html ; charset=utf-8");

//anti Cache pour HTTP/1.1
header("Cache-Control: no-cache , private");

//anti Cache pour HTTP/1.0
header("Pragma: no-cache");

require_once('connect_base.php');

try 
{
	$conn = new PDO('mysql:host='.$base.';port='.$port.';dbname='.$dbname.';charset=utf8;', $user, $pass); 
} 
catch (PDOException $e) 
{
	echo " ";
	exit; 
}

$requete = "select CONCAT(MAR_NOMMARCHE,\"\t(\",IFNULL(MAR_NUMEROSIF,''),\")\") as MAR_NOMMARCHE 
			from marche where MAR_ETAT=\"Execution\" order by 1";
$statement = $conn->query($requete);
$retour = '';
while ($row = $statement->fetch(PDO::FETCH_ASSOC))
{
	if ( $retour == '') $retour = $row['MAR_NOMMARCHE'];
	else $retour .= "*;*" . $row['MAR_NOMMARCHE'];
}
if (isset($conn)) $conn=null; 
echo $retour;
?>
