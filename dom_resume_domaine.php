<?php
include('config/dom_resume_domaine.php');
$codefonc='*';
include('prepage.php');

$objPage->debPage('center');
$tabnb=[14,26,32,5];

// première partie -> statistique
?>
   <script type="text/javascript">
   $(function(){
		try {google.charts.load('current', {'packages':['corechart','table','gauge']});} catch( ex ) {}	
		google.charts.setOnLoadCallback(drawChart);
		var gaugeOptions = {min: 0, max: 300, yellowFrom: 200, yellowTo: 250,
		redFrom: 250, redTo: 300, minorTicks: 5};
		var gauge;

		function drawChart() {
			// camenbert par domaine d'activité
			var data = google.visualization.arrayToDataTable([
			  ['Task', 'Hours per Day'],
	<?php		  
		echo "['Fournitures', " . $tabnb[0] . "],";
		echo "['Travaux',    " . $tabnb[1] . "],";
		echo "['Services',     " . $tabnb[2] . "],";
		echo "['Autres',     " . $tabnb[3] . "]";
	?>		  
			]);
			var options = {title: ''};
			var chart1 = new google.visualization.PieChart(document.getElementById('piechart1'));
			if ($("#"+idtab).is(":hidden"))	$("#"+idtab).show(); // pour corriger un bug quand chart dans div hidden
			chart1.draw(data, options);
			
			// camenbert par domaine d'activité
			var data = google.visualization.arrayToDataTable([
			  ['Year', 'En cours', 'Nouveau'],
			  ['2011',  70, 25],
			  ['2012',  72, 20],
			  ['2013',  80, 30],
			  ['2014',  68, 15],
			  ['2015',  60, 10],
			  ['2016',  62, 12]
			]);

			var options = {
			  title: '',
			  curveType: 'function',
			  legend: { position: 'bottom' }
			};

			var chart1 = new google.visualization.LineChart(document.getElementById('curve1'));

			chart1.draw(data, options);
			$("#"+idtab).css('display', ''); // pour corriger un bug quand chart dans div hidden
		}
	});
	</script>
		<div class="row"  style="margin-bottom: 15px;" >
			<div class="col-md-offset-2 col-md-4">
				<p class="titretab">Répartition des marchés par année </p>
				<div id="piechart1" style="width: 100%; height: 200px;"></div>
			</div> <!-- fin colonne gauche -->
			<div class="col-md-4">
				<p class="titretab">Répartition des commandes par année </p>
				<div id="curve1" style="width: 100%; height: 200px;"></div>
			</div> <!-- fin colonne droite -->
		</div> <!-- fin row -->	
		<div class="row">
<?php			
// préparation du tableau
$objTab = new tableau('1');

// requête d'accès à la base 
$requete = "select CMD_CLE, CMD_NUMERO, PRJ_NOMPROJET, ETA_NOMETAT
			from commande 
			left join projet on CMD_IDPROJET=PRJ_CLE
			left join fonc_etat on CMD_IDETAT=ETA_CLE
			where 1 order by 2 LIMIT 0,5";

// gestion des paramètres de lien
$objTab->setLien('numero','cmd_tab_commande.php',"?typeaction=modification&cle=#CMD_CLE#&menu=cmd",'href');

// affichage du tableau
$objTab->affTableau($requete);

// préparation du tableau
$objTab = new tableau('2');

// requête d'accès à la base 
$requete = "select MAR_CLE, MAR_NUMERODGR, MAR_NOMMARCHE, MAR_LIBELLE
			from marche where 1 order by 2 LIMIT 0,5";

// gestion des paramètres de lien
$objTab->setLien('nommarche','mar_tab_marche.php',"?typeaction=modification&cle=#MAR_CLE#&menu=mar",'href');

// affichage du tableau
$objTab->affTableau($requete);
?>			
		</div> <!-- fin row -->	
<?php
// fin de page
$objPage->finPage();
