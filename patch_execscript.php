<?php
$codefonc='pat';
require_once('prepage.php');

$retour = '';

// création d'une entrée
if (!isset($typeaction)) $typeaction = "creation";

// réception des paramètres
if($typeaction == "reception")
{
// contrôle des paramètres
	if(isset($_REQUEST['fic'])) $fic=$_REQUEST['fic'];
	else $fic='';
	if ($fic != '')
	{
		if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') 
		{
//			$commande = "C:\wamp\bin\php\php5.5.12\php.exe c:\wamp\www\marche\batch\\" . $fic;
			$commande = "c:\wamp\www\marche\batch\\" . $fic;
			$WshShell = new COM("WScript.Shell");
			$retExec = $WshShell->Run($commande, 0, false);
			$status = 0;
		}
		else
		{	
			$commande = "/home/themis/public_html/batch/" . $fic;
//			$retExec = exec('/usr/bin/php /home/themis/public_html/batch/stat_marche.php >/home/themis/public_html/batch/log.txt 2>&1 ', $result, $status);
			$retExec = exec('/bin/ksh ' . $commande . ' >/home/themis/public_html/batch/log.txt 2>&1 ', $result, $status);
//			print_r($result);
		}
		$retour = "Fichier " . $fic . " lancé = " . $status;
	}
	else $retour = "Pas de fichier a exécuter";
}

// Affichage du début de la page
$objPage->debPage('center');

if($typeaction == "creation") 
{
	$nomrep = './batch';
	$dir = dir($nomrep);
	while ($ficsrc = $dir->read()) // lecture du contenu pour les fichiers
	{
		if ($ficsrc == "." || $ficsrc == "..") continue;
		if (is_dir($nomrep . "/" . $ficsrc)) continue; // c'est un répertoire
		if (is_file($nomrep . "/" . $ficsrc)) // c'est un fichier
		{
			if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') 
			{
				if (pathinfo($ficsrc, PATHINFO_EXTENSION) != 'bat') continue;
			}
			else
			{
				if (pathinfo($ficsrc, PATHINFO_EXTENSION) != 'sh') continue;
			}
			$retour .= "<p>Fichier<span style='font-size:1.6em;color:blue;'> <a href='patch_execscript.php?typeaction=reception&fic=" . $ficsrc . "'> " . $ficsrc . "</a></span></p>";
		}
	}
}

if ($retour != '') $objPage->tampon .= $retour;
$objPage->addBouton("button","RETOUR","patch.php");

// fin du formulaire et de la page
$objPage->finPage();
