<?php
//indique que le type de la reponse renvoyee au client sera du Texte
header("Content-Type: text/html ; charset=utf-8");

//anti Cache pour HTTP/1.1
header("Cache-Control: no-cache , private");

//anti Cache pour HTTP/1.0
header("Pragma: no-cache");

//require_once('fonction.php');

if(isset($_REQUEST['iduo']))
{
	require_once('connect_base.php');
	
	try 
	{
		$conn = new PDO('mysql:host='.$base.';port='.$port.';dbname='.$dbname.';charset=utf8;', $user, $pass); 
	} 
	catch (PDOException $e) 
	{
		echo " ";
		exit; 
	}
	
	$iduo=$_REQUEST['iduo'];
	$requete = "select MUO_MONTANT, MUO_MCOSLA, MUO_MARCHANDISE, MUO_FLAGIMPUTATION, MUO_FLAGREVISION 
				from marche_uo where MUO_CLE=\"" . $iduo . "\" "; 
	$statement = $conn->query($requete);
	$resultat = $statement->fetch(PDO::FETCH_ASSOC);
	$retour = $resultat['MUO_MONTANT'] . "*;*" . $resultat['MUO_MCOSLA'] . "*;*" . $resultat['MUO_MARCHANDISE'] 
				. "*;*" . $resultat['MUO_FLAGIMPUTATION'] . "*;*" . $resultat['MUO_FLAGREVISION'];
	if (isset($conn)) $conn=null; 
	echo $retour;
}
else
{
	echo "KO";
	exit; 
}
