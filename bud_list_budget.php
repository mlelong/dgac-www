<?php
include('config/bud_list_budget.php');
$codefonc='bud';
include('prepage.php');
if (!$objProfil->ctlDroit($typeaction, 'tableau')) exit();

$objPage->debPage('center');

// préparation du tableau
$objTab = new tableau('1');

// suppression d'une entrée
if($typeaction == "suppression")
{
	if(isset($cle))	
	{
		$requete = "delete from budget_ligne where BUDL_IDBUDGET=\"" . $cle . "\"";
		$objTab->supRequete($requete);
		$requete = "delete from budget where BUD_CLE=\"" . $cle . "\"";
		$objTab->supRequete($requete);
	}
}

// requête d'accès à la base 
$requete = "select BUD_CLE, BUD_LIBELLE, BUD_ANNEE, BUD_PROGRAMME,  
			REPLACE(REPLACE(FORMAT(BUD_AEDOTAFONCT,2),',',' '),'.',',') as BUD_AEDOTAFONCT, 
			REPLACE(REPLACE(FORMAT(BUD_CPDOTAFONCT,2),',',' '),'.',',') as BUD_CPDOTAFONCT, 
			REPLACE(REPLACE(FORMAT(BUD_AEDOTAINVES,2),',',' '),'.',',') as BUD_AEDOTAINVES, 
			REPLACE(REPLACE(FORMAT(BUD_CPDOTAINVES,2),',',' '),'.',',') as BUD_CPDOTAINVES
			from budget 
			where 1 ";
$requete .= $objTab->majRequete('order by BUD_ANNEE DESC, BUD_PROGRAMME'); // ajout tri et pagination si besoin

// affichage des boutons d'enchainement
if ($objProfil->cre) $objTab->addBouton("button","AJOUTER","bud_maj_budget.php");

// gestion des paramètres de lien
$objTab->setLien('annee','bud_tab_budget.php',"?typeaction=modification&cle=#BUD_CLE#");
$objTab->setLien('supp','bud_list_budget.php',"?typeaction=suppression&cle=#BUD_CLE#","Supprimer");
//number_format($row['BUD_AEPREVFONCT'], 2, ',', ' ');

// affichage du tableau
$objTab->affTableau($requete);

// fin de page
$objPage->finPage();
