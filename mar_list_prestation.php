<?php
include('config/mar_list_prestation.php');
$codefonc='mar';
include('prepage.php');
if (!$objProfil->ctlDroit($typeaction, 'tableau')) exit();

$objPage->debPage('center');

// suppression d'une entrée
if($typeaction == "suppression")
{
	if(isset($cle))	
	{
		$requete = "delete from marche_prestation where MPRE_CLE=\"" . $cle . "\"";
		$statement = $conn->query($requete);
	}
}

// préparation du tableau
$objTab = new tableau('1');

// requête d'accès à la base 
$requete = "select MPRE_CLE, MPRE_NOMPRESTATION, MPRE_LIBELLE
			from marche_prestation where MPRE_IDMARCHE=\"" . $cleparent . "\"";
$requete .= $objTab->majRequete('order by MPRE_CLE'); // ajout tri et pagination si besoin

// affichage des boutons d'enchainement
if ($objProfil->cre) $objTab->addBouton("button","AJOUTER","mar_maj_prestation.php");
	
// gestion des paramètres de lien
$objTab->setLien('nomprestation','mar_maj_prestation.php',"?typeaction=modification&cle=#MPRE_CLE#");
$objTab->setLien('supp','mar_list_prestation.php',"?typeaction=suppression&cle=#MPRE_CLE#","Supprimer");

// affichage du tableau
$objTab->affTableau($requete);

// fin de page
$objPage->finPage();
