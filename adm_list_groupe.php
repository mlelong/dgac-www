<?php
include('config/adm_list_groupe.php');
$codefonc='gru';
include('prepage.php');
if (!$objProfil->ctlDroit($typeaction, 'tableau')) exit();

$objPage->debPage('center');

// suppression d'une entrée
if($typeaction == "suppression")
{
	if(isset($cle))	
	{
		$requete = "delete from groupe where GRU_CLE=\"" . $cle . "\"";
		$statement = $conn->query($requete);
	}
}

// préparation du tableau
$objTab = new tableau('1');

// requête d'accès à la base 
$requete = "select GRU_CLE, GRU_NOMGROUPE, GRU_DESCRIPTION	from groupe where 1 ";
$requete .= $objTab->majRequete('order by GRU_CLE'); // ajout tri et pagination si besoin

// affichage des boutons d'enchainement
if ($objProfil->cre) $objTab->addBouton("button","AJOUTER","adm_maj_groupe.php");

// gestion des paramètres de lien
$objTab->setLien('nomgroupe','adm_maj_groupe.php',"?typeaction=modification&cle=#GRU_CLE#");
$objTab->setLien('membre','adm_list_groupe-utilisateur.php',"?typeaction=modification&cle=#GRU_CLE#","Liste");
$objTab->setLien('profil','adm_list_groupe-profil.php',"?typeaction=modification&cle=#GRU_CLE#","Liste");
$objTab->setLien('supp','adm_list_groupe.php',"?typeaction=suppression&cle=#GRU_CLE#","Supprimer");

// affichage du tableau
$objTab->affTableau($requete);

// fin de page
$objPage->finPage();
