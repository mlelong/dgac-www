<?php
$codefonc='dom';
include('prepage.php');

// début de page
$objPage->debPage('center');

$objOnglet = new onglet();

// début de l'onglet
$objOnglet->debOnglet();

// creation des liens
$objOnglet->debLienOnglet();
//$objOnglet->addLienOnglet('syn','url','Synthèse','dom_resume_domaine.php?typeaction=' . $typeaction . '&cle=' . $cle);
$objOnglet->addLienOnglet('car','url','Caractéristiques','dom_maj_domaine.php?typeaction=' . $typeaction . '&cle=' . $cle);
$objOnglet->addLienOnglet('pol','url','Pôles','dom_list_pole.php?cleparent=' . $cle);
$objOnglet->finLienOnglet();

// fin de l'onglet
$objOnglet->finOnglet();

// fin de page
$objPage->finPage();
