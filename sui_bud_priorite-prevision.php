<?php
include('config/sui_bud_priorite-prevision.php');
$codefonc='rpm';
include('prepage.php');
if (!$objProfil->ctlDroit($typeaction, 'tableau')) exit();

$objPage->debPage('center');

//*******************************************************************
// remplissage des objets Nature de dépense 
//*******************************************************************

$objTab = new tableau('1');

$objTab->addBouton("reset","RETOUR","sui_list_rapport.php");

if (!isset($f_annee) || $f_annee == '') $f_annee = date("Y");
if (!isset($f_programme) || $f_programme == '') $f_programme = "P613";

// requête d'accès à la base pour récupérer les commandes
$requete = "select BUDL_CLE, BUDL_IDBUDGET, BUDL_AEDOTAFONCT, BUDL_AEDOTAINVES, 
			BUDL_PRIORITE, BUD_ANNEE, BUD_PROGRAMME			
			from budget_ligne
			left join budget on BUDL_IDBUDGET=BUD_CLE
			where 1 ";
$requete .= $objTab->majRequete('order by 1'); // ajout tri et pagination si besoin
$statement = $conn->query($requete);

// création des objets nature de dépense
$totalP0 = 0;
$totalP1 = 0;
$totalP2 = 0;
$totalP3 = 0;
$totalP4 = 0;
while ($row = $statement->fetch(PDO::FETCH_ASSOC))
{
	// calcul du montant TTC
	$totalAE = (double)$row['BUDL_AEDOTAFONCT'] + (double)$row['BUDL_AEDOTAINVES'];

	// affectation du montant
	if ($row['BUDL_PRIORITE'] == 'P0') $totalP0 += (double)$totalAE; 
	if ($row['BUDL_PRIORITE'] == 'P1') $totalP1 += (double)$totalAE; 
	if ($row['BUDL_PRIORITE'] == 'P2') $totalP2 += (double)$totalAE; 
	if ($row['BUDL_PRIORITE'] == 'P3') $totalP3 += (double)$totalAE; 
	if ($row['BUDL_PRIORITE'] == '') $totalP4 += (double)$totalAE; 
}

//*******************************************************************
// Affichage du tableau 
//*******************************************************************
// préparation du tableau
$objTab->setTableAttr("align='right'");
// affichage du tableau
$ligne = array();
$total = 0;
$totalt1 = 0;
$totalt2 = 0;
$totalt3 = 0;
$totalt4 = 0;

// affichage de la ligne priorité 0	
$ligne[0] = 'Priorité P0';
$ligne[1] = number_format($totalP0,0,',',' ');
$total += $totalP0;
$ligne[2] = number_format($total,0,',',' ');
$objTab->affLigne($ligne);

// affichage de la ligne priorité 1	
$ligne[0] = 'Priorité P1';
$ligne[1] = number_format($totalP1,0,',',' ');
$total += $totalP1;
$ligne[2] = number_format($total,0,',',' ');
$objTab->affLigne($ligne);

// affichage de la ligne priorité 2	
$ligne[0] = 'Priorité P2';
$ligne[1] = number_format($totalP2,0,',',' ');
$total += $totalP2;
$ligne[2] = number_format($total,0,',',' ');
$objTab->affLigne($ligne);

// affichage de la ligne priorité 3	
$ligne[0] = 'Priorité P3';
$ligne[1] = number_format($totalP3,0,',',' ');
$total += $totalP3;
$ligne[2] = number_format($total,0,',',' ');
$objTab->affLigne($ligne);

// affichage de la ligne priorité 4	
$ligne[0] = 'Priorité non renseignée';
$ligne[1] = number_format($totalP4,0,',',' ');
$total += $totalP4;
$ligne[2] = number_format($total,0,',',' ');
$objTab->affLigne($ligne);

$ligne[0] = 'TOTAL';
$ligne[1] = number_format($total,0,',',' ');
$ligne[2] = number_format($total,0,',',' ');
$objTab->affLigne($ligne);

$objTab->finTableau();

// fin de page
$objPage->finPage();
?>
