<?php
include('config/sui_bud_nature.php');
$codefonc='rpm';
include('prepage.php');
if (!$objProfil->ctlDroit($typeaction, 'tableau')) exit();

$objPage->debPage('center');

//*******************************************************************
// table des natures de dépense 
//*******************************************************************
$tabnature = [
	'AMOA'=>[0,0,0,0,0],
	'AMOE'=>[0,0,0,0,0],
	'Prestation de service'=>[0,0,0,0,0],
	'Prestation intellectuelle'=>[0,0,0,0,0],
	'Achat licence technique'=>[0,0,0,0,0],
	'Maintenance licence technique'=>[0,0,0,0,0],
	'Achat matériel'=>[0,0,0,0,0],
	'Renouvellement matériel'=>[0,0,0,0,0],
	'Maintenance matériel'=>[0,0,0,0,0],
	'Achat licence applicative'=>[0,0,0,0,0],
	'Maintenance licence applicative'=>[0,0,0,0,0],
	'Achat développement'=>[0,0,0,0,0],
	'Maintenance développement'=>[0,0,0,0,0],
	'Abonnement réseau'=>[0,0,0,0,0],
	'Logistique'=>[0,0,0,0,0],
	'RH'=>[0,0,0,0,0]
	];

//*******************************************************************
// remplissage des objets Nature de dépense 
//*******************************************************************

$objTab = new tableau('1');

$objTab->addBouton("reset","RETOUR","sui_list_rapport.php");

if (!isset($f_annee) || $f_annee == '') $f_annee = date("Y");
if (!isset($f_programme) || $f_programme == '') $f_programme = "P613";

// requête d'accès à la base pour récupérer les commandes
$requete = "select LCMD_CLE, LCMD_TOTALHT, LCMD_TYPEIMPUTATION, GCMD_TYPEIMPUTATION, 
			CMD_TYPEIMPUTATION, CMD_TVA, CMD_IDBUDGET, CMD_DATECRE, CMD_DATESOU, CMD_DATECMD,
			BUD_ANNEE, BUD_PROGRAMME			
			from commande_ligne
			left join commande_groupe_ligne on LCMD_IDGCMD=GCMD_CLE			
			left join commande on LCMD_IDCMD=CMD_CLE			
			left join budget on CMD_IDBUDGET=BUD_CLE
			where 1 ";
$requete .= $objTab->majRequete('order by 1'); // ajout tri et pagination si besoin
$statement = $conn->query($requete);

// création des objets nature de dépense
while ($row = $statement->fetch(PDO::FETCH_ASSOC))
{
	// recherche du type d'imputation
	if ($row['LCMD_TYPEIMPUTATION'] != '') $typeimpuation = $row['LCMD_TYPEIMPUTATION'];
	else if ($row['GCMD_TYPEIMPUTATION'] != '') $typeimpuation = $row['GCMD_TYPEIMPUTATION'];
	else if ($row['CMD_TYPEIMPUTATION'] != '') $typeimpuation = $row['CMD_TYPEIMPUTATION'];
	else $typeimpuation = '';
	if ($typeimpuation == '') continue;
	if (!isset($tabnature[$typeimpuation])) continue;
	
	// détermination du trimestre
	if ($row['CMD_DATECMD'] != '' && $row['CMD_DATECMD'] != '0000-00-00') $datetrim = $row['CMD_DATECMD'];
	else if ($row['CMD_DATESOU'] != '' && $row['CMD_DATESOU'] != '0000-00-00') $datetrim = $row['CMD_DATESOU'];
	else $datetrim = '';
	if ($datetrim == '') continue;
	$tab = explode('-',$datetrim);
	if (isset($tab[1])) $MM = $tab[1];
	else continue;
	if ($MM > 0 and $MM < 4) $trimestre = 1;
	else if ($MM > 3 and $MM < 7) $trimestre = 2;
	else if ($MM > 6 and $MM < 10) $trimestre = 3;
	else $trimestre = 4;
	
	// calcul du montant TTC
	$totalttc = (double)$row['LCMD_TOTALHT'] + (double)(($row['LCMD_TOTALHT']*$row['CMD_TVA'])/100);

	// affectation du montant
	$tabnature[$typeimpuation][0] += (double)$totalttc;
	$tabnature[$typeimpuation][$trimestre] += (double)$totalttc;
}

//*******************************************************************
// Affichage du tableau 
//*******************************************************************
// préparation du tableau
$objTab->setTableAttr("align='right'");
// affichage du tableau
$ligne = array();
$total = 0;
$totalt1 = 0;
$totalt2 = 0;
$totalt3 = 0;
$totalt4 = 0;
foreach($tabnature as $nature => $tabtrimestre)
{
	// affichage de la ligne	
	$ligne[0] = $nature;
	if ($tabtrimestre[0] > 0) 
	{
		$ligne[1] = number_format($tabtrimestre[0],0,',',' ');
		$total += $tabtrimestre[0];
	}	
	else $ligne[1] = '';
	if ($tabtrimestre[1] > 0)
	{
		$ligne[2] = number_format($tabtrimestre[1],0,',',' ');
		$totalt1 += $tabtrimestre[1];
	}	
	else $ligne[2] = '';
	if ($tabtrimestre[2] > 0)
	{
		$ligne[3] = number_format($tabtrimestre[2],0,',',' ');
		$totalt2 += $tabtrimestre[2];
	}	
	else $ligne[3] = '';
	if ($tabtrimestre[3] > 0)
	{
		$ligne[4] = number_format($tabtrimestre[3],0,',',' ');
		$totalt3 += $tabtrimestre[3];
	}	
	else $ligne[4] = '';
	if ($tabtrimestre[4] > 0)
	{
		$ligne[5] = number_format($tabtrimestre[4],0,',',' ');
		$totalt4 += $tabtrimestre[4];
	}	
	else $ligne[5] = '';
	$objTab->affLigne($ligne);
}
$ligne[0] = 'TOTAL';
$ligne[1] = number_format($total,0,',',' ');
$ligne[2] = number_format($totalt1,0,',',' ');
$ligne[3] = number_format($totalt2,0,',',' ');
$ligne[4] = number_format($totalt3,0,',',' ');
$ligne[5] = number_format($totalt4,0,',',' ');
$objTab->affLigne($ligne);

$objTab->finTableau();

// fin de page
$objPage->finPage();
?>
