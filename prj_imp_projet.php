<?php
include('config/prj_imp_projet.php');
$codefonc='prj';
require_once('prepage.php');

$objForm = new formulaire('1');

// création
if (!isset($typeaction) || $typeaction == '') $typeaction = 'creation';
if($typeaction == "creation") $objForm->initChamp();

// réception des paramètres
if($typeaction == "reception")
{
// contrôle des paramètres
	$objForm->recChamp();
	while ($objForm->erreur == '')
	{
		break;
	}
	if ($objForm->erreur == '')
	{
		$rapport = "";
		// récupération du fichier
		if ((isset($_FILES['nomfic']['name'][0])&&($_FILES['nomfic']['error'][0] == UPLOAD_ERR_OK)))
		{
			$namefic = ctlNomFichier($_FILES['nomfic']['name'][0]);
			$chemin_destination = "document/" . $namefic;   
			move_uploaded_file($_FILES['nomfic']['tmp_name'][0], $chemin_destination); 
		}
		if (!file_exists($chemin_destination)) // vérification pour savoir si le fichier existe
			{$objForm->erreur="nomfic"; $objForm->libelle="Le fichier " . $namefic . " n'a pas pu être chargé"; break;}
		//
		// création ou ajout des lignes pour le budget
		//
		$conn->setAttribute(PDO::ATTR_AUTOCOMMIT,0); 
		$conn->beginTransaction();
		$flag = false;
		while(true)
		{
			$fichier = "document/" . $namefic;
//			Trace("Traitement du fichier " . $fichier);
			$handle = fopen ($fichier,"r");
			// entete
			$ligne = fgetcsv($handle, 1000, ";");
			// test si utf8
//			echo 'resultat test encoding =' . mb_detect_encoding($ligne[0]) . "=";
			if (mb_detect_encoding($ligne[0]) != 'UTF-8') $flagutf8 = true;
			else $flagutf8 = false;
//			if ($flagutf8) echo "pas UTF-8";
			if ($flagutf8) $ligne = array_map("utf8_encode",$ligne);
			// remplace
			if ($flagsup == 'o')
			{
//				Trace("Suppression des lignes existantes");
				$requete = "TRUNCATE TABLE projet";
				$statement = $conn->query($requete);
				$requete = "TRUNCATE TABLE programme";
				$statement = $conn->query($requete);
			}
			// 
			// traitement du fichier
			//
//			Trace("Début de la boucle des lignes");
			while ($ligne = fgetcsv($handle, 1000, ";")) 
			{
				if (!isset($ligne[0])) continue;
//				print_r($ligne);
				if ($flagutf8) $ligne = array_map("utf8_encode",$ligne);
				// informations lignes
				$cleprojet = $ligne[0];
				$type = $ligne[1];
				$code = $ligne[2];
				$nomprojet = $ligne[3];
				$libelle = $ligne[4];
				$idpole = $ligne[5];
				$nompole = $ligne[6];
				$idprogramme = $ligne[7];
				$glibelle = $ligne[8];
				// insertion du groupe de projets/activités	
				$requete = "insert into programme (PRG_CLE, PRG_NOMPROGRAMME)
							values (\"" . $idprogramme . "\",\"" . addslashes($glibelle)  . "\")
							ON DUPLICATE KEY UPDATE  
									PRG_NOMPROGRAMME=\"" . addslashes($glibelle) . "\"";
				// insertion du projets/activités	
				$requete = "insert into projet (PRJ_CLE, PRJ_TYPE, PRJ_CODE, PRJ_NOMPROJET, PRJ_LIBELLE, PRJ_IDPOLE, PRJ_IDPROGRAMME)
							values (\"" . $cleprojet . "\",\"" . $type . "\",\"" . $code . "\",\"" . addslashes($nomprojet) 
									. "\",\"" . addslashes($libelle) . "\",\"" . $idpole	. "\",\"" . $idprogramme . "\")
							ON DUPLICATE KEY UPDATE  
									PRJ_TYPE=\"" . $cleprojet . "\",
									PRJ_CODE=\"" . $type . "\",
									PRJ_NOMPROJET=\"" . addslashes($nomprojet) . "\",
									PRJ_LIBELLE=\"" . addslashes($libelle) . "\",
									PRJ_IDPOLE=\"" . $idpole . "\",
									PRJ_IDPROGRAMME=\"" . $idprogramme . "\"";
				$statement = $conn->query($requete);
			}
			fclose ($handle);
			$conn->commit(); 
			$flag = true;
//			Trace("COMMIT");
			break;
		}
		if ($flag == false) 
		{
//			Trace("ROLLBACK");
			$conn->rollBack();
			$objForm->erreur="nomfic";
			if ($objForm->libelle == '') $objForm->libelle = "Erreur sur chargement des projets/activités"; 
		}
	}
	$result= unlink($fichier); // suppression du fichier
	if ($objForm->libelle == '')
	{
		RedirURL("prj_list_projet.php");
		exit();
	}
}

// Affichage du début de la page
if ($cle != '') $objPage->addParam('cle', $cle);
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();

// affichage des boutons d'enchainement
$objForm->addBouton("reset","RETOUR","prj_list_projet.php");
if (($cle == '' && $objProfil->cre) || ($cle != '' && $objProfil->maj))
{
	$objForm->addBouton("submit","ENVOYER");
}
else $objForm->setLecForm();

$objForm->affFormulaire();
$objForm->finFormulaire();

// fin de page
$objPage->finPage();
