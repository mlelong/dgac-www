<?php
include('config/patch_apply.php');

$codefonc='pat';
include('prepage.php');

$objPage->debPage('center');

if (isset($_REQUEST['dirsrc'])) $dirsrc = $_REQUEST['dirsrc'];
else $dirsrc = '';
if (isset($_REQUEST['ficsrc'])) $ficsrc = $_REQUEST['ficsrc'];
else $ficsrc = '';

// suppression d'une entrée
if($typeaction == "suppression")
{
	if($dirsrc != '' && $ficsrc != '')
	{
		$fic = "patch/" . $dirsrc . "/" . $ficsrc;
		unlink($fic);
	}
}
if($typeaction == "reception")
{
	$nbfic = 0;
	if (isset($_REQUEST['nbfic'])) $nbfic = $_REQUEST['nbfic'];
//	$objPage->tampon .= "nb patch = " . $nbfic . "<br>";
	for($cptfic=0;$cptfic<$nbfic; $cptfic++)
	{
//		$objPage->tampon .= "test patch-" . $cptfic . "<br>";
		if (isset($_REQUEST['patch-' . $cptfic])) 
		{
			$fic = $_REQUEST['patch-' . $cptfic];
			$tab = explode(";", $fic);
			$objPage->tampon .= "reception patch-" . $cptfic . " = " . $fic . "<br>";
			if (!isset($tab[0])) continue;
			if (!isset($tab[1])) continue;
			if ($tab[0] == '' || $tab[1] == '') continue;
			$dirsrc = $tab[0];
			$ficsrc = $tab[1];
			$nomficsrc = "patch/" . $dirsrc . "/" . $ficsrc; // origine du patch à appliquer
			if ($dirsrc == 'page') $nomficdest = $ficsrc; // destination du patch
			else $nomficdest = $dirsrc . "/" . $ficsrc;
			$nomficold = "patch/old_patch/" . $dirsrc . "/" . $ficsrc; // sauvegarde du patch de destination
			if (!file_exists($nomficsrc)) // vérification pour savoir si le patch à appliquer est bien là
			{
				$objPage->tampon .= "patch inconnu = " . $dirsrc . "/" . $ficsrc . "<br>"; 
				continue;
			}
			if (file_exists($nomficdest)) // sauvegarde si le fichier destination existe
			{
				$objPage->tampon .= "Sauvegarde du patch = " . $nomficdest . " vers " . $nomficold . "<br>"; 
				$result = copy($nomficdest, $nomficold);
				if ($result === false)
				{
					$objPage->tampon .= "Erreur sur la sauvegarde du patch = " . $dirsrc . "/" . $ficsrc . "<br>"; 
					continue;
				}
			}
			$objPage->tampon .= "Copie du patch = " . $nomficsrc . " vers " . $nomficdest . "<br>"; 
			$result = copy($nomficsrc, $nomficdest);
			if ($result === false)
			{
				$objPage->tampon .= "Erreur sur la copie du patch = " . $dirsrc . "/" . $ficsrc . "<br>"; 
				unlink($nomficold); // suppression de la sauvegarde
				continue;
			}
			if ($dirsrc == 'SQL') // traitement particulier si patch base de données
			{
				$current = file_get_contents($nomficdest);
				$tab = explode(";", $current);
				for($i=0; $i<count($tab); $i++)
				{
					$requete = $tab[$i];
					$requete = str_replace("\r", "", $requete);
					$requete = str_replace("\n", "", $requete);
					if ($requete == "\n" || $requete == "\r" || $requete == "\n\r" || $requete == "\r\n" || $requete == '') continue;
					if ($requete[0] == "*" || $requete[0] == "#" || $requete[0] == "/") continue;
					$objPage->tampon .= "Requete " . $i . " = " . $requete . "<br>";
//					$statement = $conn->query($requete);
				}
			}
		}
	}	
}

// préparation du tableau
$objTab = new tableau('1');

// affichage du tableau
$cptfic = 0;
$ligne = array();
$dir1 = dir("patch");
while ($dirsrc = $dir1->read()) // lecture des répertoires
{
	if($dirsrc == "." || $dirsrc == "..") continue;
	if($dirsrc == "old_patch") continue;
	$dir2 = dir("patch/" . $dirsrc);
	while ($ficsrc = $dir2->read()) // lecture des fichiers
	{
		if($ficsrc == "." || $ficsrc == "..") continue;
		$ligne[0] = $dirsrc;
		$ligne[1] = $ficsrc;
		$ligne[2] = "<input type='checkbox' name='patch-" . $cptfic . "' value=\"" . $dirsrc . ";" . $ficsrc . "\">";
		$param = "?typeaction=suppression&dirsrc=" . $dirsrc . "&ficsrc=" . $ficsrc;
		$ligne[3] = "<a " . VersURL("patch_apply.php" . $param,"supp") . " ><i class='fa fa-trash'></i></a>";
		$objTab->affLigne($ligne);
		$cptfic++;
	}
}

$objPage->tampon .= "Nb patch au total : <input type='text' name ='nbfic' value ='" . $cptfic . "' />"; 

// affichage des boutons d'enchainement
$objTab->addBouton("button","RETOUR","patch.php");
$objTab->addBouton("submit","VALIDER");

$objTab->finTableau();

// fin de page
$objPage->finPage();
