<?php
include('config/cmd_maj_commande.php');
$codefonc='cmd';
require_once('prepage.php');

$objForm = new formulaire('1');
$objWorkflow = new workflow();

// création
if($typeaction == "creation") $objForm->initChamp();
$totalht = 0;
$lastidetat = 0;
$nommarche = '';
$typeuo = '';

// modification
if($typeaction == "modification")
{
	$requete = "select CMD_CLE, CMD_NUMERO, CMD_NUMEROSIF, CMD_OBJET,
				CMD_IDPOLE, CMD_IDMARCHE, CMD_IDBUDGET, CMD_IDLIGNEBUDGET, CMD_IDPROJET, 
				CMD_IDDEMANDEUR, CMD_DEMANDEUR, CMD_IDINTERVENANT, CMD_INTERVENANT, CMD_COEFREV, 
				CMD_URGENCE, CMD_PRIORITE, CMD_PROGRAMME, CMD_TYPEIMPUTATION, CMD_LIEU, CMD_TVA,  
				DATE_FORMAT(CMD_DATECRE, '%d-%m-%Y') AS CMD_DATECRE,
				DATE_FORMAT(CMD_DATESOU, '%d-%m-%Y') AS CMD_DATESOU,
				DATE_FORMAT(CMD_DATEPRE, '%d-%m-%Y') AS CMD_DATEPRE,
				DATE_FORMAT(CMD_DATECMD, '%d-%m-%Y') AS CMD_DATECMD,
				DATE_FORMAT(CMD_DATELIVA, '%d-%m-%Y') AS CMD_DATELIVA,
				DATE_FORMAT(CMD_DATEVA, '%d-%m-%Y') AS CMD_DATEVA,
				DATE_FORMAT(CMD_DATEVSR, '%d-%m-%Y') AS CMD_DATEVSR,
				DATE_FORMAT(CMD_DATESERV, '%d-%m-%Y') AS CMD_DATESERV,
				DATE_FORMAT(CMD_DATEDEBMCO, '%d-%m-%Y') AS CMD_DATEDEBMCO,
				DATE_FORMAT(CMD_DATEFINMCO, '%d-%m-%Y') AS CMD_DATEFINMCO,
				CMD_TOTALHT, CMD_IDETAT, CMD_COMMENTAIRE, MAR_TYPEUO,
				CONCAT(MAR_NOMMARCHE,\"\t(\",IFNULL(MAR_NUMEROSIF,''),\")\") as MAR_NOMMARCHE
				from commande 
				left join marche on CMD_IDMARCHE=MAR_CLE
				where CMD_CLE=\"" . $cle . "\"";
	$resultat = $objForm->mapChamp($requete);
	$lastidetat = $idetat;
	$totalht = $resultat['CMD_TOTALHT'];
	$nommarche = $resultat['MAR_NOMMARCHE'];
	$typeuo = $resultat['MAR_TYPEUO'];
}

// réception des paramètres
if($typeaction == "reception")
{
// contrôle des paramètres
	$objForm->recChamp();
	// gestion du demandeur et de l'intervenant
	if ($iddemandeur == 0 || $iddemandeur == '')
	{
		$demandeur = $objProfil->nom;
		$iddemandeur = $objProfil->idnom;
	}
	else
	{
		$tab = explode('*-*', $demandeur);
		$iddemandeur = $tab[0];
		$demandeur = $tab[1];
	}
	if ($intervenant != '')
	{
		$tab = explode('*-*', $intervenant);
		if (isset($tab[1]))
		{
			$idintervenant = $tab[0];
			$intervenant = $tab[1];
		}
	}
	else
	{
		$idintervenant = 0;
		$intervenant = '';
	}
	if (!isset($datecre) || $datecre == '' || $datecre == '0000-00-00') {$datecre = date('d-m-Y'); $MYSQLdatecre=date('Y-m-d');} // jour courant par défaut
	if ($idetat == '' || $idetat == 0) 
	{
		if ($cle == '') $idetat = 1;
		else $idetat = $lastidetat;
	}	
	// gestion des erreurs
	if ($objForm->erreur != '')
	{
		$nommarche = $idmarche;
	}
	while($objForm->erreur == '')
	{
		// pour l'autocomplete il faut transformer les noms en ID
		$nommarche = $idmarche;
		$tab = explode("\t",$idmarche);
		if (isset($tab[0])) $temp = $tab[0];
		else $temp = '';
		$requete = "select MAR_CLE from marche where MAR_NOMMARCHE=\"" . addslashes($temp) . "\" ";
		$statement = $conn->query($requete);
		$resultat = $statement->fetch(PDO::FETCH_ASSOC);
		$idmarche = $resultat['MAR_CLE'];
		if ($idmarche == '' ||  $idmarche == 0) {$objForm->erreur='idmarche'; $objForm->libelle="Ce marché n'existe pas"; break;}
		// cohérence EJ et date EJ
		if ($numerosif != '' || ($datecmd != '' && $datecmd != '00-00-0000'))
		{
			if ($numerosif == '') {$objForm->erreur='numerosif'; $objForm->libelle="Date d'EJ renseignée sans numéro d'EJ"; break;}
			if ($datecmd == '' || $datecmd == '00-00-0000') {$objForm->erreur='datecmd'; $objForm->libelle="Numéro d'EJ renseigné sans date d'EJ"; break;}
		}
		// controle de cohérence des dates
		if (!isset($dateliva) || $dateliva == '' || $datecre == '00-00-0000') {$datecre = date('d-m-Y'); $MYSQLdatecre=date('Y-m-d');} // jour courant par défaut

		// en création la ligne budgétaire doit être renseignée
		if ($cle == '' && $idlignebudget == '') 
			{$objForm->erreur='idlignebudget'; $objForm->libelle="La ligne budgétaire doit être renseignée"; break;}

		// séparer id budget et projet
		$tab = explode("*;*", $idlignebudget);
		if (isset($tab[0])) $idlignebudget = $tab[0];
		else $idlignebudget = 0;
		if (isset($tab[1])) $idprojet = $tab[1];
		else $idprojet = 0;
		// création de l'objet commande
		$objCommande = new commande($conn, 'maj', 'commande', $cle);
		$objCommande->setIdBudget($idbudget);
		$objCommande->setIdLigneBudget($idlignebudget);
		// récupération de la révision
		if ($cle == '') $objCommande->setMarche($idmarche);
		$coefrev = $objCommande->getRevision('','force');
		if ($coefrev === false) $coefrev = 1; 
		// tva
		break;
	}
	if ($objForm->erreur == '')
	{
		// mise à jour de la commande
		$conn->setAttribute(PDO::ATTR_AUTOCOMMIT,0); 
		$conn->beginTransaction();
		$flag = false;
		while(true)
		{
			if ($cle == '')
			{
				//
				// constitution du numéro de facture si création
				//
				$requete = "update specif set SPE_NUMFACTURE=SPE_NUMFACTURE+1 where SPE_CLE=1";
				$statement = $conn->query($requete);
				if (!$statement) break; 
				$requete = "select SPE_NUMFACTURE from specif where SPE_CLE=1";
				$statement = $conn->query($requete);
				if (!$statement) break; 
				$resultat = $statement->fetch(PDO::FETCH_ASSOC);
				$numero = $resultat['SPE_NUMFACTURE'];
			}
			// gestion de la date de soumission
			if ($lastidetat == 1 && $idetat == 2) {$datesou = date("d-m-Y"); $MYSQLdatesou = date('Y-m-d');}
			//
			// mise à  jour de la commande
			//
			if ($objForm->majBdd('commande', 'CMD_CLE', $cle) === false) break;
			$objForm->libelle="Mise à jour effectuée";
			$newcle = $conn->lastInsertId();
			//
			// Mise à jour du budget global et du budget du projet si modification de la commande
			//
			if ($cle != '')
			{
				if (!$objCommande->majCommande($numerosif, $tva)) break;
			}	
			$conn->commit(); 
			$flag = true;
			break;
		}
		if ($flag == false) 
		{
			$conn->rollBack();
			$objForm->erreur='commande';
			$objForm->libelle=$objCommande->getLibelle();
			if ($objForm->libelle == '') $objForm->libelle="Erreur sur mise à jour de la commande"; 
		}
		else
		{
			// envoi de mail si changement d'état avec notification
			if ($idetat != $lastidetat)
			{
/*			
				require('lib/PHPMailer/class.phpmailer.php'); 
				require('lib/PHPMailer/class.smtp.php'); 
	 
				$mail = new PHPMailer();
				$mail->IsSMTP();
				$mail->Host = 'mail.lfpo.aviation-civile.gouv.fr';
				$mail->SMTPAuth   = true;
				$mail->Port = 587; // Par défaut
				 
				// Authentification
				$mail->Username = "marc.guyot@aviation-civile.gouv.fr";
				$mail->Password = "Password";
				 
				// Expéditeur
				$mail->SetFrom('marc.guyot@aviation-civile.gouv.fr', 'Guyot Marc');
				// Destinataire
				$mail->AddAddress('marc.guyot@aviation-civile.gouv.fr', 'Guyot Marc');

				// Objet
				$mail->Subject = utf8_decode("[Commande] commande numéro " . $numero . " en attente de validation");
				// Votre message
				$mail->MsgHTML('Contenu du message en HTML');				 
				// Envoi du mail avec gestion des erreurs
				if(!$mail->Send()) {
				  echo 'Erreur : ' . $mail->ErrorInfo;
				} else {
				  echo 'Message envoyé !';
				} 
*/
				$requete = "select ETA_NOMETAT FROM fonc_etat where ETA_CLE=\"".$idetat."\"";
				$statement = $conn->query($requete);
				$resultat = $statement->fetch(PDO::FETCH_ASSOC);
				$libetat = $resultat['ETA_NOMETAT'];
				$objEmail = new email;
				$objEmail->initEmail();
				$objetmail = "[Commande] commande " . $numero . " état(".$libetat.")";
				$objEmail->addObjet($objetmail);
				$titre = "commande numéro " . $numero;
				$objEmail->addTitre($titre);
				$objEmail->addInfos("date de création", $datecre);
				$objEmail->addInfos("Nom du demandeur", $objProfil->nom);
				$objEmail->addInfos("Objet", $objet);
				$lien = dirname($_SERVER['HTTP_REFERER']); 
				if ($newcle == 0) $newcle = $cle;
				$objEmail->addLien($lien . "/cmd_tab_commande.php?typeaction=modification&cle=".$newcle."&menu=cmd");
				// récupération des destinataires si workflow
				$destinataires = $objWorkflow->getDestinataires('cmd', $idetat);
				// ajout du demandeur
				$requete = "select UTI_MAIL from utilisateur where UTI_CLE=\"".$iddemandeur."\"";
				$statement = $conn->query($requete);
				$resultat = $statement->fetch(PDO::FETCH_ASSOC);
				if ($destinataires == '') $destinataires = $resultat['UTI_MAIL'];
				else $destinataires .= "," . $resultat['UTI_MAIL'];
				if ($destinataires != '') 
				{
					$objEmail->addDestinataires($destinataires);
					$objEmail->sendEmail();
				}	
			}
			$objForm->libelle="Mise à jour effectuée";
			if ($cle == '') RedirURL("cmd_tab_commande.php?typeaction=modification&cle=".$newcle);
		}
	}
}

// Affichage du début de la page
if ($cle != '') $objPage->addParam('cle', $cle);
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();

// les noms de programme et de projet ne sont pas modifiables
$objForm->setOpt('nomprogramme', 'disabled');
$objForm->setOpt('nomprojet', 'disabled');

if ($cle == '') 
{
	$objForm->setAff('numero', false);
	$objForm->setAff('datecre', false);
	$tva = 20;
}	
else
{
	if ($totalht != 0 && $typeuo != "Sans UO") $objForm->setOpt('idmarche', 'disabled');
	$objForm->setOpt('numero', 'disabled');
	$objForm->setOpt('datecre', 'disabled');
	$objForm->setOpt('datesou', 'disabled');
}

// si le profil n'autorise pas l'exécution certain champs ne sont pas accessibles
if (!$objProfil->exe)
{
	$objForm->setOpt('priorité', 'disabled');
	$objForm->setOpt('numerosif', 'disabled');
	$objForm->setOpt('datecmd', 'disabled');
	$objForm->setOpt('intervenant', 'disabled');
//	$objForm->setOpt('programme', 'disabled');
}

// sélection du pôle
if ($typeaction == "creation")
{
	$requete = "select RPU_IDPOLE from pole_utilisateur where RPU_IDUTILISATEUR=\"" . $objProfil->idnom . "\"";
	$statement = $conn->query($requete);
	$resultat = $statement->fetch(PDO::FETCH_ASSOC);
	if ($resultat['RPU_IDPOLE'] > 0) $idpole = $resultat['RPU_IDPOLE'];
	else $idpole = 0;
	$programme = "P613";
	$urgence = "Moyenne";
	$lieu = "Athis-mons";
	$datepre = date("d-m-Y");
}

// programme par défaut
if ($programme == '') $programme = "P613";;

// selection du pôle
$requete = "select POL_CLE, POL_NOMPOLE from pole order by 2";
$objForm->setSelect('idpole', $idpole, $requete, 'POL_CLE', 'POL_NOMPOLE');

// Pour l'autocomplete il faut transformer l'ID du marché en nom
if ($idmarche == 0) $idmarche = '';
else $idmarche = $nommarche;

// selection de l'année budgétaire en fonction du programme
$requete = "select BUD_CLE, BUD_ANNEE from budget where BUD_PROGRAMME=\"" . $programme . "\" order by 2 desc";
$objForm->setSelect('idbudget', $idbudget, $requete, 'BUD_CLE', 'BUD_ANNEE');

// selection de la ligne budgétaire en fonction du budget 
$requete = "select CONCAT(BUDL_CLE,\"*;*\",PRJ_CLE) as BUDL_CLE,
				CONCAT(PRJ_NOMPROJET,\" ->\t\",IFNULL(BUDL_LIBELLE,'')) as PRJ_NOMPROJET 
				from budget_ligne 
				left join projet on BUDL_IDPROJET=PRJ_CLE
				left join budget on BUDL_IDBUDGET=BUD_CLE
				where PRJ_IDPOLE IN (select POL_CLE from pole where POL_IDDOMAINE=(select POL_IDDOMAINE from pole where POL_CLE=".$idpole.")) 
				and BUDL_IDBUDGET=\"".$idbudget."\" order by 2";
//				echo $requete;
// regrouper id de la ligne et du projet
$idlignebudget = $idlignebudget."*;*".$idprojet;
$objForm->setSelect('idlignebudget', $idlignebudget, $requete, 'BUDL_CLE', 'PRJ_NOMPROJET');

// sélection du demandeur
if ($iddemandeur == 0 || $iddemandeur == '') 
{
	$iddemandeur = $objProfil->idnom;
	$demandeur = $objProfil->nom;
}	
$requete = "select UTI_NOM, CONCAT(UTI_CLE,\"*-*\",UTI_NOM) as CLE from utilisateur order by 1";
$objForm->setSelect('demandeur', $iddemandeur . "*-*" . $demandeur, $requete, 'CLE', 'UTI_NOM');

// sélection du gestionnaire si différent du demandeur
if (($idintervenant == 0 || $idintervenant == '') && $objProfil->exe) 
{
	if ($iddemandeur != $objProfil->idnom)
	{	
		$idintervenant = $objProfil->idnom;
		$intervenant = $objProfil->nom;
	}
}	
$requete = "select UTI_NOM, CONCAT(UTI_CLE,\"*-*\",UTI_NOM) as CLE from utilisateur order by 1";
$objForm->setSelect('intervenant', $idintervenant . "*-*" . $intervenant, $requete, 'CLE', 'UTI_NOM');

// si état non renseigné -> brouillon
if ($idetat == '' || $idetat == 0) 
{
	if ($cle == '') $idetat = 1;
	else $idetat = $lastidetat;
}
$dataliste = $objWorkflow->getListEtat($iddemandeur, $objProfil->idfonc, $idetat, $lastidetat); 
$objForm->setDataliste('idetat', $dataliste );
//$objForm->setSelect('idetat', $idetat, $requete, 'ETA_CLE', 'ETA_NOMETAT');

// affichage des boutons d'enchainement
$objForm->addBouton("reset","RETOUR","cmd_list_commande.php?menu=cmd");
if (($cle == '' && $objProfil->cre) || ($cle != '' && $objProfil->maj))
{
	$objForm->addBouton("submit","ENREGISTRER");
}
else $objForm->setLecForm();

$objForm->affFormulaire();
$objForm->finFormulaire();

// fin de page
$objPage->finPage();
?>
<script>
<?php
echo "var nommarche = \"" . addslashes($nommarche) . "\";";
?>
// variables globales
var idpole = 0;
$(function()
{
	// chargement des marchés (autocomplete)
	getMarche();

	// affichage du nom du projet et du nom du programme
	var nomprojet = getNomProjet();
	getNomProgramme(nomprojet);

	// reset des lignes budgétaires si changement de pôle 
	$("#idpole").change(function() {getBudget();});
	
	// récupération des budgets si changement de programme 
	$("#programme").change(function() {getBudget();});
	
	// récupération des lignes budgetaires si changement de budget
	$("#idbudget").change(function() {getLigneBudget();});
	
	// affichage du nom du projet et du nom du programme si changement de ligne budgetaire
	$("#idlignebudget").change(function() 
	{
		var nomprojet = getNomProjet();
		getNomProgramme(nomprojet);
	});
});

// récupération du marché	
function getMarche()
{	
	$.ajax({type: 'GET', url: 'get_marche.php', success: function (response) {actuMarche(response, nommarche);}, async:false});
}

function actuMarche(response, nommarche)
{	
//	console.log(id+" = "+nommarche);
	// mise à jour autocomplete
	var tabcomplete = response.split("*;*");
	$("#idmarche").autocomplete({
		source: tabcomplete	
	}).val(nommarche);
}

// récupération du budget pour un programme
function getBudget()
{	
	// récupération du programme 
	var programme = $("#programme option:selected").val();
	var parametres = "programme="+programme; 
	$.ajax({
		url: "get_budget.php",
		type: 'POST',
		data: parametres,
		dataType: 'json',
		async: false,
		success: function(response){actuBudget(response);}
	});
}

function actuBudget(response)
{	
//	console.log(id+" = "+response);
	// mise à jour de la liste (select)
	$("#idbudget").html('<option></option>'); // suppression des champs OPTION de la SELECT
	$.each(response, function(nom, texte) { 
		$("#idbudget").append($("<option></option>")
		.attr("value", nom.substring(1,nom.length)).text(texte)); // suppression premier caractère (tri json)
	});
	// reset lignes budgétaires
	$("#idlignebudget").html(''); // suppression des champs OPTION de la SELECT
	// reset du nom du projet et du nom du programme
	$("#nomprojet").val('');
	$("#nomprogramme").val('');
}

// récupération de la ligne budgétaire	
function getLigneBudget()
{	
	// récupération de l'année et du pôle 
	var idpole = $("#idpole option:selected").val();
	var idbudget = $("#idbudget option:selected").val();
	var parametres = "idpole="+idpole+"&idbudget="+idbudget; 
	$.ajax({
		url: "get_lignebudget.php",
		type: 'POST',
		data: parametres,
		dataType: 'json',
		async: false,
		success: function(response){actuLigneBudget(response);}
	});
}

function actuLigneBudget(response)
{	
//	console.log(id+" = "+valeur);
	// mise à jour de la liste (select)
	$("#idlignebudget").html('<option></optio'); // suppression des champs OPTION de la SELECT
//	$("#"+nomchamp).empty();
	$.each(response, function(nom, texte) {
		$("#idlignebudget").append($("<option></option>")
		.attr("value", nom.substring(1,nom.length)).text(texte)); // suppression premier caractère (tri json)
	});
	// reset du nom du projet et du nom du programme
	$("#nomprojet").val('');
	$("#nomprogramme").val('');
}

// récupération du nom du projet (champ readonly) 
function getNomProjet()
{
	var nomprojet = $("#idlignebudget option:selected").text();
	if (typeof(nomprojet)  === "undefined") return '';
	var tab = nomprojet.split(" ->\t");
	$("#nomprojet").val(tab[0]);
	return tab[0];
}

// récupération du nom du programme (champ readonly) à partir du nom du projet	
function getNomProgramme(nomprojet)
{
	if (nomprojet == '') return;
	var parametres = "nomprojet="+nomprojet; 
	$.ajax({type: 'GET', url: 'get_programme.php', data: parametres, success: function (response) {actuNomProgramme(response);}, async:false});
}

function actuNomProgramme(response)
{	
//	console.log(id+" = "+response);
	$("#nomprogramme").val(response);
	
}	
</script>
