<?php
include('config/bud_list_ligne.php');
$codefonc='bud';
include('prepage.php');
if (!$objProfil->ctlDroit($typeaction, 'tableau')) exit();

$objPage->debPage('center');

// préparation du tableau
$objTab = new tableau('1');

// suppression d'une entrée
if($typeaction == "suppression") 
{
	if(isset($cle))	
	{
		$requete = "delete from budget_ligne where BUDL_CLE=\"" . $cle . "\"";
		$objTab->supRequete($requete);
	}
}

if(isset($_REQUEST['zoom'])) 
{
	$zoom=$_REQUEST['zoom'];
	$_SESSION['zoom'] = $zoom;
}
else if(isset($_SESSION['zoom'])) $zoom=$_SESSION['zoom'];
else $zoom = 'n';

// requête d'accès à la base 
if ($zoom != 'o')
{	
	$requete = "select BUDL_CLE, PRG_NOMPROGRAMME, PRJ_NOMPROJET, BUDL_TYPELIG, BUDL_LIBELLE,  
				BUDL_TYPEIMPUTATION,BUDL_PRIORITE,
				BUDL_AEDOTAFONCT, BUDL_AEDEBLFONCT,
				BUDL_CPDOTAFONCT, BUDL_CPDEBLFONCT, 
				BUDL_AEDOTAINVES, BUDL_AEDEBLINVES,
				BUDL_CPDOTAINVES, BUDL_CPDEBLINVES, 
				IF (BUDL_IDPAPFONCT > 0, 'Oui', BUDL_IDPAPFONCT) AS BUDL_IDPAPFONCT,
				IF (BUDL_IDPAPINVES > 0, 'Oui', BUDL_IDPAPINVES) AS BUDL_IDPAPINVES,
				POL_NOMPOLE, DOM_NOMDOMAINE
				from budget_ligne 
				left join projet on BUDL_IDPROJET=PRJ_CLE
				left join programme on PRJ_IDPROGRAMME=PRG_CLE
				left join pole on PRJ_IDPOLE=POL_CLE 
				left join domaine on POL_IDDOMAINE=DOM_CLE 
				where BUDL_IDBUDGET=\"" . $cleparent . "\" ";
	$requete .= $objTab->majRequete('order by POL_NOMPOLE,PRG_NOMPROGRAMME,PRJ_NOMPROJET'); // ajout tri et pagination si besoin
}
else
{
	$requete = "select PRG_NOMPROGRAMME, PRJ_NOMPROJET, DOM_NOMDOMAINE, POL_NOMPOLE,
				SUM(BUDL_AEDOTAFONCT) as BUDL_AEDOTAFONCT, SUM(BUDL_AEDEBLFONCT) as BUDL_AEDEBLFONCT,
				SUM(BUDL_CPDOTAFONCT) as BUDL_CPDOTAFONCT, SUM(BUDL_CPDEBLFONCT) as BUDL_CPDEBLFONCT, 
				SUM(BUDL_AEDOTAINVES) as BUDL_AEDOTAINVES, SUM(BUDL_AEDEBLINVES) as BUDL_AEDEBLINVES,
				SUM(BUDL_CPDOTAINVES) as BUDL_CPDOTAINVES, SUM(BUDL_CPDEBLINVES) as BUDL_CPDEBLINVES 
				from budget_ligne 
				left join projet on BUDL_IDPROJET=PRJ_CLE
				left join programme on PRJ_IDPROGRAMME=PRG_CLE
				left join pole on PRJ_IDPOLE=POL_CLE 
				left join domaine on POL_IDDOMAINE=DOM_CLE
				group by BUDL_IDBUDGET,DOM_NOMDOMAINE,POL_NOMPOLE,PRG_NOMPROGRAMME,PRJ_NOMPROJET				
				having BUDL_IDBUDGET=\"" . $cleparent . "\" ";
	$requete .= $objTab->majRequete('order by POL_NOMPOLE,PRG_NOMPROGRAMME,PRJ_NOMPROJET'); // ajout tri et pagination si besoin
	$objTab->champT['libelle']['aff'] = false;
	$objTab->champT['idpapfonct']['aff'] = false;
	$objTab->champT['idpapinves']['aff'] = false;
	$objTab->champT['typelig']['aff'] = false;
	$objTab->champT['typeimputation']['aff'] = false;
	$objTab->champT['priorite']['aff'] = false;
}
// affichage des boutons d'enchainement
if ($zoom == 'o') $objTab->addBouton("button","ZOOM -","bud_list_ligne.php?zoom=n");
else $objTab->addBouton("button","ZOOM +","bud_list_ligne.php?zoom=o");

if ($objProfil->cre && $zoom != 'o')  
{
	$objTab->addBoutonSpe("button","EXPORTER","onclick=\"document.location.href='bud_csv_ligne.php';\"");
//	$objTab->addBouton("button","IMPORTER","bud_imp_ligne.php");
	$objTab->addBouton("button","AJOUTER","bud_maj_ligne.php");
}

// gestion des paramètres de lien
if ($zoom != 'o') 
{
	$objTab->setLien('nomprogramme','bud_maj_ligne.php',"?typeaction=modification&cle=#BUDL_CLE#");
	$objTab->setLien('supp','bud_list_ligne.php',"?typeaction=suppression&cle=#BUDL_CLE#","Supprimer");
}	

// affichage du tableau
$objTab->affTableau($requete);

// fin de page
$objPage->finPage();
