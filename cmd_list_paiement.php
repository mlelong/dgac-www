<?php
include('config/cmd_list_paiement.php');
$codefonc='cmd';
include('prepage.php');
if (!$objProfil->ctlDroit($typeaction, 'tableau')) exit();

$objPage->debPage('center');

// préparation du tableau
$objTab = new tableau('1');

// requête d'accès à la base 
$requete = "select PCMD_CLE, 
			DATE_FORMAT(PCMD_DATEFACT, '%d-%m-%Y') AS PCMD_DATEFACT, 
			DATE_FORMAT(PCMD_DATEOK, '%d-%m-%Y') AS PCMD_DATEOK, 
			DATE_FORMAT(PCMD_DATEAC, '%d-%m-%Y') AS PCMD_DATEAC, 
			DATE_FORMAT(PCMD_DATEPREV, '%d-%m-%Y') AS PCMD_DATEPREV, 
			DATE_FORMAT(PCMD_DATEPAIE, '%d-%m-%Y') AS PCMD_DATEPAIE, 
			REPLACE(REPLACE(FORMAT(PCMD_TOTALTTC,2),',',' '),'.',',') as PCMD_TOTALTTC
			from commande_paiement 
			where PCMD_IDCMD=\"" . $cleparent . "\" ";
$requete .= $objTab->majRequete('order by PCMD_CLE'); // ajout tri et pagination si besoin

// affichage du tableau
$objTab->affTableau($requete);

// fin de page
$objPage->finPage();
