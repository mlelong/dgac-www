<?php
class fctdroit {
	public $fonction;
	public $nomfonc;
}

class droit {
	private $tabdroit = array();
	private $cptdroit = 0;
	
	// creation des objets fonction et de la table des objets
	public function creFonction()
	{
		$conn = database::getIntance();
		$requete = "select FON_ORDRE, FON_CODEFONC, FON_NOMFONC 
					from fonctionnalite order by 1";
		$statement = $conn->query($requete);
		while ($row = $statement->fetch(PDO::FETCH_ASSOC))
		{
			$curfctdroit = new fctdroit;
			$curfctdroit->fonction = $row['FON_CODEFONC'];
			$curfctdroit->nomfonc = $row['FON_NOMFONC'];
			$this->tabdroit[$this->cptdroit] = $curfctdroit;
			$this->cptdroit++;
		}
	}

	// test d'existence d'une fonction dans la table des fonctions
	function existFonction($fonction)
	{
		foreach ($this->tabdroit as $curfctdroit)
		{
			if ($curfctdroit->fonction == $fonction) return $curfctdroit;
		}
		return false;
	}

	// boucle sur reception des droits
	function getDroit()
	{
		$listdroit='';
		$cptdroit = 0;
		$cptfonc = 0;
		foreach ($this->tabdroit as $curfctdroit)
		{
			$nom = "nom" . $cptdroit;
			if (isset($_REQUEST[$nom])) $lec=0x20; else $lec=0x00;
			$cptdroit++;
			$nom = "nom" . $cptdroit;
			if (isset($_REQUEST[$nom])) $cre=0x10; else $cre=0x00;
			$cptdroit++;
			$nom = "nom" . $cptdroit;
			if (isset($_REQUEST[$nom])) $maj=0x08; else $maj=0x00;
			$cptdroit++;
			$nom = "nom" . $cptdroit;
			if (isset($_REQUEST[$nom])) $sup=0x04; else $sup=0x00;
			$cptdroit++;
			$nom = "nom" . $cptdroit;
			if (isset($_REQUEST[$nom])) $exe=0x02; else $exe=0x00;
			$cptdroit++;
			$nom = "nom" . $cptdroit;
			if (isset($_REQUEST[$nom])) $vis=0x40; else $vis=0x00;
			$cptdroit++;
			$nom = "nom" . $cptdroit;
			if (isset($_REQUEST[$nom])) $sui=0x01; else $sui=0x00;
			$cptdroit++;
			$nom = "nom" . $cptdroit;
			if (isset($_REQUEST[$nom])) $adm=0x80; else $adm=0x00;
			$cptdroit++;
	//		$somme = chr($lec | $cre | $maj | $sup | $exe | $vis | $sui| $adm);
	//		echo "lec=" . $lec . " cre=" . $cre . " maj=" . $maj . " sup=" . $sup . " exe=" . $exe . " vis=" . $vis . " sui=" . $sui . " adm=" . $adm . " tous=" . $somme . "<br>";
			$listdroit .= chr($lec | $cre | $maj | $sup | $exe | $vis | $sui| $adm);
			$cptfonc++;
		}
		for ($i=$cptfonc; $i<50; $i++)
		{
			$listdroit .= chr(0x00);
		}
		return $listdroit;
	}

	// Affichage de la liste des droits
	function affDroit($objTab, $profildroit)
	{
		$conn = database::getIntance();
		$requete = "select CFG_MASQDROIT from config where CFG_CLE=1";
		$statement = $conn->query($requete);
		$resultat = $statement->fetch(PDO::FETCH_ASSOC);
		$masqdroit=$resultat['CFG_MASQDROIT'];

		$sui=0x01; $exe=0x02; $sup=0x04; $maj=0x08;	$cre=0x10; $lec=0x20; $vis=0x40; $adm=0x80;
		$flipflop = 0;
		$cptdroit = 0;
		$cptfonc = 0;
		$ligne = array();
		foreach ($this->tabdroit as $curfctdroit)
		{
			$ligne[0] = $curfctdroit->fonction;
			$ligne[1] = $curfctdroit->nomfonc;
			
			if (isset($profildroit[$cptfonc])) $droit_rol=ord($profildroit[$cptfonc]);
			else $droit_rol=0x00;
			if (isset($masqdroit[$cptfonc])) $droit_mas=ord($masqdroit[$cptfonc]);
			else $droit_mas=0x00;
	// lecture
			$opt = '';
			if ($droit_mas & $lec) {$read = ''; if ($droit_rol & $lec) $opt=" checked='checked' ";}
			else $read = " disabled='disabled' ";
			$ligne[2] = "<input type='checkbox' name='nom" . $cptdroit . "' value='1'" . $opt . $read . ">";
			$cptdroit++;
	// creation		
			$opt = '';
			if ($droit_mas & $cre) {$read = ''; if ($droit_rol & $cre) $opt=" checked='checked' ";}
			else $read = " disabled='disabled' ";
			$ligne[3] = "<input type='checkbox' name='nom" . $cptdroit . "' value='1'" . $opt . $read . ">";
			$cptdroit++;
	// modification		
			$opt = '';
			if ($droit_mas & $maj) {$read = ''; if ($droit_rol & $maj) $opt=" checked='checked' ";}
			else $read = " disabled='disabled' ";
			$ligne[4] = "<input type='checkbox' name='nom" . $cptdroit . "' value='1'" . $opt . $read . ">";
			$cptdroit++;
	// suppression		
			$opt = '';
			if ($droit_mas & $sup) {$read = ''; if ($droit_rol & $sup) $opt=" checked='checked' ";}
			else $read = " disabled='disabled' ";
			$ligne[5] = "<input type='checkbox' name='nom" . $cptdroit . "' value='1'" . $opt . $read . ">";
			$cptdroit++;
	// execution		
			$opt = '';
			if ($droit_mas & $exe) {$read = ''; if ($droit_rol & $exe) $opt=" checked='checked' ";}
			else $read = " disabled='disabled' ";
			$ligne[6] = "<input type='checkbox' name='nom" . $cptdroit . "' value='1'" . $opt . $read . ">";
			$cptdroit++;
	// lecture hors périmètre		
			$opt = '';
			if ($droit_mas & $vis) {$read = ''; if ($droit_rol & $vis) $opt=" checked='checked' ";}
			else $read = " disabled='disabled' ";
			$ligne[7] = "<input type='checkbox' name='nom" . $cptdroit . "' value='1'" . $opt . $read . ">";
			$cptdroit++;
	// modification hors périmètre		
			$opt = '';
			if ($droit_mas & $sui) {$read = ''; if ($droit_rol & $sui) $opt=" checked='checked' ";}
			else $read = " disabled='disabled' ";
			$ligne[8] = "<input type='checkbox' name='nom" . $cptdroit . "' value='1'" . $opt . $read . ">";
			$cptdroit++;
	// admin		
			$opt = '';
			if ($droit_mas & $adm) {$read = ''; if ($droit_rol & $adm) $opt=" checked='checked' ";}
			else $read = " disabled='disabled' ";
			$ligne[9] = "<input type='checkbox' name='nom" . $cptdroit . "' value='1'" . $opt . $read . ">";
			$cptdroit++;
			
			$objTab->affLigne($ligne);
			$cptfonc++;
		}
	}

	// Affichage des rôles
	function affProfil($profil, $type)
	{
		$conn = database::getIntance();
		if ($type == 'cumul') $dataliste = "<SELECT style='width:150px;' NAME='profil' onChange=\"javascript:addCheck(this.value)\" >\n";
		else $dataliste = "<SELECT style='width:150px;' NAME='profil' onChange=\"javascript:getCheck(this.value)\" >\n";
		$dataliste .= "<OPTION value=''>\n";
		$requete = "select PRO_NOMPROFIL from profil";
		$statement = $conn->query($requete);
		while ($row = $statement->fetch(PDO::FETCH_ASSOC))
		{
			if ($row['PRO_NOMPROFIL'] == $profil) {$opt = " selected ";} else {$opt = "";}
			$dataliste .= "<OPTION value=\"" . $row['PRO_NOMPROFIL'] . "\"" . $opt . ">" . $row['PRO_NOMPROFIL'] . "\n";
		}
		$dataliste .= "</SELECT>\n";
	//	echo "		<input type=\"button\" value=\"Role " . $nomrole .  "\" class=\"recherche\" onclick=\"javascript:getCheck('" . $nomrole . "')\" />\n";
		return $dataliste;
	}
}