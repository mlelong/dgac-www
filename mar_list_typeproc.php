<?php
include('config/mar_list_typeproc.php');
$codefonc='cfg';
$codemenu='typ';
include('prepage.php');
if (!$objProfil->ctlDroit($typeaction, 'tableau')) exit();

$objPage->debPage('center');

// suppression d'une entrée
if($typeaction == "suppression")
{
	if(isset($cle))	
	{
		$requete = "delete from type_procedure where TPRO_CLE=\"" . $cle . "\"";
		$statement = $conn->query($requete);
	}
}

// préparation du tableau
$objTab = new tableau('1');

// requête d'accès à la base 
$requete = "select TPRO_CLE, TPRO_TYPE, TPRO_LIBELLE from type_procedure";
$requete .= $objTab->majRequete('order by TPRO_CLE'); // ajout tri et pagination si besoin

// affichage des boutons d'enchainement
if ($objProfil->cre) $objTab->addBouton("button","AJOUTER","mar_maj_typeproc.php");

// gestion des paramètres de lien
$objTab->setLien('typeproc','mar_maj_typeproc.php',"?typeaction=modification&cle=#TPRO_CLE#");
$objTab->setLien('supp','mar_list_typeproc.php',"?typeaction=suppression&cle=#TPRO_CLE#","Supprimer");

// affichage du tableau
$objTab->affTableau($requete);

// fin de page
$objPage->finPage();
