<?php
include('config/mar_list_marche.php');
$codefonc='mar';
include('prepage.php');
if (!$objProfil->ctlDroit($typeaction, 'tableau')) exit();

$objPage->debPage('center');

// suppression d'une entrée
if($typeaction == "suppression")
{
	if(isset($cle))	
	{
		$requete = "delete from marche where MAR_CLE=\"" . $cle . "\"";
		$statement = $conn->query($requete);
	}
}

// préparation du tableau
$objTab = new tableau('1');

// requête d'accès à la base 
$requete = "select MAR_CLE, MAR_NUMERODGR, MAR_NUMEROSIF, MAR_NOMMARCHE, PRJ_NOMPROJET, MAR_NBUO,
			MAR_TYPEUO, TPRO_TYPE, TPRO_LIBELLE, POL_NOMPOLE, MAR_LIBELLE, MAR_ETAT, MAR_INTERVENANT,
			MAR_DATEPUB, MAR_DATERET, MAR_DATENOT, MAR_DATEFIN,
			MAR_TYPERECONDUC, MAR_DUREEINITIALE, MAR_NBRECONDUC, MAR_DUREERECONDUC,
			REPLACE(REPLACE(FORMAT(MAR_AEPREVTOTAL,2),',',' '),'.',',') as MAR_AEPREVTOTAL, 
			MAR_DESCRIPTION
			from marche 
			left join projet on MAR_IDPROJET=PRJ_CLE
			left join type_procedure on MAR_IDTYPEPROC=TPRO_CLE
			left join pole on MAR_IDPOLE=POL_CLE
			where 1 ";
$requete .= $objTab->majRequete('order by MAR_FLAGCB desc, MAR_NUMERODGR desc'); // ajout tri et pagination si besoin

// affichage des boutons d'enchainement
$objTab->addBouton("button","EXPORTER","mar_csv_marche.php");
if ($objProfil->cre)  $objTab->addBouton("button","AJOUTER","mar_maj_marche.php");

// gestion des paramètres de lien
$objTab->setLien('numerodgr','mar_tab_marche.php',"?typeaction=modification&cle=#MAR_CLE#");
$objTab->setLien('numerosif','mar_tab_marche.php',"?typeaction=modification&cle=#MAR_CLE#");
$objTab->setLien('supp','mar_list_marche.php',"?typeaction=suppression&cle=#MAR_CLE#","Supprimer");

// affichage du tableau
$objTab->affTableau($requete);

// fin de page
$objPage->finPage();
