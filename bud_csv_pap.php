<?php
require_once('class/database.php');
$conn = database::getIntance();

header('Content-Type: text/csv'); 
header('Content-Disposition: attachment;filename=budget_pap.csv'); 

$requete = "select BPAP_CLE, BPAP_IDBUDGET, BPAP_ORDRE, BPAP_LIGNEPAP, BPAP_FLAGIMPUTATION, BPAP_LIBELLE, BPAP_FLAGTOTAL,  
			BPAP_AEPREVFONCT, BPAP_AECONSFONCT, BPAP_CPPREVFONCT, BPAP_CPCONSFONCT, 
			BPAP_AEPREVINVES, BPAP_AECONSINVES, BPAP_CPPREVINVES, BPAP_CPCONSINVES
			from budget_pap order by 1";
$statement = $conn->query($requete);
$sortie = fopen('PHP://output', 'w');
//add BOM to fix UTF-8 in Excel
//fputs($sortie, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
fputcsv($sortie, array_map("utf8_decode",array("CLE","ID BUDGET","ORDRE","LIGNE","IMPUTATION F-I","LIBELLE","FLAGTOTAL",
					"AEPREVFONCT","AECONSFONCT","CPPREVFONCT","CPCONSFONCT",
					"AEPREVINVES","AECONSINVES","CPPREVINVES","CPCONSINVES")),";");
while ($row = $statement->fetch(PDO::FETCH_ASSOC))
{
    fputcsv($sortie, array_map("utf8_decode",array(
    	$row['BPAP_CLE'],
    	$row['BPAP_IDBUDGET'],
    	stripslashes($row['BPAP_ORDRE']),
    	stripslashes($row['BPAP_LIGNEPAP']),
     	$row['BPAP_FLAGIMPUTATION'],
		stripslashes($row['BPAP_LIBELLE']),
    	$row['BPAP_FLAGTOTAL'],
    	$row['BPAP_AEPREVFONCT'],
    	$row['BPAP_AECONSFONCT'],
    	$row['BPAP_CPPREVFONCT'],
    	$row['BPAP_CPCONSFONCT'],
    	$row['BPAP_AEPREVINVES'],
    	$row['BPAP_AECONSINVES'],
    	$row['BPAP_CPPREVINVES'],
    	$row['BPAP_CPCONSINVES']
		)),";");
}
fclose($sortie);
