<?php
include('config/adm_list_groupe-utilisateur.php');
$codefonc='pro';
include('prepage.php');
if (!$objProfil->ctlDroit($typeaction, 'formulaire',$cle)) exit();

$objForm = new formulaire('1');

$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();
// Affichage des utilisateurs visibles
$datalistecle = '';
$datalisteval = '';
$requete = "select UTI_CLE, UTI_NOM, UTI_PRENOM
			from utilisateur, utilisateur_profil
			where UTI_CLE=RUP_IDUTILISATEUR and RUP_IDPROFIL=\"" . $cle . "\"
			order by 2";
$statement = $conn->query($requete);
while ($row = $statement->fetch(PDO::FETCH_ASSOC))
{
	$affichage = $row['UTI_NOM'] . '*-*' . $row['UTI_PRENOM'];
	if($datalisteval == '') $datalisteval = $affichage;
	else $datalisteval .= ';' . $affichage;
	if($datalistecle == '') $datalistecle = $row['UTI_CLE'];
	else $datalistecle .= ';' . $row['UTI_CLE'];
}
$objForm->champF['idnomlec']['dataliste1'] = $datalistecle . "*;*" . $datalisteval;

// Affichage des autres utilisateurs
$datalistecle = '';
$datalisteval = '';
$requete = "select UTI_CLE, UTI_NOM, UTI_PRENOM from utilisateur 
			where UTI_CLE NOT IN (select RUP_IDUTILISATEUR from utilisateur_profil 
			 where RUP_IDPROFIL=\"" . $cle . "\") 
			order by 2";
$statement = $conn->query($requete);
while ($row = $statement->fetch(PDO::FETCH_ASSOC))
{
	$affichage = $row['UTI_NOM'] . '*-*' . $row['UTI_PRENOM'];
	if($datalisteval == '') $datalisteval = $affichage;
	else $datalisteval .= ';' . $affichage;
	if($datalistecle == '') $datalistecle = $row['UTI_CLE'];
	else $datalistecle .= ';' . $row['UTI_CLE'];
}
$objForm->champF['idnomlec']['dataliste2'] = $datalistecle . "*;*" . $datalisteval;

$objForm->affFormulaire();
// affichage des boutons d'enchainement
$objForm->addBouton("reset","RETOUR","adm_list_profil.php");
if ($objProfil->maj) 
{
	$objForm->addBoutonSpe("button","VALIDER","onclick=\"majFormSortableDiff('pro-u'," . $cle . ");VersURL('adm_list_profil.php');\"");
}

// fin de page
$objForm->finFormulaire();
$objPage->finPage();
