<?php
include('config/mar_imp_uo.php');
$codefonc='mar';
require_once('prepage.php');

$objForm = new formulaire('1');

// création
if (!isset($typeaction) || $typeaction == '') $typeaction = 'creation';
if($typeaction == "creation") $objForm->initChamp();

// réception des paramètres
if($typeaction == "reception")
{
// contrôle des paramètres
	$objForm->recChamp();
	while ($objForm->erreur == '')
	{
		// récupération du fichier
		if ((isset($_FILES['nomfic']['name'][0])&&($_FILES['nomfic']['error'][0] == UPLOAD_ERR_OK)))
		{
			$namefic = ctlNomFichier($_FILES['nomfic']['name'][0]);
			$chemin_destination = "document/" . $namefic;   
			move_uploaded_file($_FILES['nomfic']['tmp_name'][0], $chemin_destination); 
			if (!file_exists($chemin_destination)) // vérification pour savoir si le fichier existe
				{$objForm->erreur="nomfic"; $objForm->libelle="Le fichier " . $namefic . " n'a pas pu être chargé"; break;}
		}
		else 
		{
			if ($_FILES['nomfic']['error'][0] == UPLOAD_ERR_NO_FILE)
				{$objForm->erreur="nomfic"; $objForm->libelle="Il faut renseigner un fichier"; break;}
			else
				{$objForm->erreur="nomfic"; $objForm->libelle="Erreur sur chargement du fichier = " . $_FILES['nomfic']['error'][0]; break;}
		}
		break;
	}
	if ($objForm->erreur == '')
	{
		$rapport = "";
		//
		// création ou ajout des UO pour le marché	
		//
		$conn->setAttribute(PDO::ATTR_AUTOCOMMIT,0); 
		$conn->beginTransaction();
		$flag = false;
		while(true)
		{
			$fichier = "document/" . $namefic;
//			Trace("Traitement du fichier " . $fichier);
			$handle = fopen ($fichier,"r");
			// numéro du marché sif
			$ligne = fgetcsv($handle, 1000, ";");
			// test si utf8
//			echo 'encoding = ' . mb_detect_encoding($ligne[1]);
			if (mb_detect_encoding($ligne[1] != 'UTF-8')) $flagutf8 = true;
			else $flagutf8 = false;
//			if ($flagutf8) echo "pas UTF-8";
			if ($flagutf8) $ligne = array_map("utf8_encode",$ligne);
			$numerosif = $ligne[2];
			if ($numerosif != '')
			{
//				Trace("Traitement du marché " . $numerosif);
				$requete = "select MAR_CLE from marche where MAR_NUMEROSIF=\"" . $numerosif . "\"";
				$statement = $conn->query($requete);
				$resultat = $statement->fetch(PDO::FETCH_ASSOC);
				$idmarche  = $resultat['MAR_CLE'];
				if ($idmarche != $cleparent)
				{
//					Trace("Différence de clé idmarche=" . $idmarche . " cleparent=" . $cleparent);
					$objForm->erreur="nomfic";
					$objForm->libelle = "Le fichier ne correspond pas au marché " . $numerosif; 
					break;
				}
			}
			$idmarche = $cleparent;
			// remplace
			if ($flagsup == 'o')
			{
//				Trace("Suppression des UO existantes");
				$requete = "delete from marche_revision where MREV_IDMARCHE=\"" . $idmarche . "\"";
				$statement = $conn->query($requete);
				$requete = "delete from marche_uo where MUO_IDMARCHE=\"" . $idmarche . "\"";
				$statement = $conn->query($requete);
			}
			// 
			// traitement du fichier
			//
			$libposte = '';
			$description = '';
			// nom fournisseur
			$ligne = fgetcsv($handle, 1000, ";"); 
			if ($flagutf8) $ligne = array_map("utf8_encode",$ligne);
			$titulaire = $ligne[2];
			// numéro SSIM
			$ligne = fgetcsv($handle, 1000, ";"); 
			if ($flagutf8) $ligne = array_map("utf8_encode",$ligne);
			$numerodgr = $ligne[2];
			// TVA 
			$ligne = fgetcsv($handle, 1000, ";"); 
			if ($flagutf8) $ligne = array_map("utf8_encode",$ligne);
			$tva = str_replace("%", "", $ligne[10]); // on supprime le % si renseigné 
			// entête
			$ligne = fgetcsv($handle, 1000, ";"); 
			if ($flagutf8) $ligne = array_map("utf8_encode",$ligne);
			// lignes des uos
//			Trace("Début de la boucle des UO");
			while ($ligne = fgetcsv($handle, 1000, ";")) 
			{
		//		print_r($ligne);
				if ($flagutf8) $ligne = array_map("utf8_encode",$ligne);
				// informations UO
				$flagdsi = $ligne[0];
				if ($flagdsi == 'x') $flagdsi = 'o';
				else $flagdsi = '';
				$prestation = $ligne[1];
				if ($prestation != '')
				{	
					$tab = explode(':', $prestation);
					if (isset($tab[0])) $nomprestation = $tab[0]; 
						else $nomprestation = '';
					$libprestation = $tab[1];
					if (isset($tab[1])) $libprestation = $tab[1]; 
						else $libprestation = '';
				}
				else
				{
					$nomprestation = '';
					$libprestation = '';
				}	
				$nomuo = $ligne[2];
				if ($nomuo == '') continue;
				$nomuocourt = $ligne[3];
				$frequence = $ligne[4];
				$montant = $ligne[5];
				$flagimputation = $ligne[6];
				if ($flagimputation != 'F' && $flagimputation != 'I') $flagimputation = '';
				$marchandise = $ligne[7];
				$vide = $ligne[8];
				// informations révision de prix
				$daterevision = $ligne[9]; // JJ/MM/AAAA
				$revision = str_replace(",", ".", $ligne[10]); 
				//	
				// traitement si révision renseignée
				//	
				if ($daterevision != '' && $daterevision != 'NC')
				{
					// inversion du format de la date
					$tab = explode('-', $daterevision);
					if (count($tab) != 3) $tab = explode('/', $daterevision);
					if (count($tab) != 3) $tab = explode('.', $daterevision);
					$daterevision = $tab[2] . "-" . $tab[1] . "-" . $tab[0];
					// création ou mise à jour de la révision de prix
					$requete = "select MREV_CLE, MREV_COEFREV, MREV_DATEREV from marche_revision 
								where MREV_IDMARCHE=\"" . $idmarche . "\" and MREV_DATEREV=\"" . $daterevision . "\"";
					$statement = $conn->query($requete);
					$resultat = $statement->fetch(PDO::FETCH_ASSOC);
					$mrev_cle = $resultat['MREV_CLE'];
					$mrev_coefrev = $resultat['MREV_COEFREV'];
					$mrev_daterev = $resultat['MREV_DATEREV'];
					if ($mrev_cle > 0) // la révision existe déjà
					{
						$requete = "update marche_revision set MREV_COEFREV=\"" . $mrev_coefrev . "\"
									where MREV_CLE=\"" . $mrev_cle. "\"";
						$statement = $conn->query($requete);
					}
					else
					{
						$requete = "insert into marche_revision (MREV_IDMARCHE, MREV_COEFREV, MREV_DATEREV)
									values (\"" . $idmarche . "\",\"" . $revision . "\",\"" . $daterevision . "\")";
						$statement = $conn->query($requete);
					}
				}
				//	
				// traitement prestation
				//	
				$requete = "select MPRE_CLE, MPRE_NOMPRESTATION from marche_prestation 
							where MPRE_IDMARCHE=\"" . $idmarche . "\" and MPRE_NOMPRESTATION=\"" . addslashes($nomprestation) . "\"";
				$statement = $conn->query($requete);
				$resultat = $statement->fetch(PDO::FETCH_ASSOC);
				$mpre_cle = $resultat['MPRE_CLE'];
				if ($mpre_cle > 0) // la prestation existe déjà
				{
					$requete = "update marche_prestation set MPRE_LIBELLE=\"" . addslashes($libprestation) . "\" where MPRE_CLE=\"" . $mpre_cle. "\"";
					$statement = $conn->query($requete);
				}
				else
				{
					$requete = "insert into marche_prestation (MPRE_NOMPRESTATION, MPRE_IDMARCHE, MPRE_LIBELLE)
								values (\"" . addslashes($nomprestation) . "\",\"" . $idmarche . "\",\"" . addslashes($libprestation) . "\")";
					$statement = $conn->query($requete);
					$mpre_cle = $conn->lastInsertId();
				}
				$idprestation = $mpre_cle;
				//	
				// traitement uo
				//	
				$requete = "select MUO_CLE, MUO_NOMUO, MUO_IDMARCHE from marche_uo 
							where MUO_IDMARCHE=\"" . $idmarche . "\" and MUO_NOMUO=\"" . addslashes($nomuo) . "\"";
				$statement = $conn->query($requete);
				$resultat = $statement->fetch(PDO::FETCH_ASSOC);
				$muo_cle = $resultat['MUO_CLE'];
				$muo_nomuo = $resultat['MUO_NOMUO'];
//				Trace($requete);
				if ($muo_cle > 0) // l'uo existe déjà
				{
//					Trace("Mise à jour " . $nomuo);
					$requete = "update marche_uo set MUO_NOMUO=\"" . addslashes($nomuo) 
								. "\", MUO_IDPRESTATION=\"" . $idprestation
								. "\", MUO_IDMARCHE=\"" . $idmarche
								. "\", MUO_FREQUENCE=\"" . $frequence
								. "\", MUO_LIBPOSTE=\"" . addslashes($libposte)
								. "\", MUO_MONTANT=\"" . $montant
								. "\", MUO_MARCHANDISE=\"" . $marchandise
								. "\", MUO_FLAGDSI=\"" . $flagdsi
								. "\", MUO_FLAGIMPUTATION=\"" . $flagimputation
								. "\", MUO_DESCRIPTION=\"" . $description
								. "\" where MUO_CLE=\"" . $muo_cle. "\"";
					$statement = $conn->query($requete);
				}
				else
				{
//					Trace("Insertion " . $nomuo);
					$requete = "insert into marche_uo (MUO_NOMUO, MUO_IDPRESTATION, MUO_IDMARCHE, MUO_FREQUENCE, MUO_LIBPOSTE, 
								MUO_MONTANT, MUO_MARCHANDISE, MUO_FLAGDSI, MUO_FLAGIMPUTATION, MUO_DESCRIPTION)
								values (\"" . addslashes($nomuo) . "\",\"" . $idprestation . "\",\"" . $idmarche . "\",\"" . $frequence 
								. "\",\"" . addslashes($libposte) . "\",\"" . $montant . "\",\"" . $marchandise 
								. "\",\"" . $flagdsi . "\",\"" . $flagimputation . "\",\"" . $description . "\")";
					$statement = $conn->query($requete);
				}
			}
			fclose ($handle);
			$conn->commit(); 
			$flag = true;
//			Trace("COMMIT");
			break;
		}
		if ($flag == false) 
		{
//			Trace("ROLLBACK");
			$conn->rollBack();
			$objForm->erreur="nomfic";
			if ($objForm->libelle == '') $objForm->libelle = "Erreur sur chargement des UO"; 
		}
		if (isset($fichier)) 
		{
			error_reporting(0);
			$result= unlink($fichier); // suppression du fichier
			error_reporting(E_ALL);
		}	
	}
	if ($objForm->libelle == '')
	{
		RedirURL("mar_list_uo.php");
		exit();
	}
}

// Affichage du début de la page
if ($cle != '') $objPage->addParam('cle', $cle);
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();

// affichage des boutons d'enchainement
$objForm->addBouton("reset","RETOUR","mar_list_uo.php");
if (($cle == '' && $objProfil->cre) || ($cle != '' && $objProfil->maj))
{
	$objForm->addBouton("submit","ENVOYER");
}
else $objForm->setLecForm();

$objForm->affFormulaire();
$objForm->finFormulaire();

// fin de page
$objPage->finPage();
