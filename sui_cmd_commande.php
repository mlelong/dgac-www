<?php
include('config/sui_bud_commande.php');
$codefonc='rpm';
include('prepage.php');

$objPage->debPage('center');

// préparation du tableau
$objTab = new tableau('1');

// requête d'accès à la base 
$requete = "select BUD_ANNEE, CMD_PROGRAMME, PRJ_NOMPROJET, 
			SUM(CMD_TOTALTTC) AS TOTAL, BUDL_AEDOTAFONCT+BUDL_AEDOTAINVES AS BUDGET
			from commande 
			full join budget_ligne on CMD_IDPROJET=BUDL_IDPROJET
			left join budget on BUDL_IDBUDGET=BUD_CLE
			left join projet on CMD_IDPROJET=PRJ_CLE
			left join marche on CMD_IDMARCHE=MAR_CLE
			where BUD_ANNEE="2019"
			group by BUD_ANNEE, CMD_PROGRAMME, PRJ_NOMPROJET, BUDGET
			order by PRJ_NOMPROJET 
$requete .= $objTab->majRequete('order by POL_NOMPOLE,PRG_NOMPROGRAMME,PRJ_NOMPROJET'); // ajout tri et pagination si besoin

// affichage des boutons d'enchainement
//$objTab->addBoutonSpe("button","EXPORTER","onclick=\"document.location.href='bud_csv_ligne.php';\"");

// affichage du tableau
$objTab->affTableau($requete);

// fin de page
$objPage->finPage();
