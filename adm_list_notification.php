<?php
include('config/adm_list_notification.php');
$codefonc='adm';
$codemenu='not';
include('prepage.php');
if (!$objProfil->ctlDroit($typeaction, 'tableau')) exit();

$objPage->debPage('center');

// suppression d'une entrée
if($typeaction == "suppression")
{
	if(isset($cle))	
	{
		$requete = "delete from notification where NOT_CLE=\"" . $cle . "\"";
		$statement = $conn->query($requete);
	}
}

// préparation du tableau
$objTab = new tableau('1');

// requête d'accès à la base 
$requete = "select NOT_CLE, NOT_NOMNOTIF, NOT_DESCRIPTION	from notification where 1 ";
$requete .= $objTab->majRequete('order by NOT_CLE'); // ajout tri et pagination si besoin

// affichage des boutons d'enchainement
if ($objProfil->cre) $objTab->addBouton("button","AJOUTER","adm_maj_notification.php");

// gestion des paramètres de lien
$objTab->setLien('nomnotif','adm_maj_notification.php',"?typeaction=modification&cle=#NOT_CLE#");
$objTab->setLien('groupe','adm_list_notification-groupe.php',"?typeaction=modification&cleparent=#NOT_CLE#&cle=#NOT_CLE#","Liste");
$objTab->setLien('supp','adm_list_notification.php',"?typeaction=suppression&cle=#NOT_CLE#","Supprimer");

// affichage du tableau
$objTab->affTableau($requete);

// fin de page
$objPage->finPage();
