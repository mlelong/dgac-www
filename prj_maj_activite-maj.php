<?php
include('config/prj_maj_activite-maj.php');
$codefonc='prj';
$codemenu='ama';
require_once('prepage.php');

$objForm = new formulaire('1');

if($typeaction == "creation") $objForm->initChamp();

if($typeaction == "modification")
{
	$requete = "select PRG_CLE, PRG_TYPE, PRG_NOMPROGRAMME, PRG_LIBELLE 
				from programme where PRG_CLE=\"" . $cle . "\"";
	$resultat = $objForm->mapChamp($requete);
}

// réception des paramètres
if($typeaction == "reception")
{
// contrôle des paramètres
	$objForm->recChamp();
	while($objForm->erreur == '')
	{
		break;
	}

	if ($objForm->erreur == '')
	{
		if (!isset($type) || $type == '') $type = "Activité majeure";
		if (!$objForm->majBdd('programme', 'PRG_CLE', $cle))
		{
			if ($objForm->libelle == '1062') {$objForm->erreur='nomprogramme'; $objForm->libelle="le nom de l'activité majeure existe déjà";}
		}	
		else 
		{
			if ($cle != '' && $type == "Programme")
			{
				$requete = "update projet set PRJ_TYPE=\"Projet\" where PRJ_IDPROGRAMME=\"".$cle."\"";
				$statement = $conn->exec($requete);
			}
			RedirURL("prj_list_activite-maj.php");
			exit();
		}
	}
}

// Affichage du début de la page
if ($cle != '') $objPage->addParam('cle', $cle);
$objPage->debPage('center');

if ($cle == '') $objForm->setAff('type', false);

// Affichage du formulaire
$objForm->debFormulaire();
	
// affichage des boutons d'enchainement
$objForm->addBouton("reset","RETOUR","prj_list_activite-maj.php");
if (($cle == '' && $objProfil->cre) || ($cle != '' && $objProfil->maj))
{
	$objForm->addBouton("submit","ENVOYER");
}
else $objForm->setLecForm();

$objForm->affFormulaire();
$objForm->finFormulaire();

// fin de page
$objPage->finPage();
