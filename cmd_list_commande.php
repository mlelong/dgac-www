<?php
include('config/cmd_list_commande.php');
$codefonc='cmd';
include('prepage.php');
// if (!$objProfil->ctlDroit($typeaction, 'tableau')) exit(); // problème pour supprimer si brouillon

$objPage->debPage('center');

// suppression d'une entrée
if($typeaction == "suppression")
{
	if(isset($cle))	
	{
		//
		// Suppression de la commande avec mise à jour des montants sur les budgets 
		//
		$conn->setAttribute(PDO::ATTR_AUTOCOMMIT,0); 
		$conn->beginTransaction();
		$objCommande = new commande($conn, 'sup', 'commande', $cle);
		if ($objCommande->supCommande() === true) $conn->commit(); 
		else 
		{
			$conn->rollBack();
			$objPage->erreur='erreur';
			if ($objCommande->libelle != '') $objPage->libelle= $objCommande->libelle;
			else $objPage->libelle="Erreur sur la suppression de la commande"; 
		}
	}
}

// préparation du tableau
$objTab = new tableau('1');

// requête d'accès à la base 
$requete = "select CMD_CLE, CMD_NUMERO, CMD_NUMEROSIF, POL_NOMPOLE, MAR_NOMMARCHE, 
			PRJ_NOMPROJET, PRG_NOMPROGRAMME, 
			CMD_DATECMD, CMD_DATELIVA, CMD_DATEVA, CMD_DATESERV,
			ETA_NOMETAT, CMD_IDDEMANDEUR, CMD_DEMANDEUR, CMD_INTERVENANT, CMD_OBJET,
			CMD_IDETAT, CMD_TOTALTTC, CMD_TOTALTTCSERV, CMD_TOTALTTCFACT, CMD_TYPEIMPUTATION
			from commande 
			left join pole on CMD_IDPOLE=POL_CLE
			left join marche on CMD_IDMARCHE=MAR_CLE
			left join projet on CMD_IDPROJET=PRJ_CLE
			left join programme on PRJ_IDPROGRAMME=PRG_CLE
			left join fonc_etat on CMD_IDETAT=ETA_CLE";
if ($objProfil->exe) $requete .= " where 1";
/*			
else $requete .= " where CMD_IDPOLE in (select RPU_IDPOLE from pole_utilisateur where RPU_IDUTILISATEUR=\"" . $objProfil->idnom . "\") 
					OR CMD_IDDEMANDEUR=\"" . $objProfil->idnom . "\" OR CMD_IDINTERVENANT=\"" . $objProfil->idnom . "\" ";
*/
else $requete .= " where (CMD_IDPOLE in (select RPU_IDPOLE from pole_utilisateur where RPU_IDUTILISATEUR=\"" . $objProfil->idnom . "\" union
					select POL_CLE from pole where POL_IDDOMAINE=(select RDU_IDDOMAINE from domaine_utilisateur where RDU_IDUTILISATEUR=\"" . $objProfil->idnom . "\"))
					OR CMD_IDDEMANDEUR=\"" . $objProfil->idnom . "\" OR CMD_IDINTERVENANT=\"" . $objProfil->idnom . "\") ";
$requete .= $objTab->majRequete('order by CMD_NUMERO desc'); // ajout tri et pagination si besoin
$statement = $conn->query($requete);

/*
select RPU_IDPOLE from pole_utilisateur where RPU_IDUTILISATEUR=47
union
select RPU_IDPOLE from pole_utilisateur where RPU_IDUTILISATEUR=47 
   and RPU_IDPOLE in (select POL_CLE from pole where POL_IDDOMAINE=(select RDU_IDDOMAINE from domaine_utilisateur where RDU_IDUTILISATEUR=47))
*/
// affichage des boutons d'enchainement
$objTab->addBouton("button","EXPORTER","cmd_csv_commande.php");
if ($objProfil->cre)  $objTab->addBouton("button","AJOUTER","cmd_maj_commande.php");

$curdate = date("Y-m-d");
while ($row = $statement->fetch(PDO::FETCH_ASSOC))
{
	// si brouillon la commande n'est visible que par le créateur
//	if (!$objProfil->adm && $row['CMD_IDETAT'] == 1 && $row['CMD_IDDEMANDEUR'] != $objProfil->idnom) continue;
	$param = "?typeaction=modification&cle=" . $row['CMD_CLE'];
	$ligne[0] = "<a href=\"#\" " . VersURL('cmd_tab_commande.php' . $param) . "\" >" . stripslashes($row['CMD_NUMERO']) . "</a>";
	$ligne[1] = stripslashes($row['CMD_OBJET']);
	$ligne[2] = $row['POL_NOMPOLE'];
	$ligne[3] = $row['CMD_DEMANDEUR'];
	$ligne[4] = stripslashes($row['MAR_NOMMARCHE']);
	$ligne[5] = stripslashes($row['PRJ_NOMPROJET']);
	$ligne[6] = stripslashes($row['PRG_NOMPROGRAMME']);
	$ligne[7] = $row['CMD_INTERVENANT'];
	$ligne[8] = $row['ETA_NOMETAT'];
	$ligne[9] = stripslashes($row['CMD_TYPEIMPUTATION']);
	$ligne[10] = $row['CMD_NUMEROSIF'];
	if ($row['CMD_DATECMD'] != "0000-00-00") $ligne[11] = $row['CMD_DATECMD'];
	else $ligne[11] = '';
	if ($row['CMD_DATELIVA'] != "0000-00-00" && $row['CMD_DATELIVA'] != '') 
	{
		if ($row['CMD_DATEVA'] == "0000-00-00" || $row['CMD_DATEVA'] == '')
		{
			if ($row['CMD_DATELIVA'] < $curdate && ($row['CMD_TOTALTTC']-$row['CMD_TOTALTTCFACT'])>1)
				$ligne[12] = "<b style='color:red'>".$row['CMD_DATELIVA']."</b>";
			else $ligne[12] = $row['CMD_DATELIVA'];
		}
		else
		{
			if ($row['CMD_DATEVA'] < $curdate && ($row['CMD_TOTALTTC']-$row['CMD_TOTALTTCFACT'])>1)
				$ligne[12] = "<b style='color:red'>".$row['CMD_DATELIVA']."</b>";
			else $ligne[12] = $row['CMD_DATELIVA'];
		}
	}	
	else $ligne[12] = '';
	if ($row['CMD_DATEVA'] != "0000-00-00" && $row['CMD_DATEVA'] != '') 
	{
		if ($row['CMD_DATEVA'] < $curdate && ($row['CMD_TOTALTTC']-$row['CMD_TOTALTTCFACT'])>1)
			$ligne[13] = "<b style='color:red'>".$row['CMD_DATEVA']."</b>";
		else $ligne[13] = $row['CMD_DATEVA'];
	}	
	else $ligne[13] = '';
	if ($row['CMD_DATESERV'] != "0000-00-00") $ligne[14] = $row['CMD_DATESERV'];
	else $ligne[14] = '';
	$ligne[15] = number_format($row['CMD_TOTALTTC'],2,',',' ');
	$ligne[16] = number_format($row['CMD_TOTALTTCSERV'],2,',',' ');
	$ligne[17] = number_format($row['CMD_TOTALTTCFACT'],2,',',' ');
	$param = "?typeaction=suppression&cle=" . $row['CMD_CLE'];
	if ($objProfil->sup || ($row['CMD_IDDEMANDEUR'] == $objProfil->idnom && $row['CMD_IDETAT'] == 1)) // état brouillon
		$ligne[18] = "<a " . VersURL('cmd_list_commande.php' . $param,'supp') . " ><i class='fa fa-trash'></i></a>";
	else $ligne[18] = "";
	$objTab->affLigne($ligne);
}

// fin du tableau
$objTab->finTableau();

// fin de page
$objPage->ajouterBoutonScrollUp();
$objPage->finPage();
