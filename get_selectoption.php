<?php
//indique que le type de la reponse renvoyee au client sera du Texte
header("Content-Type: text/html ; charset=utf-8");

//anti Cache pour HTTP/1.1
header("Cache-Control: no-cache , private");

//anti Cache pour HTTP/1.0
header("Pragma: no-cache");

include('fonction.php');
if(isset($_REQUEST['nomchamp']))
{
	require_once('connect_base.php');
	$nomchamp=$_REQUEST['nomchamp'];
	$valeur=$_REQUEST['valeur'];
	
	try 
	{
		$conn = new PDO('mysql:host='.$base.';port='.$port.';dbname='.$dbname.';charset=utf8;', $user, $pass); 
	} 
	catch (PDOException $e) 
	{
		echo " ";
		exit; 
	}
	
	/* récupération du budget */
	if ($nomchamp == 'idbudget')
	{
		$requete = "select BUDL_CLE, BUDL_LIGNE from budget_ligne where BUDL_IDBUDGET=\"1\" and BUDL_IDPROJET=\"" . $valeur . "\" order by 1";
	}
//	Trace($requete);
	$statement = $conn->query($requete);
	$res = [];
	while ($row = $statement->fetch(PDO::FETCH_NUM))
	{
		$res[$row[0]] = $row[1];
	}
	// fermeture de la connexion
	if (isset($conn)) $conn=null; 
	// encodage en json et retour
	$retour = json_encode($res);
	echo $retour;
}
else
{
	echo " ";
	exit; 
}
