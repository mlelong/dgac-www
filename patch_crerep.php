<?php
include('config/patch_crerep.php');

$codefonc='pat';
require_once('prepage.php');

$objForm = new formulaire('1');

$retour = '';
$nomrep = '';
if(!isset($typeaction)) $typeaction="creation";

// création d'une entrée
if($typeaction == "creation") $objForm->initChamp();

// réception des paramètres
if($typeaction == "reception")
{
// contrôle des paramètres
	$objForm->recChamp();
	while ($objForm->erreur == '')
	{
		break;
	}
	if ($objForm->erreur == '')
	{
		if (is_dir($nomrep)) // vérification pour savoir si le répertoire existe
		{
			$retour .= "<p style='font-size:1.4em;'>Le répertoire " . $nomrep . " existe déjà<p>";
		}
		else
		{
			// création du répertoire
			if (mkdir("./" . $nomrep)) $retour .= "<p style='font-size:1.4em;'>Le répertoire " . $nomrep . " a été crée<p>";
			else $retour .= "<p style='font-size:1.4em;'>Erreur lors de la création du répertoire " . $nomrep . "<p>";
		}
	}	
}

// Affichage du début de la page
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();

// affichage des boutons d'enchainement
$objForm->addBouton("button","RETOUR","patch.php");
if ($objProfil->maj)
{
	$objForm->addBouton("submit","ENVOYER");
}
else $objForm->setLecForm();

$objForm->affFormulaire();
$objForm->finFormulaire();

if ($retour != '') $objPage->tampon .= $retour;

// fin du formulaire et de la page
$objPage->finPage();
