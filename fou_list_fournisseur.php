<?php
include('config/fou_list_fournisseur.php');
$codefonc='fou';
include('prepage.php');
if (!$objProfil->ctlDroit($typeaction, 'tableau')) exit();

$objPage->debPage('center');

// suppression d'une entrée
if($typeaction == "suppression")
{
	if(isset($cle))	
	{
		$requete = "delete from fournisseur where FOU_CLE=\"" . $cle . "\"";
		$statement = $conn->query($requete);
	}
}

// préparation du tableau
$objTab = new tableau('1');

// requête d'accès à la base 
$requete = "select FOU_CLE, FOU_NUMERO, FOU_NOMFOU, FOU_LOCALITE, FOU_CODEPOSTAL from fournisseur where 1";
$requete .= $objTab->majRequete('order by FOU_NOMFOU'); // ajout tri et pagination si besoin


// affichage des boutons d'enchainement
if ($objProfil->cre) $objTab->addBouton("button","AJOUTER","fou_maj_fournisseur.php");
	
// gestion des paramètres de lien
$objTab->setLien('numero','fou_tab_fournisseur.php',"?typeaction=modification&cle=#FOU_CLE#");
$objTab->setLien('supp','fou_list_fournisseur.php',"?typeaction=suppression&cle=#FOU_CLE#","Supprimer");

// affichage du tableau
$objTab->affTableau($requete);

// fin de page
$objPage->finPage();
