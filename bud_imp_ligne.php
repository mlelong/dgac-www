<?php
include('config/bud_imp_ligne.php');
$codefonc='bud';
require_once('prepage.php');

$objForm = new formulaire('1');

// création
if (!isset($typeaction) || $typeaction == '') $typeaction = 'creation';
if($typeaction == "creation") $objForm->initChamp();

// réception des paramètres
if($typeaction == "reception")
{
// contrôle des paramètres
	$objForm->recChamp();
	while ($objForm->erreur == '')
	{
		break;
	}
	if ($objForm->erreur == '')
	{
		$rapport = "";
		// récupération du fichier
		if ((isset($_FILES['nomfic']['name'][0])&&($_FILES['nomfic']['error'][0] == UPLOAD_ERR_OK)))
		{
			$namefic = ctlNomFichier($_FILES['nomfic']['name'][0]);
			$chemin_destination = "document/" . $namefic;   
			move_uploaded_file($_FILES['nomfic']['tmp_name'][0], $chemin_destination); 
		}
		if (!file_exists($chemin_destination)) // vérification pour savoir si le fichier existe
			{$objForm->erreur="nomfic"; $objForm->libelle="Le fichier " . $namefic . " n'a pas pu être chargé"; break;}
		//
		// création ou ajout des lignes pour le budget
		//
		$conn->setAttribute(PDO::ATTR_AUTOCOMMIT,0); 
		$conn->beginTransaction();
		$flag = false;
		while(true)
		{
			$fichier = "document/" . $namefic;
//			Trace("Traitement du fichier " . $fichier);
			$handle = fopen ($fichier,"r");
			// entete
			$ligne = fgetcsv($handle, 1000, ";");
			// test si utf8
//			echo 'encoding = ' . mb_detect_encoding($ligne[0]);
			if (mb_detect_encoding($ligne[0] != 'UTF-8')) $flagutf8 = true;
			else $flagutf8 = false;
//			if ($flagutf8) echo "pas UTF-8";
			if ($flagutf8) $ligne = array_map("utf8_encode",$ligne);
			$idbudget = $cleparent;
			// remplace
			if ($flagsup == 'o')
			{
//				Trace("Suppression des lignes existantes");
				$requete = "TRUNCATE TABLE budget_ligne";
				$statement = $conn->query($requete);
			}
			// 
			// traitement du fichier
			//
//			Trace("Début de la boucle des lignes");
			while ($ligne = fgetcsv($handle, 1000, ";")) 
			{
		//		print_r($ligne);
				if ($flagutf8) $ligne = array_map("utf8_encode",$ligne);
				// informations lignes
				$cleligne = $ligne[0];
				$idbudget = $ligne[1];
				$lignebud = $ligne[2];
				if ($lignebud == '') continue;
				$idpapfonct = $ligne[3];
				$idpapinves = $ligne[4];
				$idprojet = $ligne[5];
				$nomprojet = $ligne[6];
				$typelig = $ligne[7];
				$libelle = $ligne[8];
				$aedotafonct = str_replace(" ", "", $ligne[9]);
				$aedeblfonct = str_replace(" ", "", $ligne[10]);
				$aeprevfonct = str_replace(" ", "", $ligne[11]);
				$aeconsfonct = str_replace(" ", "", $ligne[12]);
				$cpdotafonct = str_replace(" ", "", $ligne[13]);
				$cpdeblfonct = str_replace(" ", "", $ligne[14]);
				$cpprevfonct = str_replace(" ", "", $ligne[15]);
				$cpconsfonct = str_replace(" ", "", $ligne[16]);
				$aedotainves = str_replace(" ", "", $ligne[17]);
				$aedeblinves = str_replace(" ", "", $ligne[18]);
				$aeprevinves = str_replace(" ", "", $ligne[19]);
				$aeconsinves = str_replace(" ", "", $ligne[20]);
				$cpdotainves = str_replace(" ", "", $ligne[21]);
				$cpdeblinves = str_replace(" ", "", $ligne[22]);
				$cpprevinves = str_replace(" ", "", $ligne[23]);
				$cpconsinves = str_replace(" ", "", $ligne[24]);
				// insertion de la ligne budgétaire	
				$requete = "insert into budget_ligne (BUDL_LIGNE, BUDL_IDBUDGET, BUDL_IDPAPFONCT, BUDL_IDPAPINVES, BUDL_IDPROJET, BUDL_TYPELIG, BUDL_LIBELLE, 
													  BUDL_AEDOTAFONCT, BUDL_AEDEBLFONCT, BUDL_AEPREVFONCT, BUDL_AECONSFONCT, 
													  BUDL_CPDOTAFONCT, BUDL_CPDEBLFONCT, BUDL_CPPREVFONCT, BUDL_CPCONSFONCT,
													  BUDL_AEDOTAINVES, BUDL_AEDEBLINVES, BUDL_AEPREVINVES, BUDL_AECONSINVES,
													  BUDL_CPDOTAINVES, BUDL_CPDEBLINVES, BUDL_CPPREVINVES, BUDL_CPCONSINVES)
							values (\"" . $lignebud . "\",\"" . $idbudget . "\",\"" . $idpapfonct . "\",\"" . $idpapinves . "\",\"" . $idprojet . "\",\"" . $typelig . "\",\"" . $libelle . "\",\"" 
										. $aedotafonct . "\",\"" . $aedeblfonct . "\",\"" . $aeprevfonct . "\",\"" . $aeconsfonct . "\",\""
										. $cpdotafonct . "\",\"" . $cpdeblfonct . "\",\"" . $cpprevfonct . "\",\"" . $cpconsfonct . "\",\""
										. $aedotainves . "\",\"" . $aedeblinves . "\",\"" . $aeprevinves . "\",\"" . $aeconsinves . "\",\""
										. $cpdotainves . "\",\"" . $cpdeblinves . "\",\"" . $cpprevinves . "\",\"" . $cpconsinves . "\")";
				$statement = $conn->query($requete);
			}
			fclose ($handle);
			$conn->commit(); 
			$flag = true;
//			Trace("COMMIT");
			break;
		}
		if ($flag == false) 
		{
//			Trace("ROLLBACK");
			$conn->rollBack();
			$objForm->erreur="nomfic";
			if ($objForm->libelle == '') $objForm->libelle = "Erreur sur chargement des lignes budgétaires"; 
		}
	}
	$result= unlink($fichier); // suppression du fichier
	if ($objForm->libelle == '')
	{
		RedirURL("bud_list_ligne.php");
		exit();
	}
}

// Affichage du début de la page
if ($cle != '') $objPage->addParam('cle', $cle);
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();

// affichage des boutons d'enchainement
$objForm->addBouton("reset","RETOUR","bud_list_ligne.php");
if (($cle == '' && $objProfil->cre) || ($cle != '' && $objProfil->maj))
{
	$objForm->addBouton("submit","ENVOYER");
}
else $objForm->setLecForm();

$objForm->affFormulaire();
$objForm->finFormulaire();

// fin de page
$objPage->finPage();
