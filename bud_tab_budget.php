<?php
$codefonc='bud';
include('prepage.php');

// début de page
$objPage->debPage('center');

$objOnglet = new onglet();

// début de l'onglet
$objOnglet->debOnglet();

// creation des liens
$objOnglet->debLienOnglet();
$objOnglet->addLienOnglet('bud','url','Budget global','bud_maj_budget.php?typeaction=' . $typeaction . '&cle=' . $cle);
$objOnglet->addLienOnglet('lig','url','Budget projet','bud_list_ligne.php?cleparent=' . $cle);
$objOnglet->addLienOnglet('prv','url','Prévision trimestre','bud_list_prevision.php?cleparent=' . $cle);
$objOnglet->addLienOnglet('pap','url','Budget PAP','bud_list_pap.php?cleparent=' . $cle);
$objOnglet->addLienOnglet('com','url','Commentaires','bud_list_commentaire.php?cleparent=' . $cle);
$objOnglet->finLienOnglet();

// fin de l'onglet
$objOnglet->finOnglet();

// trace d'accès au budget
TraceAcces($conn, $objProfil->idnom, "budget", $cle);

// fin de page
$objPage->finPage();
