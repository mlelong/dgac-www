<?php
$codefonc='*';
include('prepage.php');

$objPage->debPage('center');

//select d'affichage du montant des commandes
$requete_cmd = "select  
    SUM(CMD_TOTALTTC) as CMD_TOTALTTC, SUM(CMD_INVESTTC) as CMD_INVESTTC, SUM(CMD_FONCTTC) as CMD_FONCTTC
    from commande where CMD_IDBUDGET in (select BUD_CLE from budget where BUD_ANNEE=\"2019\")";

//select d'affichage du montant des budgets
$requete_bud = "select BUD_PROGRAMME, 
    BUD_AEPREVFONCT as BUD_PREVFONCT, BUD_AECONSFONCT as BUD_CONSFONCT,
    BUD_AEPREVINVES as BUD_PREVINVES, BUD_AECONSINVES as BUD_CONSINVES
    from budget where BUD_ANNEE=\"2019\"";
$requete_sbud = "select  
    SUM(BUD_AEPREVFONCT) as BUD_TOT_PREVFONCT, SUM(BUD_AECONSFONCT) as BUD_TOT_CONSFONCT,
    SUM(BUD_AEPREVINVES) as BUD_TOT_PREVINVES, SUM(BUD_AECONSINVES) as BUD_TOT_CONSINVES
    from budget where BUD_ANNEE=\"2019\"";
$requete_sbudl = "select 
    SUM(BUDL_AEPREVFONCT) as BUDL_TOT_PREVFONCT, SUM(BUDL_AECONSFONCT) as BUDL_TOT_CONSFONCT,
    SUM(BUDL_AEPREVINVES) as BUDL_TOT_PREVINVES, SUM(BUDL_AECONSINVES) as BUDL_TOT_CONSINVES
    from budget_ligne where BUDL_IDBUDGET in (select BUD_CLE from budget where BUD_ANNEE=\"2019\")";

// préparation de la requête de mise à jour
$requete_maj = "update test set ";

// récupération des derniers résultats en bdd
$requete = "select CMD_TOTALTTC, CMD_INVESTTC, CMD_FONCTTC,
			BUD1_PREVFONCT, BUD1_CONSFONCT, BUD1_PREVINVES, BUD1_CONSINVES,
			BUD2_PREVFONCT, BUD2_CONSFONCT, BUD2_PREVINVES, BUD2_CONSINVES,
			BUD3_PREVFONCT, BUD3_CONSFONCT, BUD3_PREVINVES, BUD3_CONSINVES,
			BUD4_PREVFONCT, BUD4_CONSFONCT, BUD4_PREVINVES, BUD4_CONSINVES,
			BUD_TOT_PREVFONCT, BUD_TOT_CONSFONCT, BUD_TOT_PREVINVES, BUD_TOT_CONSINVES,
			BUDL_TOT_PREVFONCT, BUDL_TOT_CONSFONCT, BUDL_TOT_PREVINVES, BUDL_TOT_CONSINVES
			from test where TES_CLE=\"1\"";
$statement = $conn->query($requete);
$resultat = $statement->fetch(PDO::FETCH_ASSOC);

// affichage des données sur les commandes
$statement1 = $conn->query($requete_cmd);
$resultat1 = $statement1->fetch(PDO::FETCH_ASSOC);
$difftotal = ($resultat1['CMD_TOTALTTC']-$resultat['CMD_TOTALTTC']);
$diffinvest = ($resultat1['CMD_INVESTTC']-$resultat['CMD_INVESTTC']);
$difffonc = ($resultat1['CMD_FONCTTC']-$resultat['CMD_FONCTTC']);
$totalttc = number_format($resultat['CMD_TOTALTTC'],2,',',' ');
$totalttc1 = number_format($resultat1['CMD_TOTALTTC'],2,',',' ');
$investtc = number_format($resultat['CMD_INVESTTC'],2,',',' ');
$investtc1 = number_format($resultat1['CMD_INVESTTC'],2,',',' ');
$foncttc = number_format($resultat['CMD_FONCTTC'],2,',',' ');
$foncttc1 = number_format($resultat1['CMD_FONCTTC'],2,',',' ');
$ligne = "&ensp;<B>Données commande</B><br>";
$ligne .= "&ensp;Avant maj CMD_TOTALTTC&ensp; = ".$totalttc."&ensp;après = ".$totalttc1."&ensp;diff = ".number_format($difftotal,2,',',' ')."<br>";
$ligne .= "&ensp;Avant maj CMD_INVESTTC&ensp; = ".$investtc."&ensp;après = ".$investtc1."&ensp;diff = ".number_format($diffinvest,2,',',' ')."<br>";
$ligne .= "&ensp;Avant maj CMD_FONCTTC&ensp; = ".$foncttc."&ensp;après = ".$foncttc1."&ensp;diff = ".number_format($difffonc,2,',',' ')."<br>";

$requete_maj .=	"CMD_TOTALTTC=\"" . $resultat1['CMD_TOTALTTC'] . "\",
			CMD_INVESTTC=\"" . $resultat1['CMD_INVESTTC'] . "\",
			CMD_FONCTTC=\"" . $resultat1['CMD_FONCTTC'] . "\",";

// affichage des données par programme
$cpt=1;
$statement2 = $conn->query($requete_bud);
while ($resultat2 = $statement2->fetch(PDO::FETCH_ASSOC))
{
	$diffprevfonct = ($resultat2['BUD_PREVFONCT']-$resultat['BUD'.$cpt.'_PREVFONCT']);
	$diffconsfonct = ($resultat2['BUD_CONSFONCT']-$resultat['BUD'.$cpt.'_CONSFONCT']);
	$diffprevinves = ($resultat2['BUD_PREVINVES']-$resultat['BUD'.$cpt.'_PREVINVES']);
	$diffconsinves = ($resultat2['BUD_CONSINVES']-$resultat['BUD'.$cpt.'_CONSINVES']);
	$prevfonct = number_format($resultat['BUD'.$cpt.'_PREVFONCT'],2,',',' ');
	$consfonct = number_format($resultat['BUD'.$cpt.'_CONSFONCT'],2,',',' ');
	$previnves = number_format($resultat['BUD'.$cpt.'_PREVINVES'],2,',',' ');
	$consinves = number_format($resultat['BUD'.$cpt.'_CONSINVES'],2,',',' ');
	$prevfonct2 = number_format($resultat2['BUD_PREVFONCT'],2,',',' ');
	$consfonct2 = number_format($resultat2['BUD_CONSFONCT'],2,',',' ');
	$previnves2 = number_format($resultat2['BUD_PREVINVES'],2,',',' ');
	$consinves2 = number_format($resultat2['BUD_CONSINVES'],2,',',' ');
	$ligne .= "&ensp;<B>Données du programme ".$resultat2['BUD_PROGRAMME']."</B><br>";
	$ligne .= "&ensp;Avant maj BUD_PREVFONCT&ensp; = ".$prevfonct."&ensp;après = ".$prevfonct2."&ensp;diff = ".number_format($diffprevfonct,2,',',' ')."<br>";
	$ligne .= "&ensp;Avant maj BUD_CONSFONCT&ensp; = ".$consfonct."&ensp;après = ".$consfonct2."&ensp;diff = ".number_format($diffconsfonct,2,',',' ')."<br>";
	$ligne .= "&ensp;Avant maj BUD_PREVINVES&ensp; = ".$previnves."&ensp;après = ".$previnves2."&ensp;diff = ".number_format($diffprevinves,2,',',' ')."<br>";
	$ligne .= "&ensp;Avant maj BUD_CONSINVES&ensp; = ".$consinves."&ensp;après = ".$consinves2."&ensp;diff = ".number_format($diffconsinves,2,',',' ')."<br>";

	$requete_maj .=	"BUD".$cpt."_PREVFONCT=\"" . $resultat2['BUD_PREVFONCT'] . "\",
			BUD".$cpt."_CONSFONCT=\"" . $resultat2['BUD_CONSFONCT'] . "\",
			BUD".$cpt."_PREVINVES=\"" . $resultat2['BUD_PREVINVES'] . "\",
			BUD".$cpt."_CONSINVES=\"" . $resultat2['BUD_CONSINVES'] . "\",";
	$cpt++;
}
// affichage de la somme des programmes
$statement3 = $conn->query($requete_sbud);
$resultat3 = $statement3->fetch(PDO::FETCH_ASSOC);
$diffprevfonct = ($resultat3['BUD_TOT_PREVFONCT']-$resultat['BUD_TOT_PREVFONCT']);
$diffconsfonct = ($resultat3['BUD_TOT_CONSFONCT']-$resultat['BUD_TOT_CONSFONCT']);
$diffprevinves = ($resultat3['BUD_TOT_PREVINVES']-$resultat['BUD_TOT_PREVINVES']);
$diffconsinves = ($resultat3['BUD_TOT_CONSINVES']-$resultat['BUD_TOT_CONSINVES']);
$prevfonct = number_format($resultat['BUD_TOT_PREVFONCT'],2,',',' ');
$consfonct = number_format($resultat['BUD_TOT_CONSFONCT'],2,',',' ');
$previnves = number_format($resultat['BUD_TOT_PREVINVES'],2,',',' ');
$consinves = number_format($resultat['BUD_TOT_CONSINVES'],2,',',' ');
$prevfonct3 = number_format($resultat3['BUD_TOT_PREVFONCT'],2,',',' ');
$consfonct3 = number_format($resultat3['BUD_TOT_CONSFONCT'],2,',',' ');
$previnves3 = number_format($resultat3['BUD_TOT_PREVINVES'],2,',',' ');
$consinves3 = number_format($resultat3['BUD_TOT_CONSINVES'],2,',',' ');
$ligne .= "&ensp;<B>Données somme des programmes</B><br>";
$ligne .= "&ensp;Avant maj BUD_TOT_PREVFONCT&ensp; = ".$prevfonct."&ensp;après = ".$prevfonct3."&ensp;diff = ".number_format($diffprevfonct,2,',',' ')."<br>";
$ligne .= "&ensp;Avant maj BUD_TOT_CONSFONCT&ensp; = ".$consfonct."&ensp;après = ".$consfonct3."&ensp;diff = ".number_format($diffconsfonct,2,',',' ')."<br>";
$ligne .= "&ensp;Avant maj BUD_TOT_PREVINVES&ensp; = ".$previnves."&ensp;après = ".$previnves3."&ensp;diff = ".number_format($diffprevinves,2,',',' ')."<br>";
$ligne .= "&ensp;Avant maj BUD_TOT_CONSINVES&ensp; = ".$consinves."&ensp;après = ".$consinves3."&ensp;diff = ".number_format($diffconsinves,2,',',' ')."<br>";

$requete_maj .=	"BUD_TOT_PREVFONCT=\"" . $resultat3['BUD_TOT_PREVFONCT'] . "\",
			BUD_TOT_CONSFONCT=\"" . $resultat3['BUD_TOT_CONSFONCT'] . "\",
			BUD_TOT_PREVINVES=\"" . $resultat3['BUD_TOT_PREVINVES'] . "\",
			BUD_TOT_CONSINVES=\"" . $resultat3['BUD_TOT_CONSINVES'] . "\",";

// affichage de la somme des lignes budgétaire
$statement4 = $conn->query($requete_sbudl);
$resultat4 = $statement4->fetch(PDO::FETCH_ASSOC);
$diffprevfonct = ($resultat4['BUDL_TOT_PREVFONCT']-$resultat['BUDL_TOT_PREVFONCT']);
$diffconsfonct = ($resultat4['BUDL_TOT_CONSFONCT']-$resultat['BUDL_TOT_CONSFONCT']);
$diffprevinves = ($resultat4['BUDL_TOT_PREVINVES']-$resultat['BUDL_TOT_PREVINVES']);
$diffconsinves = ($resultat4['BUDL_TOT_CONSINVES']-$resultat['BUDL_TOT_CONSINVES']);
$prevfonct = number_format($resultat['BUDL_TOT_PREVFONCT'],2,',',' ');
$consfonct = number_format($resultat['BUDL_TOT_CONSFONCT'],2,',',' ');
$previnves = number_format($resultat['BUDL_TOT_PREVINVES'],2,',',' ');
$consinves = number_format($resultat['BUDL_TOT_CONSINVES'],2,',',' ');
$prevfonct4 = number_format($resultat4['BUDL_TOT_PREVFONCT'],2,',',' ');
$consfonct4 = number_format($resultat4['BUDL_TOT_CONSFONCT'],2,',',' ');
$previnves4 = number_format($resultat4['BUDL_TOT_PREVINVES'],2,',',' ');
$consinves4 = number_format($resultat4['BUDL_TOT_CONSINVES'],2,',',' ');
$ligne .= "&ensp;<B>Données somme des lignes budgétaires</B><br>";
$ligne .= "&ensp;Avant maj BUDL_TOT_PREVFONCT&ensp; = ".$prevfonct."&ensp;après = ".$prevfonct4."&ensp;diff = ".number_format($diffprevfonct,2,',',' ')."<br>";
$ligne .= "&ensp;Avant maj BUDL_TOT_CONSFONCT&ensp; = ".$consfonct."&ensp;après = ".$consfonct4."&ensp;diff = ".number_format($diffconsfonct,2,',',' ')."<br>";
$ligne .= "&ensp;Avant maj BUDL_TOT_PREVINVES&ensp; = ".$previnves."&ensp;après = ".$previnves4."&ensp;diff = ".number_format($diffprevinves,2,',',' ')."<br>";
$ligne .= "&ensp;Avant maj BUDL_TOT_CONSINVES&ensp; = ".$consinves."&ensp;après = ".$consinves4."&ensp;diff = ".number_format($diffconsinves,2,',',' ')."<br>";

$requete_maj .=	"BUDL_TOT_PREVFONCT=\"" . $resultat4['BUDL_TOT_PREVFONCT'] . "\",
			BUDL_TOT_CONSFONCT=\"" . $resultat4['BUDL_TOT_CONSFONCT'] . "\",
			BUDL_TOT_PREVINVES=\"" . $resultat4['BUDL_TOT_PREVINVES'] . "\",
			BUDL_TOT_CONSINVES=\"" . $resultat4['BUDL_TOT_CONSINVES'] . "\"";

$objPage->tampon .= $ligne;

$requete_maj .= " where TES_CLE=\"1\""; 
$statement = $conn->exec($requete_maj);
//echo $requete_maj;

// fin de page
$objPage->finPage();
