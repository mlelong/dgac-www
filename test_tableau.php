<?php
include('config/test_tableau.php');
include('prepage.php');

$objPage->debPage('center');

// préparation du tableau
$objTab = new tableau('1');

// requête d'accès à la base 
$requete = "select MAR_CLE, MAR_NUMERODGR, MAR_NUMEROSIF, MAR_NOMMARCHE, PRJ_NOMPROJET,
			POL_NOMPOLE, MAR_FLAGRMA, MAR_FLAGCB, MAR_ETAT,MAR_AEPREVTOTAL
			from marche 
			left join projet on MAR_IDPROJET=PRJ_CLE
			left join pole on MAR_IDPOLE=POL_CLE
			where 1 ";
$requete .= $objTab->majRequete('order by MAR_NUMEROSIF desc'); // ajout tri et pagination si besoin

// affichage des boutons d'enchainement
$objTab->addBoutonSpe("button","VALIDER","onclick=\"getChampTableau()\"");

$objTab->setLigneAttr(" id='#MAR_CLE#'");

// gestion des paramètres de lien
$objTab->setLien('numerodgr','mar_list_marche.php',"?typeaction=modification&cle=#MAR_CLE#");
$objTab->setLien('numerosif','mar_list_marche.php',"?typeaction=modification&cle=#MAR_CLE#");
$objTab->setLien('supp','mar_list_marche.php',"?typeaction=suppression&cle=#MAR_CLE#","Supprimer");

// affichage du tableau
$objTab->affTableau($requete);

// fin de page
$objPage->finPage();
?>
<script>

$(function(){
	// on coche la checkbox après affichage du tableau
	$("[mgf-checkbox='o']").each(function()
	{
		$(this).prop("checked",true);
	});
	// on mémorise les checkbox modifiés
	$(".mgf_checkable").change(function()
	{
		$(this).addClass("mgf_checkchange");
	});
	// on mémorise les input modifiés
	$(".mgf_editable").change(function()
	{
		$(this).addClass("mgf_editchange");
	});
});

function getChampTableau() 
{
	var objCle = {}; // déclaration de l'objet json
	var tabCle = []; // déclaration du tableau des lignes
	objCle.fonction = 'Marché';
	objCle.tabCle = tabCle;
	// boucle sur toutes les lignes avec des checkbox validées
	$(".mgf_checkchange").each(function() {
//		console.log('id=' + $(this).parent().parent().attr('id') + ' nom=' + $(this).attr('mgf-nom') + ' valeur=o');
		// on regarde si la checkbox est coché
		if ($(this).is(":checked")) var valeur = 'o';
		else var valeur = 'n';
		// création d'un objet json par ligne
		var cle = {
			"cle" : $(this).parent().parent().attr('id'),
			"nom" : $(this).attr('mgf-nom'),
			"valeur" : valeur
		}
		// ajout dans le tableau  des objets
		objCle.tabCle.push(cle);
	});
	// boucle sur toutes les lignes avec des champs editables modifiés
	$(".mgf_editchange").each(function() {
		// création d'un objet json par ligne
		var cle = {
			"cle" : $(this).parent().parent().attr('id'),
			"nom" : $(this).attr('mgf-nom'),
			"valeur" : $(this).val()
		}
		// ajout dans le tableau  des objets
		objCle.tabCle.push(cle);
	});
	console.log(JSON.stringify(objCle));
	var retjson = false;
	if (retjson)
	{	
		$.ajax({
			url: "test_tableau-json.php",
			type: 'POST',
			data: {objCle : JSON.stringify(objCle)},
			dataType: 'json',
			async: false,
			success: function(response){RETgetChampTableau(response);}
		});
	}
	else
	{	
		$.ajax({
			url: "test_tableau-json.php",
			type: 'POST',
			data: {objCle : JSON.stringify(objCle)},
			async: false,
			success: function(response){RETgetChampTableau(response);}
		});
	}
}
// retour AJAX de getChampTableau
function RETgetChampTableau(response) 
{
	console.log('RETgetChampTableau=' + response);
	var objCle = JSON.parse(response);
	var fonction = objCle.fonction;
	// on supprime les indicateurs de changement après mise à jour
	$(".mgf_checkchange").removeClass('mgf_checkchange');
	$(".mgf_editchange").removeClass('mgf_editchange');
}

</script>