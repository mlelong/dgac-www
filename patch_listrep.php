<?php
include('config/patch_listrep.php');

$codefonc='pat';
require_once('prepage.php');

$objForm = new formulaire('1');

$retour = '';
$nomrep = '';

// création d'une entrée
if($typeaction == "creation") $objForm->initChamp();

// réception des paramètres
if($typeaction == "reception")
{
// contrôle des paramètres
	$objForm->recChamp();
	while ($objForm->erreur == '')
	{
		break;
	}
	if ($objForm->erreur == '')
	{
		if ($nomrep == '') $nomrep = ".";
		$tabrep = array();
		$cptrep = 0;
		$dir = dir($nomrep);
		while ($ficsrc = $dir->read()) // lecture du contenu pour les répertoires
		{
			if ($ficsrc == "." || $ficsrc == "..") continue;
			if (is_dir($nomrep . "/" . $ficsrc)) // c'est un répertoire
			{
				$tabrep[$cptrep] = $ficsrc;
				$cptrep++;
			}
		}
		
		$tabfic = array();
		$cptfic = 0;
		$dir = dir($nomrep);
		while ($ficsrc = $dir->read()) // lecture du contenu pour les fichiers
		{
			if ($ficsrc == "." || $ficsrc == "..") continue;
			if (is_file($nomrep . "/" . $ficsrc)) // c'est un fichier
			{
				$tabfic[$cptfic] = $ficsrc;
				$cptfic++;
			}
		}
	}	
}

// préparation du résultat

if (isset($tabrep))
{	
	sort($tabrep);
	for($i=0; $i<count($tabrep); $i++)
	{
		$retour .= "<p>Répertoire<span style='font-size:1.6em;color:blue;'> " . $tabrep[$i] . "</span></p>";
	}
}

if (isset($tabfic))
{	
	sort($tabfic);
	for($i=0; $i<count($tabfic); $i++)
	{
		$retour .= "<p>Fichier<span style='font-size:1.6em;color:blue;'> <a href='patch_download.php?fic=" . $nomrep . "/" . $tabfic[$i] . "'> " . $tabfic[$i] . "</a></span></p>";
	}
}

// Affichage du début de la page
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();

// affichage des boutons d'enchainement
$objForm->addBouton("button","RETOUR","patch.php");
if ($objProfil->maj)
{
	$objForm->addBouton("submit","ENVOYER");
}
else $objForm->setLecForm();

$objForm->affFormulaire();
$objForm->finFormulaire();

if ($retour != '') $objPage->tampon .= $retour;

// fin du formulaire et de la page
$objPage->finPage();
