<?php
//error_reporting(E_ALL ^ E_WARNING); 
//error_reporting(E_ALL ^ E_DEPRECATED);
ini_set ('session.bug_compat_42', 0);
ini_set ('session.bug_compat_warn', 0); 

// autoload des fichiers de class lors de l'appel à new
spl_autoload_register(function ($nomclass) {
    require_once('class/' . $nomclass . '.php');
});

// fonctions utilisables dans la page
require_once('fonction.php');

// la classe profil doit être déclarée avant le start session et avant la classe page
require_once('class/profil.php');
session_start(); 

// préparation de la page
$objPage = page::getPage();

// connexion à la base de données
$conn = database::getIntance();

// pas de session en cours (24 minutes)-> coockie ou connexion
if(!isset($_SESSION['s_profil']))
{
	// true si on veut gérer les cookies sinon false 
	$cookie = false; 
	$flagprofil = false;
	// cookies non gérés -> on dirige vers la connexion
	if ($cookie === true) 
	{
		// coockies gérés -> on regarde s'il y a un cookie -> on récupère les infos de l'utilisateur	
		if (isset($_COOKIE['s_idnom']) && $_COOKIE['s_idnom'] != '') 
		{
			$requete = "select UTI_CLE, UTI_IDENTIFIANT, UTI_NOM, UTI_PRENOM, UTI_PWD
						from utilisateur where UTI_CLE=\"" . $_COOKIE['s_idnom'] . "\"";
			$statement = $conn->query($requete);
			$resultat = $statement->fetch(PDO::FETCH_ASSOC);
			if ($resultat['UTI_IDENTIFIANT'] != '') 
			{
				// l'identifiant existe -> on positionne les droits
				$objProfil = profil::getProfil();
				$objProfil->objPage = $objPage;
				$objProfil->setProfil($resultat);
				$flagprofil = true;
			}	
		}
	}
	// utilisateur pas ou plus identifié -> on force la connexion
	if ($flagprofil === false) 
	{
		$_SESSION['request_uri'] = $_SERVER['REQUEST_URI'];
		header("Location: log_connexion.php");
		exit();
	}
}
// session en cours -> on récupère les droits à partir de la session	
else $objProfil = profil::getSession();

// gestion des variables cle cleparent et typeaction
if(isset($_REQUEST['cle'])) $cle=$_REQUEST['cle'];
else $cle = '';
if(isset($_REQUEST['cleparent'])) $cleparent=$_REQUEST['cleparent'];
else $cleparent = '';
if ($cleparent != '') $objPage->addParam('cleparent', $cleparent);
// echo "cle=" . $cle . " cleparent=" . $cleparent;
// on récupère le type d'action a effectuer (par défaut c'est une création)
if(isset($_REQUEST['typeaction'])) $typeaction=$_REQUEST['typeaction'];
if(!isset($typeaction) || $typeaction == '') $typeaction="creation";

// test pour savoir si la fonction est autorisée pour ce profil 
$objProfil->ctlFonc($codefonc, $typeaction);
