<?php
include('config/patch_base.php');

$codefonc='pat';
require_once('prepage.php');

$objForm = new formulaire('1');

$retour = '';
if(!isset($typeaction)) $typeaction="creation";

// création d'une entrée
if($typeaction == "creation") $objForm->initChamp();

// réception des paramètres
if($typeaction == "reception")
{
// contrôle des paramètres
	$objForm->recChamp();
	while ($objForm->erreur == '')
	{
		break;
	}
// mise a jour 
	if ($objForm->erreur == '')
	{
		$requetes = $req;
		$requetes = str_replace("\r\n", "\n", $requetes);
		$requetes = str_replace("\r", "", $requetes);
		$requetes = str_replace("\n", "", $requetes);
		$listreq = explode(";", $requetes);
		for ($i=0; $i<count($listreq); $i++)
		{
			if ($listreq[$i] == '') continue; 
			$retour .= "<p style='font-size:1.4em;'>-------------------------------------------------------------------------------------<br>";
			$retour .= "Requete = " . $listreq[$i] . "</p>";
			$requete = $listreq[$i];
			$statement = $conn->query($requete);
			if($statement !== false)
			{
				$retour .= "<p style='font-size:1.4em;'>Résultat</p>";
				if ($statement === true)
				{
					$retour .= "<p style='font-size:1.1em;'>Requête exécutée sans erreur</p>";
				}
				else
				{
					$retour .= "<div><table style='width:100%;overflow:auto;border:1px solid gray;border-collapse:collapse;'>";
					$TabResultat=array();
					$cpt = 0;
					while ($row = $statement->fetch(PDO::FETCH_ASSOC))
					{
						$ligne = '';
						$lignecol = '';
						foreach ($row as $clef => $valeur) 
						{
							if ($cpt == 0) 
							{
								if ($lignecol == '') $lignecol .= "<tr>"; 
								$lignecol .= "<th style='border:1px solid gray;border-collapse:collapse;padding-left:2px;padding-right:2px;'>" . $clef . "</th>"; 
							}	
							if ($ligne == '') $ligne .= "<tr>"; 
							$ligne .= "<td style='border:1px solid gray;border-collapse:collapse;padding-left:2px;padding-right:2px;'>" . $valeur . "</td>"; 
						}
						if ($lignecol != '') $retour .= $lignecol . "</tr>";
						$retour .= $ligne . "</tr>";
						$cpt++;
					}
					$retour .= "</table></div>";
				}
			}
			else
			{
				$retour .= "<p style='font-size:1.4em;'>Erreur numéro " . $conn->errorInfo() . "<p>";
			}
		}
	}	
}
else $requetes = '';

// Affichage du début de la page
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();

// affichage des boutons d'enchainement
$objForm->addBouton("button","RETOUR","patch.php");
if ($objProfil->maj)
{
	$objForm->addBouton("submit","ENVOYER");
}
else $objForm->setLecForm();

$objForm->affFormulaire();
$objForm->finFormulaire();

if ($retour != '') $objPage->tampon .= $retour;

// fin du formulaire et de la page
$objPage->finPage();
