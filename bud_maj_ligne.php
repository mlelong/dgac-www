<?php
include('config/bud_maj_ligne.php');
$codefonc='bud';
require_once('prepage.php');

$objForm = new formulaire('1');

if($typeaction == "creation") $objForm->initChamp();

if($typeaction == "modification")
{
	$requete = "select BUDL_CLE, BUDL_LIGNE, BUDL_IDBUDGET, BUDL_IDPAPFONCT, BUDL_IDPAPINVES, 
				BUDL_IDPROJET, BUDL_TYPELIG, BUDL_LIBELLE,BUDL_TYPEIMPUTATION,  BUDL_AEDOTAFONCT, 
				BUDL_CPDOTAFONCT, BUDL_AEDOTAINVES, BUDL_CPDOTAINVES, BUDL_PRIORITE, BUDL_COMMENTAIRE
				from budget_ligne where BUDL_CLE=\"" . $cle . "\"";
	$resultat = $objForm->mapChamp($requete);
}

// réception des paramètres
if($typeaction == "reception")
{
// contrôle des paramètres
	$objForm->recChamp();
	while($objForm->erreur == '')
	{
		break;
	}

	if ($objForm->erreur == '')
	{
		if ($aedotafonct == '') $aedotafonct=0;
		if ($aedotainves == '') $aedotainves=0;
		if ($cpdotafonct == '') $cpdotafonct=0;
		if ($cpdotainves == '') $cpdotainves=0;
		// mise à jour de la ligne budgétaire
		if (!$objForm->majBdd('budget_ligne', 'BUDL_CLE', $cle)) 
		{
			if ($objForm->libelle == '1062') {$objForm->erreur='idprojet'; $objForm->libelle="le projet a déjà une ligne budgétaire";}
		}	
		else 
		{
			$objForm->libelle="Mise à jour effectuée";
			RedirURL("bud_list_ligne.php");
			exit();
		}
	}
}

// Affichage du début de la page
if ($cle != '') $objPage->addParam('cle', $cle);
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();

$requete = "select BPAP_CLE, BPAP_LIGNEPAP from budget_pap 
			where BPAP_FLAGIMPUTATION=\"F\" and BPAP_IDBUDGET=\"" . $cleparent . "\" order by 2";
$objForm->setSelect('idpapfonct', $idpapfonct, $requete, 'BPAP_CLE', 'BPAP_LIGNEPAP');

$requete = "select BPAP_CLE, BPAP_LIGNEPAP from budget_pap 
			where BPAP_FLAGIMPUTATION=\"I\" and BPAP_IDBUDGET=\"" . $cleparent . "\" order by 2";
$objForm->setSelect('idpapinves', $idpapinves, $requete, 'BPAP_CLE', 'BPAP_LIGNEPAP');

if ($cle == '')
	$requete = "select PRJ_CLE, PRJ_NOMPROJET from projet order by 2";
//				where PRJ_CLE not in (select BUDL_IDPROJET from budget_ligne where BUDL_IDBUDGET=\"" . $cleparent . "\") order by 2";
else				
	$requete = "select PRJ_CLE, PRJ_NOMPROJET from projet 
				where PRJ_CLE=\"" . $idprojet . "\" order by 2";
$objForm->setSelect('idprojet', $idprojet, $requete, 'PRJ_CLE', 'PRJ_NOMPROJET');

// affichage des boutons d'enchainement
$objForm->addBouton("reset","RETOUR","bud_list_ligne.php");
if (($cle == '' && $objProfil->cre) || ($cle != '' && $objProfil->maj))
{
	$objForm->addBouton("submit","ENVOYER");
}
else $objForm->setLecForm();

$objForm->affFormulaire();
$objForm->finFormulaire();

// fin de page
$objPage->finPage();
