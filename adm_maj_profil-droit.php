<?php
include('config/adm_maj_profil-droit.php');
$codefonc='pro';
require_once('prepage.php');

require_once('adm_fonc_droit.php');
$objDroit = new droit();

if(!isset($typeaction)) $typeaction="modification";
if(!isset($cle)) $cle="";

$objDroit->creFonction();

// réception des paramètres
if($typeaction == "reception")
{
	$requete   = "select PRO_LISTDROIT FROM profil where PRO_CLE=\"" .$cle . "\"";
	$statement = $conn->query($requete);
	$resultat  = $statement->fetch(PDO::FETCH_ASSOC);
	$olddroit=$resultat['PRO_LISTDROIT'];
	
	$newdroit=$objDroit->getDroit();
	$requete   = "update profil set PRO_LISTDROIT=\"" . $newdroit . "\" 
				where PRO_CLE=\"" .$cle . "\"";
	$statement = $conn->query($requete);

	RedirURL("adm_list_profil.php");
	exit();
}

if ($cle != '') $objPage->addParam('cle', $cle);
$objPage->debPage('center');

// préparation du tableau
$objTab = new tableau('1');

// creation dynamique des boutons de profil
//$objPage->tampon .= "<label><B>Rôle affecté :</B></label>\n";
//$objPage->tampon .= affProfil($profil, 'cumul');

$requete   = "select PRO_LISTDROIT FROM profil where PRO_CLE=\"" .$cle . "\"";
$statement = $conn->query($requete);
$resultat  = $statement->fetch(PDO::FETCH_ASSOC);
$nom = $cle;
$objDroit->affDroit($objTab, $resultat['PRO_LISTDROIT']);

// affichage des boutons d'enchainement
$objTab->addBouton("button","RETOUR","adm_list_profil.php");
$objTab->addBoutonSpe("button","Effacer tout","onclick=\"javascript:razCheck()\"");
$objTab->addBoutonSpe("button","Sel. tout","onclick=\"javascript:selectCheck()\"");
$objTab->addBouton("submit","VALIDER");

$objTab->finTableau();

// fin du formulaire et de la page
$objPage->finPage();
?>
<script type="text/javascript" src="js/anu_fonc_droit.js"></script>
