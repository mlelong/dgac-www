<?php
include('config/patch_visufic.php');

$codefonc='pat';
require_once('prepage.php');

$objForm = new formulaire('1');

$retour = '';
$nomfic = '';

// création d'une entrée
if($typeaction == "creation") $objForm->initChamp();

// réception des paramètres
if($typeaction == "reception")
{
// contrôle des paramètres
	$objForm->recChamp();
	while ($objForm->erreur == '')
	{
		break;
	}
	if ($objForm->erreur == '')
	{
		if (file_exists($nomfic)) // vérification pour savoir si le fichier existe
		{
			$retour .= "<PRE style='font-size:1.1em;'>" . htmlspecialchars(file_get_contents($nomfic)) . "</PRE>";
		}
		else
		{
			$retour .= "<p style='font-size:1.4em;'>Le fichier n'existe pas<p>";
		}
	}	
}

// Affichage du début de la page
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();

// affichage des boutons d'enchainement
$objForm->addBouton("button","RETOUR","patch.php");
if ($objProfil->maj)
{
	$objForm->addBouton("submit","ENVOYER");
}
else $objForm->setLecForm();

$objForm->affFormulaire();
$objForm->finFormulaire();

if ($retour != '') $objPage->tampon .= $retour;

// fin du formulaire et de la page
$objPage->finPage();
