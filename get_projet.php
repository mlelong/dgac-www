<?php
//indique que le type de la reponse renvoyee au client sera du Texte
header("Content-Type: text/html ; charset=utf-8");

//anti Cache pour HTTP/1.1
header("Cache-Control: no-cache , private");

//anti Cache pour HTTP/1.0
header("Pragma: no-cache");

require_once('connect_base.php');

try 
{
	$conn = new PDO('mysql:host='.$base.';port='.$port.';dbname='.$dbname.';charset=utf8;', $user, $pass); 
} 
catch (PDOException $e) 
{
	echo " ";
	exit; 
}

// r�cup�ration de l'id du p�le
if(isset($_REQUEST['idpole'])) $idpole = $_REQUEST['idpole'];
else $idpole = 0;
// affichage des projets en fonction du p�le
if($idpole > 0)
{
	// r�cup�ration des dates
	if(isset($_REQUEST['datecre'])) $datecre = $_REQUEST['datecre'];
	else $datecre = '';
	if(isset($_REQUEST['datepre'])) $datepre = $_REQUEST['datepre'];
	else $datepre = '';
	if(isset($_REQUEST['datecmd'])) $datecmd = $_REQUEST['datecmd'];
	else $datecmd = '';
	// extraction des ann�es
	if ($datecre != '') 
	{
		$tab = explode('-', $datecre);
		if (count($tab) != 3) $tab = explode('/', $datecre);
		if (count($tab) == 3) $datecre = $tab[2];
		else $datecre = '';
	}
	if ($datepre != '') 
	{
		$tab = explode('-', $datepre);
		if (count($tab) != 3) $tab = explode('/', $datepre);
		if (count($tab) == 3) $datepre = $tab[2];
		else $datepre = '';
	}
	if ($datecmd != '') 
	{
		$tab = explode('-', $datecmd);
		if (count($tab) != 3) $tab = explode('/', $datecmd);
		if (count($tab) == 3) $datecmd = $tab[2];
		else $datecmd = '';
	}
	// calcul de l'ann�e budg�taire 
	if ($datecmd != '') $curdate = $datecmd;
	else if ($datepre != '') $curdate = $datepre;
	else if ($datecre != '') $curdate = $datecre;
	else $curdate = date("Y");
	// recherche des projets avec une ligne budg�taire pour l'ann�e
	$requete = "select CONCAT(PRJ_NOMPROJET,\" ->\t\",IFNULL(BUDL_LIBELLE,'')) as PRJ_NOMPROJET from budget_ligne 
				left join projet on BUDL_IDPROJET=PRJ_CLE
				left join budget on BUDL_IDBUDGET=BUD_CLE
				where PRJ_IDPOLE IN (select POL_CLE from pole where POL_IDDOMAINE=(select POL_IDDOMAINE from pole where POL_CLE=".$idpole.")) 
				and BUD_ANNEE=\"".$curdate."\" order by 1";
}
// ou affichage de l'ensemble des projets
else $requete = "select CONCAT(PRJ_NOMPROJET,\" ->\t\",BUDL_LIBELLE) as PRJ_NOMPROJET from budget_ligne 
				left join projet on BUDL_IDPROJET=PRJ_CLE
				left join budget on BUDL_IDBUDGET=BUD_CLE
				where BUD_ANNEE=\"".$curdate."\" order by 1";
$statement = $conn->query($requete);
$retour = '';
while ($row = $statement->fetch(PDO::FETCH_ASSOC))
{
	if ( $retour == '') $retour = $row['PRJ_NOMPROJET'];
	else $retour .= "*;*" . $row['PRJ_NOMPROJET'];
}
if (isset($conn)) $conn=null; 
echo $retour;

