<?php
include('config/adm_list_profil-groupe.php');
$codefonc='pro';
include('prepage.php');
if (!$objProfil->ctlDroit($typeaction, 'formulaire',$cle)) exit();

$objForm = new formulaire('1');

$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();
// Affichage des groupes de l'utilisateur
$datalistecle = '';
$datalisteval = '';
$requete = "select GRU_CLE, GRU_NOMGROUPE
			from groupe, groupe_profil
			where GRU_CLE=RGP_IDGROUPE and RGP_IDPROFIL=\"" . $cle . "\"
			order by 2";
$statement = $conn->query($requete);
while ($row = $statement->fetch(PDO::FETCH_ASSOC))
{
	$affichage = $row['GRU_NOMGROUPE'];
	if($datalisteval == '') $datalisteval = $affichage;
	else $datalisteval .= ';' . $affichage;
	if($datalistecle == '') $datalistecle = $row['GRU_CLE'];
	else $datalistecle .= ';' . $row['GRU_CLE'];
}
$objForm->champF['idnomlec']['dataliste1'] = $datalistecle . "*;*" . $datalisteval;

// Affichage des autres groupes
$datalistecle = '';
$datalisteval = '';
$requete = "select GRU_CLE, GRU_NOMGROUPE from groupe 
			where GRU_CLE NOT IN (select RGP_IDGROUPE from groupe_profil where RGP_IDPROFIL=\"" . $cle . "\") 
			order by 2";
$statement = $conn->query($requete);
while ($row = $statement->fetch(PDO::FETCH_ASSOC))
{
	$affichage = $row['GRU_NOMGROUPE'];
	if($datalisteval == '') $datalisteval = $affichage;
	else $datalisteval .= ';' . $affichage;
	if($datalistecle == '') $datalistecle = $row['GRU_CLE'];
	else $datalistecle .= ';' . $row['GRU_CLE'];
}
$objForm->champF['idnomlec']['dataliste2'] = $datalistecle . "*;*" . $datalisteval;

$objForm->affFormulaire();
// affichage des boutons d'enchainement
$objForm->addBouton("reset","RETOUR","adm_list_profil.php");
if ($objProfil->maj) 
{
	$objForm->addBoutonSpe("button","VALIDER","onclick=\"majFormSortableDiff('pro-g'," . $cle . ");VersURL('adm_list_profil.php');\"");
}

// fin de page
$objForm->finFormulaire();
$objPage->finPage();
