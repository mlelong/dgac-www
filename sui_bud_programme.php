<?php
include('config/sui_bud_programme.php');
$codefonc='rpm';
include('prepage.php');

$objPage->debPage('center');

// préparation du tableau
$objTab = new tableau('1');

$objTab->addBouton("reset","RETOUR","sui_list_rapport.php");

if (!isset($f_annee) || $f_annee == '') $f_annee = date("Y");
if (!isset($f_programme) || $f_programme == '') $f_programme = "P613";

// requête d'accès à la base 
	$requete = "select \"Fonctionnement\" as  BUD_LIBELLE,
	BUD_AEDOTAFONCT as BUD_AEDOTA, BUD_AEDEBLFONCT as BUD_AEDEBL, BUD_AEPREVFONCT as BUD_AEPREV, BUD_AECONSFONCT as BUD_AECONS,
	BUD_CPDOTAFONCT as BUD_CPDOTA, BUD_CPDEBLFONCT as BUD_CPDEBL, BUD_CPPREVFONCT as BUD_CPPREV, BUD_CPCONSFONCT as BUD_CPCONS
	from budget where BUD_ANNEE=\"".$f_annee."\" and BUD_PROGRAMME=\"".$f_programme."\"
	union
	select \"Investissement\" as  BUD_LIBELLE,	
	BUD_AEDOTAINVES as BUD_AEDOTA, BUD_AEDEBLINVES as BUD_AEDEBL, BUD_AEPREVINVES as BUD_AEPREV, BUD_AECONSINVES as BUD_AECONS,
	BUD_CPDOTAINVES as BUD_CPDOTA, BUD_CPDEBLINVES as BUD_CPDEBL, BUD_CPP
	REVINVES as BUD_CPPREV, BUD_CPCONSINVES as BUD_CPCONS
	from budget where BUD_ANNEE=\"".$f_annee."\" and BUD_PROGRAMME=\"".$f_programme."\"";
$requete .= $objTab->majRequete(''); // ajout tri et pagination si besoin

// affichage des boutons d'enchainement
//$objTab->addBoutonSpe("button","EXPORTER","onclick=\"document.location.href='bud_csv_ligne.php';\"");

// affichage du tableau
$objTab->affTableau($requete);

// fin de page
$objPage->finPage();
