<?php
include('config/sui_maj_rapport.php');
$codefonc='rpm';
require_once('prepage.php');

$objForm = new formulaire('1');

$objPage->addParam('rapport', $_REQUEST['rapport']);

if($typeaction == "creation") $objForm->initChamp();

if($typeaction == "modification")
{
	$requete = "select RAP_CLE, RAP_NOMRAPPORT, RAP_NOMEXE, RAP_CFGFILTER, RAP_CFGTABLEAU,
				RAP_CFGDESC, RAP_REQUETE, RAP_LIEN, RAP_TYPERAPPORT, RAP_DESCRIPTION
				from rapport where RAP_CLE=\"" . $cle . "\"";
	$resultat = $objForm->mapChamp($requete);
}

// réception des paramètres
if($typeaction == "reception")
{
// contrôle des paramètres
	$objForm->recChamp();
	while($objForm->erreur == '')
	{
		break;
	}

	if ($objForm->erreur == '')
	{
		$objForm->majBdd('rapport', 'RAP_CLE', $cle);
		// écriture du fichier de configuration
//		$config = utf8_encode($config);
//		file_put_contents('config/'.$nomfic.'.php', $config);
		// retour
		RedirURL("sui_list_rapport.php");
		exit();
	}
}

// Affichage du début de la page
if ($cle != '') $objPage->addParam('cle', $cle);
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();
	
// affichage des boutons d'enchainement
$objForm->addBouton("reset","RETOUR","sui_list_rapport.php");
if (($cle == '' && $objProfil->cre) || ($cle != '' && $objProfil->maj))
{
	$objForm->addBouton("submit","ENVOYER");
}
else $objForm->setLecForm();

$objForm->affFormulaire();
$objForm->finFormulaire();

// fin de page
$objPage->finPage();
