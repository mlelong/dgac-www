<?php
include('config/bud_list_prevision.php');
$codefonc='bud';
include('prepage.php');
if (!$objProfil->ctlDroit($typeaction, 'tableau')) exit();

$objPage->debPage('center');

// préparation du tableau
$objTab = new tableau('1');

// requête d'accès à la base 
$requete = "select BUDL_CLE, PRG_NOMPROGRAMME, PRJ_NOMPROJET, BUDL_TYPELIG, BUDL_LIBELLE,  
			(BUDL_AEDOTAFONCT + BUDL_AEDOTAINVES) BUDL_AE, '0' AS BUDL_CUMULAE,
			(BUDL_CPDOTAFONCT + BUDL_CPDOTAINVES) BUDL_CP, '0' AS BUDL_CUMULCP,
			BUDL_AEPREVT1, BUDL_AEPREVT2, BUDL_AEPREVT3, BUDL_AEPREVT4,
			BUDL_CPPREVT1, BUDL_CPPREVT2, BUDL_CPPREVT3, BUDL_CPPREVT4,
			POL_NOMPOLE, DOM_NOMDOMAINE
			from budget_ligne 
			left join projet on BUDL_IDPROJET=PRJ_CLE
			left join programme on PRJ_IDPROGRAMME=PRG_CLE
			left join pole on PRJ_IDPOLE=POL_CLE 
			left join domaine on POL_IDDOMAINE=DOM_CLE 
			where BUDL_IDBUDGET=\"" . $cleparent . "\" ";
$requete .= $objTab->majRequete('order by POL_NOMPOLE,PRG_NOMPROGRAMME,PRJ_NOMPROJET'); // ajout tri et pagination si besoin

// gestion des paramètres de lien
$objTab->setLien('nomprogramme','bud_maj_prevision.php',"?typeaction=modification&cle=#BUDL_CLE#");

// affichage du tableau
$objTab->affTableau($requete, 'ctlPrevision');
function ctlPrevision(&$valcallback) 
{
	$budl_ae = $valcallback[5];
	$budl_cp = $valcallback[7];
	
	$budl_aeprevt1 = $valcallback[9];
	$budl_aeprevt2 = $valcallback[10];
	$budl_aeprevt3 = $valcallback[11];
	$budl_aeprevt4 = $valcallback[12];
	$budl_cpprevt1 = $valcallback[13];
	$budl_cpprevt2 = $valcallback[14];
	$budl_cpprevt3 = $valcallback[15];
	$budl_cpprevt4 = $valcallback[16];
	
	$budl_cumulae = $budl_aeprevt1+$budl_aeprevt2+$budl_aeprevt3+$budl_aeprevt4;
	if ($budl_cumulae > $budl_ae)
	{
		$valcallback[6] = "<span style='color:red;'>".number_format($budl_cumulae,0,',',' ')."</span>";
	}
	else $valcallback[6] = number_format($budl_cumulae,0,',',' ');
//	echo "call ae = ".$valcallback[6];
	$budl_cumulcp = $budl_cpprevt1+$budl_cpprevt2+$budl_cpprevt3+$budl_cpprevt4;
	if ($budl_cumulcp > $budl_cp)
	{
		$valcallback[8] = "<span style='color:red;'>".number_format($budl_cumulcp,0,',',' ')."</span>";
	}	
	else $valcallback[8] = number_format($budl_cumulcp,0,',',' ');
//	echo "call cp = ".$valcallback[8];
}

// fin de page
$objPage->finPage();
