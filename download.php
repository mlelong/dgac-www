<?php
// autoload des fichiers de class lors de l'appel � new
spl_autoload_register(function ($nomclass) {
    require_once('class/' . $nomclass . '.php');
});
$conn = database::getIntance();

include('fonction.php');
if(isset($_REQUEST['cle'])) 
{
	$cle = $_REQUEST['cle'];
	$requete = "select PCE_NOMFIC from piece where PCE_CLE=\"" . $cle . "\"";
	$statement = $conn->query($requete);
	$resultat = $statement->fetch(PDO::FETCH_ASSOC);
	$fic = $resultat["PCE_NOMFIC"];
	$nomfic='document/f' . $cle . '-' . ctlNomFichier($fic);
	if (file_exists($nomfic)) 
	{
		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header("Content-Disposition: attachment; filename=\"".$fic."\"");
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-Length: ' . filesize($nomfic));
		ob_clean();
		flush();
		readfile($nomfic);
		exit;
	}
}

/*
header("Cache-Control: no-cache, must-revalidate");
header("Cache-Control: post-check=0,pre-check=0");
header("Cache-Control: max-age=0");
header("Pragma: no-cache");
header("Expires: 0");
header("Content-Type: application/force-download");
header('Content-Disposition: attachment; filename="'.$name.'"');
*/
