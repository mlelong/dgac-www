<?php
include('config/cmd_list_ligne.php');
$codefonc='cmd';
include('prepage.php');
//if (!$objProfil->ctlDroit($typeaction, 'tableau')) exit();

$objPage->debPage('center');

// suppression d'une entrée
if($typeaction == "suppression")
{
	if(isset($cle))	
	{
		//
		// mise à jour des montants sur le budget global et sur le budget projet
		//
		$conn->setAttribute(PDO::ATTR_AUTOCOMMIT,0); 
		$conn->beginTransaction();
		$objCommande = new commande($conn, 'sup', 'ligne commande', $cleparent, $cle);
		if ($objCommande->supLigneCommande() === true) $conn->commit();
		else 
		{
			$conn->rollBack();
			$objPage->erreur='erreur';
			if ($objCommande->libelle != '') $objPage->libelle= $objCommande->libelle;
			else $objPage->libelle="Erreur sur la suppression de la ligne commande"; 
		}
	}
}

// vérification de commande parente
if($typeaction == "verification")
{
		$conn->setAttribute(PDO::ATTR_AUTOCOMMIT,0); 
		$conn->beginTransaction();
		$objCommande = new commande($conn, 'maj', 'commande', $cleparent);
		if ($objCommande->ctlCommande() === true) 
		{
			$conn->commit();
			$objPage->libelle="Vérification de la ligne de commande effectuée"; 
			$requete = "update commande set CMD_FLAGERR='' where CMD_CLE=\"" . $cleparent . "\"";
			$statement = $conn->query($requete);

		}
		else 
		{
			$conn->rollBack();
			$objPage->erreur='erreur';
			if ($objCommande->libelle != '') $objPage->libelle= $objCommande->libelle;
			else $objPage->libelle="Erreur sur la vérification de la commande"; 
		}
	$typeaction = '';
}

// récupération des informations de la commande
$requete = "select CMD_NUMERO, CMD_PROGRAMME, CMD_NUMEROSIF, CMD_TVA, CMD_FLAGERR, MAR_TYPEUO
			from commande left join marche on CMD_IDMARCHE=MAR_CLE where CMD_CLE=\"" . $cleparent . "\" ";
$statement = $conn->query($requete);
$resultat = $statement->fetch(PDO::FETCH_ASSOC);
$titresuite = " Commande " . $resultat['CMD_NUMERO'] . " Num SIF " . $resultat['CMD_NUMEROSIF'] . " Programme " . $resultat['CMD_PROGRAMME'];
$tva = $resultat['CMD_TVA'];
$flagerr = $resultat['CMD_FLAGERR'];
$typeuo = $resultat['MAR_TYPEUO'];

// préparation du tableau
$objTab = new tableau('1');

// requête d'accès à la base 
$requete = "select LCMD_CLE, LCMD_POSTE, GCMD_LIBELLE, MUO_NOMUO, MUO_MONTANT, LCMD_FLAGIMPUTATION,
			LCMD_MARCHANDISE, LCMD_TOTALHT, LCMD_LIBELLE, LCMD_MONTANTREV, LCMD_COEFREDU, LCMD_QUANTITE,
			DATE_FORMAT(LCMD_DATELIVA, '%d-%m-%Y') AS LCMD_DATELIVA,
			DATE_FORMAT(LCMD_DATEVA, '%d-%m-%Y') AS LCMD_DATEVA,
			CMD_IDDEMANDEUR, CMD_IDETAT,
			DATE_FORMAT(GCMD_DATELIVA, '%d-%m-%Y') AS GCMD_DATELIVA,
			DATE_FORMAT(GCMD_DATEVA, '%d-%m-%Y') AS GCMD_DATEVA
			from commande_ligne 
			left join commande on LCMD_IDCMD=CMD_CLE
			left join marche_uo on LCMD_IDUO=MUO_CLE
			left join commande_groupe_ligne on LCMD_IDGCMD=GCMD_CLE
			where LCMD_IDCMD=\"" . $cleparent . "\" ";
$requete .= $objTab->majRequete('order by GCMD_CLE, LCMD_CLE'); // ajout tri et pagination si besoin
$statement = $conn->query($requete);

// affichage des boutons d'enchainement
//if ($typeuo == "BPU avec SLA") $url="cmd_maj_ligne-sla.php";
if ($typeuo == "Sans UO") $url="cmd_maj_ligne-sansuo.php";
else $url="cmd_maj_ligne.php";
if ($objProfil->exe) $objTab->addBouton("button","VERIFIER","cmd_list_ligne.php?typeaction=verification");
if ($objProfil->maj) $objTab->addBouton("button","IMPRIMER","cmd_imp_ligne.php?menu=cmd");
if ($objProfil->cre) $objTab->addBouton("button","AJOUTER",$url);

// affichage du tableau
$totalHT = 0;
$ligne = array();
$refgroupe = '';
while ($row = $statement->fetch(PDO::FETCH_ASSOC))
{
	// Affichage des groupes de poste si presents
	if ($refgroupe != $row['GCMD_LIBELLE'])
	{
		$ligne[0] = "<B>" . $row['GCMD_LIBELLE'] . "</B>";
		$ligne[1] = '';$ligne[2] = '';$ligne[3] = '';$ligne[4] = '';$ligne[5] = '';$ligne[6] = '';
		if ($row['GCMD_DATELIVA'] == '00-00-0000') $ligne[7] = '';
		else $ligne[7] = $row['GCMD_DATELIVA'];
		if ($row['GCMD_DATEVA'] == '00-00-0000') $ligne[8] = '';
		else $ligne[8] = $row['GCMD_DATEVA'];
		$ligne[9] = '';
		if ($row['GCMD_LIBELLE'] != '') $objTab->affLigne($ligne);
		$refgroupe = $row['GCMD_LIBELLE'];
	}
	// affichage de la ligne
	$param = "?typeaction=modification&cle=" . $row['LCMD_CLE'];
	if ($typeuo == "Sans UO") $poste = stripslashes($row['LCMD_POSTE']);
	else $poste = stripslashes($row['MUO_NOMUO']);
	if ($poste == '') $poste = "POSTE A RENESEIGNER";
	$ligne[0] = "<a href=\"#\" " . VersURL($url . $param) . "\" >" . $poste . "</a>";
	$ligne[1] = $row['LCMD_MARCHANDISE'];
	$ligne[2] = $row['LCMD_FLAGIMPUTATION'];
	$ligne[3] = number_format($row['LCMD_MONTANTREV'],2,',',' ');
	$ligne[4] = $row['LCMD_QUANTITE'];
	$ligne[5] = $row['LCMD_COEFREDU'];
	$ligne[6] = number_format($row['LCMD_TOTALHT'],2,',',' ');
	if ($row['LCMD_DATELIVA'] == '00-00-0000') $ligne[7] = '';
	else $ligne[7] = $row['LCMD_DATELIVA'];
	if ($row['LCMD_DATEVA'] == '00-00-0000') $ligne[8] = '';
	else $ligne[8] = $row['LCMD_DATEVA'];
	$param = "?typeaction=suppression&cle=" . $row['LCMD_CLE'];
	if ($objProfil->sup || ($row['CMD_IDDEMANDEUR'] == $objProfil->idnom && $row['CMD_IDETAT'] == 1)) // état brouillon
		$ligne[9] = "<a " . VersURL('cmd_list_ligne.php' . $param,'supp') . " ><i class='fa fa-trash'></i></a>";
	else $ligne[9] = "";
	$objTab->affLigne($ligne);
	// incrémentation du total
	$totalHT += $row['LCMD_TOTALHT'];
	// affiche du libellé si présent
	if ($row['LCMD_LIBELLE'] != '')
	{
		$ligne[0] = "<span style='margin-left:15px;'>" . $row['LCMD_LIBELLE'] . "</span>";
		$ligne[1] = '';$ligne[2] = '';$ligne[3] = '';$ligne[4] = '';$ligne[5] = '';$ligne[6] = '';$ligne[7] = '';$ligne[8] = '';$ligne[9] = '';
		$objTab->affLigne($ligne);
	}
}
// affichage total
$ligne[0] = '';$ligne[1] = '';$ligne[2] = '';$ligne[3] = '';$ligne[4] = '';$ligne[5] = '';$ligne[6] = '';$ligne[7] = '';$ligne[8] = '';$ligne[9] = '';
$ligne[5] = "TOTAL commande";
$objTab->affLigne($ligne);
// affichage total ht
$ligne[5] = "TOTAL HT";
$ligne[6] = number_format($totalHT,2,',',' ');
$objTab->affLigne($ligne);
// affichage TVA
$ligne[5] = "TVA";
$ligne[6] = number_format($tva,2,',',' ').'%';
$objTab->affLigne($ligne);
// affichage total TTC
$totalTTC=$totalHT+(($totalHT*$tva)/100);
$ligne[5] = "TOTAL TTC";
$ligne[6] = number_format($totalTTC,2,',',' ');
$objTab->affLigne($ligne);
// fin du tableau
$objTab->finTableau();

// fin de page
$objPage->finPage();
