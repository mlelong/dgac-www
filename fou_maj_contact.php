<?php
include('config/fou_maj_contact.php');
$codefonc='fou';
require_once('prepage.php');

$objForm = new formulaire('1');

if($typeaction == "creation") $objForm->initChamp();

if($typeaction == "modification")
{
	$requete = "select CTC_CLE, CTC_IDFOURNISSEUR, CTC_NOM, CTC_PRENOM, CTC_CIVILITE, 
				CTC_ADRESSE, CTC_COMMUNE, CTC_CODEPOSTAL, CTC_DEPARTEMENT, CTC_TELEPHONE,
				CTC_PORTABLE, CTC_FAX, CTC_MAIL, CTC_ETAT, CTC_DESCRIPTION
				from contact where CTC_CLE=\"" . $cle . "\"";
	$resultat = $objForm->mapChamp($requete);
}

// réception des paramètres
if($typeaction == "reception")
{
// contrôle des paramètres
	$objForm->recChamp();
	while($objForm->erreur == '')
	{
		break;
	}

	if ($objForm->erreur == '')
	{
		$objForm->majBdd('contact', 'CTC_CLE', $cle);
		RedirURL("fou_list_contact.php");
		exit();
	}
}

// Affichage du début de la page
if ($cle != '') $objPage->addParam('cle', $cle);
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();
	
// affichage des boutons d'enchainement
$objForm->addBouton("reset","RETOUR","fou_list_contact.php");
if (($cle == '' && $objProfil->cre) || ($cle != '' && $objProfil->maj))
{
	$objForm->addBouton("submit","ENVOYER");
}
else $objForm->setLecForm();

$objForm->affFormulaire();
$objForm->finFormulaire();

// fin de page
$objPage->finPage();
