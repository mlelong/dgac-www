<?php
include('config/bud_list_pap.php');
$codefonc='bud';
include('prepage.php');
if (!$objProfil->ctlDroit($typeaction, 'tableau')) exit();

$objPage->debPage('center');

// suppression d'une entrée
if($typeaction == "suppression")
{
	if(isset($cle))	
	{
		$requete = "delete from budget_pap where BPAP_CLE=\"" . $cle . "\"";
		$statement = $conn->query($requete);
	}
}

//*******************************************************************
// Objet PAP 
//*******************************************************************
class pap 
{
	public $cle;  			/* Cle */
	public $lignepap;    	/* libellé de la ligne du PAP */
	public $ordre;  		/* ordre hiérachique d'affichage */
	public $cleparent;  	/* clé du parent */
	public $imputation;  	/* investissement ou fonctionnement */
	public $total;       	/* flag affichage du total */
	public $aepap;    		/* montant AE du PAP */
	public $cppap;    		/* montant CP du PAP */
	public $aebudget;    	/* montant AE consommé */
	public $cpbudget;    	/* montant CP consommé */
}
	
// créé ou retourne un objet PAP en fonction de la clé
$tabpap = array();
$tabparent = array();
function crePap($clepap, $ordre='')
{
	global $tabpap;
	global $tabparent;
	if (isset($tabpap[$clepap])) return $tabpap[$clepap];
	$curpap = new pap;
	$curpap->cle = $clepap;
	$curpap->cleparent = '';
	$curpap->aepap = 0;
	$curpap->cppap = 0;
	$curpap->aebudget = 0;
	$curpap->cpbudget = 0;
	$curpap->total = false;
	$tabpap[$clepap] = $curpap;
	$tabparent[$ordre] = $curpap;
	return $curpap;
}

// retourne un objet PAP en fonction de la clé
function getPap($clepap)
{
	global $tabpap;
	if (isset($tabpap[$clepap])) return $tabpap[$clepap];
	else return false;
}

//*******************************************************************
// remplissage des objets PAP 
//*******************************************************************

$objTab = new tableau('1');

// la première entrée est pour les budgets sans relation avec le PAP
$objpap = crePap('0');
$objpap->lignepap = "Budget sans affectation PAP";
$objpap->ordre = "S";
$objpap->imputation = '';

// requête d'accès à la base pour récupérer les lignes du PAP
$requete = "select BPAP_CLE, BPAP_ORDRE, BPAP_LIGNEPAP, BPAP_FLAGIMPUTATION, BPAP_FLAGTOTAL,  
			BPAP_AEPREVFONCT, BPAP_CPPREVFONCT,	BPAP_AEPREVINVES, BPAP_CPPREVINVES
			from budget_pap 
			where BPAP_IDBUDGET=\"" . $cleparent . "\" ";
$requete .= $objTab->majRequete('order by 2'); // ajout tri et pagination si besoin
$statement = $conn->query($requete);
// création des objets PAP
while ($row = $statement->fetch(PDO::FETCH_ASSOC))
{
	$objpap = crePap($row['BPAP_CLE'], $row['BPAP_ORDRE']);
	$objpap->lignepap = stripslashes($row['BPAP_LIGNEPAP']);
	$objpap->ordre = $row['BPAP_ORDRE'];
	$objpap->total = $row['BPAP_FLAGTOTAL'];
	
	// gestion de l'imputation
	$objpap->imputation = $row['BPAP_FLAGIMPUTATION'];
	if ($objpap->imputation == "F")
	{
		$objpap->aepap = $row['BPAP_AEPREVFONCT'];
		$objpap->cppap = $row['BPAP_CPPREVFONCT'];
	}
	else
	{
		$objpap->aepap = $row['BPAP_AEPREVINVES'];
		$objpap->cppap = $row['BPAP_CPPREVINVES'];
	}
	
	// gestion de la hiérarchie
	$nbniveau = substr_count($objpap->ordre, '-'); // 0 si pas de -
	if ($nbniveau > 0) 
	{
		// récupération du parent -> on enlève le dernier élément (blanc si pas de parent)
		$parent = implode('-', explode('-', $objpap->ordre, -1));
		if (isset($tabparent[$parent])) 
		{
			$objpap->cleparent = $tabparent[$parent]->cle;
		}	
	}
}

// requête d'accès à la base pour récupérer les lignes budgétaires
// et mise en correspondance de chaque ligne budgétaire avec une ligne du PAP
$requete = "select BUDL_CLE, BUDL_LIGNE, BUDL_IDPAPFONCT, BUDL_IDPAPINVES, BUDL_TYPELIG, BUDL_LIBELLE,  
			BUDL_AEDEBLFONCT, BUDL_CPDEBLFONCT,	BUDL_AEDEBLINVES, BUDL_CPDEBLINVES
			from budget_ligne where BUDL_IDBUDGET=\"" . $cleparent . "\" ";
$requete .= $objTab->majRequete('order by 1'); // ajout tri et pagination si besoin
$statement = $conn->query($requete);
// mise à jour des objets PAP
while ($row = $statement->fetch(PDO::FETCH_ASSOC))
{
	// budget de fonctionnement
	if ($row['BUDL_AEDEBLFONCT'] > 0 || $row['BUDL_CPDEBLFONCT'] > 0) 
	{
		if ($row['BUDL_IDPAPFONCT'] > 0) $objpap = getPap($row['BUDL_IDPAPFONCT']);
		else $objpap = getPap('0');
		$objpap->aebudget += $row['BUDL_AEDEBLFONCT'];
		$objpap->cpbudget += $row['BUDL_CPDEBLFONCT'];
	}
	// budget d'investissement
	if ($row['BUDL_AEDEBLINVES'] > 0 || $row['BUDL_CPDEBLINVES'] > 0)
	{
		if ($row['BUDL_IDPAPINVES'] > 0) $objpap = getPap($row['BUDL_IDPAPINVES']);
		else $objpap = getPap('0');
		$objpap->aebudget += $row['BUDL_AEDEBLINVES'];
		$objpap->cpbudget += $row['BUDL_CPDEBLINVES'];
	}
}

//*******************************************************************
// Calcul des totaux 
//*******************************************************************
// boucle pour chaque ligne du PAP
foreach($tabpap as $elem)
{
	$objpap = $elem;
	$objcur = $elem;
	// boucle pour faire le cumul des totaux pour tous les parents
	while(true)
	{
		if ($objcur->cleparent == '') break;
		$objparent = $tabpap[$objcur->cleparent];
		$objparent->aepap += $objpap->aepap;
		$objparent->cppap += $objpap->cppap;
		$objparent->aebudget += $objpap->aebudget;
		$objparent->cpbudget += $objpap->cpbudget;
		$objparent->total = true;
		$objcur = $objparent;
	}
}
//*******************************************************************
// Affichage du PAP à partir des objets
//*******************************************************************
// préparation du tableau
$objTab->setTableAttr(" id='treegrid' align='right' ");

// affichage du tableau
$cpt = 1;
$tabtree = array();
$ligne = array();
foreach($tabpap as $objpap)
{
	// préparation de l'arborescence
	$tabtree[$objpap->ordre] = $cpt;
	$class = 'expanded treegrid-' . $cpt;
	$classparent = '';
	$tab = explode('-', $objpap->ordre);
	$nbtree = count($tab);
	if ($nbtree > 1) $classparent = implode('-', explode('-', $objpap->ordre, -1));
	if ($classparent != '')
	{
		if (isset($tabtree[$classparent])) $class .= ' treegrid-parent-' . $tabtree[$classparent];
	}
	$objTab->setLigneClass($class);
	// affichage de la ligne	
	$param = "?typeaction=modification&cle=" . $objpap->cle;
	$ligne[0] = "<a href=\"#\" " . VersURL('bud_maj_pap.php' . $param) . "\" >" . $objpap->lignepap . "</a>";
	if ($objpap->total) {$debbold = "<font color='#08088A'><b>"; $finbold = "</b></font>";}
	else {$debbold = ""; $finbold = "";}
	$ligne[1] = $debbold . number_format($objpap->aepap,0,',',' ') . $finbold;
	$ligne[2] = $debbold . number_format($objpap->cppap,0,',',' ') . $finbold;
	$ligne[3] = $debbold . number_format($objpap->aebudget,0,',',' ') . $finbold;
	$ligne[4] = $debbold . number_format($objpap->cpbudget,0,',',' ') . $finbold;
	$param = "?typeaction=suppression&cle=" . $objpap->cle;
	if ($objProfil->sup) $ligne[5] = "<a " . VersURL('bud_list_pap.php' . $param,'supp') . " ><i class='fa fa-trash'></i></a>";
	else $ligne[5] = "";
	$objTab->affLigne($ligne);
	$cpt++;
}

// affichage des boutons d'enchainement
if ($objProfil->cre)
{
	$objTab->addBoutonSpe("button","EXPORTER","onclick=\"document.location.href='bud_csv_pap.php';\"");
//	$objTab->addBouton("button","IMPORTER","bud_imp_pap.php");
	$objTab->addBouton("button","AJOUTER","bud_maj_pap.php");
}	

$objTab->finTableau();

// fin de page
$objPage->finPage();
?>
<script type="text/javascript" src="js/jquery.treegrid.js"></script>
<script>
/***********************************************************/
// Exécuté après chargement de la page
/***********************************************************/
$(document).ready(function() {
	$('#treegrid').treegrid();
});

</script>
