<?php
include('config/dom_list_pole-domaine.php');
$codefonc='dom';
include('prepage.php');
if (!$objProfil->ctlDroit($typeaction, 'formulaire',$cle)) exit();

$objForm = new formulaire('1');

if ($cle != '') $objPage->addParam('cle', $cle);
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();
// Affichage des utilisateurs visibles
$datalistecle = '';
$datalisteval = '';
$requete = "select UTI_CLE, UTI_NOM, UTI_PRENOM
			from utilisateur, domaine_utilisateur
			where UTI_CLE=RDU_IDUTILISATEUR and RDU_IDDOMAINE=\"" . $cle . "\" and RDU_RELATION=\"d\"
			order by 2";
$statement = $conn->query($requete);
while ($row = $statement->fetch(PDO::FETCH_ASSOC))
{
	$affichage = $row['UTI_NOM'] . '*-*' . $row['UTI_PRENOM'];
	if($datalisteval == '') $datalisteval = $affichage;
	else $datalisteval .= ';' . $affichage;
	if($datalistecle == '') $datalistecle = $row['UTI_CLE'];
	else $datalistecle .= ';' . $row['UTI_CLE'];
}
$objForm->champF['idnomlec']['dataliste1'] = $datalistecle . "*;*" . $datalisteval;

// Affichage des autres utilisateurs
$datalistecle = '';
$datalisteval = '';
$requete = "select UTI_CLE, UTI_NOM, UTI_PRENOM from utilisateur 
			where UTI_CLE NOT IN (select RDU_IDUTILISATEUR from domaine_utilisateur 
			 where RDU_IDDOMAINE=\"" . $cle . "\" and RDU_RELATION=\"d\") 
			order by 2";
$statement = $conn->query($requete);
while ($row = $statement->fetch(PDO::FETCH_ASSOC))
{
	$affichage = $row['UTI_NOM'] . '*-*' . $row['UTI_PRENOM'];
	if($datalisteval == '') $datalisteval = $affichage;
	else $datalisteval .= ';' . $affichage;
	if($datalistecle == '') $datalistecle = $row['UTI_CLE'];
	else $datalistecle .= ';' . $row['UTI_CLE'];
}
$objForm->champF['idnomlec']['dataliste2'] = $datalistecle . "*;*" . $datalisteval;

$objForm->affFormulaire();
// affichage des boutons d'enchainement
$objForm->addBouton("reset","RETOUR","dom_list_domaine.php");
if ($objProfil->maj) 
{
	$objForm->addBoutonSpe("button","VALIDER","onclick=\"majFormSortableDiff('dom-d'," . $cle . ");VersURL('dom_list_domaine.php');\"");
}

// fin de page
$objForm->finFormulaire();
$objPage->finPage();
