<?php
include('config/prj_list_ligne.php');
$codefonc='bud';
include('prepage.php');
if (!$objProfil->ctlDroit($typeaction, 'tableau')) exit();

$objPage->debPage('center');

// suppression d'une entrée
if($typeaction == "suppression")
{
	if(isset($cle))	
	{
		$requete = "delete from budget_ligne where BUDL_CLE=\"" . $cle . "\"";
		$statement = $conn->query($requete);
	}
}

// préparation du tableau
$objTab = new tableau('1');

// requête d'accès à la base 
$requete = "select BUDL_CLE, BUDL_LIGNE, BUDL_LIBELLE, BUDL_REFSIF, 
			REPLACE(FORMAT(BUDL_AEPREVFONCT,2),',',' ') as BUDL_AEPREVFONCT, 
			REPLACE(FORMAT(BUDL_CPPREVFONCT,2),',',' ') as BUDL_CPPREVFONCT, 
			REPLACE(FORMAT(BUDL_AEPREVINVES,2),',',' ') as BUDL_AEPREVINVES, 
			REPLACE(FORMAT(BUDL_CPPREVINVES,2),',',' ') as BUDL_CPPREVINVES
			from budget_ligne 
			where BUDL_IDPROJET=\"" . $cleparent . "\" ";
$requete .= $objTab->majRequete('order by BUDL_CLE'); // ajout tri et pagination si besoin

// affichage des boutons d'enchainement
if ($objProfil->cre) $objTab->addBouton("button","AJOUTER","prj_maj_ligne.php");

// gestion des paramètres de lien
$objTab->setLien('lignebud','prj_maj_ligne.php',"?typeaction=modification&cle=#BUDL_CLE#");
$objTab->setLien('supp','prj_list_ligne.php',"?typeaction=suppression&cle=#BUDL_CLE#","Supprimer");

// affichage du tableau
$objTab->affTableau($requete);
// fin de page
$objPage->finPage();
