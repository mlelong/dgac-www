<?php
include('config/adm_maj_notification-groupe.php');
$codefonc='adm';
$codemenu='not';
require_once('prepage.php');

$objForm = new formulaire('1');

if($typeaction == "creation") 
{
	$objForm->initChamp();
	$idnotif = $cleparent;
}	

if($typeaction == "modification")
{
	$requete = "select GNOT_IDNOTIF, GNOT_NOMGROUPE, GNOT_IDGROUPEDEM, GNOT_IDGROUPEVAL, GNOT_DESCRIPTION
				from notification_groupe where GNOT_CLE=\"" . $cle . "\"";
	$resultat = $objForm->mapChamp($requete);
}

// réception des paramètres
if($typeaction == "reception")
{
// contrôle des paramètres
	$objForm->recChamp();
	while($objForm->erreur == '')
	{
		break;
	}

	if ($objForm->erreur == '')
	{
		$objForm->majBdd('notification_groupe', 'GNOT_CLE', $cle);
		RedirURL("adm_list_notification-groupe.php");
		exit();
	}
}

// Affichage du début de la page
if ($cle != '') $objPage->addParam('cle', $cle);
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();
	
$requete = "select GRU_CLE, GRU_NOMGROUPE from groupe order by 1";
$objForm->setSelect('idgroupedem', $idgroupedem, $requete, 'GRU_CLE', 'GRU_NOMGROUPE');
$requete = "select GRU_CLE, GRU_NOMGROUPE from groupe order by 1";
$objForm->setSelect('idgroupeval', $idgroupeval, $requete, 'GRU_CLE', 'GRU_NOMGROUPE');

// affichage des boutons d'enchainement
$objForm->addBouton("reset","RETOUR","adm_list_notification-groupe.php");
if (($cle == '' && $objProfil->cre) || ($cle != '' && $objProfil->maj))
{
	$objForm->addBouton("submit","ENVOYER");
}
else $objForm->setLecForm();

$objForm->affFormulaire();
$objForm->finFormulaire();

// fin de page
$objPage->finPage();
