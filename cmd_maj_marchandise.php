<?php
include('config/cmd_maj_marchandise.php');
$codefonc='cfg';
$codemenu='grm';
require_once('prepage.php');

$objForm = new formulaire('1');

// création
if($typeaction == "creation") $objForm->initChamp();

// modification
if($typeaction == "modification")
{
	$requete = "select GRM_CLE, GRM_NUMERO, GRM_LIBELLE, GRM_DESCRIPTION, GRM_FLAGDSI
				from marchandise where GRM_CLE=\"" . $cle . "\"";
	$resultat = $objForm->mapChamp($requete);
}

// réception des paramètres
if($typeaction == "reception")
{
// contrôle des paramètres
	$objForm->recChamp();
	while($objForm->erreur == '')
	{
		break;
	}

	if ($objForm->erreur == '')
	{
		$objForm->majBdd('marchandise', 'GRM_CLE', $cle);
		RedirURL("cmd_list_marchandise.php");
		exit();
	}
}

// Affichage du début de la page
if ($cle != '') $objPage->addParam('cle', $cle);
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();

if ($flagdsi == "o") $opt = " checked "; else $opt = "";
$objForm->setOpt('flagdsi', $opt);
$flagdsi = "o";

// affichage des boutons d'enchainement
$objForm->addBouton("reset","RETOUR","cmd_list_marchandise.php");
if (($cle == '' && $objProfil->cre) || ($cle != '' && $objProfil->maj))
{
	$objForm->addBouton("submit","ENVOYER");
}
else $objForm->setLecForm();

$objForm->affFormulaire();
$objForm->finFormulaire();

// fin de page
$objPage->finPage();
