<?php
include('config/patch_delete.php');

$codefonc='pat';
include('prepage.php');

$objPage->debPage('center');

if (isset($_REQUEST['dirsrc'])) $dirsrc = $_REQUEST['dirsrc'];
else $dirsrc = '';
if (isset($_REQUEST['ficsrc'])) $ficsrc = $_REQUEST['ficsrc'];
else $ficsrc = '';

if($typeaction == "reception")
{
	$nbfic = 0;
	if (isset($_REQUEST['nbfic'])) $nbfic = $_REQUEST['nbfic'];
	$objPage->tampon .= "nb patch = " . $nbfic . "<br>";
	for($cptfic=0;$cptfic<$nbfic; $cptfic++)
	{
		$objPage->tampon .= "test patch-" . $cptfic . "<br>";
		if (isset($_REQUEST['patch-' . $cptfic])) 
		{
			$fic = $_REQUEST['patch-' . $cptfic];
			$tab = explode(";", $fic);
			$objPage->tampon .= "reception patch-" . $cptfic . " = " . $fic . "<br>";
			if (!isset($tab[0])) continue;
			if (!isset($tab[1])) continue;
			if ($tab[0] == '' || $tab[1] == '') continue;
			$dirsrc = $tab[0];
			$ficsrc = $tab[1];
			$nomficold = "patch/old_patch/" . $dirsrc . "/" . $ficsrc; // sauvegarde du patch de destination
			if (file_exists($nomficold)) // sauvegarde si le fichier destination existe
			{
				$objPage->tampon .= "Suppression du patch = " . $nomficold . "<br>"; 
				$result= unlink($nomficold); // suppression de la sauvegarde
				if ($result === false)
				{
					$objPage->tampon .= "Erreur sur la suppression du patch = " . $dirsrc . "/" . $ficsrc . "<br>"; 
					continue;
				}
			}
		}
	}	
}

// préparation du tableau
$objTab = new tableau('1');

// affichage du tableau
$cptfic = 0;
$ligne = array();
$dir1 = dir("patch/old_patch");
while ($dirsrc = $dir1->read()) // lecture des répertoires
{
	if($dirsrc == "." || $dirsrc == "..") continue;
	$dir2 = dir("patch/old_patch/" . $dirsrc);
	while ($ficsrc = $dir2->read()) // lecture des fichiers
	{
		if($ficsrc == "." || $ficsrc == "..") continue;
		$ligne[0] = $dirsrc;
		$ligne[1] = $ficsrc;
		$ligne[2] = "<input type='checkbox' name='patch-" . $cptfic . "' value=\"" . $dirsrc . ";" . $ficsrc . "\">";
		$objTab->affLigne($ligne);
		$cptfic++;
	}
}

$objPage->tampon .= "Nb patch au total : <input type='text' name ='nbfic' value ='" . $cptfic . "' />"; 

// affichage des boutons d'enchainement
$objTab->addBouton("button","RETOUR","patch.php");
$objTab->addBouton("submit","VALIDER");

$objTab->finTableau();

// fin de page
$objPage->finPage();
