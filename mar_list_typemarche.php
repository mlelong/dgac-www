<?php
include('config/mar_list_typemarche.php');
$codefonc='cfg';
$codemenu='tym';
include('prepage.php');
if (!$objProfil->ctlDroit($typeaction, 'tableau')) exit();

$objPage->debPage('center');

// suppression d'une entrée
if($typeaction == "suppression")
{
	if(isset($cle))	
	{
		$requete = "delete from type_marche where TMAR_CLE=\"" . $cle . "\"";
		$statement = $conn->query($requete);
	}
}

// préparation du tableau
$objTab = new tableau('1');

// requête d'accès à la base 
$requete = "select TMAR_CLE, TMAR_TYPE, TMAR_LIBELLE from type_marche where 1";
$requete .= $objTab->majRequete('order by TMAR_CLE desc'); // ajout tri et pagination si besoin

// affichage des boutons d'enchainement
if ($objProfil->cre) $objTab->addBouton("button","AJOUTER","mar_maj_typemarche.php");

// gestion des paramètres de lien
$objTab->setLien('typemarche','mar_maj_typemarche.php',"?typeaction=modification&cle=#TMAR_CLE#");
$objTab->setLien('supp','mar_list_typemarche.php',"?typeaction=suppression&cle=#TMAR_CLE#","Supprimer");

// affichage du tableau
$objTab->affTableau($requete);

// fin de page
$objPage->finPage();
