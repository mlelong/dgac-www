<?php
include('config/cmd_maj_paiement.php');
$codefonc='cmd';
require_once('prepage.php');

$objForm = new formulaire('1');
$objTab = new tableau('1');

if($typeaction == "creation") $objForm->initChamp();

if($typeaction == "modification")
{
	$requete = "select PCMD_CLE, PCMD_ETAT, PCMD_TYPEPAIE, PCMD_TOTALTTC, 
				PCMD_IDDEMANDEUR, PCMD_DEMANDEUR, PCMD_IDINTERVENANT, PCMD_INTERVENANT, 
				PCMD_URGENCE, PCMD_PRIORITE, PCMD_LIBELLE, PCMD_IDCMD, PCMD_NUMEROSIF,
				PCMD_IDBUDGET, PCMD_IDLIGNEBUDGET,				
				DATE_FORMAT(PCMD_DATECRE, '%d-%m-%Y') AS PCMD_DATECRE,
				DATE_FORMAT(PCMD_DATEPRE, '%d-%m-%Y') AS PCMD_DATEPRE,
				DATE_FORMAT(PCMD_DATESERV, '%d-%m-%Y') AS PCMD_DATESERV,
				DATE_FORMAT(PCMD_DATEPAIE, '%d-%m-%Y') AS PCMD_DATEPAIE
				from commande_paiement 
				left join commande on PCMD_IDCMD=CMD_CLE
				where PCMD_CLE=\"" . $cle . "\"";
	$resultat = $objForm->mapChamp($requete);
}

// réception des paramètres
if($typeaction == "reception")
{
// contrôle des paramètres
	$objTab->recChamp();
	$objForm->recChamp();
	// gestion du demandeur et de l'intervenant
	if ($iddemandeur == 0 || $iddemandeur == '')
	{
		$demandeur = $objProfil->nom;
		$iddemandeur = $objProfil->idnom;
	}
	if ($intervenant != '')
	{
		$tab = explode('*-*', $intervenant);
		$idintervenant = $tab[0];
		$intervenant = $tab[1];
	}
	else
	{
		$idintervenant = 0;
		$intervenant = '';
	}

	while($objForm->erreur == '' && $objTab->erreur == '')
	{
		if ($MYSQLdatepaie != '' && $MYSQLdateserv != '' && $MYSQLdatepaie != '0000-00-00' && $MYSQLdateserv != '0000-00-00' && $MYSQLdatepaie < $MYSQLdateserv) 
				{$objForm->erreur='datepaie'; $objForm->libelle="La date de paiement doit être > à la date de service fait"; break;}
		$tabqte = explode(",", $hiddenqte);
		$tabval = explode(",", $hiddenval);
		$cpt = 0;
		foreach($tabqte as $elem)
		{
			// contrôle de la date
			if (isset($datelivr[$cpt]) && $datelivr[$cpt] != '')
			{
				if (!ctlDate($datelivr[$cpt])) {$objTab->erreur="datelivr".$cpt; $objTab->libelle="Date incorrecte"; break;}
			}	
			// la quantité saisie doit être < ou = à la quantité restant à payer
			$qtepayer[$cpt]=str_replace(",", ".", $qtepayer[$cpt]);
			if ($qtepayer[$cpt] > ($elem+$tabval[$cpt])) {$objTab->erreur="qtepayer".$cpt; $objTab->libelle="Problème sur les quantités"; break;}
			$cpt++;
		}
		// création de l'objet commande
		include('class/commande.php');
		$objCommande = new commande($conn, 'maj', 'paiement', $cleparent, $cle);
		// sélection du budget
		$programme = $objCommande->getProgramme();
		$idbudget = $objCommande->getIdBudget($datecre, $datepre, $datepaie, $programme);
		if ($idbudget === false) {$objForm->erreur='datecre'; $objForm->libelle="Pas de budget pour ces dates"; break;}
		// sélection de la ligne budgétaire
		$idprojet = $objCommande->getIdProjet();
		$idlignebudget = $objCommande->getIdLigneBudget($idbudget, $idprojet);
		if ($idlignebudget === false) $idlignebudget = 0; // pas bloquant pour le moment
		break;
	}
		
	if ($objForm->erreur == '' && $objTab->erreur == '')
	{
		$conn->setAttribute(PDO::ATTR_AUTOCOMMIT,0); 
		$conn->beginTransaction();
		$montantTOT = 0;
		$montantINV = 0;
		$montantFON = 0;
		$flag = false;
		while(true)
		{
//			Trace("typepaie=" . $typepaie);
			if (!isset($datecre) || $datecre == '' || $datecre == '0000-00-00') {$datecre = date('d-m-Y'); $MYSQLdatecre=date('Y-m-d');} // jour courant par défaut
			if($objForm->majBdd('commande_paiement', 'PCMD_CLE', $cle) === false) break;
			// récupération de la clé si création
			if ($cle == '') 
			{
				$cle = $conn->lastInsertId();
				$objCommande->cle = $cle;
			}	
			//
			// mise à jour des quantités à payer
			//
			$tabcleLCMD = explode(",", $hiddenLCMD);
			$tabcleRPL = explode(",", $hiddenRPL);
			$tabval = explode(",", $hiddenval);
			$cpt = 0;
			$flagmaj = false;
			foreach($tabcleLCMD as $elem)
			{
				if ($datelivr[$cpt] != '') $MYSQLdatelivr = invDate($datelivr[$cpt]);
				else $MYSQLdatelivr = '';
				$qtepayer[$cpt]=str_replace(",", ".", $qtepayer[$cpt]);
				if ($qtepayer[$cpt] =='') $qtesaisie = 0; else $qtesaisie = $qtepayer[$cpt];
				if ($tabval[$cpt] != $qtesaisie) // la quantité a changée donc on traite
				{
					// ajout d'une nouvelle relation
					if ($qtesaisie > 0 && $tabval[$cpt] == 0)
					$requete = "insert into paiement_ligne (RPL_IDPCMD, RPL_IDLCMD, RPL_DATELIVR, RPL_QUANTITE) 
								values (\"" . $cle . "\",\"" . $elem . "\",\"" . $MYSQLdatelivr . "\",\"" . $qtepayer[$cpt] . "\")";
					// suppression de la relation courante
					else if ($qtesaisie == 0 && $tabval[$cpt] > 0)
					$requete = "delete from paiement_ligne where RPL_CLE=\"" . $tabcleRPL[$cpt] . "\"";
					// mise à jour de la relation courante
					else 
					$requete = "update paiement_ligne set RPL_DATELIVR=\"" . $MYSQLdatelivr 
								. "\",  RPL_QUANTITE=\"" . $qtepayer[$cpt] 
								. "\" where RPL_CLE=\"" . $tabcleRPL[$cpt] . "\"";
					// exécution de la requête
					$statement = $conn->query($requete);
					if (!$statement) break; 
					// ajustement de la quantité payée au niveau de la ligne de commande
					$newqantite = ($qtepayer[$cpt]-$tabval[$cpt]);
					$requete = "update commande_ligne set LCMD_QTEPAYER=(LCMD_QTEPAYER+" . $newqantite . ") where LCMD_CLE=\"" . $elem . "\"";
					$statement = $conn->query($requete);
					if (!$statement) break; 
					$flagmaj = true;
				}
				else // sinon on met juste la date à jour
				{
					$requete = "update paiement_ligne set RPL_DATELIVR=\"" . $MYSQLdatelivr . "\" where RPL_CLE=\"" . $tabcleRPL[$cpt] . "\"";
					$statement = $conn->query($requete);
					if (!$statement) break; 
				}
				$cpt++;
			}
			//
			// mise à jour des montants si le paiement est modifié
			//
			if ($flagmaj)
			{
				// récupération des informations de la commande
				$cmd_tva = $objCommande->getTva();
				//
				// récupération de la dernière révision du marché
				//
				$mrev_coefrev = $objCommande->getRevision();
				//
				// calcul du cumul des montants HT pour les uos à payer 
				//
				$tabcleMUO = explode(",", $hiddenMUO);
				$cpt = 0;
//				Trace("montantTOT=" . $montantTOT . " montantINV=" . $montantINV . " montantFON= " . $montantFON);
				foreach($tabcleMUO as $elem)
				{
//					Trace("tabcleMUO=" . $elem);
					//
					// montant HT et imputation de l'UO selectionnée
					//
					$tab = explode("=", $elem);
					$muo_flagimputation = $tab[0]; // F = fonctionnement I = investissement
					$muo_montant = $tab[1];
					$lcmd_coefredu = $tab[2];
					if ($muo_flagimputation != 'F' && $muo_flagimputation != 'I') $muo_flagimputation = 'F';
					//
					// calcul des montants HT révisés et remisés en fonction de la quantité
					//
					$lcmd_coefredu = str_replace("%", "", $lcmd_coefredu); // on supprime le % si renseigné
					if ($lcmd_coefredu == '' || $lcmd_coefredu == 0) $remise = 1;
					else $remise = (100-$lcmd_coefredu)/100; // convertir % en coefficient de réduction (25% -> 0,75)
					$montantHT = ($muo_montant * $mrev_coefrev * $remise) * $qtepayer[$cpt];
					$montantTOT += $montantHT;
//					Trace("montantHT=" . $montantHT . " muo_montant=" . $muo_montant . " mrev_coefrev= " . $mrev_coefrev . " remise=" . $remise . " qtepayer=" . $qtepayer[$cpt]);
					if ($muo_flagimputation == 'I') $montantINV += $montantHT;
					else $montantFON += $montantHT;
//					Trace("montantTOT=" . $montantTOT . " montantINV=" . $montantINV . " montantFON= " . $montantFON);
					$cpt++;
				}
				// calcul des montants TTC 
				$pcmd_newtotalht = $montantTOT;
				$pcmd_newinvesht = $montantINV;
				$pcmd_newfoncht = $montantFON;
				$pcmd_newtotalttc = $montantTOT + (($montantTOT * $cmd_tva) / 100);
				$pcmd_newinvesttc = $montantINV + (($montantINV * $cmd_tva) / 100);
				$pcmd_newfoncttc = $montantFON + (($montantFON * $cmd_tva) / 100);
				//
				// mise à jour des montants pour la ligne de paiement 
				//
				$requete = "update commande_paiement set 
							PCMD_TOTALHT=\"" . $pcmd_newtotalht . "\", 
							PCMD_INVESHT=\"" . $pcmd_newinvesht . "\",
							PCMD_FONCHT=\"" . $pcmd_newfoncht . "\",
							PCMD_TOTALTTC=\"" . $pcmd_newtotalttc . "\", 
							PCMD_INVESTTC=\"" . $pcmd_newinvesttc . "\",
							PCMD_FONCTTC=\"" . $pcmd_newfoncttc . "\"
							where PCMD_CLE=\"" . $cle . "\""; 
				$statement = $conn->query($requete);
//				Trace($requete);
				if (!$statement) break;
			}
	
			//
			// force mise à jour du budget global et du budget projet dans tous les cas
			//
			if (!$objCommande->majBudgetPaiement($numerosif, $montantTOT, $montantINV, $montantFON)) break;
			$conn->commit(); 
			$flag = true;
			break;
		}
		if ($flag == false) 
		{
			$conn->rollBack();
			$objForm->erreur='commande';
			$objForm->libelle=$objCommande->getLibelle();
			if ($objForm->libelle == '') $objForm->libelle="Erreur sur mise à jour du paiement"; 
		}
		else
		{
			//
			// retour à la liste
			//
			RedirURL("cmd_list_paiement.php");
			exit();
		}	
	}
}

// Affichage du début de la page
if ($cle != '') $objPage->addParam('cle', $cle);
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();

if ($etat == '') $etat = "Brouillon";

$requete = "select UTI_NOM, CONCAT(UTI_CLE,\"*-*\",UTI_NOM) as CLE from utilisateur order by 1";
$objForm->setSelect('intervenant', $idintervenant . "*-*" . $intervenant, $requete, 'CLE', 'UTI_NOM');

//Trace ("Form typepaie=" . $typepaie);

if ($cle == '') 
{
	$objForm->setOpt('typepaie', 'onChange="affPaiement()"');
	$objForm->setAff('datecre', false);
}	
else
{
	$objForm->setOpt('datecre', 'disabled');
//	$objForm->setOpt('typepaie', 'disabled');
}

// si le profil n'autorise pas l'exécution certain champ ne sont pas accessibles
if (!$objProfil->exe)
{
	$dataliste = array("Acompte (avec Service Fait)"=>"Acompte (avec Service Fait)","Acompte sur avancement"=>"Acompte sur avancement","Solde (avec Service Fait)"=>"Solde (avec Service Fait)","Paiement unique"=>"Paiement unique");
	$objForm->setDataliste('typepaie', $dataliste );
	$objForm->setOpt('priorité', 'disabled');
	$objForm->setOpt('numerosif', 'disabled');
	$objForm->setOpt('datepaie', 'disabled');
}

// requête d'accès à la base 
$requete = "select LCMD_CLE, MUO_CLE, MUO_NOMUO, MUO_MONTANT, MUO_FLAGIMPUTATION, 
			LCMD_TOTALHT, LCMD_LIBELLE, LCMD_COEFREDU, LCMD_FLAGIMPUTATION, 
			LCMD_QUANTITE, (LCMD_QUANTITE-LCMD_QTEPAYER) as LCMD_QTEPAYER, 
			RPL_CLE, RPL_QUANTITE,
			DATE_FORMAT(RPL_DATELIVR, '%d-%m-%Y') AS RPL_DATELIVR
			from commande_ligne 
			left join marche_uo on LCMD_IDUO=MUO_CLE
			left join paiement_ligne on RPL_IDLCMD=LCMD_CLE and RPL_IDPCMD=\"" . $cle . "\"
			left join commande_paiement on RPL_IDPCMD=PCMD_CLE
			where LCMD_IDCMD=\"" . $cleparent . "\" ";
$requete .= $objTab->majRequete('order by LCMD_CLE'); // ajout tri et pagination si besoin
$statement = $conn->query($requete);

// affichage des boutons d'enchainement
$objForm->addBouton("reset","RETOUR","cmd_list_paiement.php");
if (($cle == '' && $objProfil->cre) || ($cle != '' && $objProfil->maj))
{
	$objForm->addBouton("submit","ENVOYER");
}
else $objForm->setLecForm();

// affichage du formulaire
$objForm->affFormulaire();

// cas particulier d'un tableau éditable inclu dans un formumaire avec affichage optionnel en fonction du type de paiement
// on insère une balise DIV avec un id et par défaut on n'affiche pas
if ($typepaie == 'Acompte (avec Service Fait)' || $typepaie == 'Acompte sur avancement' || $typepaie == 'Solde (avec Service Fait)') 
$objPage->tampon .= "<div id='tableqte'>"; 
else $objPage->tampon .= "<div id='tableqte' style='display:none;'>"; 
// récupération des clés
$hiddenMUO = '';
$hiddenLCMD = '';
$hiddenRPL = '';
$hiddenqte = '';
$hiddenval = '';
// affichage du tableau
$ligne = array();
$cpt = 0;
while ($row = $statement->fetch(PDO::FETCH_ASSOC))
{
	if ($row['LCMD_FLAGIMPUTATION'] == '') $flagimputation = $row['MUO_FLAGIMPUTATION'];
	else $flagimputation = $row['LCMD_FLAGIMPUTATION'];
	$hiddenMUO .= $flagimputation."=".$row['MUO_MONTANT']."=".$row['LCMD_COEFREDU'].",";
	$hiddenLCMD .= $row['LCMD_CLE'].",";
	$hiddenRPL .= $row['RPL_CLE'].",";
	if ($row['LCMD_QTEPAYER'] == '') $hiddenqte .= '0,'; else $hiddenqte .= $row['LCMD_QTEPAYER'].",";
	if ($row['RPL_QUANTITE'] == '') $hiddenval .= '0,'; else $hiddenval .= $row['RPL_QUANTITE'].",";

	$ligne[0] = stripslashes($row['MUO_NOMUO']);
	if (isset($datelivr[$cpt])) $ligne[1] = $datelivr[$cpt];
	else if ($row['RPL_DATELIVR'] == '00-00-0000') $ligne[1] = '';
	else $ligne[1] = $row['RPL_DATELIVR'];
	$ligne[2] = $row['LCMD_TOTALHT'];
	$ligne[3] = $row['LCMD_QUANTITE'];
	if ($row['LCMD_QTEPAYER'] > '0') $ligne[4] = $row['LCMD_QTEPAYER'];
	else $ligne[4] = '0';
	if (isset($qtepayer[$cpt])) $ligne[5] = $qtepayer[$cpt];
	else if ($row['RPL_QUANTITE'] != '') $ligne[5] = $row['RPL_QUANTITE'];
	else $ligne[5] = '0';
	$objTab->affLigne($ligne);
	$cpt++;
}
// supprime la dernère virgule
$hiddenMUO = substr($hiddenMUO,0,-1);
$hiddenLCMD = substr($hiddenLCMD,0,-1);
$hiddenRPL = substr($hiddenRPL,0,-1);
$hiddenqte = substr($hiddenqte,0,-1);
$hiddenval = substr($hiddenval,0,-1);

$objTab->finTableau();
$objPage->tampon .= "</div>";

$objForm->finFormulaire();

// fin de page
$objPage->finPage();
?>
<script>
/***********************************************************/
// Exécuté après chargement de la page
/***********************************************************/
$(document).ready(function() {
	affPaiement();
	$('.mgf_editable').each(function()
	{
		if ($(this).val() == '') $(this).val('0');
	});
});

/***********************************************************/
// gestion de l'affichage en fonction du type de paiement
// Avance Acompte Solde Intérêts
/***********************************************************/
function affPaiement() 
{
	var typepaiement = $("#typepaie").val();
	// si accompte on affiche 
	if (typepaiement == 'Acompte (avec Service Fait)' || typepaiement == 'Acompte sur avancement' || typepaiement == 'Solde (avec Service Fait)') 
	{
		$("#tableqte").show(); 
		$("#totalttc").hide();
	}	
	else 
	{
		$("#tableqte").hide();
		$("#totalttc").show();
	}	
}
</script>
