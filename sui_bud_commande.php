<?php
include('config/sui_bud_commande.php');
$codefonc='rpm';
include('prepage.php');

$objPage->debPage('center');

// préparation du tableau
$objTab = new tableau('1');

$objTab->addBouton("reset","RETOUR","sui_list_rapport.php");

if (!isset($f_annee) || $f_annee == '') $f_annee = date("Y");
if (!isset($f_programme) || $f_programme == '') $f_programme = "P613";

// requête d'accès à la base 
$requete = "select CMD_NUMERO, CMD_PROGRAMME, PRG_NOMPROGRAMME, PRJ_NOMPROJET, 
			DOM_NOMDOMAINE, POL_NOMPOLE, CMD_OBJET, CMD_TOTALTTC, CMD_TOTALTTCFACT, CMD_DATESOU
			from commande 
			left join projet on CMD_IDPROJET=PRJ_CLE
			left join programme on PRJ_IDPROGRAMME=PRG_CLE
			left join marche on CMD_IDMARCHE=MAR_CLE
			left join pole on CMD_IDPOLE=POL_CLE
			left join domaine on POL_IDDOMAINE=DOM_CLE
			where DATE_FORMAT(CMD_DATESOU, '%Y')=\"".$f_annee."\"
			and CMD_IDETAT IN ('2','3','6','8','10')"; 
$requete .= $objTab->majRequete('order by PRG_NOMPROGRAMME,PRJ_NOMPROJET, CMD_NUMERO desc'); // ajout tri et pagination si besoin

// affichage des boutons d'enchainement
//$objTab->addBoutonSpe("button","EXPORTER","onclick=\"document.location.href='bud_csv_ligne.php';\"");

// affichage du tableau
$objTab->affTableau($requete);

// fin de page
$objPage->finPage();
