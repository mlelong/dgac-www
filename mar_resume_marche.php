<?php
include('config/mar_resume_marche.php');
$codefonc='*';
include('prepage.php');

$objPage->debPage('center');

// fabrication du javascript pour affichage les graphiques
$retour = "
		<div class='row'>
			<div class='col-md-6'>
				<h1>Budget DSI(613)</h1>
				<div id='columnchart1' style='width: 500px; height: 275px;'><canvas id='bar-chart' width='500px' height='275px'></canvas></div>
			</div> <!-- fin colonne gauche -->
			
                        <div class='col-md-6'>
				<h1>Echéances marchés </h1>
				<div id='piechart1' style='width: 500px; height: 275px;'><canvas id='pie-chart' width='500px' height='275px'></div>
			</div> <!-- fin colonne droite -->
		</div>
		
                <div class='row'>
                <script type=\"text/javascript\">
                new Chart(document.getElementById('bar-chart'), {
                        type: 'bar',
                        data: {
                          labels: ['AE Dotation','AE Consommé','CP Dotation','CP Consommé'],
                          datasets: [
                                {
                                  label: 'Budget DSI',
                                  backgroundColor: ['#3e95cd', '#8e5ea2','#3cba9f','#e8c3b9'],";
// consommation des AE et CP par programme
$requete = "select BUD_PROGRAMME, 
			BUD_AEDOTAFONCT, BUD_AEDOTAINVES, BUD_CPDOTAFONCT, BUD_CPDOTAINVES,
			BUD_AEDEBLFONCT, BUD_AEDEBLINVES, BUD_CPDEBLFONCT, BUD_CPDEBLINVES,
			BUD_AEPREVFONCT, BUD_AEPREVINVES, BUD_CPPREVFONCT, BUD_CPPREVINVES,
			BUD_AECONSFONCT, BUD_AECONSINVES, BUD_CPCONSFONCT, BUD_CPCONSINVES
			from budget where BUD_ANNEE=\"" . date("Y") . "\" and BUD_PROGRAMME=\"P613\"";
$statement = $conn->query($requete); 
$resultat = $statement->fetch(PDO::FETCH_ASSOC);
$aedota = ($resultat['BUD_AEDOTAFONCT']+$resultat['BUD_AEDOTAINVES'])/1000000;
$cpdota = ($resultat['BUD_CPDOTAFONCT']+$resultat['BUD_CPDOTAINVES'])/1000000;
$aedebl = ($resultat['BUD_AEDEBLFONCT']+$resultat['BUD_AEDEBLINVES'])/1000000;
$cpdebl = ($resultat['BUD_CPDEBLFONCT']+$resultat['BUD_CPDEBLINVES'])/1000000;
$aeprev = ($resultat['BUD_AEPREVFONCT']+$resultat['BUD_AEPREVINVES'])/1000000;
$cpprev = ($resultat['BUD_CPPREVFONCT']+$resultat['BUD_CPPREVINVES'])/1000000;
$aecons = ($resultat['BUD_AECONSFONCT']+$resultat['BUD_AECONSINVES'])/1000000;
$cpcons = ($resultat['BUD_CPCONSFONCT']+$resultat['BUD_CPCONSINVES'])/1000000;
$retour .= "
			data: [".round($aedota,2).",".round(($aeprev+$aecons),2).",".round($cpdota,2).",".round(($cpprev+$cpcons),2)."]";
$retour .= "
			}
		  ]
		},
		options: {
		  legend: { display: false },
		  title: {
			display: false,
			text: 'Predicted world population (millions) in 2050'
		  },
		  scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
		}
	});
	new Chart(document.getElementById('pie-chart'), {
		type: 'pie',
		data: {
		  labels: ['Préparation', 'Exécution', 'Reconduction', 'Fin 6 mois', 'Fin 12 mois'],
		  datasets: [{
			label: 'Echéances marchés',
			backgroundColor: ['#3e95cd', '#8e5ea2','#3cba9f','#e8c3b9','#c45850'],";
// nombre de commandes par mois 
$requete = "select MSTA_NBPREP, MSTA_NBEXEC, MSTA_NBRECONDUC, MSTA_NBFIN6M, MSTA_NBFIN12M
			from marche_stats where MSTA_CLE=1";
$statement = $conn->query($requete); 
$resultat = $statement->fetch(PDO::FETCH_ASSOC);
$retour .= "
			data: [".$resultat['MSTA_NBPREP'].",".$resultat['MSTA_NBEXEC'].",".$resultat['MSTA_NBRECONDUC'].",".$resultat['MSTA_NBFIN6M'].",".$resultat['MSTA_NBFIN12M']."]";
$retour .= "
		  }]
		},
		options: {
		  title: {
			display: false,
			text: 'Predicted world population (millions) in 2050'
		  }
		}
	});
	</script>";

// cumul dans le tampon de la page
$objPage->tampon .= $retour;
			
// préparation du tableau
$objTab = new tableau('1');

// requête d'accès à la base 
$requete = "select CMD_CLE, CMD_NUMERO, PRJ_NOMPROJET, ETA_NOMETAT
			from trace_acces 
			left join commande on TRA_IDCLE=CMD_CLE
			left join projet on CMD_IDPROJET=PRJ_CLE
			left join fonc_etat on CMD_IDETAT=ETA_CLE
			where CMD_CLE>0 and TRA_IDNOM=\"" . $objProfil->idnom . "\" and  TRA_IDTABLE=\"1\"
			order by TRA_DATEACC desc LIMIT 0,5";

// gestion des paramètres de lien
$objTab->setLien('numero','cmd_tab_commande.php',"?typeaction=modification&cle=#CMD_CLE#&menu=cmd",'href');

// affichage du tableau
$objTab->affTableau($requete);

// fermeture de la div
$objPage->tampon .= "</div> <!-- fin row -->";	

// fin de page
$objPage->finPage();
