/* 
	fonctions de gestion des tableaux
	version 1.01 le 24/06/2017
*/
/***********************************************************/
// Exécuté après le chargement de la page
/***********************************************************/
$(function(){
	if( $('#script').length) modeDefil(); // tableau en mode défilement
	$(".datepicker").datepicker({language: 'fr'});
	$("#tabs").find('li').find('.active').click(); //charge l'onglet actif à l'affichage de la page
});

/***********************************************************/
// appelé lors du clic sur le bouton qui permet d'afficher 
// les filtres supplémentaires sur une deuxième ligne 
/***********************************************************/
function affFiltreOption(nomform) 
{   
//	console.log('affFiltreOption ' + nomform);
	if ($('#'+nomform+'-fa').hasClass('fa-chevron-down'))
	{
		$('#'+nomform+'-fa').removeClass('fa-chevron-down');
		$('#'+nomform+'-fa').addClass('fa-chevron-up');
		$('#'+nomform+'-div').show();
		
	}
	else if ($('#'+nomform+'-fa').hasClass('fa-chevron-up'))
	{
		$('#'+nomform+'-fa').removeClass('fa-chevron-up');
		$('#'+nomform+'-fa').addClass('fa-chevron-down');
		$('#'+nomform+'-div').hide();
	}	
}

/***********************************************************/
// appelé lors du chargement du tableau depuis le serveur php
// le positionnement des filtres est sauvegardé dans l'espace 
// de stockage du navigateur (sessionStorage) pour la session
/***********************************************************/
function initTableauCol(origine) { 
//	console.log(origine+' initTableauCol chargement tableau depuis php');
	// test si le navigateur supporte le stockage interne pour la session
	if(window.sessionStorage) {
//		console.log('initTableauCol sessionStorage ok');
		// récupération du nom du script sans les paramètres
		var NomDuFichier_sans_param = getScriptName();
		// récupération des filtres depuis le stockage du navigateur
		var listfiltre = sessionStorage.getItem(NomDuFichier_sans_param);
		if (listfiltre === null) return; // rien n'est stocké, on sort
//		console.log('initTableauCol listfiltre=' + listfiltre);
		var tabfiltre = listfiltre.split(";");
//		console.log('initTableauCol tabfiltre=' + tabfiltre);
		for(var i = 0; i < 50; i++) {
			if (!$('#affcol'+i).length) break; // fin des colonnes
			// postionnement de l'attribut checked à true (0) ou false (1)
			if (tabfiltre[i] == 1) $('#affcol'+i).prop('checked', false);
			else $('#affcol'+i).prop('checked', true);
		}
	}
	// filtre du tableau en fonction des valeurs
//	console.log('initTableauCol appel a filtrerTableauCol');
	filtrerTableauCol();
}  
/***********************************************************/
// appelé lors du clic sur le bouton "Appliquer" 
// de la boite modale de filtrage des colonnes du tableau
/***********************************************************/
function filtrerTableauCol() {    
//	console.log('filtrerTableauCol Application des filtres du tableau');
	var tabfiltre = [];
    for(var i = 0; i < 50; i++) {
		if (!$('#affcol'+i).length) break; // fin des colonnes
        if ($('#affcol'+i).is(":checked")){
            $('td:nth-child('+(i+1)+')').show();
            $('th:nth-child('+(i+1)+')').show();
            tabfiltre[i] = 0; // true
        }
        else {
            $('td:nth-child('+(i+1)+')').hide();
            $('th:nth-child('+(i+1)+')').hide();
            tabfiltre[i] = 1; // false
        }    
    }
	// test si le navigateur supporte le stockage interne pour la session
	if(window.sessionStorage) { 
//		console.log('filtrerTableauCol sessionStorage ok');
		// sérialisation du tableau des filtres
		var listfiltre = tabfiltre.join(";"); 
		// récupération du nom du script sans les paramètres
		var NomDuFichier_sans_param = getScriptName();
		// sauvegarde des filtres courants
//		console.log('filtrerTableauCol listfiltre=' + listfiltre);
		sessionStorage.setItem(NomDuFichier_sans_param, listfiltre);
	}
} 

/***********************************************************/
// récupération du nom du script courant
/***********************************************************/
function getScriptName() { 
	var CheminComplet = document.location.href;
	var CheminRepertoire = CheminComplet.substring( 0 ,CheminComplet.lastIndexOf( "/" ) );
	var NomDuFichier = CheminComplet.substring(CheminComplet.lastIndexOf( "/" )+1 );
	var stopIndex=NomDuFichier.indexOf("?");
	var NomDuFichier_sans_param=NomDuFichier.substr(0,stopIndex);
	return NomDuFichier_sans_param;
}

/***********************************************************/
// pas utilisé pour le moment
/***********************************************************/
function sort_table(tbody, col, asc)
{
    var rows = tbody.rows;
    var rlen = rows.length;
    var arr = new Array();
    var i, j, cells, clen;
    // fill the array with values from the table
    for(i = 0; i < rlen; i++)
    {
        cells = rows[i].cells;
        clen = cells.length;
        arr[i] = new Array();
      for(j = 0; j < clen; j++) { arr[i][j] = cells[j].innerHTML; }
    }
    // sort the array by the specified column number (col) and order (asc)
    arr.sort(function(a, b)
    {
        var retval=0;
        var fA=parseFloat(a[col]);
        var fB=parseFloat(b[col]);
        if(a[col] != b[col])
        {
            if((fA==a[col]) && (fB==b[col]) ){ retval=( fA > fB ) ? asc : -1*asc; } //numerical
            else { retval=(a[col] > b[col]) ? asc : -1*asc;}
        }
        return retval;      
    });
    for(var rowidx=0;rowidx<rlen;rowidx++)
    {
        for(var colidx=0;colidx<arr[rowidx].length;colidx++){ tbody.rows[rowidx].cells[colidx].innerHTML=arr[rowidx][colidx]; }
    }
}

/********************************************************************************/
// test du défilement dynamique des tableaux                                
// $(window).height() Retourne la hauteur de la fenetre du navigateur
// $(document).height() Retourne la hauteur du document HTML
// $(window).scrollTop() Retourne la position verticale actuelle de la barre de défilement
//           scrollTop = 0 --> haut de page
//           scrolltop = $(document).height() - $(window).height() --> bas de page
/********************************************************************************/
var nextlimite = 0;
var nomscript = '';
var valnombre = 0;
var scrollready = true; // afin d'éviter le multiple appel à la fonction scroll
function modeDefil() 
{
	nomscript = $('#script').attr('script');
	valnombre = $('#script').attr('valnombre');
	nextlimite = valnombre;
//	console.log('nomscript=' + nomscript + ' valnombre=' + valnombre);
	$(window).scroll(function()
	{

/*
// de boris
		const scrollHeight = $(document).height();
		const scrollPosition = $(window).height() + $(window).scrollTop();
		// Si l'utilisateur est en bas de la page
		if ((scrollHeight - scrollPosition) / scrollHeight === 0) 
*/		
//		if ($(document).height() - $(window).height() < ($(window).scrollTop())+100)
//		if ($(document).height() - $(window).height() == $(window).scrollTop())
		if (scrollready && (($(window).scrollTop() + $(window).height()) + 100 > $(document).height()))
		{
			scrollready = false; // afin d'éviter le multiple appel à la fonction scroll
//			console.log('nextlimite=' + nextlimite);
//			console.log('scrollTop=' + $(window).scrollTop() + ' doc.height=' + $(document).height() + ' window.height=' + $(window).height());
			var url = nomscript;
			var parametres = "defil=o&nextlimite=" + nextlimite; // on indique qu'on veut juste la suite d'un tableau
			nextlimite = parseInt(nextlimite)+parseInt(valnombre);
//			console.log(url+'?'+parametres);
			$.ajax({type: 'POST', url: url, data: parametres, success: function(response){actuScroll(response);}});
		}
	});
}

// retour AJAX sur défilement des tableaux
function actuScroll(response) 
{
//	console.log('actuScroll');
	if (response == '')
	{
		// désactivation de l'évènement scroll
		$(window).off( "scroll" );
		return;
	}
	else
	{
//		console.log('affichage retour');
//		console.log(response);
		$(".table tr:last").parent().append(response);
		// ajustement si besoin de l'affichage des colonnes
		filtrerTableauCol();
	}
	scrollready = true; // évènement scroll de nouveau OK
}

/***************************************************************************************/
/* demande de confirmation des suppressions dans les tableaux                          */
/***************************************************************************************/
function supprimer() 
{ 
	if(confirm('Voulez vous vraiment supprimer ?')) return true
	else return false;
} 

