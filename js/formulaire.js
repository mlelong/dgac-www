/* 
	class de gestion des formulaires
	version 1.01 le 24/06/2017
*/
$( function() {
/*
	autocomplete combobox
	il y a des conflits de nom entre jquery et bootstrap sur tooltip et button
	il faut inclure ces deux lignes
		$.widget.bridge("uitooltip", $.ui.tooltip);
		$.widget.bridge("uibutton", $.ui.button);	
	dans la section <head> après la déclaration de jquery et avant celle de bootstrap
	il faut aussi renommer dans le code suivant tooltip par uitooltip et button par uibutton
*/
	$.widget( "custom.combobox", {
		_create: function() {
			this.wrapper = $( "<span>" )
				.addClass( "custom-combobox" )
				.insertAfter( this.element );

			this.element.hide();
			this._createAutocomplete();
			this._createShowAllButton();
		},

		_createAutocomplete: function() {
			var selected = this.element.children( ":selected" ),
				value = selected.val() ? selected.text() : "";

			this.input = $( "<input>" )
				.appendTo( this.wrapper )
				.val( value )
				.attr( "title", "" )
				.addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )
				.autocomplete({
					delay: 0,
					minLength: 0,
					source: $.proxy( this, "_source" )
				})
				.uitooltip({ // tooltip -> uitooltip
					classes: {
						"ui-tooltip": "ui-state-highlight"
					}
				});

			this._on( this.input, {
				autocompleteselect: function( event, ui ) {
					ui.item.option.selected = true;
					this._trigger( "select", event, {
						item: ui.item.option
					});
				},

				autocompletechange: "_removeIfInvalid"
			});
		},

		_createShowAllButton: function() {
			var input = this.input,
				wasOpen = false;

			$( "<a>" )
				.attr( "tabIndex", -1 )
				.attr( "title", "Voir tous les éléments" )
				.uitooltip()  // tooltip -> uitooltip
				.appendTo( this.wrapper )
				.uibutton({ // button -> uibutton
					icons: {
						primary: "ui-icon-triangle-1-s"
					},
					text: false
				})
				.removeClass( "ui-corner-all" )
				.addClass( "custom-combobox-toggle ui-corner-right" )
				.on( "mousedown", function() {
					wasOpen = input.autocomplete( "widget" ).is( ":visible" );
				})
				.on( "click", function() {
					input.trigger( "focus" );

					// Close if already visible
					if ( wasOpen ) {
						return;
					}

					// Pass empty string as value to search for, displaying all results
					input.autocomplete( "search", "" );
				});
		},

		_source: function( request, response ) {
			var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
			response( this.element.children( "option" ).map(function() {
				var text = $( this ).text();
				if ( this.value && ( !request.term || matcher.test(text) ) )
					return {
						label: text,
						value: text,
						option: this
					};
			}) );
		},

		_removeIfInvalid: function( event, ui ) {

			// Selected an item, nothing to do
			if ( ui.item ) {
				return;
			}

			// Search for a match (case-insensitive)
			var value = this.input.val(),
				valueLowerCase = value.toLowerCase(),
				valid = false;
			this.element.children( "option" ).each(function() {
				if ( $( this ).text().toLowerCase() === valueLowerCase ) {
					this.selected = valid = true;
					return false;
				}
			});

			// Found a match, nothing to do
			if ( valid ) {
				return;
			}

			// Remove invalid value
			this.input
				.val( "" )
				.attr( "title", value + " Cet élément n'existe pas" )
				.uitooltip( "open" ); // tooltip -> uitooltip
			this.element.val( "" );
			this._delay(function() {
				this.input.uitooltip( "close" ).attr( "title", "" ); // tooltip -> uitooltip
			}, 2500 );
			this.input.autocomplete( "instance" ).term = "";
		},

		_destroy: function() {
			this.wrapper.remove();
			this.element.show();
		}
	});
} );

// appel ajax pour garnir les champs autocomplete
/*
$(document).ready(function() {
	$(".liste-ac").each(function()	{
		var idchamp = $(this).attr('id');
		var parametres = "idmarche=" + idchamp; 
		$.ajax({type: 'POST', url: 'get_liste.php', data: parametres, dataType: 'json', success: function(response){actuAutoComplete(response, idchamp);}});
	});

});
*/
// retour AJAX de get_liste.php
/*
function actuAutoComplete2(tabcomplete, idchamp)
{
	var nbelem = tabcomplete.length;
	// si plus de 50 UO -> autocomplete
	if (nbelem > 50)
	{
//		typeaffuo = 'auto';
		$("#"+idchamp).autocomplete({
			source: tabcomplete,
			select: function (event, ui) {
				$("input[name=iduo]").val(ui.item.id);
			},
			close: function (event, ui) {
				setCmdLigne();
			}
		});
	}
	// si moins de 50 UO -> SELECT
	else
	{
//		typeaffuo = 'select';
		var opt = '';
		var select = "<select id='nomuo' class='form-control input-sm' name='nomuo' onchange='setUoId()'><option></option>";
		$.each(tabcomplete, function(cle, valeur) {
			select += "<option value=\"" + valeur['id'] + "\"" + opt + ">" + valeur['value'] + "</option>";
		});
		select += "</select>";
		$("#"+id).replaceWith(select);
	}
}	
*/
function delAttach(formulaire, nomfic, cptfic) 
{
	if (supprimer())
	{
		document.forms[formulaire].action += "&attache=" + nomfic + "&cptfic=" + cptfic;
		document.forms[formulaire].action=document.forms[formulaire].action.replace('typeaction=reception', 'typeaction=suppression');
		document.forms[formulaire].submit();
//		console.log("action=" + document.forms[formulaire].action);
	}
	else return;
}

function razid(id) 
{
	var elementAfficher = document.getElementById(id);
	if (elementAfficher) elementAfficher.innerHTML = '';	
}

function setFocus(id) 
{
	alert(id);
	var elementAfficher = document.getElementById(id);
	if (elementAfficher) elementAfficher.focus();	
}

function checkSize(formulaire, max_img_size)
{
    var elementInput = document.getElementById("uploadid");
    // check for browser support (may need to be modified)
	if (elementInput)
    {
		if(elementInput.files && elementInput.files.length == 1)
		{           
			if (elementInput.files[0].size > max_img_size) 
			{
				alert("Le fichier doit être inférieur à " + (max_img_size/1024/1024) + "MB");
				elementInput.value = '';
				return false;
			}
		}
	}
    return true;
}

function razCheck() 
{
	$(":checkbox[disabled!='disabled']").prop("checked",false);
}

function selectCheck() 
{
	$(":checkbox[disabled!='disabled']").prop("checked",true);
}

function addUpload() 
{    
	// This will add or delete new input field
	$('#myinput').children('span:first-child').children('input:first-child').change(function(){
		$("#myinput").prepend("<span><input name='nomfic[]' type='file' /></span>");
		$("#mylist").append("<div><div class='col-sm-1 form-poub-file'><a><img class='imgfiltre' src='images/corbeille.gif' ></a></div><div class='col-sm-11 form-lib-file'>" + $(this).val() + "</div></div>");
		$(".imgfiltre").click(function(){$(this).parent().parent().parent().remove();});
	});
	$('#myinput').children('span:first-child').children('input:first-child').click();            
}
function delUpload(nomfic, cptfic) 
{    
	// This will delete old upload file
	$("#mylist").prepend("<INPUT type='HIDDEN' NAME='delnomfic[]' VALUE='" + cptfic + "*;*" + nomfic + "' />");
	$("#delupload" + cptfic).remove();
}

/* limite le nombre de caractères saisis dans les zones de texte */
function limite(zone,maxi) 
{ 
	if(zone.value.length>=maxi){zone.value=zone.value.substring(0,maxi);} 
}

/***************************************************************************************/
/* gestion des champs OPTION dans une SELECT dépendant d'une autre SELECT              */
/***************************************************************************************/
/* récupération des champs OPTION en AJAX */
function getSelectOption(nomchamp, valeur) 
{ 
//	console.log('getSelectOption valeur=' + valeur);
	var parametres = "nomchamp=" + nomchamp + "&valeur=" + valeur; 
	$.ajax({
		url: "get_selectoption.php",
		type: 'POST',
		data: parametres,
		dataType: 'json',
		async: false,
		success: function(response){actuSelectOption(response, nomchamp);}
	});
}

// retour AJAX de getSelectOption
function actuSelectOption(response, nomchamp) 
{
//	console.log('actuSelectOption nomchamp=' + nomchamp);
	$("#"+nomchamp).html(''); // suppression des champs OPTION de la SELECT
//	$("#"+nomchamp).empty();
	$.each(response, function(nom, texte) {
		$("#"+nomchamp).append($("<option></option>")
		.attr("value", name).text(texte));
	});
}
