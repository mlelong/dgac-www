/* 
	scripts spécifiques à l'application
	version 1.01 le 24/06/2017
*/

/***************************************************************************************/
/* Affichage de la boite modal des messages de validation et d'erreur                  */
/***************************************************************************************/
function openModalMessage(couleur, libelle) 
{
	$('#messErreurData').attr('style','font-size: 1.1em; font-weight: bold; color:'+couleur+';');
	$('#messErreurData').text(libelle);
	$('#messErreur').modal('toggle');
	setTimeout(function() {$('#messErreur').modal('hide');}, 1500);
}

/***************************************************************************************/
/* mise à jour de l'URL pour la page d'accueil                                         */
/***************************************************************************************/
function setFavori() 
{ 
	var url = "maj_favori.php"; 
	var parametres = "url=" + document.location.href;
//	console.log(url+'?'+parametres);
	$.ajax({type: 'POST', url: url, data: parametres});
}  