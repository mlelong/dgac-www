function getCheck(profil) 
{
	$.ajax(
	{
		type: 'POST',
		url: 'get_profil.php',
		data: "cle=" + profil,
		success: rappelgetCheck
	});
}

function rappelgetCheck(reponse)
{
	var cpt=0;
	$(":checkbox").each(function()
	{
		if ($(this).attr('disabled') == 'disabled')
		{
			$(this).prop("checked",false);
			$(this).removeAttr('disabled');
		}
		if (reponse.charAt(cpt) == 1) 
		{
			$(this).prop("checked",true);
			$(this).attr('disabled','disabled');
		}
		cpt++;
	});
}

function addCheck(profil) 
{
	$.ajax(
	{
		type: 'POST',
		url: 'get_profil.php',
		data: "cle=" + profil,
		success: rappeladdCheck
	});
}

function rappeladdCheck(reponse)
{
	var cpt=0;
	$(":checkbox").each(function()
	{
		if (reponse.charAt(cpt) == 1) 
		{
			$(this).prop("checked",true);
		}
		cpt++;
	});
}
