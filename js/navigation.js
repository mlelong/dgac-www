/* 
	fonctions de navigation et de gestion des onglets
	version 1.01 le 24/06/2017
*/
/***********************************************************/
// Exécuté après le chargement de la page
/***********************************************************/
$(function(){
	$(".datepicker").datepicker({language: 'fr'});
	$("#tabs").find('li').find('.active').click(); //charge l'onglet actif à l'affichage de la page
});

/***************************************************************************************/
/* gestion des onglets en mode AJAX                                                    */
/***************************************************************************************/
// affichage de l'onglet en fonction de la demande
// appelé quand on clique sur un onglet de type AJAX pour récupérer la page à afficher
function getTabPage(curid, url) 
{ 
	idtab = curid; // on mémorise l'onglet courant
//	console.log('getTabPage idtab=' + idtab + ' url=' + url);
	if ($("#"+idtab).hasClass("loadok")) return; // le chargement a déjà été effectué
	var parametres = "onglet=o"; // on indique que ce sera une page à afficher dans un onglet
//	$.post(url,parametres,function(response){actuTabPage(response, curid);});  // post asynchrone par défaut
	$.ajax({type: 'POST', url: url, data: parametres, success: function(response){actuTabPage(response, curid);}, async:false});
	$("#"+idtab).addClass("loadok"); // on indique que le chargement de la page est fait
	$(".datepicker").datepicker({language: 'fr'});
}

// envoi du formulaire en AJAX pour modifier le composant en base de données
function setTabPage(nomform, url) 
{   
	var curid = idtab;
	var idform = 'idform' + nomform.charAt(nomform.length-1);
//	var objform = $('#'+idform);
	var objform = $("form[name=" + nomform + "]");
//	console.log('setTabPage idtab = ' + idtab + ' nomform = ' + nomform + ' idform = ' + idform);
	// formulaire avec upload de fichier
	if (objform.find('input:file').length >= 1) 
	{
//		console.log('upload de fichier nomform = ' + nomform);
		// envoi formulaire via AJAX si objet FormData
	    var formdata = false;
		if (window.FormData){ 
			// utilisation de l'objet FormData pour la mise en forme
			var formdata = new FormData(objform[0]);
			if (formdata) // FormData OK
			{
				formdata.append('onglet', 'o'); // on ajoute que ce sera une page à afficher dans un onglet
//				console.log('upload de fichier via FormData');
				// contentType: false et processData: false  obligatoires pour de l'upload de fichier
				$.ajax({type: 'POST', url: url, data: formdata, success: function (response) {actuTabPage(response, curid);}, async:false, contentType: false, processData: false});
			}
			else // FormData KO --> serialize
			{
				var parametres = "onglet=o"; // on indique que ce sera une page à afficher dans un onglet
				parametres += "&" + objform.serialize();
//				console.log('upload de fichier via serialize = ' + parametres);
				// contentType: false et processData: false  obligatoires pour de l'upload de fichier
				$.ajax({type: 'POST', url: url, data: parametres, success: function (response) {actuTabPage(response, curid);}, async:false});
			}
		}
		// sinon envoi formulaire via iframe
		else setTabFrame(nomform, url);
	}
	else
	// formulaire sans upload de fichier
	{
//		console.log('pas de upload de fichier');
		var parametres = "onglet=o"; // on indique que ce sera une page à afficher dans un onglet
		parametres = parametres + "&" + objform.serialize();
//		console.log('param=' + parametres);
	//  on supprime la class loadok pour recharger les autres onglets
		$(".loadok").removeClass("loadok");
		$.ajax({type: 'POST', url: url, data: parametres, success: function(response){actuTabPage(response, curid);}, async:false});
	}
}

// retour AJAX de getTabPage et setTabPage
function actuTabPage(response, curid) 
{
//	console.log('actuTabPage curid=' + curid);
	actuPage(response, curid);
}

/***************************************************************************************/
/* gestion des onglets en mode IFRAME (fichier attaché pas géré en AJAX)               */
/***************************************************************************************/
// l'affichage des formulaires se fait toujours en AJAX mais l'envoi se fait en SUBMIT
// avec retour vers iframe s'il y a des fichiers attachés (AJAX ne gère pas les fichiers attachés)
// sauvegarde des onglets avant envoi formulaire puis restauration après
function setTabFrame(nomform, url) 
{   
	var curid = idtab;
//	console.log('setTabFrame idtab=' + idtab + ' nomform=' + nomform);
	var objform = $("form[name=" + nomform + "]");
//  on supprime la class loadok pour recharger les autres onglets
	$(".loadok").removeClass("loadok");
	objform.attr({action: url+"&onglet=o",target: 'tab_iframe'}).submit(); // on indique que c'est avec onglet
	$("#tabframe").attr('nomform',nomform); // sauvegarde du nom de formulaire dans l'IFRAME
}

// retour IFRAME de setTabFrame (actuTabFrame est appelée dans le onload de l'IFRAME positionnée dans onglet.php)           
// <IFRAME id='tabframe' onload=\"actuTabFrame()\" NAME='tab_iframe' style=\"display:none;\" ></IFRAME>
function actuTabFrame() 
{
	var curid = idtab;
//	console.log('actuTabFrame curid=' + curid);
	var nomform = $("#tabframe").attr('nomform'); // récupération du nom de formulaire dans l'IFRAME
	var objform = $("form[name=" + nomform + "]");
	objform.removeAttr('target'); // suppression du target
	$("#tabframe").removeAttr('nomform'); // suppression du nom du formulaire de l'IFRAME
	var response = $("#tabframe").contents().find('body').html(); 
	if (response == '') return;
	actuPage(response, curid); // appel actuPage pour traiter le résultat
}

/***************************************************************************************/
/* actualisation de la page après retour AJAX ou IFRAME                                */
/***************************************************************************************/
function actuPage(response, curid) 
{
//	console.log('actuPage reponse=' + response);
	// affichage de l'écran retournée en réponse 
	// html() exécute les javascripts contenus dans la réponse
	$("#"+curid).html(response);
	$(".datepicker").datepicker({language: 'fr'});

//	$('.mce_editable').each(function() {
//		initEditor();	
//		$(this).css('display','block');
//	});
}

/***************************************************************************************/
/* prévisualisation des contenus dans une iframe à cause des CSS                       */
/***************************************************************************************/
function actu_edframe()
{
//	console.log('actued');
  	var tag = $('#id_edframe');
	var final_height = tag.contents().height();
	tag.attr('height', final_height);
}

/***********************************************************/
// Redirection vers une URL
// si onglet --> appel de la fonction getTabPage
// si pas onglet --> appel de la fonction href
/***********************************************************/
function VersURL (url) 
{
//	trace(1,'idtab=' + idtab + ' VersURL=' + url);
	// test si onglet ou non
	if (idtab == 'page') document.location.href=url; // c'est une page normale
	else 
	{
		$("#"+idtab).removeClass("loadok"); // pour forcer le chargement de la page
		getTabPage(idtab, url) // c'est une page avec des onglets
	}	
}

/***********************************************************/
// Appelée pour la soumission d'un formulaire  
// si onglet --> appel de la fonction setTabPage
// si pas onglet --> appel de la fonction submit
/***********************************************************/
function SubmitForm(nomform, action) 
{  
//	if (param != '') document.forms[nomform].action += "&" + param;
	if (action != '') document.forms[nomform].action=document.forms[nomform].action.replace('typeaction=reception', 'typeaction='+action);
	if (idtab == 'page') document.forms[nomform].submit(); // c'est une page normale
	else setTabPage(nomform, document.forms[nomform].action); // c'est une page avec des onglets
}

/***************************************************************************************/
/* gestion des spy scroll en mode AJAX                                                 */
/***************************************************************************************/
function getSpyPage(curid, url) 
{ 
//	console.log('getSpyPage curid=' + curid + ' url=' + url);
	if ($("#"+curid).hasClass("loadok")) return; // le chargement a déjà été effectué
	var parametres = "onglet=o"; // on indique que ce sera une page à afficher dans un spy
	$.ajax({type: 'POST', url: url, data: parametres, success: function(response){actuSpyPage(response, curid);}, async:false});
	$("#"+curid).addClass("loadok"); // on indique que le chargement de la page est fait
	$(".datepicker").datepicker({language: 'fr'});
}

// retour AJAX de getTabPage et setTabPage
function actuSpyPage(response, curid) 
{
//	console.log('actuSpyPage curid=' + curid);
	$("#"+curid).html(response);
}

/***************************************************************************************/
/* gestion du scroll top                                                               */
/***************************************************************************************/
function topFunction() 
{
	document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}

/***************************************************************************************/
/* affichage du scroll top                                                               */
/***************************************************************************************/
window.onscroll = function() {scrollFunction()};
function scrollFunction() {
	if(document.getElementById("scrollUp") !== null){
		if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
			document.getElementById("scrollUp").style.display = "block";
		} else {
			document.getElementById("scrollUp").style.display = "none";
		}
	}
	else return false;
}