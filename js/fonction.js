/* 
	fonctions
	version 1.01 le 24/06/2017
*/
// niveau de trace
var leveltrace=2;

/***************************************************************************************/
/* fonction de gestion des dates et heures                                             */
/***************************************************************************************/
function affDate(id, nom) 
{
	$('#' + id).val(recupDate());	
}

function affDateHeure(id, nom) 
{
	$('#' + id).val(recupDateHeure());	
}

function affHeure(id, nom) 
{
	$('#' + id).val(recupHeure());	
}

function recupDate() 
{
	aujourd_hui = new Date();
	jj = aujourd_hui.getDate();
	if (jj < 10) jj = "0" + jj;
	mm = aujourd_hui.getMonth();
	mm += 1;
	if (mm < 10) mm = "0" + mm;
	aa = aujourd_hui.getFullYear();
	jjmmaa = jj + "-" + mm + "-" + aa;
	return jjmmaa;	
}

function recupDateHeure() 
{
	aujourd_hui = new Date();
	jj = aujourd_hui.getDate();
	if (jj < 10) jj = "0" + jj;
	mm = aujourd_hui.getMonth();
	mm += 1;
	if (mm < 10) mm = "0" + mm;
	aa = aujourd_hui.getFullYear();
	hh = aujourd_hui.getHours();
	if (hh < 10) hh = "0" + hh;
	mn = aujourd_hui.getMinutes();
	if (mn < 10) mn = "0" + mn;
	jjmmaahhmn = jj + "-" + mm + "-" + aa + " " + hh + ":" + mn;
	return jjmmaahhmn;	
}

function recupHeure() 
{
	aujourd_hui = new Date();
	hh = aujourd_hui.getHours();
	if (hh < 10) hh = "0" + hh;
	mn = aujourd_hui.getMinutes();
	if (mn < 10) mn = "0" + mn;
	hhmn = hh + ":" + mn;
	return hhmn;	
}

function getTemps(temps) 
{
	hh = temps / 3600;
	mm = temps % 3600;
	hhmn = hh + "h" + mn;
	return hhmn;	
}

/***************************************************************************************/
/* gestion du scroll (PLUS UTILISER)                                                   */
/***************************************************************************************/
var scrOfX = 0, scrOfY = 0;
function savScroll() 
{  
  if( typeof( window.pageYOffset ) == 'number' ) {
    //Netscape compliant
    scrOfY = window.pageYOffset;
    scrOfX = window.pageXOffset;
  } else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ) {
    //DOM compliant
    scrOfY = document.body.scrollTop;
    scrOfX = document.body.scrollLeft;
  } else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) {
    //IE6 standards compliant mode
    scrOfY = document.documentElement.scrollTop;
    scrOfX = document.documentElement.scrollLeft;
//	alert("ou=" + scrOfY + "=" + scrOfX);
  }
	document.cookie = 'scrOfX='+ scrOfX;
	document.cookie = 'scrOfY='+ scrOfY;
}

function getScroll() 
{  
	scrOfX = readCookie('scrOfX');
	scrOfY = readCookie('scrOfY');
//	alert("in=" + scrOfY + "=" + scrOfX);
	window.scrollTo(scrOfX,scrOfY);
	eraseCookie('scrOfX');
	eraseCookie('scrOfY');
}

function getScrollPosition()
{	
	return (document.documentElement && document.documentElement.scrollTop) || window.pageYOffset || self.pageYOffset || document.body.scrollTop;
}

/***************************************************************************************/
/* gestion des coockies                                                                */
/***************************************************************************************/
function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) 
{
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}
function eraseCookie(name) 
{
	createCookie(name,"",-1);
}

/***************************************************************************************/
/* affichage des traces                                                                */
/***************************************************************************************/
function trace(level,data)
{
	if (level <= leveltrace)
	{
//		if (navigator.appName == "Netscape") console.log(data);
		if(typeof(console) != 'undefined') console.log(data);
//		else alert(data);
	}
}
