<?php
include('config/bud_maj_budget.php');
$codefonc='bud';
require_once('prepage.php');

$objForm = new formulaire('1');

if($typeaction == "creation") 
{
	$objForm->initChamp();
	// on créé l'année suivante si l'année en cours existe
	$requete = "select MAX(BUD_ANNEE) from budget;";
	$statement = $conn->query($requete);
	if ($statement->rowCount() > 0) 
	{
		$resultat = $statement->fetch(PDO::FETCH_ASSOC);
		if ($resultat['MAX(BUD_ANNEE)'] == '') $annee = date("Y");
		else 
		{
			$annee = $resultat['MAX(BUD_ANNEE)'];
			$annee = (int)$annee+1;
		}	
	}
	// ou l'année en cours si aucune année précédante
	else $annee = date("Y");
}

if($typeaction == "modification")
{
	$requete = "select BUD_CLE, BUD_LIBELLE, BUD_ANNEE, BUD_PROGRAMME, 
				BUD_AEDOTAFONCT, BUD_AEDEBLFONCT, BUD_CPDOTAFONCT, BUD_CPDEBLFONCT,
				BUD_AEDOTAINVES, BUD_AEDEBLINVES, BUD_CPDOTAINVES, BUD_CPDEBLINVES,
				BUD_COMMENTAIRE
				from budget where BUD_CLE=\"" . $cle . "\"";
	$resultat = $objForm->mapChamp($requete);
}

// réception des paramètres
if($typeaction == "reception")
{
// contrôle des paramètres
	$objForm->recChamp();
	while($objForm->erreur == '')
	{
		break;
	}

	if ($objForm->erreur == '')
	{
		if ($aedotafonct == '') $aedotafonct=0;
		if ($aedotainves == '') $aedotainves=0;
		if ($cpdotafonct == '') $cpdotafonct=0;
		if ($cpdotainves == '') $cpdotainves=0;
		if ($aedeblfonct == '') $aedeblfonct=0;
		if ($aedeblinves == '') $aedeblinves=0;
		if ($cpdeblfonct == '') $cpdeblfonct=0;
		if ($cpdeblinves == '') $cpdeblinves=0;
		$objForm->majBdd('budget', 'BUD_CLE', $cle);
		if ($cle == '') $newcle = $conn->lastInsertId();
		// option copie des lignes à partir de l'année précédante
		if ($flagcopie == 'o')
		{
			$annee_prec = (int)$annee-1;
			$requete = "select BUD_CLE, BUD_ANNEE from budget where BUD_ANNEE=\"" . $annee_prec . "\"
						and BUD_PROGRAMME=\"" . $programme . "\"";
			$statement = $conn->query($requete);
			$resultat = $statement->fetch(PDO::FETCH_ASSOC);
			if ($resultat['BUD_ANNEE'] == $annee_prec);
			{
				$cle_prec = $resultat['BUD_CLE'];
				$requete = "INSERT INTO budget_ligne (BUDL_IDBUDGET, BUDL_LIGNE, BUDL_IDPROJET, BUDL_IDMARCHE, BUDL_LIBELLE, BUDL_REFSIF,
							BUDL_AEDOTAFONCT, BUDL_AEDEBLFONCT, BUDL_CPDOTAFONCT, BUDL_CPDEBLFONCT,
							BUDL_AEDOTAINVES, BUDL_AEDEBLINVES, BUDL_CPDOTAINVES, BUDL_CPDEBLINVES)
							SELECT \"" . $newcle . "\", BUDL_LIGNE, BUDL_IDPROJET, BUDL_IDMARCHE, BUDL_LIBELLE, BUDL_REFSIF,
							BUDL_AEDOTAFONCT, BUDL_AEDEBLFONCT, BUDL_CPDOTAFONCT, BUDL_CPDEBLFONCT,
							BUDL_AEDOTAINVES, BUDL_AEDEBLINVES, BUDL_CPDOTAINVES, BUDL_CPDEBLINVES
							FROM budget_ligne where BUDL_IDBUDGET=\"" . $cle_prec . "\"";			
				$statement = $conn->query($requete);
			}
		}
		$objForm->libelle="Mise à jour effectuée";
		if ($cle == '') RedirURL("bud_tab_budget.php?typeaction=modification&cle=".$newcle);
	}
}

// Affichage du début de la page
if ($cle != '') $objPage->addParam('cle', $cle);
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();

if ($cle != '') $objForm->setOpt('annee', 'readonly');
if ($cle != '')	$objForm->setAff('flagcopie', false);

$objForm->affFormulaire();
	
// affichage des boutons d'enchainement
$objForm->addBouton("reset","RETOUR","bud_list_budget.php?menu=bud");
if (($cle == '' && $objProfil->cre) || ($cle != '' && $objProfil->maj))
{
	$objForm->addBouton("submit","ENVOYER");
}
$objForm->finFormulaire();

// fin de page
$objPage->finPage();
