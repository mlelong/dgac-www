<?php
include('config/prj_maj_programme.php');
$codefonc='prj';
$codemenu='prg';
require_once('prepage.php');

$objForm = new formulaire('1');

if($typeaction == "creation") $objForm->initChamp();

if($typeaction == "modification")
{
	$requete = "select PRG_CLE, PRG_TYPE, PRG_NOMPROGRAMME, PRG_LIBELLE 
				from programme where PRG_CLE=\"" . $cle . "\"";
	$resultat = $objForm->mapChamp($requete);
}

// réception des paramètres
if($typeaction == "reception")
{
// contrôle des paramètres
	$objForm->recChamp();
	while($objForm->erreur == '')
	{
		break;
	}

	if ($objForm->erreur == '')
	{
		$type = "Programme";
		if (!$objForm->majBdd('programme', 'PRG_CLE', $cle))
		{
			if ($objForm->libelle == '1062') {$objForm->erreur='nomprogramme'; $objForm->libelle="le nom du programme existe déjà";}
		}	
		else 
		{
			RedirURL("prj_list_programme.php");
			exit();
		}
	}
}

// Affichage du début de la page
if ($cle != '') $objPage->addParam('cle', $cle);
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();
	
// affichage des boutons d'enchainement
$objForm->addBouton("reset","RETOUR","prj_list_programme.php");
if (($cle == '' && $objProfil->cre) || ($cle != '' && $objProfil->maj))
{
	$objForm->addBouton("submit","ENVOYER");
}
else $objForm->setLecForm();

$objForm->affFormulaire();
$objForm->finFormulaire();

// fin de page
$objPage->finPage();
