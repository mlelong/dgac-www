<?php
include('config/dom_maj_domaine.php');
$codefonc='dom';
require_once('prepage.php');

$objForm = new formulaire('1');

if($typeaction == "creation") $objForm->initChamp();

if($typeaction == "modification")
{
	$requete = "select DOM_CLE, DOM_NOMDOMAINE, DOM_DIRECTION, DOM_LIBELLE from domaine where DOM_CLE=\"" . $cle . "\"";
	$resultat = $objForm->mapChamp($requete);
}

// réception des paramètres
if($typeaction == "reception")
{
// contrôle des paramètres
	$objForm->recChamp();
	while($objForm->erreur == '')
	{
		break;
	}

	if ($objForm->erreur == '')
	{
		$objForm->majBdd('domaine', 'DOM_CLE', $cle);
		$objForm->libelle="Mise à jour effectuée";
		if ($cle == '') $newcle = $conn->lastInsertId();
		$objForm->libelle="Mise à jour effectuée";
		if ($cle == '') RedirURL("/dom/dom_tab_domaine.php?typeaction=modification&cle=".$newcle);
	}
}

// Affichage du début de la page
if ($cle != '') $objPage->addParam('cle', $cle);
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();
	
// affichage des boutons d'enchainement
$objForm->addBouton("reset","RETOUR","dom_list_domaine.php?menu=dom");
if (($cle == '' && $objProfil->cre) || ($cle != '' && $objProfil->maj))
{
	$objForm->addBouton("submit","ENVOYER");
}
else $objForm->setLecForm();

$objForm->affFormulaire();
$objForm->finFormulaire();

// fin de page
$objPage->finPage();
