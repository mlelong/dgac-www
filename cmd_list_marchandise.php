<?php
include('config/cmd_list_marchandise.php');
$codefonc='cfg';
$codemenu='grm';
include('prepage.php');
if (!$objProfil->ctlDroit($typeaction, 'tableau')) exit();

$objPage->debPage('center');

// suppression d'une entrée
if($typeaction == "suppression")
{
	if(isset($cle))	
	{
		$requete = "delete from marchandise where GRM_CLE=\"" . $cle . "\"";
		$statement = $conn->query($requete);
	}
}

// préparation du tableau
$objTab = new tableau('1');

// requête d'accès à la base 
$requete = "select GRM_CLE, GRM_NUMERO, GRM_LIBELLE, GRM_DESCRIPTION, GRM_FLAGDSI from marchandise where 1";
$requete .= $objTab->majRequete('order by GRM_FLAGDSI, GRM_LIBELLE'); // ajout tri et pagination si besoin

// affichage des boutons d'enchainement
if ($objProfil->cre) $objTab->addBouton("button","AJOUTER","cmd_maj_marchandise.php");

// gestion des paramètres de lien
$objTab->setLien('numero','cmd_maj_marchandise.php',"?typeaction=modification&cle=#GRM_CLE#");
$objTab->setLien('supp','cmd_list_marchandise.php',"?typeaction=suppression&cle=#GRM_CLE#","Supprimer");

// affichage du tableau
$objTab->affTableau($requete);

// fin de page
$objPage->finPage();
