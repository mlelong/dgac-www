<?php
require_once('class/database.php');
$conn = database::getIntance();

header('Content-Type: text/csv'); 
header('Content-Disposition: attachment;filename=budget_ligne.csv'); 

$requete = "select BUDL_CLE, BUDL_IDBUDGET, BUDL_LIGNE, BUDL_IDPAPFONCT, BUDL_IDPAPINVES, PRG_NOMPROGRAMME,
			BUDL_IDPROJET, PRJ_NOMPROJET, POL_NOMPOLE, DOM_NOMDOMAINE, BUDL_TYPELIG, BUDL_LIBELLE,  
			BUDL_AEDOTAFONCT, BUDL_AEDEBLFONCT, BUDL_AEPREVFONCT, BUDL_AECONSFONCT, BUDL_CPDOTAFONCT, BUDL_CPDEBLFONCT,
			BUDL_CPPREVFONCT, BUDL_CPCONSFONCT, BUDL_AEDOTAINVES, BUDL_AEDEBLINVES, BUDL_AEPREVINVES, BUDL_AECONSINVES,
			BUDL_CPDOTAINVES, BUDL_CPDEBLINVES, BUDL_CPPREVINVES, BUDL_CPCONSINVES
			from budget_ligne 
			left join projet on BUDL_IDPROJET=PRJ_CLE
			left join programme on PRJ_IDPROGRAMME=PRG_CLE
			left join pole on PRJ_IDPOLE=POL_CLE 
			left join domaine on POL_IDDOMAINE=DOM_CLE 
			order by 1";
$statement = $conn->query($requete);
$sortie = fopen('PHP://output', 'w');
//add BOM to fix UTF-8 in Excel
//fputs($sortie, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
fputcsv($sortie, array_map("utf8_decode",array("CLE","ID BUDGET","LIGNE","ID PAP FONC","ID PAP INVES",
					"NOM PROGRAMME","ID PROJET","NOM PROJET","NOM POLE","NOM DOMAINE","TYPE LIGNE","LIBELLE",
					"AEDOTAFONCT","AEDEBLFONCT","AEPREVFONCT","AECONSFONCT","CPDOTAFONCT","CPDEBLFONCT",
					"CPPREVFONCT","CPCONSFONCT","AEDOTAINVES","AEDEBLINVES","AEPREVINVES","AECONSINVES",
					"CPDOTAINVES","CPDEBLINVES","CPPREVINVES","CPCONSINVES")),";");
while ($row = $statement->fetch(PDO::FETCH_ASSOC))
{
    fputcsv($sortie, array_map("utf8_decode",array(
    	$row['BUDL_CLE'],
    	$row['BUDL_IDBUDGET'],
    	stripslashes($row['BUDL_LIGNE']),
    	$row['BUDL_IDPAPFONCT'],
    	$row['BUDL_IDPAPINVES'],
     	stripslashes($row['PRG_NOMPROGRAMME']),
    	$row['BUDL_IDPROJET'],
     	stripslashes($row['PRJ_NOMPROJET']),
     	stripslashes($row['POL_NOMPOLE']),
     	stripslashes($row['DOM_NOMDOMAINE']),
		$row['BUDL_TYPELIG'],
    	stripslashes($row['BUDL_LIBELLE']),
    	$row['BUDL_AEDOTAFONCT'],
    	$row['BUDL_AEDEBLFONCT'],
    	$row['BUDL_AEPREVFONCT'],
    	$row['BUDL_AECONSFONCT'],
    	$row['BUDL_CPDOTAFONCT'],
    	$row['BUDL_CPDEBLFONCT'],
    	$row['BUDL_CPPREVFONCT'],
    	$row['BUDL_CPCONSFONCT'],
    	$row['BUDL_AEDOTAINVES'],
    	$row['BUDL_AEDEBLINVES'],
    	$row['BUDL_AEPREVINVES'],
    	$row['BUDL_AECONSINVES'],
    	$row['BUDL_CPDOTAINVES'],
    	$row['BUDL_CPDEBLINVES'],
    	$row['BUDL_CPPREVINVES'],
    	$row['BUDL_CPCONSINVES']
		)),";");
}
fclose($sortie);
