<?php
include('config/prj_maj_ligne.php');
$codefonc='bud';
require_once('prepage.php');

$objForm = new formulaire('1');

if($typeaction == "creation") 
{
	$objForm->initChamp();
	$idprojet = $cleparent;
}	

if($typeaction == "modification")
{
	$requete = "select BUDL_CLE, BUDL_IDBUDGET, BUDL_LIGNE, BUDL_IDPROJET, BUDL_IDMARCHE, BUDL_LIBELLE, BUDL_REFSIF, 
				BUDL_AEPREVFONCT, BUDL_CPPREVFONCT, BUDL_AEPREVINVES, BUDL_CPPREVINVES
				from budget_ligne where BUDL_CLE=\"" . $cle . "\"";
	$resultat = $objForm->mapChamp($requete);
}

// réception des paramètres
if($typeaction == "reception")
{
// contrôle des paramètres
	$objForm->recChamp();
	while($objForm->erreur == '')
	{
		break;
	}

	if ($objForm->erreur == '')
	{
		$objForm->majBdd('budget_ligne', 'BUDL_CLE', $cle);
		RedirURL("prj_list_ligne.php");
		exit();
	}
}

// Affichage du début de la page
if ($cle != '') $objPage->addParam('cle', $cle);
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();

$requete = "select BUD_CLE, BUD_ANNEE from budget order by 1";
$objForm->setSelect('idbudget', $idbudget, $requete, 'BUD_CLE', 'BUD_ANNEE');
$requete = "select MAR_CLE, MAR_NOMMARCHE from marche order by 1";
$objForm->setSelect('idmarche', $idmarche, $requete, 'MAR_CLE', 'MAR_NOMMARCHE');

// affichage des boutons d'enchainement
$objForm->addBouton("reset","RETOUR","prj_list_ligne.php");
if (($cle == '' && $objProfil->cre) || ($cle != '' && $objProfil->maj))
{
	$objForm->addBouton("submit","ENVOYER");
}
else $objForm->setLecForm();

$objForm->affFormulaire();
$objForm->finFormulaire();

// fin de page
$objPage->finPage();
