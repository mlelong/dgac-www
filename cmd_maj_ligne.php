<?php
include('config/cmd_maj_ligne.php');
$codefonc='cmd';
require_once('prepage.php');

$objForm = new formulaire('1');

if($typeaction == "creation") 
{
	$objForm->initChamp();
	$montantrev = 0;
	$flagrevision = '';
	$cle = '';
}

if($typeaction == "modification")
{
	$requete = "select LCMD_CLE, LCMD_IDCMD, LCMD_IDGCMD, LCMD_IDUO, LCMD_TYPEUO, MUO_NOMUO, MUO_MONTANT, MUO_MCOSLA,
				MUO_FLAGIMPUTATION, MUO_MARCHANDISE, MUO_FLAGREVISION, LCMD_FLAGIMPUTATION, LCMD_MARCHANDISE,
				LCMD_TYPEIMPUTATION, LCMD_LIBELLE, LCMD_MONTANTREV, LCMD_COEFREDU, LCMD_QUANTITE, LCMD_TOTALHT,
				DATE_FORMAT(LCMD_DATELIVA, '%d-%m-%Y') AS LCMD_DATELIVA,
				DATE_FORMAT(LCMD_DATEVA, '%d-%m-%Y') AS LCMD_DATEVA,
				DATE_FORMAT(LCMD_DATEVSR, '%d-%m-%Y') AS LCMD_DATEVSR
				from commande_ligne 
				left join marche_uo on LCMD_IDUO=MUO_CLE
				where LCMD_CLE=\"" . $cle . "\"";
	$resultat = $objForm->mapChamp($requete);
	if ($resultat['LCMD_MARCHANDISE'] == '') $marchandise = $resultat['MUO_MARCHANDISE'];
	if ($resultat['LCMD_FLAGIMPUTATION'] == '') $flagimputation = $resultat['MUO_FLAGIMPUTATION'];
	$flagrevision = $resultat['MUO_FLAGREVISION'];
	$muo_montant = $resultat['MUO_MONTANT'];
	$muo_mcosla = $resultat['MUO_MCOSLA'];
	$muo_nomuo = $resultat['MUO_NOMUO'];
	$lcmd_typeuo = $resultat['LCMD_TYPEUO'];
	$montant = $resultat['LCMD_TOTALHT'];
}

// réception des paramètres
if($typeaction == "reception")
{
// contrôle des paramètres
	$objForm->recChamp();
	while($objForm->erreur == '')
	{
		$quantite = str_replace(",", ".", $quantite); // remplacement de la virgule par un point
		if ($quantite == 0) {$objForm->erreur='quantite'; $objForm->libelle="La quantité doit être > 0"; break;}
		break;
	}

	if ($objForm->erreur == '')
	{
		//
		// récupération des informations de la commande avant mise à jour
		//
		$objCommande = new commande($conn, 'maj', 'ligne commande', $cleparent, $cle);
		//
		// montant HT de l'UO selectionnée dans le formulaire
		//
		$requete = "select MUO_MONTANT, MUO_MCOSLA, MUO_FLAGREVISION, MUO_MARCHANDISE,MUO_FLAGIMPUTATION
					from marche_uo where MUO_CLE=\"" . $iduo . "\"";
		$statement = $conn->query($requete);
		$resultat = $statement->fetch(PDO::FETCH_ASSOC);
		$muo_montant = $resultat['MUO_MONTANT'];
		$muo_mcosla = $resultat['MUO_MCOSLA'];
		$muo_flagrevision = $resultat['MUO_FLAGREVISION'];
		$muo_marchandise = $resultat['MUO_MARCHANDISE'];
		$muo_flagimputation = $resultat['MUO_FLAGIMPUTATION'];
		if ($flagimputation != 'F' && $flagimputation != 'I') $flagimputation = $muo_flagimputation;
		if ($marchandise == '') $marchandise = $muo_marchandise;
		// traitement uo ou sla de l'uo
		// le champs typeuo est ajouté dynamiquement lors de la select de l'UO si c'est une UO avec des SLA
		// les montants des SLA sont dans MUO_MCOSLA
		// le type de SLA (S1,S2,S3,S4) est indiqué dans LCMD_TYPEUO
		if(isset($_REQUEST['typeuo'])) $typeuo=$_REQUEST['typeuo'];
		else $typeuo = '';
		if ($typeuo != '' || $typeuo != 'UO')
		{
			$tabmcosla = explode(';',$muo_mcosla);
			if ($typeuo == 'S1' && isset($tabmcosla[0])) $muo_montant = $tabmcosla[0];
			else if ($typeuo == 'S2' && isset($tabmcosla[1])) $muo_montant = $tabmcosla[1];
			else if ($typeuo == 'S3' && isset($tabmcosla[2])) $muo_montant = $tabmcosla[2];
			else if ($typeuo == 'S4' && isset($tabmcosla[3])) $muo_montant = $tabmcosla[3];
			else $typeuo = '';
		}
		else $typeuo = '';
		
//		Trace('muo_montant=' . $muo_montant);
		//
		// calcul du montant HT révisé et remisé de la ligne en fonction de la quantité
		//
		$coefredu = str_replace("%", "", $coefredu); // on supprime le % si renseigné
		$coefredu = str_replace(",", ".", $coefredu); // on remplace la virgule par un point
		if ($coefredu == '' || $coefredu == 0) $remise = 1;
		else $remise = (100-$coefredu)/100; // convertir % en coefficient de réduction (25% -> 0,75)
		// pour les lignes avec UO montantrev est calculé avec la dernière révision
		// vérification si la révision est à appliquée sur cette UO
		if ($muo_flagrevision == 'o') $mrev_coefrev = 1;
		// récupération de la dernière révision du marché
		else $mrev_coefrev = $objCommande->getRevision();
		$montantrev = ($muo_montant * $mrev_coefrev);
		$montantHT = ($montantrev * $quantite);
		$montantHT = ($montantHT * $remise);
//		Trace('muo_montant=' . $muo_montant . ' mrev_coefrev=' . $mrev_coefrev . ' quantite=' . $quantite . ' montantHT=' . $montantHT);
		//
		// mise à jour de la ligne de commande en BDD
		//
		$conn->setAttribute(PDO::ATTR_AUTOCOMMIT,0); 
		$conn->beginTransaction();
		$flag = false;
		while(true)
		{
			// mise à jour de la ligne de commande en BDD 
			$totalht = $montantHT; // pour maj bdd uniquement
			$objForm->addChamp('typeuo', 'LCMD_TYPEUO');
			if ($cle != '') $objForm->delChampBdd('iduo');
			if($objForm->majBdd('commande_ligne', 'LCMD_CLE', $cle) === false) break;
			
			// mise à jour de la commande en BDD si le montant HT ou l'imputation de la ligne ont changé
			if ($objCommande->majLigneCommande($totalht, $flagimputation) === false) break;
			
			// validation des mises à jour
			$conn->commit(); 
			$flag = true;
			break;
		}
		if ($flag == false) 
		{
			$conn->rollBack();
			$objForm->erreur='commande';
			$objForm->libelle=$objCommande->getLibelle();
			if ($objForm->libelle == '') $objForm->libelle="Erreur sur mise à jour de la ligne de commande"; 
			$flagrevision = $muo_flagrevision;
		}
		else
		{
			// retour à la liste
			RedirURL("cmd_list_ligne.php");
			exit();
		}
	}
}

// Affichage du début de la page
if ($cle != '') $objPage->addParam('cle', $cle);
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();

/*
$requete = "select MUO_CLE, CONCAT(IFNULL(MUO_REFEXTERNE,''),\"\t\", IFNULL(MUO_NOMUO,''),\"\t\",IFNULL(MUO_MONTANT,''),' Euros') as TEXTE 
			from marche_uo where MUO_FLAGDSI=\"o\" 
			and	MUO_IDMARCHE=(select CMD_IDMARCHE from commande where CMD_CLE=" . $cleparent . ") order by 1";
if ($cle == '') $objForm->setSelect('iduo', $iduo, $requete, 'MUO_CLE', 'TEXTE', 'onChange="setCmdLigne()"');
else 
{
	$objForm->setSelect('iduo', $iduo, $requete, 'MUO_CLE', 'TEXTE', 'disabled');
	$objForm->setOpt('iduo', 'disabled');
}
*/
if ($cle != '') 
{
	$objForm->setOpt('nomuo', 'disabled');
	if ($objForm->libelle != '');
	else if (isset($muo_nomuo)) $nomuo = $muo_nomuo."\t".$muo_montant." Euros";
	else $nomuo = '';
}	

$requete = "select GCMD_CLE, GCMD_LIBELLE from commande_groupe_ligne where GCMD_IDCMD=" . $cleparent . " order by 1";
$objForm->setSelect('idgcmd', $idgcmd, $requete, 'GCMD_CLE', 'GCMD_LIBELLE', '');

// si le profil n'autorise pas l'exécution certain champ ne sont pas accessibles
if (!$objProfil->exe)
{
	$objForm->setOpt('marchandise', 'disabled');
//	$objForm->setOpt('flagimputation', 'disabled');
}

$objForm->setOpt('montant', 'disabled');

// affichage des boutons d'enchainement
$objForm->addBouton("reset","RETOUR","cmd_list_ligne.php");
if (($cle == '' && $objProfil->cre) || ($cle != '' && $objProfil->maj))
{
	$objForm->addBouton("submit","ENVOYER");
}
else $objForm->setLecForm();

$objForm->affFormulaire();
$objForm->finFormulaire();

$requete = "select CMD_IDMARCHE, CMD_TVA, CMD_COEFREV from commande where CMD_CLE=\"" . $cleparent . "\"";
$statement = $conn->query($requete);
$resultat = $statement->fetch(PDO::FETCH_ASSOC);
$tva = $resultat['CMD_TVA'];
if (!isset($flagrevision)) $flagrevision = "";
if ($flagrevision != 'o')
{
	$coefrev = $resultat['CMD_COEFREV'];
	if ($coefrev == '' || $coefrev == 0) $coefrev = 1;
}
else $coefrev = 1;

// fin de page
$objPage->finPage();

?>
<script>
<?php
echo "var idmarche = '" . $cleparent . "';";
echo "var tva = '" . $tva . "';";
echo "var coefrev = " . $coefrev . ";";
echo "var flagrevision = '" . $flagrevision . "';";
if (isset($muo_montant)) echo "var muo_montant = '" . $muo_montant . "';";
if (isset($muo_mcosla)) echo "var muo_mcosla = '" . $muo_mcosla . "';";
else echo "var muo_mcosla = 'no';";
if (isset($lcmd_typeuo)) echo "var lcmd_typeuo = '" . $lcmd_typeuo . "';";
else echo "var lcmd_typeuo = 'UO';";
?>
/***********************************************************/
// Exécuté après chargement de la page
/***********************************************************/
var montant = 0;
var remise = 1;
var quantite = 0;
var typeaffuo = 'texte';
$(document).ready(function() {
/*
	// alternative à l'autocomplete avec un champ supplémentaire de filtre qui modifie le contenu du champ SELECT
	if ($('#iduo').is(':disabled') == false)
	{
		if ($('#iduo > option').length > 50)
		{
			var filtreuo = "<div class=\"form-group row\"><label class=\"control-label col-sm-4\" for=\"filtreuo\"><b>Filtre Poste :</b></label>";
			filtreuo += "<div class=\"col-sm-6\"><div class=\"input-group\"><input id=\"filtreuo\" class=\"form-control input-sm\" type=\"text\" value='' maxlength=\"100\" name=\"filtreuo\">";
			filtreuo += "<span class=\"input-group-btn\"><button class=\"btn btn-default\" type=\"button\" onClick=\"getUO();\" >Filtrer</button></span></div></div>";
			var parent = $("#iduo").parents(".form-group");
			parent.after(filtreuo);
		}
	}
*/
	// si mise à jour de la ligne de commande le champ est en texte et disabled -> pas d'appel AJAX
	if (!$('#nomuo').is(':disabled'))
	{
		traceCmdLigne('ready idmarche = ' +idmarche);
		var parametres = "idmarche=" + idmarche; 
		$.ajax({type: 'POST', url: 'get_uo.php', data: parametres, dataType: 'json', success: function(response){actuUO(response, 'nomuo');}});
		$("#nomuo").change(function() {
			getMontant();
			affMontant();
		});
	}
	
	if (muo_mcosla != 'no') addSLA(muo_montant, muo_mcosla, lcmd_typeuo);
	getMontant();
	getQuantite();
	affMontant();
	// calcul du montant si changement UO
	// calcul du montant si changement quantite
	$("#quantite").change(function() {
		getQuantite();
		affMontant();
	});
});

// retour AJAX de get_uo.php
function actuUO(tabcomplete, id)
{
	var nbelem = tabcomplete.length;
	traceCmdLigne('actuUO long='+nbelem);
	// si plus de 50 UO -> autocomplete
	if (nbelem > 50)
	{
		typeaffuo = 'auto';
		$("#"+id).autocomplete({
			source: tabcomplete,
			select: function (event, ui) {
				traceCmdLigne('autocomplete select id = '+ui.item.id);
				$("input[name=iduo]").val(ui.item.id);
			},
			close: function (event, ui) {
				traceCmdLigne('autocomplete close nomuo = '+$("#nomuo").val());
				setCmdLigne();
			}
		});
	}
	// si moins de 50 UO -> SELECT
	else
	{
		typeaffuo = 'select';
		var opt = '';
		var select = "<select id='nomuo' class='form-control input-sm' name='nomuo' onchange='setUoId()'><option></option>";
		$.each(tabcomplete, function(cle, valeur) {
			select += "<option value=\"" + valeur['id'] + "\"" + opt + ">" + valeur['value'] + "</option>";
		});
		select += "</select>";
		select += '<button type="button" class="btn btn-outline-secondary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="sr-only">Toggle Dropdown</span></button>';
		$("#"+id).replaceWith(select);
//		$("#nomuo").combobox();
//		$(".custom-combobox-input").removeClass("custom-combobox-input").addClass("form-control input-sm");
//		$(".custom-combobox-toogle").addClass("form-control input-sm");
	}
}	

/***********************************************************/
// récupération de l'ID de l'UO et sauvegarde dans le champs caché iduo
/***********************************************************/
function setUoId() 
{
	var id=$("#nomuo option:selected").val();
	$("input[name=iduo]").val(id);
	setCmdLigne();
}

/***********************************************************/
// filtre les UO (plus utilisé)
/***********************************************************/
function getUO() 
{
	var valfiltreuo = $("#filtreuo").val();
	if (valfiltreuo == '') 
	{
		$("#iduo > option").show();
	}	
	else
	{
		$("#iduo > option").hide();
		$("#iduo > option:contains("+valfiltreuo+")").show();
	}
}

/***********************************************************/
// récupère le nom de l'UO sélectionnée et son montant
// en fonction du type d'affichage (texte disbaled, autocomplete ou select)
/***********************************************************/
function getSelectUoLigne() 
{
	if (typeaffuo == 'texte') var ligne = $("#nomuo").val();
	else if (typeaffuo == 'select') var ligne=$("#nomuo option:selected").text();
	else if (typeaffuo == 'auto') var ligne=$("#nomuo").val();
	else var ligne='';
	traceCmdLigne('getSelectUoLigne = ' + ligne);
	return ligne;
}

/***********************************************************/
// récupère l'ID de l'UO sélectionnée
// en fonction du type d'affichage (texte disbaled, autocomplete ou select)
/***********************************************************/
function getSelectUoId() 
{
	if (typeaffuo == 'texte') var id = $("input[name=iduo]").val();
	else if (typeaffuo == 'select') var id=$("#nomuo option:selected").val();
	else if (typeaffuo == 'auto') var id=$("input[name=iduo]").val();
	else var id='';
	traceCmdLigne('getSelectUoId = ' + id);
	return id;
}

/***********************************************************/
// récupère le montant à partir de l'UO sélectionnée
/***********************************************************/
function getMontant() 
{
	// UO avec SLA
	if ($('input[name=typeuo]').length > 0)
		montant = $('input[name=typeuo]:checked').attr('montant');
	else
	{
		var ligne = getSelectUoLigne();
		if (ligne != "")
		{	
			var tab = ligne.split("\t");
			traceCmdLigne('tab0=' + tab[0] + ' tab1=' + tab[1]);
			var tab1 = tab[1].split(" Euros");
			montant = tab1[0];
		}	
	}	
	traceCmdLigne('getMontant montant='+montant);
}

/***********************************************************/
// récupère la quantité saisie dans le formulaire
/***********************************************************/
function getQuantite() 
{
	quantite=$("#quantite").val().replace(',', '.');
	if (quantite == '' || quantite == 0) quantite = 0;
	traceCmdLigne('getQuantite quantite='+quantite);
}

/***********************************************************/
// calcul et affiche le montant pour l'UO sélectionnée 
// à partir de la quantité, de la révision et du la réduction
/***********************************************************/
function affMontant() 
{
	if (coefrev == '' || coefrev == 0) coefrev = 1;
	if (flagrevision == 'o') coefrev = 1;
	if (tva == '' || tva == '0') tva = 20;
	remise=$("#coefredu").val().replace(',', '.');
	if (remise == '' || remise == '0') remise = 1;
	else remise = (100-remise)/100;
	traceCmdLigne('affmontant montant=' + montant + ' quantite=' + quantite + ' remise=' + remise + ' coefrev=' + coefrev);
	var cumul = montant*quantite*coefrev*remise;
	cumul = Number(Math.round(cumul+'e2')+'e-2');
//	cumul = cumul+((cumul*20)/100);
	if (cumul == 0) cumul = '';
	$("#montant").val(cumul);
}

/***********************************************************/
// récupère des informations sur l'UO qui vient d'être sélectionnée
// dans la select ou l'autocomplete
/***********************************************************/
function setCmdLigne() 
{   
	var ligne = getSelectUoLigne();
	traceCmdLigne('setCmdLigne ligne=' + ligne + '=');
	if (ligne == "")
	{
		$("#libelle").val('');
	}
	else
	{
//		var tab = ligne.split("\t");
//		$("#libelle").val(tab[0]);
		var iduo = getSelectUoId();
		traceCmdLigne('setCmdLigne iduo=' + iduo + '=');
		var parametres = "iduo=" + iduo; 
		$.ajax({type: 'POST', url: 'get_marchandise.php', data: parametres, success: function(response){actuCmdLigne(response);}, async:false});
//		traceCmdLigne('ajax ' + parametres);
	}
}

// retour AJAX de setCmdLigne get_marchandise.php
function actuCmdLigne(response) 
{
	traceCmdLigne('retour actuCmdLigne=' + response);
	if (response != 'KO') 
	{
		// retour MUO_MONTANT, MUO_MCOSLA, MUO_MARCHANDISE, MUO_FLAGIMPUTATION, MUO_FLAGREVISION 
		var tab = response.split("*;*");
		$("#marchandise").val(tab[2]);
		if (tab[3] == 'F') $('input:radio[name=flagimputation]:nth(0)').attr('checked',true);
		else $('input:radio[name=flagimputation]:nth(1)').attr('checked',true);
		flagrevision = tab[4];
		if (tab[1] != '') addSLA(tab[0], tab[1], '');
		getMontant();
		affMontant();
	}	
}

/***********************************************************/
// Ajout des chanps de saisie des SLA si UO avec SLA
/***********************************************************/
function addSLA(montant, mcosla, typeuo) 
{
	traceCmdLigne('addSLA montant='+montant+' mcosla='+mcosla+' typeuo='+typeuo);
	var tab = mcosla.split(";");
	var elem = '<div id="divtypeuo" class="form-group row">';
	elem += '	<label class="control-label col-sm-4" for="typeuo"><b>Support/SLA associé (niveau 1 2 3 4) :</b></label>';
	elem += '	<div class="col-sm-6">';
	var opt = '';
	if (typeuo == '' || typeuo == 'UO') opt = 'checked="checked"';
	elem += '		<label class="radio-inline"><input name="typeuo" value="UO" '+opt+' montant="'+montant+'" type="radio">'+montant+' UO</label>';
	if (typeuo == 'S1') opt = 'checked="checked"';
	else opt = '';
	if (tab[0] != '') elem += '		<label class="radio-inline"><input name="typeuo" value="S1" '+opt+' montant="'+tab[0]+'" type="radio">'+tab[0]+' SLA-1</label>';
	if (typeuo == 'S2') opt = 'checked="checked"';
	else opt = '';
	if (tab[1] != '') elem += '		<label class="radio-inline"><input name="typeuo" value="S2" '+opt+' montant="'+tab[1]+'" type="radio">'+tab[1]+' SLA-2</label>';
	if (typeuo == 'S3') opt = 'checked="checked"';
	else opt = '';
	if (tab[2] != '') elem += '		<label class="radio-inline"><input name="typeuo" value="S3" '+opt+' montant="'+tab[2]+'" type="radio">'+tab[2]+' SLA-3</label>';
	if (typeuo == 'S4') opt = 'checked="checked"';
	else opt = '';
	if (tab[3] != '') elem += '		<label class="radio-inline"><input name="typeuo" value="S4" '+opt+' montant="'+tab[3]+'" type="radio">'+tab[3]+' SLA-4</label>';
	elem += '	</div>';
	elem += '</div>';
	if ($("#divtypeuo").length > 0) $("#divtypeuo").replaceWith(elem);
	else $("#marchandise").parent().parent().after(elem);
	$('input[name=typeuo]').change(function() {
		getMontant();
		affMontant();
	});
}

/***********************************************************/
// trace 
/***********************************************************/
function traceCmdLigne(message) 
{
//	console.log(message);
}
</script>
