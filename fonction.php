<?php
function VersURL() // arg1=url arg2=supp
{
	$objPage = page::getPage();
	$url = func_get_arg(0);
	if (strpos($url, "?") === false) $url .= "?" . $objPage->param; // ajout des paramètres d'URL 
	else $url .= "&" . $objPage->param;
	if (func_num_args() == 2) $confirm = func_get_arg(1);
	else $confirm = '';
	if ($confirm == "supp") $retour = "onclick=\"if (supprimer()) VersURL('" . $url . "');\"";
	else $retour = "onclick=\"VersURL('" . $url . "')\"";
	return $retour;
}

function RedirURL($url) // redirection d'URL pour page avec maj et retour vers une liste
{
	$objPage = page::getPage();
	if (!is_object($objPage))
	{
		header("Location: log_connexion.php");
		return;
	}	
	$param = $objPage->param; // ajout des paramètres d'URL 
	if ($objPage->isOnglet())
	{
		if ($param == '') $param = "onglet=o";
		else $param .= "&onglet=o";
	}
	if (strpos($url, "?") === false) $retour = $url . "?" . $param;
	else $retour = $url . "&" . $param;
//	Trace("RedirURL = " . $retour . " onglet=" . $objPage->onglet);
	if ($objPage->isOnglet()) echo "<script type='text/javascript'>VersURL('" . $retour . "'); </script>";
	else header("Location:" . $retour);
//	return $retour;
}

function getFichier($repertoire)
{
   $open = opendir($repertoire); 
   while ($file = readdir($open)) 
   {
      if (is_file($repertoire.$file)) 
      {
         $extension = strtolower(substr(strrchr($file,  "." ), 1)); 
         $extsupport = array("jpg", "jpeg", "gif", "png", "JPG"); 
         if (in_array($extension, $extsupport) and ($file[0] != "#")) 
         {
            $files[] = $file; 
         }
      }
   }
   closedir($open); 
   if (count($files) != 0) sort($files, SORT_NUMERIC);
   return $files;
}

function ctlNomFichier($fichier)
{
// | ; , ! @ # $ ( ) < > / \ " ' ` ~ { } [ ] = + & ^ <space> <tab>
//	$fichier = utf8_decode($fichier);
	$supprimer = array("|", ";", ",", "!", "@", "#", "$", "/", "\\", '"', "'", "`", "~", "{", "}", "[", "]", "=", "+", "*", "&", "^", "?", "(", ")");
	$fichier = str_replace($supprimer, "", $fichier);
	$aremplacer = explode(","," ç,æ,œ,á,é,í,ó,ú,à,è,ì,ò,ù,ä,ë,ï,ö,ü,ÿ,â,ê,î,ô,û,å,e,i,ø,u");
	$remplace = explode(",","-c,ae,oe,a,e,i,o,u,a,e,i,o,u,a,e,i,o,u,y,a,e,i,o,u,a,e,i,o,u");
	$fichier = str_replace($aremplacer, $remplace, $fichier);
/*	
	$fichier = str_replace('é','e',$fichier);
	$fichier = str_replace('è','e',$fichier);
	$fichier = str_replace('ê','e',$fichier);
	$fichier = str_replace('à','a',$fichier);
	$fichier = str_replace('â','a',$fichier);
	$fichier = str_replace('î','i',$fichier);
	$fichier = str_replace('û','u',$fichier);
	$fichier = str_replace('ù','u',$fichier);
	$fichier = str_replace('ô','o',$fichier);
*/	
//	$fichier = utf8_encode($fichier);
	return $fichier;
}


function ctlHeure($heure)
{
	$tab = explode(':', $heure);
	if (count($tab) != 2) return FALSE;
	if (!is_numeric($tab[0]) && !is_numeric($tab[1])) return FALSE;
	if ($tab[1] > 59) return FALSE; 
	return TRUE;
}

function ctlDate($date)
{
	$tab = explode('-', $date);
	if (count($tab) != 3)
	{
		$tab = explode('/', $date);
		if (count($tab) != 3) return FALSE;
	}
	if (!is_numeric($tab[0]) && !is_numeric($tab[1]) && !is_numeric($tab[2])) return FALSE;
	if (!checkdate($tab[1],$tab[0],$tab[2])) return FALSE; // check sur mois, jour, annee
	return TRUE;
}

function invDate($date)
{
	$tab = explode('-', $date);
	if (count($tab) != 3) $tab = explode('/', $date);
	$res = $tab[2] . "-" . $tab[1] . "-" . $tab[0];
	return $res;
}

function ctlDateHeure($date)
{
	$tabDH = explode(' ', $date);
	if (count($tabDH) != 2) return FALSE;
	$tabD = explode('-', $tabDH[0]);
	if (count($tabD) != 3)
	{
		$tabD = explode('/', $tabDH[0]);
		if (count($tabD) != 3) return FALSE;
	}
	if (!is_numeric($tabD[0]) && !is_numeric($tabD[1]) && !is_numeric($tabD[2])) return FALSE;
	if (!checkdate($tabD[1],$tabD[0],$tabD[2])) return FALSE; // check sur mois, jour, annee
	$tab = explode(':', $tabDH[1]);
	if (count($tab) != 2) return FALSE;
	if (!is_numeric($tab[0]) && !is_numeric($tab[1])) return FALSE;
	if ($tab[1] > 59) return FALSE; 
	return TRUE;
}

function invDateHeure($date)
{
	$tabDH = explode(' ', $date);
	if (count($tabDH) != 2) return FALSE;
	$tabD = explode('-', $tabDH[0]);
	if (count($tabD) != 3) $tabD = explode('/', $tabDH[0]);
	$res = $tabD[2] . "-" . $tabD[1] . "-" . $tabD[0] . " " . $tabDH[1];
	return $res;
}

function affTemps($temps)
{
	$temp = $temps*86400;
	$ecart = (integer)$temp;
	$jj = $ecart/86400;
	$reste = $ecart%86400;
	$hh = $reste/3600;
	$reste = $reste%3600;
	$mm = $reste/60;
//	return $jj . "j" . $hh . "h" . $mm . "m";
	return $temps;
}

function getTemps($temps)
{
	$hh = floor($temps/3600);
	$mn = floor(($temps%3600)/60);
	if ($hh < 10) $hh = "0" . $hh;
	if ($mn < 10) $mn = "0" . $mn;
	$hhmn = $hh . "h" . $mn;
	return $hhmn;	
}

function getFullTemps($temps)
{
	$jj = floor($temps/(3600*24));
	$reste = floor($temps%(3600*24));
	$hh = floor($reste/3600);
	$mn = floor(($reste%3600)/60);
	if ($hh < 10) $hh = "0" . $hh;
	if ($mn < 10) $mn = "0" . $mn;
	if ($jj == 0) $hhmn = $hh . "h" . $mn;
	else $hhmn = $jj . "j" . $hh . "h" . $mn;
	return $hhmn;	
}

function getVariable($variable)
{
	if (isset($_SESSION[$variable])) return $_SESSION[$variable];
	else return '';
}

function setVariable($variable,$valeur)
{
	$_SESSION[$variable] = $valeur;
	return $_SESSION[$variable];
}

function delVariable($variable)
{
	if (isset($_SESSION[$variable])) unset($_SESSION[$variable]);
	return true;
}

function testVariable($variable)
{
	if (isset($_SESSION[$variable])) return true;
	else return false;
}

function Trace($data)
{
	$datetrace = date("Y-m-d h:i:s");
	$fictrace = "log/trace.txt";
	$f1 = fopen($fictrace, "a");
	fwrite($f1, $datetrace . ' ' . $data . "\n");
	fclose($f1);
}

function TraceAcces($conn, $idnom, $table, $cle)
{
	if ($table == 'commande') $idtable = 1;
	else if ($table == 'marche') $idtable = 2;
	else if ($table == 'budget') $idtable = 3;
	else if ($table == 'projet') $idtable = 4;
	else return false;
	$curdate = date("Y-m-d");
	$requete = "insert into trace_acces (TRA_IDNOM, TRA_IDTABLE, TRA_IDCLE, TRA_TABLE , TRA_DATEACC)
				values (\"" . $idnom . "\",\"" . $idtable . "\",\"" . $cle . "\",\"" . $table . "\",\"" . $curdate . "\")
				ON DUPLICATE KEY UPDATE TRA_DATEACC=\"" . $curdate . "\"";
	$statement = $conn->query($requete);
	return;
}
function errTrap()
{
}
