<?php
/* 
	class de gestion de la connexion à la base de données
*/
class database 
{
/*
	correspondance mysqli /pdo
	
	mysqli_connect		$conn = new PDO("mysql:host=localhost;port=xxxx;dbname=test", "sqluser", "sqlpassword");
	mysqli_close		$conn = null;
	mysqli_error		$error = $conn->errorInfo();
	mysqli_errno
	mysqli_query 		$statement = $conn->query($requete); ou $nombre = $conn->exec($requete); (INSERT, UPDATE ou DELETE)
	mysqli_fetch_assoc	$resultat = $statement->fetch(PDO::FETCH_ASSOC); PDO::FETCH_ASSOC est par défaut
	mysqli_fetch_array	$resultat = $statement->fetch(PDO::FETCH_BOTH);
	mysqli_fetch_row	$resultat = $statement->fetch(PDO::FETCH_NUM);
	mysqli_fetch_fields	for( $i=0 ; $i < $statement->columnCount() ; $i++)
						{
							$meta[$i]=$statement->getColumnMeta($i);
						}
	mysql_free_result	$statement->closeCursor();
	mysqli_num_rows		$num_rows = $statement->rowCount();
	mysqli_insert_id	$conn->lastInsertId();
	mysqli_data_seek	pas d'équivalence
	mysqli_set_charset  $conn = new PDO("mysql:host=localhost;port=xxxx;dbname=test;charset=utf8;", "sqluser", "sqlpassword");
	
	$conn->beginTransaction(); 
	$conn->commit();
	$conn->rollBack();
	$conn->setAttribute(PDO::ATTR_AUTOCOMMIT,0); // juste en mysql -> donc pas d'équivalence
*/
	private $errconn = '';		/* numéro du formulaire incrémenté à chaque nouveau formulaire */
// objet sur la page en cours
	private static $objConn = null;
	private static $conn = null;

// récupération de la connexion bdd
	public static function getIntance()
	{
		if (self::$objConn == 'erreur') return false;
		if (self::$objConn == null) self::$objConn = new database();
		return self::$conn;		
	}
	
	private function errConnexion(){}
// création de la connexion à la base de données	
	public function __construct()
	{
		// évite l'affichage des erreurs à l'écran
		set_error_handler(array($this, 'errConnexion'), E_WARNING);

/*
// connexion mysql directe
$conn = mysqli_connect($base, $user, $pass, '', $port);
if (!$conn) 
{
	$numerreur = mysqli_connect_errno();
	$errconn .= "<h4>Impossible de se connecter au moteur de base de données " . $numerreur . "</h4>";
	if ($numerreur == 2002) $errconn .= "<h4>Serveur et/ou port invalide</h4>";
	else if ($numerreur == 1045) $errconn .= "<h4>Utilisateur et/ou mot de passe invalide</h4>";
	$errconn .= "<h4>Veuillez contacter votre administrateur</h4>";
}
else if (!mysqli_select_db($conn, $dbname))
{
	$errconn .= "<h4>La base de données utilisateur n'est pas accessible. Veuillez contacter votre administrateur</h4>";
	$conn = null;
	unset($conn);
}
*/
		include('connect_base.php');
		// connexion mysql via pdo
		try 
		{
			self::$conn = new PDO('mysql:host='.$base.';port='.$port.';dbname='.$dbname.';charset=utf8;', $user, $pass); 
		} 
		catch (PDOException $e) 
		{
			$this->errconn .= "<h4>Impossible de se connecter à la base de données " . utf8_encode($e->getMessage())  . "</h4>";
		}
		restore_error_handler(); 

		if ($this->errconn != '')
		{
			self::$objConn = 'erreur'; // on indique que la connexion à la bdd n'est pas possible
			$objPage = page::getPage();
			$objPage->debPage('erreur');
			$objPage->tampon .= $this->errconn;
			$objPage->finPage();
			exit();
		}
		else
		{
			$statement = self::$conn->query("SET SESSION sql_mode = ''"); 
		}
	}	
}
