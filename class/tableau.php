<?php
/* 
	version 1.01 le 24/06/2014

	classe de gestion des listes en format tableau
	
	trois mode d'affichage
	1- global avec passage de la requête
	2- idem 1 mais avec une fonction de rappel permettant de modifier les champs
	3- ligne par ligne (la requête est faite dans le script qui appelle)
	
	Définition de la configuration globale du tableau
        affform       (bool) 	: tableau qui comprend des champs de formulaire
	colonne       (bool)   	: l'option d'affichage des colonnes est-il affiché ?
        filtre        (bool)   	: le tableau est-il filtrable ? Faux par défaut.
        largeur       (int)    	: largeur du tableau, en unité de douzième de page. Doit être un nombre entre 1 et 12. 12 par défaut.
        largeur-decal (int)    	: décalage du tableau sur la droite, en unité de douzième de page. Doit être un nombre entre 0 et 12. 0 par défaut.
        nombre        (int)    	: nombre maximal de lignes à afficher. Obligatoire si la pagination est activée, interdit sinon.
        pagination    (bool)   	: le tableau est-il paginé ? Faux par défaut.
        defil         (bool)   	: La pagination est-elle en mode défilement ? Faux par défaut.
        titre         (string) 	: le titre du tableau. null par défaut.
        titresuite    (bool) 	: Affiche de données spécifiques (nom de l'élément) à la suite du titre.
        tri           (bool)   	: les colonnes du tableau peuvent-elles être triables ? Faux par défaut.
        deforder      (string)  : Close order par défaut.
        cacher-mobile (bool)   : Tous les champs sont visibles par defaut, il faut cacher les champs qu'on ne veut pas voir sur mobile 

	Types de colonne possibles
		Case à cocher                           : champ case à cocher
		Texte					: champ texte
		Date					: champ datepicker
		Lien					: champ lien vers une url
		Montant					: champ Montant
		Nombre					: champ nombre
		Pourcentage				: champ pourcentage avec %

	Définition des options pour chaque colonne
	aff     	  (bool)   	: la colonne est-elle visible ? Vrai par défaut.
        bdd      	  (string) 	: libellé de la colonne équivalente en base de données, ou bien l'indice de la colonne dans le cas d'une ligne ajoutée manuellement.
        label    	  (string) 	: libellé de la colonne. Obligatoire.
        param    	  (string) 	: la liste, en texte plat, des paramètres à associer à l'url du lien. null par défaut. N'est valable que pour le type 'Lien'.
        largeur_col	  (int)    	: la largeur de la colonne, en pixels. Largeur automatique par défaut.
        taille   	  (int)    	: Taille de la zone de saisie des champs Texte (pas implémenté).
        total    	  (bool)   	: doit-on afficher une ligne calculant le total des valeurs de cette colonne ? Faux par défaut.
        tri      	  (bool)   	: la colonne est-elle triable ? Faux par défaut.
        type     	  (string) 	: type de la donnée. Doit être l'une des constantes de type de données. 'Texte' par défaut.
	typelien 	  (string) 	: le type de lien. N'est valable que pour le type 'Lien'. HREF par défaut.
	decimale          (int)   	: Nombre de décimales après la virgule pour les types Montant, Nombre et Poucentage.
        url      	  (string) 	: l'url du lien. N'est valable que pour le type 'Lien'.
        saisie            (bool)   	: la colonne est-elle saisissable ? Faux par défaut.
*/
class tableau 
{
	public  $numtab;			/* numéro du tableau */
	private $affform;			/* affichage du tableau dans un formulaire existant */
	private $nomform;			/* nom du formulaire */
	private $idform;			/* id (html) du formulaire */
	private $titre;				/* titre du tableau */
	private $titresuite;		/* nom de la variable pour affichage à la suite du titre du tableau */
	private $titre2;			/* valeur de la suite du titre du tableau */
	public  $erreur='';			/* id du champ en erreur */
	public  $libelle='';		/* message d'erreur ou de validation */
	private $pagination;		/* indicateur de pagination */
	private $defil;				/* indicateur de pagination avec défilement*/
	private $nombre;	  		/* nombre de lignes par page. Peut être changé dynamiquement */
	private $limite;	  		/* pointeur de la page courante */
	private $navigation;		/* barre de navigation  */
	private $navigationH;		/* barre de navigation uniquement sur la partie haute */
	private $tri;				/* indicateur de tri */
	private $deforder;			/* clause order par defaut ou à ajouter à la colonne à trier */
	public  $t_tri;				/* numero de la colonne pour le tri */
	private $t_order;			/* clause order by */
	private $t_ordre;			/* sens du tri (asc desc) */
	private $colonne;			/* indicateur de gestion d'affichage dynamique des colonnes */
	private $largeur_decal; 	/* décalage du tableau à droite */
	private $largeur;			/* largeur du tableau */
	private $scriptname;		/* nom du script php courant */
	private $droit;				/* gestion des droits sur nom et groupe */
	private $visible;			/* gestion de la visibilité sur les groupes */
	private $table_attr;		/* attribut du tableau */
	private $table_class;		/* class du tableau */
	private $table_style;		/* style du tableau */
	private $entete_attr;		/* attribut de l'entête */
	private $entete_class;		/* class de l'entête */
	private $entete_style;		/* style de l'entête */
	private $ligne_attr;		/* attribut d'une ligne */
	private $ligne_class;		/* class d'une ligne */
	private $ligne_style;		/* style d'une ligne */
	private $sortable;			/* id si tableau sortable */
	private $bouton;			/* boutons d'enchainement */
	private $afftableau;		/* indicateur pour affichage du tableau en une seule fois */
	private $preptableau;		/* indicateur pour préparation du tableau */
	private $ligne;				/* template de ligne pour affichage du tableau */
	private $numligne;			/* numéro de la ligne pour les ID */
	private $typechamp;			/* utilisation des valeurs bdd ou variable */
	private $tabchamps;			/* tableau des noms des champs variabilisés */
	private $variable;			/* tableau des variables à remplacer dans le preg_replace */
	private $total;				/* indicateur de gestion des totaux */
	private $tabcoltot;			/* tableau des colonnes avec un total à gérer */
	private $tabnombdd;			/* relation entre indice et nom pour la gestion des totaux */
	private $valeurtot;			/* tableau des valeurs des totaux */
	private $suppzero;			/* indicateur d'affichage ou non des 0 */
	private $idligne;			/* indicateur de gestion des ID pour certains champs de la ligne  */
	private $errorbdd;			/* Erreur bdd retournée par PDO */ 
	private $decimale;			/* nombre de décimales */ 
	
	public  $filtre;			/* indicateur de gestion des filtres */
	public  $objFiltre;			/* pointeur sur l'objet filtre */
	public  $champT;			/* liste des champs du tableau */
	
	private $tampon;			/* buffer tampon d'affichage du tableau */

/******************************************************************************/
// initialisation du tableau lors de la construction de la classe
// tableau numéro 1 par défaut
/******************************************************************************/
	public function __construct($numtab='1')
	{
		global $codefonc;
		$objPage = page::getPage();

		// Le tampon d'affichage pointe vers le tampon de l'objet page 
		$this->tampon = &$objPage->tampon;
		
		// Pointeur vers la description du tableau
		$this->numtab = $numtab;
		$vartab="descT".$numtab;
		global $$vartab;
		$typetableau=&$$vartab;
		
		// gestion de numéro de formulaire si champs formulaire dans le tableau
		if (isset($typetableau['affform'])) $this->affform = $typetableau['affform'];
		else $this->affform = false;
		if ($this->affform !== true || $this->affform !== false) $this->affform = false;
		if (!$this->affform)
		{
			$this->nomform = "nomform" . $_SESSION['curform']; // name de la balise <FORM> par défaut
			$this->idform = "idform" . $_SESSION['curform']; // id de la balise <FORM> par défaut
			$_SESSION['curform']++; // numéro du prochain formulaire
		}	
		
//		$cpttab = count($typetableau); // a supprimer
		// Récupération du titre du tableau
		$this->titre = $typetableau['titre'];
		if (isset($typetableau['titresuite'])) $this->titresuite = $typetableau['titresuite'];
		else $this->titresuite = false;
		
		// gestion de la position et de la largeur du tableau
		$this->largeur = $typetableau['largeur'];
		if (isset($typetableau['largeur-decal'])) $this->largeur_decal = $typetableau['largeur-decal'];
		else $this->largeur_decal = 0;
		
		// gestion des décimales
		if (isset($typetableau['decimale'])) $this->decimale = $typetableau['decimale'];
		else $this->decimale = 2;
		if ($this->decimale < 0) $this->decimale = 0;
		if ($this->decimale > 4) $this->decimale = 4;
		
		// si la fonction est différente on reset les variables de session
		$this->scriptname = $_SERVER['SCRIPT_NAME'];
		if (testVariable('scriptname')) $oldscriptname = getVariable('scriptname');
		else $oldscriptname = '';
		if (testVariable('codefonc')) $oldcodefonc = getVariable('codefonc');
		else $oldcodefonc = '';
//		if (($objPage->isOnglet() && $oldscriptname != $this->scriptname) || (!$objPage->isOnglet() && $oldcodefonc != $codefonc))
		if ($oldscriptname != $this->scriptname)
		{
			delVariable('limite'); // curseur courant dans clause LIMIT
			delVariable('nombre'); // nombre à afficher dans clause LIMIT
			delVariable('t_tri'); // numéro de la colonne à trier
			delVariable('t_ordre'); // ordre du tri (desc ou asc)
		}
		setVariable('scriptname',$this->scriptname);
		setVariable('codefonc',$codefonc);
		
		// Récupération du mode de pagination 
		if (isset($typetableau['pagination'])) $this->pagination = $typetableau['pagination'];
		else $this->pagination = false;
		if ($this->pagination !== false && $this->pagination !== true) $this->pagination = false;
		
		// Gestion du scroll si pagination avec mode 'defil' 
		if (isset($typetableau['defil'])) $this->defil = $typetableau['defil'];
		else $this->defil = false;
		if ($this->defil !== false && $this->defil !== true) $this->defil = false;
		if ($this->pagination === false) $this->defil = false;
		
		// Gestion du tri
		if (isset($typetableau['tri'])) $this->tri = $typetableau['tri'];
		else $this->tri = false;
		
		// Gestion des filtres
		if (isset($typetableau['filtre'])) $this->filtre = $typetableau['filtre'];
		else $this->filtre = false;
		
		// Gestion de l'affichage dynamique des colonnes
		if (isset($typetableau['colonne'])) $this->colonne = $typetableau['colonne'];
		else $this->colonne = true;
		
		// si affichage dans un formulaire existant --> pas de pagination, tri, filtre et colonne
		if ($this->affform) 
		{
			$this->pagination = false;
			$this->defil = false;
			$this->tri = false;
			$this->filtre = false;
			$this->colonne = false;
		}
		
		/******************************************************************/
		// Gestion de la clause SQL LIMIT pour afficher en mode pagination
		// LIMIT limite,nombre
		// limite = Numéro de ligne début. On démarre à 0 et on ajoute nombre à chaque itération.
		//          initlimite, nextlimite et prevlimite sont des champs cachés avec les valeurs
		//          pour permettre de faire suivant et précédant.
		// nombre = Nombre de lignes à afficher ou à ajouter si mode defil (20 par défaut).
		//
		/******************************************************************/
		if (isset($typetableau['nombre'])) $this->nombre = $typetableau['nombre']; 
		else $this->nombre = 20;
		$this->limite = 0; 
		if ($this->pagination === true)
		{
			if(isset($_REQUEST['typeaction'])) $typeaction=$_REQUEST['typeaction'];
			else  $typeaction="";
			if (testVariable('nombre')) $this->nombre = getVariable('nombre');
			// nombre à afficher modifié en dynamique par l'utilisateur (5,10,...,100,tout)
			if ($typeaction == 'initnombre') 
			{
				if (isset($_REQUEST['valnombre'])) $this->nombre=$_REQUEST['valnombre'];
				setVariable('nombre', $this->nombre);
			}
			// en mode défilement les champs formulaire de navigation ne sont pas affichés
			if ($this->defil === true)
			{
				if (isset($_REQUEST['initlimite'])) $this->limite=0;
				else if (isset($_REQUEST['nextlimite'])) $this->limite=$_REQUEST['nextlimite'];
//				else $this->limite = getVariable('limite'); 				
				if ($this->limite == '') $this->limite=0;
//				setVariable('limite',$this->limite+$this->nombre);

			}
			// sinon affectation en fonction de l'interaction utilisateur < (début) << (précédant) ou >> (suivant)
			else
			{
				if ($typeaction == 'initpage') $this->limite=$_REQUEST['initlimite'];
				else if ($typeaction == 'nextpage') $this->limite=$_REQUEST['nextlimite'];
				else if ($typeaction == 'prevpage') $this->limite=$_REQUEST['prevlimite'];
				else 
				{
					$this->limite = getVariable('limite'); 
					if ($this->limite == '') $this->limite=0;
				}
				setVariable('limite',$this->limite);
			}
		}
		
		/******************************************************************/
		// Gestion de la clause SQL ORDER BY pour le tri des données
		/******************************************************************/
		$this->deforder = $typetableau['deforder'];
		if ($this->tri === true)
		{
			if(isset($_REQUEST['t_tri'])) $this->t_tri=$_REQUEST['t_tri'];
			else $this->t_tri = getVariable('t_tri');
			if(isset($_REQUEST['t_ordre'])) $this->t_ordre=$_REQUEST['t_ordre'];
			else $this->t_ordre = getVariable('t_ordre');
			setVariable('t_tri',$this->t_tri);
			setVariable('t_ordre',$this->t_ordre);
			
			if ($this->t_tri == '') // pas de tri d'une colonne, on prend la clause par défaut ou 1
			{ 
				if ($this->deforder == '') $this->t_order = " order by 1 desc";
				else $this->t_order = " order by  1 desc" . $this->deforder;
				if (strpos($this->t_order, "desc")) $this->t_ordre = "desc";
				else $this->t_ordre = "asc";	   
			}
			else // un tri sur une colonne est demandé
			{
				$this->t_order = " order by " . $this->t_tri;
				if ($this->t_ordre == ''){$this->t_order .= " desc";} // si ordre pas initialisé -> desc
				else
				{
					$this->t_order .= " " . $this->t_ordre; // ajout du sens du tri
					if ($this->t_tri != 1) $this->t_order .= $this->deforder ; // ajout du tri multicritère sauf pour colonne 1
					if ($this->t_ordre == "asc") $this->t_ordre = "desc"; // inversion du sens du tri pour prochaine demande
					else $this->t_ordre = "asc";
			   	}
			}
		}
		
		// Relation avec l'objet filtre 
		if ($this->filtre === true)
		{
			// on crée l'objet filtre si déclaré en passant l'objet tableau courant
			$varfiltre="champFL".$this->numtab; 
			global $$varfiltre;
			if (is_array($$varfiltre)) $this->objFiltre = new filtre($this);
			else $this->filtre = false;
		}		
		
		// initialisation des variables
		$this->table_attr = '';
		$this->table_class = '';
		$this->table_style = '';
		$this->entete_attr = '';
		$this->entete_class = '';
		$this->entete_style = '';
		$this->ligne_attr = '';
		$this->ligne_class = '';
		$this->ligne_style = '';
		$this->bouton = '';
		$this->afftableau = false;
		$this->preptableau = false;
		$this->suppzero = true;
		$this->idligne = false;
		$this->numligne = 0;
		$this->typechamp = '';
		
		// Pointeur vers la description champs (colonnes) du tableau
		$vartab="champT".$this->numtab;
		global $$vartab;
		$this->champT = &$$vartab; 
	}
	
/******************************************************************************/
// suppression d'une ligne
/******************************************************************************/
	public function supRequete($ordre)
	{
		$conn = database::getIntance();
		$statement = $conn->exec($ordre);
		// erreur sur la suppression
		if ($statement === false) 
		{
			$this->errorbdd = $conn->errorInfo();
			if ($this->errorbdd[1] == 1451) $this->libelle = "Suppression impossible, il exite des enfants";
			else $objPage->libelle = $this->errorbdd[1] . " " . $this->errorbdd[2];
			return false;
		}
		else return true;
	}

	public function getErrBdd()
	{
		if ($this->errorbdd[1] == 1451) return "Suppression impossible, il exite des enfants";
		else return $this->errorbdd[1] . " " . $this->errorbdd[2];
	}

/******************************************************************************/
// ajout des clauses de filtre (LIKE), order et limit sur la requete 
/******************************************************************************/
	public function majRequete($ordre)
	{
		$objProfil = profil::getProfil();
		$requete = '';
		// ajout des filtres LIKE
		if (isset($this->objFiltre->champF) && is_array($this->objFiltre->champF))
		{	
			foreach ($this->objFiltre->champF as $curcle => $curchamp)
			{
				global ${$curcle};
				if (isset($curchamp['requete']) && $curchamp['requete'] === false) continue;
				if (isset($curchamp['compare'])) $compare = $curchamp['compare'];
				else $compare = "like";
				if ($compare != "egal" && $compare != "like") $compare = "like";
				if ($$curcle != '' && isset($curchamp['bdd'])) 
				{
					$requete .= " and " . $curchamp['bdd'];
					if ($compare == "like") $requete .= " LIKE \"%" . $$curcle . "%\" ";
					else $requete .= "=\"" . $$curcle . "\" ";
				}	
			}
		}	
		// ajout de la clause ORDER 
		//	$requete .= " order by CAST(REPLACE(FAQ_ORDRE,':','') as signed integer) ";
		if ($this->t_tri != '') $requete .= $this->t_order; // demande de tri sur une colonne  
		else if ($ordre != '') $requete .= ' ' . $ordre; // sinon on prend la clause order passée en paramètre
		else $requete .= $this->t_order;  // sinon on prend la clause order de l'objet tableau
		// ajout de la clause LIMIT 
		if ($this->pagination && $this->nombre > 0) 
			$requete .= " LIMIT " . $this->limite . "," . $this->nombre; 
		return $requete;
	}

/******************************************************************************/
// Paramètre de lien pour une colonne du tableau (appelée uniquement si affichage global)
/******************************************************************************/
	public function setLien($nomchamp, $lien, $param, $typelien='')
	{
		$this->champT[$nomchamp]['url'] = $lien;	
		$this->champT[$nomchamp]['param'] = $param;	
		if ($typelien == '') $this->champT[$nomchamp]['typelien'] = 'VersURL';
		else $this->champT[$nomchamp]['typelien'] = $typelien;
	}

/******************************************************************************/
// Gestion des attributs (appelé depuis les pages php)
/******************************************************************************/
	// Attribut du tableau
	public function setTableAttr($attr)
	{
		$this->table_attr .= ' ' . $attr . ' ';
	}
	// Class du tableau
	public function setTableClass($class)
	{
		$this->table_class .= ' ' . $class . ' ';
	}
	// Style du tableau
	public function setTableStyle($style)
	{
		$this->table_style .= $style;
	}
	// Attribut de l'entête du tableau
	public function setEnteteAttr($attr)
	{
		$this->entete_attr .= ' ' . $attr . ' ';
	}
	// Class de l'entête du tableau
	public function setEnteteClass($class)
	{
		$this->entete_class .= ' ' . $class . ' ';
	}
	// Style de l'entête du tableau
	public function setEnteteStyle($style)
	{
		$this->entete_style .= $style;
	}
	// Attribut d'une ligne de tableau
	public function setLigneAttr($attr)
	{
		$this->ligne_attr = ' ' . $attr . ' ';
	}
	// Class d'une ligne de tableau
	public function setLigneClass($class)
	{
		$this->ligne_class = ' ' . $class . ' ';
	}
	// Style d'une ligne de tableau
	public function setLigneStyle($style)
	{
		$this->ligne_style = $style;
	}

/******************************************************************************/
// initialisation de la pagination
/******************************************************************************/
	public function setLimite($nb)
	{
		if (is_numeric($nb))
		{
			$this->limite = $nb;
			return true;
		}
		else return false;
	}
	
/******************************************************************************/
// récupération de l'objet filtre s'il existe
/******************************************************************************/
	public function getFiltre()
	{
		if (is_object($this->objFiltre)) return $this->objFiltre;
		else return false;
	}
	
/******************************************************************************/
// affichage global du tableau
// 		indication d'un affichage de toutes les lignes du tableau
// 		préparation du tableau
//    	exécution de la requête bdd
//		affichage de toutes les lignes
//		finTableau
/******************************************************************************/
	public function affTableau($requete, $fonction='')
	{
		$conn = database::getIntance();
		
		// on indique un affichage global du tableau
		$this->afftableau = true;

		// on utilise la variable bdd du fichier de config
		$this->typechamp = 'bdd'; 	
		
		// exécution de la requête
		$statement = $conn->query($requete);
		
		// construction d'un tableau avec les noms des champs à partir de la requête 
		$nbcol = $statement->columnCount();
		for ($i=0; $i<$nbcol; $i++)
		{
			$this->tabchamps[] = (object) array('name'=>$statement->getColumnMeta($i)['name']);
		}
		
		// préparation du tableau avant affichage des lignes
		$this->prepTableau();

		//
		// boucle sur toutes les lignes de la select, remplacement des champs dans le masque de ligne et affichage
		//
		while ($valeur = $statement->fetch(PDO::FETCH_NUM))
		{
			// si modification des champs avant affichage 
			// -> appel de la fonction passée en paramètre de affTableau
			// le tableau valeur est indicé (0-n)
			//
			// affTableau($requete, 'testcallback')
			// function testcallback(&$valcallback) 
			// {
			//		$valcallback[0]="val1";
			//		$valcallback[1]="val2";
			//		$valcallback[n]="valn";
			//	}
			if ($fonction != '') call_user_func_array($fonction, array(&$valeur));
			// cumul des totaux pour les colonnes avec total
			if ($this->total) $this->cumulTotal($valeur);	

			// affichage de la ligne après remplacement des variables par les valeurs
			if ($this->ligne_style != '') $this->ligne_style = " style=\"" . $this->ligne_style . "\" ";
			if ($this->ligne_class != '') $this->ligne_class = " class=\"" . $this->ligne_class . "\" ";
			// ligne complète avant remplacement des variables
			$bufftemp = "<tr ".$this->ligne_attr.$this->ligne_style.$this->ligne_class.">".$this->ligne."</tr>\n";
			if ($this->idligne) 
			{
				// gestion ID si champ formulaire
				$bufftemp = preg_replace("*#NUMLIGNE#*", $this->numligne, $bufftemp);
				$this->numligne++;
			}
			$bufftemp = stripslashes(preg_replace($this->variable, $valeur, $bufftemp)); 
			// inversion des dates si besoin
//			$bufftemp = preg_replace('#(\d{4})\D(\d{2})\D(\d{2})#', '$3-$2-$1', $bufftemp);
			$bufftemp = preg_replace('#(\d{4})-(\d{2})-(\d{2})#', '$3-$2-$1', $bufftemp);
			$this->tampon .= $bufftemp; 
		}
		
		// affichage des totaux si besoin
		if ($this->total) $this->affTotal();	
		
		// fin du tableau
		$this->finTableau();
	}

/******************************************************************************/
// affichage ligne par ligne du tableau
/******************************************************************************/
	public function affLigne($valeur)
	{            
		// au premier appel -> préparation du tableau avant affichage des lignes
		if (!$this->preptableau) 
		{
			// création d'un tableau d'objets à l'identique de mysqli_fetch_fields
			// mais contenant les noms des variables du tableau du fichier de config
			foreach ($this->champT as $curcle => $curchamp)
			{
//				if ($curchamp['aff'] === false) continue;
				$this->tabchamps[] = (object) array('name'=>$curcle);
			}
			// préparation du tableau
			$this->prepTableau();
		}	

		// cumul des totaux pour les colonnes avec total
		if ($this->total) $this->cumulTotal($valeur);	
		
		// affichage de la ligne après remplacement des variables par les valeurs
		if ($this->ligne_style != '') $this->ligne_style = " style=\"" . $this->ligne_style . "\" ";
		if ($this->ligne_class != '') $this->ligne_class = " class=\"" . $this->ligne_class . "\" ";
		$bufftemp = "<tr ".$this->ligne_attr.$this->ligne_style.$this->ligne_class.">".$this->ligne."</tr>\n";
		if ($this->idligne) 
		{
			// gestion ID si champ formulaire
			$bufftemp = preg_replace("*#NUMLIGNE#*", $this->numligne, $bufftemp);
			$this->numligne++;
		}	
		$bufftemp = stripslashes(preg_replace($this->variable, $valeur, $bufftemp)); 
		// inversion des dates si besoin
		$bufftemp = preg_replace('#(\d{4})\D(\d{2})\D(\d{2})#', '$3-$2-$1', $bufftemp);
		$this->tampon .= $bufftemp; 
	}

/******************************************************************************/
// préparation du tableau avant affichage des lignes (global ou ligne par ligne)
// 		affichage du début du tableau 
//    	préparation du masque des champs à afficher sur une ligne
//    	préparation du masque des lignes du tableau
//		préparation des variables
//		préparation des totaux si besoin
//		affichage de l'entête du tableau
/******************************************************************************/
	private function prepTableau()
	{
		// affichage des filtres, de la pagination et de l'entête du tableau
		$this->debTableau();
		
		// préparation du masque de ligne à partir du fichier de config du tableau
		$ligne = $this->prepMasqueChamp();
		
		// préparation du masque des lignes du tableau (balise HTML)
		$this->prepMasqueLigneTableau($ligne); // stocké dans $this->ligne
		
		// le nom du champ bdd servira à faire le lien pour le remplacement dans le masque de la ligne
		// si affichage global sinon on prend les nom des variables des colonnes 
		$this->prepVariable();
		
		 // Gestion des totaux si au moins une colonne du tableau nécessite un total
		if ($this->total) $this->prepTotal();

		// affiche de l'entête du tableau après le formatage de la ligne (cadrage des colonnes en fonction du type)
		$this->affEntete();
		
		// indique que la préparation du tableau est faite pour la gestion ligne par ligne
		$this->preptableau = true;
	}
	
/******************************************************************************/
// Affichage de l'entête du tableau
/******************************************************************************/
	public function debTableau()
	{
		// si pagination en mode défilement le début du tableau n'est affiché que la première fois
		if ($this->defil && $this->limite > 0) return;
		// focus sur le champ en erreur si erreur
		if ($this->erreur != "") 
		{
			$this->tampon .= "<script type=\"text/javascript\">";
			$this->tampon .= "$(document).ready(function (){document.getElementById('" . $this->erreur . "').focus();})";
			$this->tampon .= "</script>";
		}

		// gestion de la largeur
		if ($this->largeur == "") {
                    $this->largeur = "12";
                }
		$class = "class='item ";
		if ($this->largeur_decal > 0) {
                    $class .= "col-md-offset-" . $this->largeur_decal . " ";
                }
		$class .= "col-md-" . $this->largeur . "'";
		$this->tampon .= "<div " . $class . ">\n";
                
		// affichage du titre
		if ($this->titresuite === true && isset($_SESSION['titresuite'])) {
                    $this->titre2 = " / " . $_SESSION['titresuite'];
                } else if ($this->titresuite !== false) {
                    // on récupère la variable
                    global ${$this->titresuite}; 
                    // et on la sauvegarde en session si présente
                    if (${$this->titresuite} != '') {
                            $_SESSION['titresuite'] = ${$this->titresuite};
                            $this->titre2 = " / " . ${$this->titresuite};
                    } else {
                        $this->titre2 = "";
                    }
		}
		else { 
                    $this->titre2 = "";
                }
		$this->tampon .= "<h1>" . $this->titre . $this->titre2 . "</h1>\n";
	}

/******************************************************************************/
// préparation du masque des champs à partir du fichier de config du tableau
// retourne un tableau comprenant le nom du champ base de données ou le nom de la variable
/******************************************************************************/
	private function prepMasqueChamp()
	{
		$ligne = array();
		$this->total = false;
		$i=0; // on garde $i pour la compatibilité avec ligne[n]
		foreach ($this->champT as $curcle => $curchamp)
		{
//			if ($curchamp['aff'] === false) continue;
			// alignement des montants
			if (isset($curchamp['type']) && ($curchamp['type'] == "Montant" || $curchamp['type'] == "Nombre" || $curchamp['type'] == "Pourcentage"))
			{
				$this->champT[$curcle]['entete_style'] = "text-align:right";
				$this->champT[$curcle]['ligne_style'] = "text-align:right";
			}		
			// gestion du total pour la colonne
			if (isset($curchamp['total']) && $curchamp['total'] === true) $this->total = true;
			// gestion du nom du champ 
//			echo "typechamp=" . $this->typechamp . " champ=" . $curcle . "<br>";
			if ($this->typechamp == 'bdd') // nom base de données
			{	
				if (isset($curchamp['bdd'])) $valchamp = "#" . $curchamp['bdd'] . "#";
				else $valchamp = '';
			}
			else $valchamp = "#" . $curcle . "#"; // nom du champ
			$ligne[] = $valchamp;
		}
		return $ligne; // Traité par prepMasqueLigneTableau
	}	
	
/******************************************************************************/
// préparation du masque des lignes du tableau (balise HTML)
// prend en entrée le tableau retourné par prepMasqueChamp()
//     le nom du champ sera inséré en remplacement de la valeur
//     il sera remplacé par la vraie valeur venant de la requête bdd 
//     via preg_replace au moment de l'affichage de la ligne
/******************************************************************************/
	private function prepMasqueLigneTableau($listligne)
	{	
		$objProfil = profil::getProfil();
		$ligne = "";
		$i=0; // on garde $i pour la compatibilité avec ligne[n]
		foreach ($this->champT as $curcle => $curchamp)
		{
//			if ($curchamp['aff'] === false) continue;
			// type de champ en saisie ou en affichage
			if (isset($curchamp['saisie'])) $saisie = $curchamp['saisie'];
			else $saisie = false;
			if ($saisie !== true && $saisie !== false) {
                            $saisie = false;
                        }
                        // gestion des class
                        $class = " class='";
                        if ($curchamp['cacher-mobile'] === true) {
                            $class .= "hidden-xs ";
                        }                        
			if ($class == " class='") {
                            $class = ''; // aucune classe en option
                        } else {
                            $class .= "'";
                        }                       
			// gestion du style css
			$param = " style='";
			if ($curchamp['aff'] === false) {
                            $param .= "display:none; ";
                        }
                        if (!$saisie) {
                            $param .= "vertical-align: middle;";
                        }
			if (isset($curchamp['ligne_style'])) {
                            $param .= $curchamp['ligne_style'] . ";";
                        }
			if (isset($curchamp['cesure']) && $curchamp['cesure'] === false) {
				$param .= "white-space:nowrap;";	
			}
			if ($param == " style='") {
                            $param = ''; // aucun style en option
                        } else {
                            $param .= "'";
                        }
			if (isset($curchamp['largeur_col']) && $curchamp['largeur_col'] !== false) {
                            $param .= " width=" . $curchamp['largeur_col'];
                        }
			if (isset($curchamp['ligne_attr'])) {
				$param .= " " . $curchamp['ligne_attr'] . " ";
				unset($curchamp['ligne_attr']);
			}
			// récupération du type du type de champ
			if (isset($curchamp['type'])) {
                            $typechamp = $curchamp['type'];
                        } else {  
                            $typechamp = '';
                        }
			// traitement des champs de saisie
			if ($saisie)
			{
				switch ($typechamp)
				{
					case 'Case à cocher':
						// si affichage global on ne peut pas indiquer 'checked' à ce niveau
						if ($this->afftableau) $opt = "mgf-checkbox='" . $listligne[$i] . "'";
						else if ($listligne[$i] == 'o') $opt = " checked "; 
						else $opt = "";
						$ligne .= "<td " . $param . $class . " >";
						$ligne .= "<INPUT class='mgf_checkable' TYPE='checkbox' NAME='" . $curcle . "[]' VALUE='o' mgf-nom='" . $curcle . "' " .  $opt . " />";
						$ligne .= "</td>";
						break; // fin
						
					case 'Montant':
					case 'Nombre':
					case 'Pourcentage':
					case 'Texte':
						$this->idligne = true;
						$ligne .= "<td " . $param . $class ." >";
						$ligne .= "<INPUT ID='" . $curcle . "#NUMLIGNE#' class='mgf_editable form-control input-sm' TYPE=text NAME='" . $curcle . "[]' MAXLENGTH=250 
									style='padding:1px; width:100%;' VALUE=\"" .  htmlspecialchars($listligne[$i]) . "\" mgf-nom='" . $curcle . "'";
						$ligne .=  ">"; 
						$ligne .= "</td>";
						break; // fin
						
					case 'Date':
						$this->idligne = true;
						$ligne .= "<td " . $param . $class ." >";
						$datepicker = 'datepicker';
						$opt = '';
						$ligne .= "<INPUT ID='" . $curcle . "#NUMLIGNE#' class='form-control input-sm " . $datepicker . "' TYPE=text NAME='" . $curcle . "[]' MAXLENGTH=20 VALUE='" . $listligne[$i] ."'" . $opt . ">";
						$ligne .= "</td>";
						break; // fin
				}
			}	
			// traitement des champs d'affichage
			else
			{
				$style='vertical-align: middle;';
				switch ($typechamp)
				{
					case 'Lien':
						$ligne .= "<td " . $param . $class ." >";
						if (isset($curchamp['url']))
						{
							if (isset($curchamp['param'])) $param = $curchamp['param'];
							else $param='';
							if ($curchamp['typelien'] == 'VersURL')
								$ligne .= "<a href='#' " . VersURL($curchamp['url'] . $param) . " >" . $listligne[$i] . "</a>";
							else if ($curchamp['typelien'] == 'href')
								$ligne .= "<a href='" . $curchamp['url'] . $param . "' >" . $listligne[$i] . "</a>";
							else if ($curchamp['typelien'] == 'Supprimer')
							{
								if ($objProfil->sup) $ligne .= "<a " . VersURL($curchamp['url'] . $param,"supp") . " ><i class='fa fa-trash'></i></a>";
								else $ligne .= "";
							}	
							else if ($curchamp['typelien'] == 'Liste')
								$ligne .= "<a " . VersURL($curchamp['url'] . $param) . " ><i class='fa fa-list-alt'></i></a>";
							else if ($curchamp['typelien'] == 'Téléchargement')
								$ligne .= "<a href=\"download.php?cle=#PCE_CLE#&fic=#PCE_NOMFIC#\"><i class='fa fa-download'></i></a>";
							else
								$ligne .= "<a href='" . $curchamp['url'] . $param . "' >" . $listligne[$i] . "</a>";
						}
						else $ligne .=  $listligne[$i]; 
						$ligne .= "</td>";
						break; // fin
						
					case 'Montant':
					case 'Nombre':
					case 'Pourcentage':
						$this->champT[$curcle]['entete_style'] = "text-align:right";
						$this->champT[$curcle]['ligne_style'] = "text-align:right";
						$ligne .= "<td " . $param . $class ." >" . $listligne[$i] . "</td>";
						break; // fin
						
					default:
						$ligne .= "<td " . $param . $class ." >" . $listligne[$i] . "</td>";
						if (isset($curchamp['td'])) $ligne .= $curchamp['td'];
						break; // fin
				}		
			}
			$i++;
		}
		$this->ligne = $ligne; // stockage du masque
	}

/******************************************************************************/
// création du tableau des champs pour le remplacement des variables par des valeurs
/******************************************************************************/
	private function prepVariable()
	{
		$this->variable = array();
		foreach ($this->tabchamps as $val) { $this->variable[] = '*#' . $val->name . '#*'; }
		// valeurs à ne pas afficher
		$this->variable[] = "*0000-00-00*";
		$this->variable[] = "*0000/00/00*";
		$this->variable[] = "*00-00-0000*";
		$this->variable[] = "*00/00/0000*";
		$this->variable[] = "*>0\.00<*"; 
		$this->variable[] = "*>0\,00<*"; 
		if ($this->suppzero == true) $this->variable[] = "*>0<*"; 
	}
	
/******************************************************************************/
// préparation du total pour les colonnes avec total
/******************************************************************************/
	private function prepTotal()
	{
		$this->tabcoltot = array();
		$this->tabnombdd = array();
		$this->valeurtot = array();
		
		$this->variable[] = "*>0<*"; 
		// table indicée sur le nom des champs de la requête indiquant son numéro d'ordre
		$i = 0;
		foreach ($this->tabchamps as $val) 
		{
			$this->tabnombdd[$val->name] = $i; 
			$this->valeurtot[$i] = 0.00;
			$i++;
		}
		// selection des colonnes avec total
		foreach ($this->champT as $curchamp)
		{
//			if ($curchamp['aff'] === false) continue;
			if (isset($curchamp['bdd']) && isset($this->tabnombdd[$curchamp['bdd']])) 
			{
				if (isset($curchamp['total']) && $curchamp['total'] === true) 
				{
					$this->tabcoltot[$curchamp['bdd']] = $this->tabnombdd[$curchamp['bdd']];
				}	
			}
		}
	}

/******************************************************************************/
// cumul des totaux pour les colonnes avec total
/******************************************************************************/
	private function cumulTotal(&$valeur)
	{
		foreach($this->tabcoltot as $curcol)
		{
			$this->valeurtot[$curcol] += $valeur[$curcol];
			$valeur[$curcol] = number_format($valeur[$curcol],$this->decimale,',',' '); // on formate après le cumul
		}	
	}
	
/******************************************************************************/
// affichage de l'entete du tableau avec si besoin les filtres et la pagination 
/******************************************************************************/
	public function affEntete()
	{
		// si pagination en mode défilement le début du tableau n'est affiché que la première fois
		if ($this->defil && $this->limite > 0) return;
		
		$objPage = page::getPage();
		// affiche les champs de filtrage des colonnes si l'option filtre = true 
		if ($this->filtre === true && $this->objFiltre->etat === false) // affichage du filtre si pas déjà affiché
		{
			$this->objFiltre->debFiltre();
			$this->objFiltre->affFiltres();
			$this->objFiltre->finFiltre();
		}
		
		// la balise <FORM> doit être derrière le formulaire de filtre
		// elle sera fermée dans la fonction fintableau
		$url = $_SERVER['SCRIPT_NAME'] . "?typeaction=reception";
		if ($objPage->param != '') $url .= "&" . $objPage->param;
		if (!$this->affform) $this->tampon .= "<FORM role='form' class='form-horizontal' id='" . $this->idform . "' name='" . $this->nomform . "' ACTION=\"" . $url . "\"  METHOD=post>";
		
		// affichage des bouton de déplacement de la pagination sauf si mode défilement
		if ($this->pagination === true && $this->defil === false) 
		{
			$this->addPagination();
			$this->navigationH .= $this->affFiltreCol();
			$this->affPagination('haut');
		}
		// sinon affichage uniquement du bouton pour filtrer les colonnes
		else if ($this->colonne === true)
		{
			$this->navigation = '';
			$this->navigationH = $this->affFiltreCol();
			$this->affPagination('haut');
		}	

		// création du tableau et affichage de la ligne d'entête
		if ($this->table_style != '') $this->table_style = " style=\"" . $this->table_style . "\" ";
		if ($this->defil === true) $idscript = " id='script' script=\"" . $this->scriptname . "\" valnombre=\"" . $this->nombre . "\" ";
		else $idscript = '';
		$this->tampon .= "<table" .  $idscript . $this->table_attr . $this->table_style . " width=100% cellspacing=0 class=\"table table-condensed table-striped table-hover" . $this->table_class . "\">\n";
		if ($this->entete_style != '') $this->entete_style = " style=\"" . $this->entete_style . "\" ";
		$colonne = "<colgroup>";
		$colgroup = false;
		$ligne = "<tr " . $this->entete_attr . $this->entete_style . " class='active thead tr-nowrap" . $this->entete_class . "'>\n";
		$curtrinum = 1;
                
		foreach ($this->champT as $curchamp)
		{
//			if ($curchamp['aff'] === false) continue;
			if ($this->tri !== false && isset($curchamp['tri']) && $curchamp['tri'] !== false)
			{
				// si trinum -> numéro (plus utilisé) sinon trinum -> nom champ BDD
				if (isset($curchamp['trinum']) && $curchamp['trinum'] > 0) $trinum = $curchamp['trinum'];
				else if (isset($curchamp['bdd'])) $trinum = $curchamp['bdd'];
				// par défaut trinum = numéro de la colonne
				else $trinum = $curtrinum;
				// y a t-il une demande de tri sur la colonne
				if ($this->t_tri == $trinum)
				{
					$trilien ="href='#' " . VersURL($this->scriptname . "?t_tri=" . $trinum . "&t_ordre=" . $this->t_ordre);
					if ($this->t_ordre == 'desc') $paramTri = "<a style='color: #fff;' " . $trilien . " ><span class='fa fa-sort-down'></span></a>";
					else $paramTri = "<a style='color: #fff;' " . $trilien . " ><span class='fa fa-sort-up'></span></a><br class='visible-xs' />";
				}
				else
				{
					$trilien ="href='#' " . VersURL($this->scriptname . "?t_tri=" . $trinum . "&t_ordre=desc");
					$paramTri = "<a style='color: #fff;' " . $trilien . " ><span class='fa fa-sort'></span></a><br class='visible-xs' />";
				}
			}	
			else {
                            $paramTri = "";
                        }
			if (isset($curchamp['largeur_col']) && $curchamp['largeur_col'] !== false)
				$paramTaille = " width=" . $curchamp['largeur_col'];
			else $paramTaille = "";
			if (isset($curchamp['entete_attr'])) $entete_attr = $curchamp['entete_attr']; else $entete_attr = '';
			$entete_style = 'vertical-align: middle; ';
			if ($curchamp['aff'] === false) $entete_style .= "display:none;";                        
			if (isset($curchamp['entete_style'])) $entete_style .= $curchamp['entete_style'];  
			if ($entete_style != '') $entete_style = " style=\"" . $entete_style . "\"";
			if (isset($curchamp['entete_class'])) $entete_class = $curchamp['entete_class']; else $entete_class = '';
                        if ($curchamp['cacher-mobile'] === true) $entete_class .= " hidden-xs"; 
                        
			$ligne .= "<th class='nowrap " . $entete_class . "' id='head".$curchamp['label']."' " . $entete_attr . $entete_style . " " . $paramTaille . ">" . $paramTri . " " . $curchamp['label'] . "</th>";
			
                        // gestion du colgroup
			if (isset($curchamp['col_attr']) && $curchamp['col_attr'] != '') $col_attr = $curchamp['col_attr']; else $col_attr = '';
			if (isset($curchamp['col_class']) && $curchamp['col_class'] != '') $col_class = " class=\"" . $curchamp['col_class'] . "\" "; else $col_class = '';
			if (isset($curchamp['col_style']) && $curchamp['col_style'] != '') $col_style = " style=\"" . $curchamp['col_style'] . "\" "; else $col_style = '';
			if ($col_attr != '' || $col_style != '' || $col_class != '') {$colonne .= "<col " .  $col_attr . $col_style . $col_class . ">"; $colgroup = true;}
			else $colonne .= "<col>";
			$curtrinum++;
		}
		$ligne .= "</tr>\n";
		if ($colgroup) $this->tampon .= $colonne . '</colgroup>';
		$this->tampon .= $ligne;  
		if ($this->sortable != '') $this->tampon .= "<tbody id='" . $this->sortable . "'>";
	}

/******************************************************************************/
// Création de la boite de dialogue modale servant à filtrer les colonnes du tableau
// appelé par fintableau si option colonne = true
/******************************************************************************/
	private function addFiltreCol() 
	{
		$i = 0;
		$this->tampon .= '
			<div class="modal fade" id="filtreModal" role="dialog">
				<div class="modal-dialog">
				  <!-- Modal content-->
				  <div class="modal-content">
					<div class="modal-header">
					  <button type="button" class="close" data-dismiss="modal">&times;</button>
					  <h4 class="modal-title">Filtrer les colonnes</h4>
					</div>
					<div class="modal-body">';
						foreach ($this->champT as $c)	{
							if($c['aff'] === false) $opt = '';
							else $opt = ' checked="check" ';
							$this->tampon .= '<div class="[ form-group ]">
								<input type="checkbox" name="'.$c['label'].'" id="affcol'.$i.'"'.$opt.' />
								<label for="'.$c['label'].'">'.$c['label'].'</label>
							</div></br>';
							$i++;
						}
					$this->tampon .= '</div>
					<div class="modal-footer">
					  <button type="button" class="btn btn-default" data-dismiss="modal" onclick="filtrerTableauCol()">Appliquer</button>
					</div>
				  </div>
				</div>
			</div>';
		$this->tampon .= "<script type=\"text/javascript\">";
		$this->tampon .= "$(document).ready(function (){initTableauCol('init');})";
		$this->tampon .= "</script>";
	}
	
	// affichage du bouton pour filtrer les colonnes
	private function affFiltreCol() 
	{
		if ($this->colonne === true) return '<span class="espace-btn btn btn-default btn-xs" data-toggle="modal" data-target="#filtreModal"><i class="fa fa-filter fa-2x"></i></span>';
		else return;
	}	

/******************************************************************************/
// Gestion de la pagination du tableau
/******************************************************************************/
	// ajout de la pagination. Appelé par affEntete
	private function addPagination()
	{
		if ($this->limite > 0) 
		{
			$previous = $this->limite - $this->nombre;
			$next = $this->limite + $this->nombre;
		}
		else
		{
			$previous = 0;
			$next = $this->nombre;
		}
//		$this->navigation = '<input class="btn-navig" type="image" src="images/bt-first-gray.gif" onClick="SubmitForm(\'initpage\')">';
//		$this->navigation = '<button type="submit" class="espace-btn btn btn-default btn-xs" onClick="SubmitForm(\'initpage\')"><span class="glyphicon glyphicon-fast-backward"></span></button>';
		$this->navigation = '<button type="submit" class="espace-btn btn btn-default btn-xs" onClick="SubmitForm(\'' . $this->nomform . '\',\'initpage\')"><i class="fa fa-step-backward"></i></button>';
		$this->navigation .= '<input type="hidden" value="0" name="initlimite">';
//		$this->navigation .= '<input class="btn-navig" type="image" src="images/bt-prev-gray.gif" onClick="SubmitForm(\'prevpage\')">';
		$this->navigation .= '<button type="submit" class="espace-btn btn btn-default btn-xs" onClick="SubmitForm(\'' . $this->nomform . '\',\'prevpage\')"><i class="fa fa-backward"></i></button>';
		$this->navigation .= '<input type="hidden" value="'.$previous.'" name="prevlimite">';
//		$this->navigation .= '<input class="btn-navig" type="image" src="images/bt-next-gray.gif" onClick="SubmitForm(\'nextpage\')">';
		$this->navigation .= '<button type="submit" class="espace-btn btn btn-default btn-xs" onClick="SubmitForm(\'' . $this->nomform . '\',\'nextpage\')"><i class="fa fa-forward"></i></button>';
		$this->navigation .= '<input type="hidden" value="'.$next.'" name="nextlimite">';
		$this->navigation .= "<label class='label-navig'><B>Courant " . $this->limite . " </B></label>\n";

		$this->navigationH = "<label class='label-navig'><B>Afficher #</B></label>\n";
		$this->navigationH .= "<select class='form-control input-sm' TYPE=text NAME='afficher' MAXLENGTH=5 style='width:75px;' onChange=\"SubmitForm('" . $this->nomform . "','initnombre&valnombre=' + this.value)\">";
		if ($this->nombre == 5) $this->navigationH .= '<option selected="selected" value="5">5</option>';
		else $this->navigationH .= '<option value="5">5</option>';
		if ($this->nombre == 10) $this->navigationH .= '<option selected="selected" value="10">10</option>';
		else $this->navigationH .= '<option value="10">10</option>';
		if ($this->nombre == 15) $this->navigationH .= '<option selected="selected" value="15">15</option>';
		else $this->navigationH .= '<option value="15">15</option>';
		if ($this->nombre == 20) $this->navigationH .= '<option selected="selected" value="20">20</option>';
		else $this->navigationH .= '<option value="20">20</option>';
		if ($this->nombre == 25) $this->navigationH .= '<option selected="selected" value="25">25</option>';
		else $this->navigationH .= '<option value="25">25</option>';
		if ($this->nombre == 30) $this->navigationH .= '<option selected="selected" value="30">30</option>';
		else $this->navigationH .= '<option value="30">30</option>';
		if ($this->nombre == 50) $this->navigationH .= '<option selected="selected" value="50">50</option>';
		else $this->navigationH .= '<option value="50">50</option>';
		if ($this->nombre == 100) $this->navigationH .= '<option selected="selected" value="100">100</option>';
		else $this->navigationH .= '<option value="100">100</option>';
		if ($this->nombre == 0) $this->navigationH .= '<option selected="selected" value="0">Tout</option>';
		else $this->navigationH .= '<option value="0">Tout</option>';
		$this->navigationH .= '</select>';	
	}

	// affichage de la navigation = pagination + boutons. Appelé par affEntete
	private function affPagination($hautbas)
	{
		$this->tampon .= "<div class='form-inline espace' style='margin-top:5px;margin-bottom:5px;'>";
		if ($hautbas == 'haut')	$this->tampon .= $this->navigation . $this->navigationH;
		else $this->tampon .= $this->navigation;
		$this->affBouton();
		$this->tampon .= "</div>";
	}
	
/******************************************************************************/
// Gestion de la fonctionnalité "sortable" du tableau
// appelé depuis les pages php qui contiennent des tableaux triables
/******************************************************************************/
	public function Sortable($id, $prefixe, $table)
	{
		$this->sortable = $id;
		$retour = "\n<script type=\"text/javascript\">\n";
		$retour .= "$(function(){";
		$retour .= "$('#" . $id . "').mouseover(function() {\$(this).css('cursor','move');});";
		$retour .= "$('#" . $id . "').sortable({";
		$retour .= "	axis: 'y',
			containment: $('#" . $id . "').parent(),
			scroll: true,
			update : function()	{
				$('tr').filter('tr:odd').addClass('altern');
				$('tr').filter('tr:even').removeClass('altern');
			},
			stop: function(event, ui) {
				var result = '';
				$('#" . $id . "').find('tr').each(function() { 
					if ($(this).is('[id]')) result += this.id + ';';									   
				});
	            $.get('maj_sortable.php', {'list': result,'prefixe':'" . $prefixe . "','table':'" . $table . "'} ,
	              function(data){});
			}
		}).disableSelection();});";
		$retour .= "\n</script>";
		$this->tampon .= $retour;
	}
	
/*****************************************************************************************************/
// traitement des boutons
// type de bouton 'button' 'reset' 'submit'
/*****************************************************************************************************/
	// Ajout d'un bouton d'enchainement
	// le champ lien est une URL sauf si submit ou c'est typeaction
	public function addBouton($type, $libelle, $lien='')
	{
		$objPage = page::getPage();
		$bouton = "<input type=\"" . $type . "\" value=\"" . $libelle . "\" class=\"espace-btn btn btn-default btn-sm\"";
		if ($type == 'submit') $bouton .= " onclick=\"SubmitForm('" . $this->nomform . "','" . $lien . "')\" />";
		else 
		{
			if (strpos($lien, "?") === false) $url = $lien . "?" . $objPage->param; // ajout des paramètres d'URL 
			else $url = $lien . "&" . $objPage->param;
			// cas particulier du retour ou il faut sortir du mode onglet
			if (strpos($lien, "?menu") === false) $bouton .= " onclick=\"VersURL('" . $url . "')\" />";
			else $bouton .= " onclick=\"document.location.href='" . $url . "'\" />";
		}
		$this->bouton .= $bouton;
	}
	
	// Ajout d'un bouton d'enchainement spécifique
	// le champ lien est une clause onclick 
	public function addBoutonSpe($type, $libelle, $lien='')
	{
		$bouton = "<input type=\"" . $type . "\" value=\"" . $libelle . "\" class=\"espace-btn btn btn-default btn-sm\" " . $lien . " />";
		$this->bouton .= $bouton;
	}

	// affichage des boutons
	private function affBouton()
	{
		$this->tampon .= "<div class='pull-right'>" . $this->bouton . "</div><div class='clearfix'></div>";
//		$this->tampon .= "<div><div style='float:right;height:25px;margin-bottom:3px;'>" . $this->bouton . "</div><div class='clr'></div></div>";
	}
	
/******************************************************************************/
// affichage du total en fin de tableau
/******************************************************************************/
	private function affTotal()
	{
		// formatage des totaux
		foreach($this->tabcoltot as $curcol) $this->valeurtot[$curcol] = number_format($this->valeurtot[$curcol],$this->decimale,',',' ');
		// affichage de la ligne
		$this->tampon .= stripslashes(preg_replace($this->variable, $this->valeurtot, $this->ligne)); 
	}
	
/******************************************************************************/
// fin du tableau
/******************************************************************************/
	public function finTableau()
	{
		// si pagination en mode défilement le début du tableau n'est affiché que la première fois
		if (!$this->preptableau)
		{
			// création d'un tableau d'objets à l'identique de mysqli_fetch_fields
			// mais contenant les noms des variables du tableau du fichier de config
			foreach ($this->champT as $curcle => $curchamp)
			{
//				if ($curchamp['aff'] === false) continue;
				$this->tabchamps[] = (object) array('name'=>$curcle);
			}
			// préparation du tableau
			$this->prepTableau();
		}	
		if ($this->defil && $this->limite > 0) return;
		else
		{
			// affichage des totaux si besoin et si affichage ligne par ligne
			if ($this->total && $this->afftableau != true) affTotal();	
			
			// fermeture du sortable si besoin
			if ($this->sortable != '') $this->tampon .= "</tbody>";
			
			// fermeture du tableau
			$this->tampon .= "</table>\n";
			
			// affichage pagination en bas de page si besoin
			if ($this->bouton != '' && $this->pagination === true && $this->defil === false) $this->affPagination('bas');
			else if ($this->bouton != '') $this->affBouton();
			
			// fermeture formulaire si tableau avec des champs de formulaire
			if (!$this->affform) $this->tampon .= "</FORM>\n";
			
			// ajout de la boite modale pour filtrer les colonnes à afficher
			if ($this->colonne === true) $this->addFiltreCol(); 		
			$this->tampon .= "</div>\n";
			if ($this->erreur != "") $couleurmess = 'red';
			else $couleurmess = 'blue';
			if ($this->libelle != "") 
			{
				$this->tampon .= "<script type=\"text/javascript\">";
				$this->tampon .= "$(document).ready(function (){openModalMessage('" .$couleurmess. "',\"" .$this->libelle. "\");})";
				$this->tampon .= "</script>";
			}
		}
	}
	// fin de la classe tableau

/******************************************************************************/
//  reception des paramètres formulaires du tableau
/******************************************************************************/
	public function recChamp()
	{
		foreach ($this->champT as $curcle => $curchamp)
		{
			// si type non renseigné on ne traite pas
			if (!isset($curchamp['type'])) continue;
			// si ce n'est pas un champ saisissable on ne traite pas
			if (!isset($curchamp['saisie'])) continue;
			if ($curchamp['saisie'] !== true) continue;
			// affection des valeurs du formulaire sous forme d'un tableau 
			// le tableau contient une entrée par ligne du tableau même si le champ n'est pas renseigné
			global ${$curcle};
			if(isset($_REQUEST[$curcle])) ${$curcle}=$_REQUEST[$curcle];
			else ${$curcle}="";
		}
		// contrôle des valeurs renseignées dans le formulaire en fonction du type
		return $this->ctlChamp($this->champT);
	}	

/******************************************************************************/
//  controle des paramètres formulaires du tableau (appelé par recChamp)
/******************************************************************************/
	private function ctlChamp(&$champ)
	{
		$this->erreur = '';
		$this->libelle = '';
		foreach ($champ as $curcle => $curchamp)
		{
			// si type non renseigné on ne traite pas
			if (!isset($curchamp['type'])) continue;
			// si ce n'est pas un champ saisissable on ne traite pas
			if (!isset($curchamp['saisie'])) continue;
			if ($curchamp['saisie'] !== true) continue;
			// controle du champ en fonction de son type
			global $$curcle;
			// les variables doivent être dans un tableau
			if (!is_array($$curcle)) continue; 
			$libchampT = $curchamp['label'];
			$typechampT = $curchamp['type'];
			// on ne traite pas les champs groupe de formulaire
			if($typechampT == 'Groupe') continue; 
			if (isset($curchamp['ctl'])) $flagctlT = $curchamp['ctl'];
			else $flagctlT = '';
			// boucle sur toutes les lignes pour contrôler 
			$nbcle = count($$curcle);
			for ($i=0; $i<$nbcle; $i++)
			{
				if ($flagctlT == "o" && $$curcle[$i] == '') {$this->erreur=$curcle.$i; $this->libelle="Le champ doit être renseigné (" . $libchampT . ")"; break;}
				if ($typechampT == 'Montant' || $typechampT == 'Nombre' || $typechampT == 'Pourcentage')
				{
					$$curcle[$i] = str_replace(" ", "", $$curcle[$i]);
					$$curcle[$i] = str_replace("€", "", $$curcle[$i]);
					$$curcle[$i] = str_replace("$", "", $$curcle[$i]);
					$$curcle[$i] = str_replace("%", "", $$curcle[$i]);
					$$curcle[$i] = str_replace(",", ".", $$curcle[$i]);
					$$curcle[$i] = preg_replace('/\s+/', '', $$curcle[$i]);
				}
				if ($typechampT == "Date")
				{
					if ($$curcle[$i] != '')
					{
						if (!ctlDate($$curcle[$i])) {$this->erreur=$curcle.$i; $this->libelle="Date incorrecte"; break;}
					}	
					$tempvar = 'MYSQL' . $curcle;
					if (!isset($$tempvar))
					{
						global $$tempvar;
						$$tempvar = array();
					}
					if ($$curcle[$i] != '') $$tempvar[$i] = invDate($$curcle[$i]);
				}
				if ($typechampT == "Date/heure")
				{
					if ($$curcle[$i] != '')
					{
						if (!ctlDateheure($$curcle[$i])) {$this->erreur=$curcle.$i; $this->libelle="Date incorrecte"; break;}
					}	
					$tempvar = 'MYSQL' . $curcle;
					if (!isset($$tempvar))
					{
						global $$tempvar;
						$$tempvar = array();
					}
					if ($$curcle[$i] != '') $$tempvar[$i] = invDateHeure($$curcle[$i]);
				}
			}	
		}
		if ($this->erreur != '') return false;
	}
	
/******************************************************************************/
//  fin de la classe
/******************************************************************************/
}
