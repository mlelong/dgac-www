<?php
/* 
	class de gestion de l'affichage des commentaires
	version 1.01 le 24/06/2014
*/
class commentaire {
	public $numtab;			/* numéro du tableau */
	public $titre;			/* titre de la liste des commentaires */
	public $bouton;			/* boutons d'enchainement  */

	public $tampon;		    /* buffer tampon d'affichage des commentaires */

/******************************************************************************/
// initialisation des commentaires lors de la construction de la classe
/******************************************************************************/
	public function __construct($numtab='1')
	{
		$objPage = page::getPage();
		// Le tampon d'affichage pointe vers le tampon de l'objet page 
//		$this->tampon = &$objPage->tampon;
		$this->tampon = '';
		
		// gestion de la description du tableau
		$this->numtab = $numtab;
		$vartab="descT".$numtab;
		global $$vartab;
		$typetableau=&$$vartab;
		$this->titre = $typetableau['titre'];
	}
	
/*****************************************************************************************************/
// gestion des commentaires
/*****************************************************************************************************/

// initialisation de la liste des commentaires
    function debCommentaire($titresuite='') 
	{
		$this->tampon .= "<div class='col-md-12' >";
 		if ($titresuite == '') $this->tampon .= "<p class='titretab'>" . $this->titre . "</p>\n";
		else $this->tampon .= "<p class='titretab'>" . $this->titre . " - " . $titresuite . "</p>\n";
   }

    function affCommentaire($nom, $date, $texte) {
        $this->tampon .= '<div class="commentaire">';
            $this->tampon .= '<div class="header-commentaire">';
                $this->tampon .= '<h4 style="float:left;">'.$nom.'<small> le '.$date.'</small></h4>';
            $this->tampon .= '</div>';
            $this->tampon .= '<div class="contenu-commentaire">';
                $this->tampon .= '<p>'.$texte.'</p>';
            $this->tampon .= '</div>';
        $this->tampon .= '</div>';
    }

	/*****************************************************************************************************/
// traitement des boutons
// type de bouton 'button' 'reset' 'submit'
/*****************************************************************************************************/
	// Ajout d'un bouton d'enchainement
	// le champ lien est une URL sauf si submit ou c'est typeaction
	function addBouton($type, $libelle, $lien='')
	{
		$objPage = page::getPage();
		$bouton = "<input type=\"" . $type . "\" value=\"" . $libelle . "\" class=\"espace-btn btn btn-default btn-sm\"";
		if ($type == 'submit') $bouton .= " onclick=\"SubmitForm('" . $this->nomform . "','" . $lien . "')\" />";
		else 
		{
			if (strpos($lien, "?") === false) $url = $lien . "?" . $objPage->param; // ajout des paramètres d'URL 
			else $url = $lien . "&" . $objPage->param;
			$bouton .= " onclick=\"VersURL('" . $url . "')\" />";
		}
		$this->bouton .= $bouton;
	}
	
	// Ajout d'un bouton d'enchainement spécifique
	// le champ lien est une clause onclick 
	function addBoutonSpe($type, $libelle, $lien='')
	{
		$bouton = "<input type=\"" . $type . "\" value=\"" . $libelle . "\" class=\"espace-btn btn btn-default btn-sm\" " . $lien . " />";
		$this->bouton .= $bouton;
	}

	// affichage des boutons
	function affBouton()
	{
		$this->tampon .= "<div class='pull-right'>" . $this->bouton . "</div></div>";
	}

/******************************************************************************/
// fin de la liste des commentaires
/******************************************************************************/
    function finCommentaire() 
	{
        $this->tampon .= '</div>';
		if ($this->bouton != '') $this->affBouton();
    }
// fin de la classe commentaire
}