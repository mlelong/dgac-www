<?php
/* 
	class de gestion du menu principal
	version 1.01 le 24/06/2014
*/
class menu 
{
	private $contenumenu=''; /* contenu du menu à afficher */
	private $curmenu;        /* nom du menu courant */
        private $items = [];
	
	// objet sur le menu en cours
	private static $objMenu = null;

	// récupération de l'objet sur le menu courant
	public static function getMenu()
	{
		if (self::$objMenu == null) {
                    self::$objMenu = new menu();
                    self::$objMenu->generateMenuItems();
                }
                
		return self::$objMenu;		
	}

	public function generateMenuItems() 
	{
		$objProfil = profil::getProfil();
                $this->items = [];
                
		if ((isset($objProfil->listfonc) && $objProfil->listfonc != '') || $objProfil->identifiant == 'admin')
		{	
			// table des droits sur les fonctions
			$tabfonc=explode(';', $objProfil->listfonc);
			$nbdroit=count($tabfonc);
			for ($i=0; $i<$nbdroit; $i++)
			{
				$tabdroit[$tabfonc[$i]]=ord($objProfil->droit[$i]);
			}
			$sui=0x01; $exe=0x02; $sup=0x04; $maj=0x08;	$cre=0x10; $lec=0x20; $vis=0x40; $adm=0x80;
			$cms=$cre | $maj | $sup;
			$lcms=$lec | $cre | $maj | $sup;

			switch ($codemenu) {
				case 'prg':
				case 'prj':
				case 'ama':
				case 'ami': // projet/activité
					$this->addMenu('*',   'Accueil',  		 'index.php',                 '', 'fa-home');
					$this->addMenu('prg', 'Programme',       'prj_list_programme.php',    '', 'fa-book');
					$this->addMenu('prj', 'Projet',       	 'prj_list_projet.php',    	  '', 'fa-pencil');
					$this->addMenu('ama', 'Activité majeure', 'prj_list_activite-maj.php', '', 'fa-cube');
					$this->addMenu('ami', 'Activité mineure', 'prj_list_activite-min.php', '', 'fa-cubes');
					break;
				case 'tym':
				case 'typ':
				case 'grm':
				case 'cfg': // configuration
					$this->addMenu('*',   'Accueil',               'index.php',                '', 'fa-home');
					$this->addMenu('tym', 'Type de marché',        'mar_list_typemarche.php',  '', 'fa-briefcase');
					$this->addMenu('typ', 'Type de procédure',     'mar_list_typeproc.php',    '', 'fa-legal');
					$this->addMenu('grm', 'Groupe de marchandise', 'cmd_list_marchandise.php', '', 'fa-shopping-bag');
					break;
				case 'adm': // administration
				case 'pro':
				case 'gru':
				case 'uti':
				case 'par':
				case 'pat':
				case 'not':
					$this->addMenu('*',   'Accueil',        'index.php',                   '', 'fa-home');
					$this->addMenu('adm', 'Fonctionnalité', 'adm_list_fonctionnalite.php', '', 'fa-cog');
					$this->addMenu('pro', 'Profil',         'adm_list_profil.php',         '', 'fa-barcode');
					$this->addMenu('not', 'Notification',   'adm_list_notification.php',   '', 'fa-legal');
					$this->addMenu('gru', 'Groupe',         'adm_list_groupe.php',         '', 'fa-group');
					$this->addMenu('uti', 'Utilisateur',    'adm_list_utilisateur.php',    '', 'fa-user');
					$this->addMenu('par', 'Paramètre',      'cfg_maj_parametre.php',       '', 'fa-wrench');
					$this->addMenu('pat', 'Patch',          'patch.php',                   '', 'fa-bolt');
					break;
				case '*':
				default:
					$this->addMenu('*',   'Accueil',         'index.php',                   '', 'fa-home');
					if ($tabdroit['dom'] != '0x00') $this->addMenu('dom', 'Domaine/pôle',    'dom_list_domaine.php',        '', 'fa-puzzle-piece');
					if ($tabdroit['prj'] != '0x00') $this->addMenu('prj', 'Projet/Activité', 'prj_list_projet.php',         '', 'fa-rocket');
					if ($tabdroit['mar'] != '0x00') $this->addMenu('mar', 'Marché/Devis',    'mar_list_marche.php',         '', 'fa-briefcase');
					if ($tabdroit['cmd'] != '0x00') $this->addMenu('cmd', 'Commande',        'cmd_list_commande.php',       '', 'fa-paper-plane');
					if ($tabdroit['bud'] != '0x00') $this->addMenu('bud', 'Budget',          'bud_list_budget.php',         '', 'fa-university');
					if ($tabdroit['rpm'] != '0x00') $this->addMenu('rpm', 'Suivi/Rapport',   'sui_tab_rapport.php',         '', 'fa-desktop');
					if ($tabdroit['fou'] != '0x00') $this->addMenu('fou', 'Fournisseur',     'fou_list_fournisseur.php',    '', 'fa-cube');
					if ($tabdroit['cfg'] != '0x00') $this->addMenu('tym', 'Configuration',   'mar_list_typemarche.php',     '', 'fa-sitemap');
					if ($tabdroit['adm'] & $lcms)   $this->addMenu('adm', 'Administration',  'adm_list_fonctionnalite.php', '', 'fa-cog');
					break;
			}
		} 
        }        
        
	// Affichage du menu
	public function affMenu() 
	{
		global $codefonc, $codemenu;
		// appelé depuis la fonction debpage de l'objet page
		if ($codemenu == '') $codemenu = $codefonc;
		$this->debMenu($codemenu);
                $this->ajouteItems();
		$this->finMenu();
		return $this->contenumenu;
	}
	
	// initialisation des menus
	private function debMenu($codemenu) 
	{
		global $cle;
		// récupération du menu à afficher
		if (isset($_REQUEST['menu'])) 
                    $this->curmenu = $_REQUEST['menu'];
		else 
                    $this->curmenu = $codemenu;
		
// conteneur du menu
		$this->contenumenu .= ' 
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-sm-3 col-md-2 sidebar">
                                                    <ul class="nav nav-sidebar">                        
                        ';
                
	}
	
	// ajout d'un menu
	private function addMenu($menu, $titre, $url, $param, $icone) {
		if (!$icone) throw new Exception("Icone manquant.");

                $item = 
                        [
                                    'active' => false,
                                    'menu'   => $menu,
                                    'titre'  => $titre,
                                    'url'    => $url,
                                    'param'  => $param,
                                    'icone'  => $icone,                        
                ];
                
                if ($menu == $this->curmenu || ($menu == '*' && $this->curmenu == '')) {
                    $item['active'] = true;
                }
                
                $this->items[] = $item;
            
                return $this;
	}
        
            public function getItemsHtmlContent() {
            
            $html = '';
            foreach($this->items as $item) {
                $active = '';
                if ($item['active']) {
                    $active = "class='active'";
                }
                $html .= "<li ".$active."><a href='".$item['url']."?menu=".$item['menu']."".$item['param']."&razfiltre'><i class='fa ".$item['icone']." '></i> ".$item['titre']."</a></li>";
            }
            return $html;    
        }
        
        private function ajouteItems() {
            $this->contenumenu .= $this->getItemsHtmlContent();  
        }        

// fin des menus
	private function finMenu()
	{
		$this->contenumenu .=  '
                                                </ul>
                                            </div>
                                        </div>
                                    </div>    
                                ';
	}
}

