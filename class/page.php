<?php
/* 
	class de gestion des pages
	version 1.01 le 24/06/2014
*/
class page 
{
	public  $onglet;		/* indicateur de page à afficher dans un onglet positionné dans prepage.php */
	public  $defil;			/* indicateur de page avec un tableau en mode défilement */
	private $curform;		/* numéro du formulaire incrémenté à chaque nouveau formulaire */
	private $tabparam;		/* tableau de paramètres d'URL */
	private $nomparent;		/* nom du parent */
	private $bouton;		/* boutons d'enchainement  */
	private $titreCartouche;        /* titre du cartouche (facultatif) */
	public  $param = '';            /* paramètres d'URL complémentaires fabriqués à partir de tabparam */
        private $boutonScroll;          /* contenu du scrollup */
	public  $erreur='';		/* id du champ en erreur ou indicateur d'erreur */
	public  $libelle='';		/* message d'erreur ou de validation */
	public  $tampon;		/* buffer tampon d'affichage de la page */

	// objet sur la page en cours
	private static $objPage = null;
	
	public function __construct()
	{
		// on vérifie si c'est une page qui doit s'afficher dans un onglet
		if(isset($_REQUEST['onglet'])) $this->onglet=$_REQUEST['onglet'];
		else $this->onglet = '';
		// on vérifie si c'est une page qui doit afficher un tableau en mode défilement
		if(isset($_REQUEST['defil'])) $this->defil=$_REQUEST['defil'];
		else $this->defil = '';
		// initialisation du numéro de formulaire sauf si onglet
		if ($this->onglet != 'o') $_SESSION['curform'] = 1;
		// initialisation du tableau de paramètres
		$this->tabparam = array(); 
		$this->tampon = '';
	}
        
        // récupération de l'objet sur la page courante
	public static function getPage()
	{
		if (self::$objPage == null) self::$objPage = new page();
		return self::$objPage;		
	}

        // gestion des paramètres d'URL
	public function initParam()
	{
		$this->tabparam = array(); 
	}
	public function addParam($param, $valeur)
	{
		$this->tabparam[$param] = $valeur;
		$this->creParamUrl();
	}
	public function delParam($param)
	{
		unset($this->tabparam[$param]);
		$this->creParamUrl();
	}
	private function creParamUrl()
	{
		$this->param = "";
		foreach ($this->tabparam as $param => $valeur)
		{
			if ($this->param != '') $this->param .= "&";
			$this->param .= $param ."=". $valeur;
		}		
	}
	
        // test si onglet ou non
	public function isOnglet()
	{
		if ($this->onglet == 'o') return true;
		return false;
	}
	
        // initialisation de la page
	public function debPage($typeaff)
	{
		// si c'est une page à afficher dans un onglet on ne traite pas
		// onglet=o est envoyé via getTabPage et setTabPage 
		// puis positionné dans $this->onglet par le constructeur de l'objet page
		if ($this->onglet == 'o') return;
		if ($this->defil == 'o') return;
		
                // PWA config
                include('config/pwa.php');
                
		// affichage du header
		$this->tampon .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
		$this->tampon .= '<html xmlns="http://www.w3.org/1999/xhtml">';
		$this->tampon .= '<head>';
		$this->tampon .= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
                // PWA : metas
                $this->tampon .= '<meta name="description" content="'.$pwa['title'].'">';
                $this->tampon .= '<meta name="theme-color" content="'.$pwa['color'].'">';
                $this->tampon .= '<meta name="viewport" content="width=device-width, user-scalable=no">';                
                $this->tampon .= '<meta name="apple-mobile-web-app-capable" content="yes">';
                $this->tampon .= '<meta name="apple-mobile-web-app-status-bar-style" content="'.$pwa['color'].'">';
                $this->tampon .= '<meta name="apple-mobile-web-app-title" content="'.$pwa['title'].'">';
                $this->tampon .= '<link rel="icon" type="image/png" href="/images/icons/icon-512x512.png" />';
                $this->tampon .= '<link rel="apple-touch-icon" href="/images/icons/icon-192x192.png">';
                $this->tampon .= '<link rel="manifest" href="/manifest.json">';
                // fin PWA
		$this->tampon .= '<link rel="stylesheet" type="text/css" media="all" href="css/jquery/jquery-ui-1.12.1.custom.min.css" />';
		$this->tampon .= '<link rel="stylesheet" type="text/css" media="all" href="css/bootstrap-3.3.6/bootstrap.css" />';
		$this->tampon .= '<link rel="stylesheet" type="text/css" media="all" href="css/font-awesome.css" />';
		$this->tampon .= '<link rel="stylesheet" type="text/css" media="all" href="css/bootstrap-datepicker.css" />';
		$this->tampon .= '<link rel="stylesheet" type="text/css" media="all" href="css/summernote.css" />';
		$this->tampon .= '<link rel="stylesheet" type="text/css" media="all" href="css/jquery.treegrid.css" />';
		//$this->tampon .= '<link rel="stylesheet" type="text/css" media="all" href="css/style.css" />';
                $this->tampon .= '<link rel="stylesheet" type="text/css" media="all" href="css/style-new-layout.css" />';
                
		if ($typeaff == 'planning')
		{
                    $this->tampon .= '<link rel="stylesheet" type="text/css" href="css/style-planning.css" />';
		}
                
		$this->tampon .= '<script type="text/javascript" src="js/jquery-ui-1.12.1.custom.min.js"></script>';
		$this->tampon .= '<script type="text/javascript">
						$.widget.bridge("uitooltip", $.ui.tooltip);
						$.widget.bridge("uibutton", $.ui.button);						
						</script>';
		$this->tampon .= '<script type="text/javascript" src="js/fonction.js"></script>';
		$this->tampon .= '<script type="text/javascript" src="js/cartouche.js"></script>';
		$this->tampon .= '<script type="text/javascript" src="js/tableau.js"></script>';
		$this->tampon .= '<script type="text/javascript" src="js/formulaire.js"></script>';
		$this->tampon .= '<script type="text/javascript" src="js/navigation.js"></script>';
		$this->tampon .= '<script type="text/javascript" src="js/datepicker/bootstrap-datepicker.js"></script>';
		$this->tampon .= '<script type="text/javascript" src="js/datepicker/bootstrap-datepicker.fr.js"></script>';
		$this->tampon .= '<script type="text/javascript" src="js/scripts.js"></script>';
		$this->tampon .= '<script type="text/javascript" src="js/chartjs/Chart.min.js"></script>';
                
		$this->tampon .= '</head>';
                
		$this->tampon .= '<body>';
		$this->tampon .= '<script> var idtab = "page"; </script>';
		
		$this->tampon .= '<div id="messErreur" class="modal fade" data-backdrop="false">';
		$this->tampon .= '  <div class="modal-dialog modal-dialog-centered">';
 		$this->tampon .= '    <div class="modal-content">';
 		$this->tampon .= '      <div class="modal-header">';
		$this->tampon .= '        <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>';
 		$this->tampon .= '        <h4 class="modal-title" id="messErreurTitle">Validation des mises à jour</h4>';
		$this->tampon .= '      </div>';
		$this->tampon .= '      <div id="messErreurData" class="modal-body">Mise à jour effectuée</div>';
		$this->tampon .= '      <div class="modal-footer">';
 		$this->tampon .= '       <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>';
		$this->tampon .= '      </div>';
		$this->tampon .= '    </div>';
		$this->tampon .= '  </div>';
		$this->tampon .= '</div>';
		
// gestion de l'info bulle		
		$this->tampon .= '<div id="curseur" class="infobulle"></div>';
		
// affichage de l'entête		
		include('entete.php');
                
// Ajout du menu sauf si connexion ou erreur bdd	
		if ($typeaff != 'erreur' && $typeaff != 'connexion')
		{
			$objMenu = menu::getMenu();
			$this->tampon .= $objMenu->affMenu();
                        
			if ($this->titreCartouche) $this->tampon .= '<div id="cartouche"><div class="pull-left">' . $this->titreCartouche . '</div></div>';
			$this->tampon .= '
                                            <div class="container-fluid">
                                                <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                                        ';
                        
                        
                        
		}
	}
		
/*****************************************************************************************************/
// traitement des boutons
// type de bouton 'button' 'reset' 'submit'
/*****************************************************************************************************/
        
	// Ajout d'un bouton d'enchainement
	// le champ lien est une URL 
	public function addBouton($type, $libelle, $lien='')
	{
		$bouton = '<input type="' . $type . '" value="' . $libelle . '" class="espace-btn btn btn-default"';
		$bouton .= " onclick=\"VersURL('" . $lien . "')\" />";
		$this->bouton .= $bouton;
	}

	// affichage des boutons
	public function affBouton()
	{
		$this->tampon .= '<div class="pull-right">' . $this->bouton . '</div>\n';
	}

/******************************************************************************/
// Ajout du bouton permettant de retourner en haut de la page
/******************************************************************************/
    public function ajouterBoutonScrollUp()
    {
        $buttonScroll = "<button onclick=\"topFunction()\" id=\"scrollUp\" ";
        $style = "style=\"";

        $style .= "display: none; ";
        $style .= "position: fixed; ";
        $style .= "z-index: 5; ";
        $style .= "bottom: 50px; ";
        $style .= "right: 30px; ";
        $style .= "background-color: #6361a9; ";
        $style .= "width: 50px; ";
        $style .= "height: 50px; ";
        $style .= "border: 2px solid #57559c; ";
        $style .= "border-radius: 50%; ";
        $style .= "font-size: 1.3em; ";
        $style .= "\" >";

        $arrow = "<i class=\"fa fa-angle-up\" style=\"color:#f8f8f8;\"></i></button>";

        $this->boutonScroll = $buttonScroll . $style . $arrow;
        $this->tampon .= $this->boutonScroll;
    }

/******************************************************************************/
// fin du formulaire
/******************************************************************************/
    
	public function finPage()
	{
		$conn = database::getIntance();
		// fermeture de la connexion
		if (isset($conn)) $conn=null; // ou unset($conn) 
			
		// gestion de l'affichage d'un message d'erreur de de signalement
		if ($this->erreur != "") $couleurmess = 'red';
		else $couleurmess = 'blue';
		if ($this->libelle != "") 
		{
			$this->tampon .= "<script type=\"text/javascript\">";
			$this->tampon .= "$(document).ready(function (){openModalMessage('" .$couleurmess. "',\"" .$this->libelle. "\");})";
			$this->tampon .= "</script>";
		}
		
		// en mode défilement on n'affiche pas les boutons 
		if ($this->defil == 'o') 
		{
			echo $this->tampon;
			return;
		}
		if ($this->bouton != '') $this->affBouton();
		
		// si c'est une page à afficher dans un onglet on ne traite pas
		if ($this->onglet == 'o') 
		{
			echo $this->tampon;
			return;
		}
                $this->tampon .= "</div></div>";
		$this->tampon .= "<!-- footer -->";
		$this->tampon .= "<div class=\"footer\"></div>";
		$this->tampon .= "<!-- fin footer -->";
		$this->tampon .= '</div>'; // fin du div container 
                $this->tampon .= '<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>';
                $this->tampon .= '<script src="./js/bootstrap-3.3.6/bootstrap.js"></script>';
		$this->tampon .= '</body>';
		$this->tampon .= '</html>';
		echo $this->tampon;
	}
// fin de la classe page
}
