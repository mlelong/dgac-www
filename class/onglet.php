<?php
/* 
	class de gestion des onglets (tab)
	version 1.01 le 24/06/2014
*/
class onglet 
{	
	public $objPage;			/* pointeur sur l'objet page */
	public $nbtab=0;			/* nombre d'onglets */
	public $paramTab;			/* paramètres pour affichage des onglets */
	public $listtab=array();	/* table des codes onglet */
	
	private $tampon;			/* buffer tampon d'affichage des onglets */
	
// initialisation de l'onglet
	function debOnglet()
	{
		$objPage = page::getPage();
		$conn = database::getIntance();

		// Le tampon d'affichage pointe vers le tampon de l'objet page 
		$this->tampon = &$objPage->tampon;
		$this->tampon .= "<IFRAME id='tabframe' onload=\"actuTabFrame()\" NAME='tab_iframe' style=\"display:none;\" ></IFRAME>";
		$this->tampon .= "<div class='tab-content' id='tabs'>";
		$this->nbtab=0;
	}
	
// initialisation des liens des onglets
	function debLienOnglet()
	{
		$this->tampon .= "<ul class='nav nav-tabs'>";
	}
	
// ajout d'un lien d'onglet
	function addLienOnglet($onglet, $mode, $titre, $lien)
	{
		if ($this->nbtab == 0) $active = " class='active' "; 
		else $active = '';
		$this->listtab[$this->nbtab] = $onglet;
		$this->nbtab++;
		if ($mode == 'ancre') $this->tampon .= "<li" . $active . "><a data-toggle='tab' href='#tabs-" . $onglet . "'>" . $titre . "</a></li>";
		// le clic=null permet de ne charger la page qu'une seule fois
		else if ($mode == 'url') $this->tampon .= "<li" . $active . "><a data-toggle='tab' " . $active . " href=\"#tabs-" . $onglet . "\" onclick=\"getTabPage('tabs-" . $onglet . "','" . $lien . "');\">" . $titre . "</a></li>";
	}
	
// fin des liens des onglets
	function finLienOnglet()
	{
		$this->tampon .= "</ul>";
	}
	
// ajout d'un onglet
	function addOnglet()
	{
		for ($i=0; $i<$this->nbtab; $i++) // s'il y a des onglets à traiter
		if ($this->nbtab == 0) $active="tab-pane fade in active";
		else $active="tab-pane fade";
		if ($this->nbtab > 0) $this->tampon .= "</div>";
		$this->nbtab++;
		if ($this->nbtab > 0) $this->tampon .= "<div class=\"" . $active . "\" id=\"tabs-" . $onglet . "\">";
	}
	
// fin des onglets
	function finOnglet()
	{
		if ($this->nbtab > 0)
		{
			$active="tab-pane fade in active";
			for ($i=0; $i<$this->nbtab; $i++) // s'il y a des onglets à traiter
			{
				$this->tampon .= "<div class=\"" . $active . "\" id=\"tabs-" . $this->listtab[$i] . "\"></div>";
				$active="tab-pane fade";
			}
		}
		$this->tampon .= "</div>"; // fin du container tab-content
	}
}
