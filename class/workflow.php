<?php
/* 
	class de gestion des état d'un workflow
	version 1.01 le 24/06/2014
*/
class workflow 
{
	public $destinataires;	/* destinataires du mail */
	
/******************************************************************************/
// initialisation du formulaire lors de la construction de la classe
/******************************************************************************/
	public function __construct()
	{
	}

/******************************************************************************/
// Gestion des destinataires pour envoi de mail
/******************************************************************************/

	// création de la liste des destinatires
	public function addDestinataires($destinataires)
	{
		$this->destinataires = $destinataires;
		return true;
	}
	
	// récupération de la liste des destinataires en fonction de la fonction
	public function getDestinataires($fonc, $idetat)
	{
		$objProfil = profil::getProfil();
		$conn = database::getIntance();
		
//		Trace("entrée dans getDestinataires fonc=" . $fonc . " idetat=" . $idetat . " s_profil->codefonc=" . $objProfil->codefonc);
		if ($fonc == '' || $fonc != $objProfil->codefonc) return false;
		$this->destinataires = '';
		// récupération de la notifcation et du mail par défaut
		$requete = "select ETA_MAIL, ETA_IDNOTIF, ETA_TYPENOTIF from fonc_etat 
					where ETA_IDFONC=\"" . $objProfil->idfonc . "\" and ETA_CLE=\"" . $idetat . "\"";
		$statement = $conn->query($requete);
		$resultat = $statement->fetch(PDO::FETCH_ASSOC);
		$idnotif = $resultat['ETA_IDNOTIF'];
		$typenotif = $resultat['ETA_TYPENOTIF'];
		$mail = $resultat['ETA_MAIL'];
		// si notification spécifique
		if ($typenotif == "Notification" && $idnotif > 0)
		{
			$requete = "select UTI_MAIL from groupe_utilisateur letf join utilisateur on RGU_IDUTILISATEUR=UTI_CLE where RGU_IDGROUPE IN
				(select GNOT_IDGROUPEVAL from notification_groupe where GNOT_IDGROUPEDEM IN 
				(select RGU_IDGROUPE from groupe_utilisateur letf join utilisateur on RGU_IDUTILISATEUR=UTI_CLE where RGU_IDGROUPE IN
				(select GNOT_IDGROUPEDEM from notification_groupe where GNOT_IDNOTIF=\"".$idnotif."\" and RGU_IDUTILISATEUR=\"".$objProfil->idnom."\")))";
			$statement = $conn->query($requete);
			$retour = '';
			while ($row = $statement->fetch(PDO::FETCH_ASSOC))
			{
				if ($row['UTI_MAIL'] != '') $retour .= $row['UTI_MAIL'] . ',';
			}
			// si aucun mail on prend la notification par défaut
			if ($retour != '') $mail = $retour;
		}
		$this->destinataires .= str_replace(";", ",", $mail);
//		Trace("Destinataires =" . $this->destinataires);
		if ($this->destinataires != '') return $this->destinataires;
		else return false;
	}

/******************************************************************************/
// Gestion des changements d'état
/******************************************************************************/
	public function getListEtat($iddemandeur, $idfonc, $idetat, $idlastetat)
	{
//		Trace("entrée dans getListEtat idfonc=" . $idfonc . " idetat=" . $idetat . " idlastetat=" . $idlastetat);
		$objProfil = profil::getProfil();
		$conn = database::getIntance();

		// recherche des états possibles en fonction du profil
		$requete = "select ETA_CLE, ETA_NOMETAT, ETA_TYPENOTIF, ETA_IDNOTIF from fonc_etat where ETA_CLE IN 
						(select WOR_IDNEWETAT from workflow where 
							WOR_IDFONC=\"" . $idfonc . "\" 
							and WOR_IDCURETAT=\"" . $idetat . "\"
							and WOR_IDPROFIL IN 
								(select PRO_CLE from profil, utilisateur_profil 
									where RUP_IDPROFIL=PRO_CLE and RUP_IDUTILISATEUR=\"" . $objProfil->idnom . "\" union 
									select PRO_CLE from profil, groupe_profil 
									where RGP_IDPROFIL=PRO_CLE and RGP_IDGROUPE in 
									(select RGU_IDGROUPE from groupe_utilisateur where RGU_IDUTILISATEUR=\"" . $objProfil->idnom . "\")
								)
						)
					order by ETA_ORDRE";
//		Trace("requete etat =" . $requete);
		$statement = $conn->query($requete);
		$dataliste = array();
		while ($row = $statement->fetch(PDO::FETCH_ASSOC))
		{
			$idnotif = $row['ETA_IDNOTIF'];
			$typenotif = $row['ETA_TYPENOTIF'];
			// vérification si notification particulière de validation
//			Trace("etat =" . $row['ETA_NOMETAT'] . " cle =" . $row['ETA_CLE'] . " idetat =" . $idetat . " idnotif =" .$idnotif . " typenotif =" . $typenotif);
			if ($typenotif == "Validation" && $idnotif >0 && $row['ETA_CLE'] !== $idetat)
			{
				// recherche des utilisateurs autorisés pour cet état
				$requete = "select UTI_CLE from groupe_utilisateur letf join utilisateur on RGU_IDUTILISATEUR=UTI_CLE where RGU_IDGROUPE IN
					(select GNOT_IDGROUPEVAL from notification_groupe where GNOT_IDGROUPEDEM IN 
					(select RGU_IDGROUPE from groupe_utilisateur letf join utilisateur on RGU_IDUTILISATEUR=UTI_CLE where RGU_IDGROUPE IN
					(select GNOT_IDGROUPEDEM from notification_groupe where GNOT_IDNOTIF=\"".$idnotif."\" and RGU_IDUTILISATEUR=\"".$iddemandeur."\")))";
				$statement1 = $conn->query($requete);
//				Trace("requete idnotif>0 =" . $requete);
				while ($row1 = $statement1->fetch(PDO::FETCH_ASSOC))
				{
					if ($row1['UTI_CLE'] != $objProfil->idnom) continue;
					// si l'utilisateur est dans la liste des personnes autorisée on valide l'état
					$dataliste[$row['ETA_CLE']]=$row['ETA_NOMETAT'];
					break;
				}
			}
			// sinon ajout de l'état à la liste
			else $dataliste[$row['ETA_CLE']]=$row['ETA_NOMETAT'];
		}
		
		// retour de la liste des états valides pour la fonction
		return $dataliste;
	}
}
