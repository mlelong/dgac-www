<?php
/* 
	version 1.01 le 24/06/2014

	classe de gestion des filtres pour les tableau et certaines pages
	
	Gestion des filtres à partir de la définition déclarée dans le fichier de configuration
	Types de fitre possibles
		Texte					: champ texte
		Libre					: champ select garni dynamiquement
		Liste					: champ select avec les valeurs indiquées dans le fichier de config
		Date					: champ datepicker

	Définition des options pour chaque champ filtre

        aff       	  (bool)   	: le filtre est-il visible ? Vrai par défaut. Si faux, alors le filtre est masqué et on peut le rendre visible en cliquant sur le bouton “Afficher plus”.
        bdd               (string) 	: libellé de la colonne équivalente en base de donnée. Obligatoire sauf pour le type 'Liste'.
        dataliste 	  (array)  	: tableau associatif simple contenant les valeurs du menu déroulant ainsi que leurs clefs. Non valable pour le type TYPE_FILTRE_TEXTE, obligatoire pour les autres.
        label     	  (string) 	: le libellé du filtre. Obligatoire.
        taille    	  (int)    	: taille du champ, en pixel. 110 par défaut.
        type      	  (string) 	: type du champ. Type 'Texte' par défaut.
        compare    	  (string) 	: type de comparaison (= ou like). like par défaut.
        cacher-mobile     (bool)        : tous les filtres sont visibles par defaut, il faut cacher les filtres qu'on ne veut pas voir sur mobile  
*/
class filtre 
{
	public $filtre;				/* indicateur de gestion des filtres */
	public $numfiltre;			/* numéro du filtre */
	public $nomform;			/* nom du formulaire */
	public $idform;				/* id (html) du formulaire */
	public $objTab;				/* pointeur sur objet tableau si filtre sur tableau */
	public $etat;				/* indique si le filtre est déjà affiché ou non */
	public $param = '';			/* paramètres d'URL complémentaires */
	public $change;				/* indicateur bouton raz et valider */
	
	public $champF = array();	/* liste des champs du filtre */
	
	private $tampon;			/* buffer tampon d'affichage du filtre */

/******************************************************************************/
// initialisation du filtre lors de la construction de la classe
/******************************************************************************/
	public function __construct($param)
	{
		global $typeaction;
		$objPage = page::getPage();

		// Le tampon d'affichage pointe vers le tampon de l'objet page 
		$this->tampon = &$objPage->tampon;
		
		if ($param == '') return;
		// créé depuis l'objet tableau si champFL déclaré dans le fichier config
		if (is_object($param)) 
		{
			$this->objTab = $param;				// pointeur vers le tableau 
			$this->filtre = $param->filtre;  	// option true ou false issue du fichier config
			$this->numfiltre = $param->numtab; 	// numéro tu tableau
		}
		else // sinon créé en direct (pour une utilisation en dehors des tableaux)
		{
			$this->numfiltre = $param;
			$this->filtre = true;
		}
		$this->nomform = "nomform" . $_SESSION['curform']; // name de la balise <FORM> par défaut
		$this->idform = "idform" . $_SESSION['curform']; // id de la balise <FORM> par défaut
		$_SESSION['curform']++; // numéro du prochain formulaire
		$varfiltre="champFL".$this->numfiltre;
		global $$varfiltre;
		$this->champF = $$varfiltre;
		$this->etat = false;
		$this->change = false;

		// récupération des variables
		if ($this->filtre !== true) 
		{
			$this->initFiltre();
			return false;
		}
		else
		{	
			if($typeaction == 'razfiltre') $raz='oui';
			else $raz="";
			if(isset($_REQUEST['razfiltre'])) $raz='oui';
			if (is_array($this->champF)) 
			{	
				foreach ($this->champF as $curcle => $curchamp)
				{
					global ${$curcle};
					if ($raz == 'oui') 
					{
						delVariable($curcle);
						${$curcle}='';	
					}
					else
					{
						if(isset($_REQUEST[$curcle])) 
						{
							${$curcle}=$_REQUEST[$curcle]; // le filtre est renseigné
							if (is_object($this->objTab)) $this->objTab->setLimite(0); // réinitialisation de la pagination si besoin
						}	
						elseif(isset(${$curcle}) && ${$curcle} != '') ${$curcle}=${$curcle}; // le filtre a été positionné avant appel
						elseif(testVariable($curcle)) ${$curcle}=getVariable($curcle); // le filtre a été sauvegardée dans la navigation
						elseif(!isset(${$curcle})) ${$curcle}=""; // le filtre n'est pas positionné
						else ${$curcle}=${$curcle};
						if (${$curcle} == "raz") {unset($_SESSION[$curcle]);${$curcle}="";}
						setVariable($curcle,${$curcle}); // sauvegarde du filtre dans la navigation
					}
				}
			}	
		}
	}
	
/******************************************************************************/
// Gestion des attributs (appelé depuis les pages php)
/******************************************************************************/
	// modification de l'option d'affichage
	function setAff($nomchamp, $aff)
	{
		if ($aff === true) $this->champF[$nomchamp]['aff'] = true;
		else if ($aff === false) $this->champF[$nomchamp]['aff'] = false;
	}

	// modification du champ dataliste
	function setDataliste($nomchamp, $dataliste)
	{
		$this->champF[$nomchamp]['dataliste'] = $dataliste;
	}

	// remplacer bouton par évènement onChange()
	function setChange()
	{
		$this->change = true;
	}

/******************************************************************************/
//  affichage de l'ensemble des filtres en une seule fois  
/******************************************************************************/
	function affFiltres()
	{
		$varfiltre="champFL".$this->numfiltre;
		global $$varfiltre;
		$filtre=&$$varfiltre;
		// première boucle pour afficher les filtres toujours présents (aff=true)
		$option = false;
		$nbfiltre = 0;
		$ouvert = false;
		foreach ($filtre as $curcle => $champ)
		{
			$curchamp = $this->champF[$curcle];
			global $$curcle;
			if ($curchamp['aff'] !== true) 
			{
				$option = true; 
				if ($$curcle != '') $ouvert = true; // si filtre renseigné -> on ouvre les champs en option
				continue;
			}
			if (isset($curchamp['dataliste'])) $dataliste = $curchamp['dataliste'];
			else $dataliste = '';
			$this->affFiltre($$curcle, $curcle, $dataliste);
			$nbfiltre++; 
		}
		// affichage des boutons si filtres présents
		if ($nbfiltre > 0)
		{	
			if ($this->change === false)
			{
				$this->tampon .= "<button type=\"submit\" class=\"espace-btn btn btn-default btn-xs\" onClick=\"SubmitForm('" . $this->nomform . "','filtre')\"><i class=\"fa fa-check\"></i></button>";
				$this->tampon .= "<button type=\"submit\" class=\"espace-btn btn btn-default btn-xs\" onClick=\"SubmitForm('" . $this->nomform . "','razfiltre')\"><i class=\"fa fa-remove\"></i></button>";
			}
			// deuxième boucle pour afficher les filtres optionnels (aff=false)
			if ($option && $nbfiltre > 0)
			{	
				if ($ouvert === false)
				{
					$this->tampon .= "<button type=\"button\" class=\"espace-btn btn btn-default btn-xs\" onClick=\"affFiltreOption('" . $this->nomform . "')\"><i id=\"" . $this->nomform . "-fa\" class=\"fa fa-chevron-down\"></i></button>";
					$this->tampon .= "<div id=\"" . $this->nomform . "-div\" style='display:none;margin-top:3px;'>";
				}
				else
				{	
					$this->tampon .= "<button type=\"button\" class=\"espace-btn btn btn-default btn-xs\" onClick=\"affFiltreOption('" . $this->nomform . "')\"><i id=\"" . $this->nomform . "-fa\" class=\"fa fa-chevron-up\"></i></button>";
					$this->tampon .= "<div id=\"" . $this->nomform . "-div\" style='margin-top:3px;'>";
				}	
				foreach ($filtre as $curcle => $champ)
				{
					$curchamp = $this->champF[$curcle];
					if ($curchamp['aff'] !== false) continue;
					global $$curcle;
					if (isset($curchamp['dataliste'])) $dataliste = $curchamp['dataliste'];
					else $dataliste = '';
					$this->affFiltre($$curcle, $curcle, $dataliste);
				}
				$this->tampon .= "</div>";
			}
		}	
	}

/******************************************************************************/
// initialisation des filtres
/******************************************************************************/
	function debFiltre()
	{
		global $typeaction;
		$objPage = page::getPage();

		if($typeaction == 'razfiltre') $raz='oui';
		else $raz="";
		if ($raz == 'oui' && is_object($this->objTab)) $this->objTab->t_tri='';
		$this->tampon .= "<div class='form-inline espace'>";

		$url = $_SERVER['SCRIPT_NAME'] . "?typeaction=reception";
		if ($objPage->param != '') $url .= "&" . $objPage->param;
		$this->tampon .= "<FORM role='form' class='form-horizontal' id='" . $this->idform . "' name='" . $this->nomform . "' ACTION=\"" . $url . "\"  METHOD=post >";

		return true;
	}
	
/******************************************************************************/
// initialisation des variables de filtre
/******************************************************************************/
	function initFiltre()
	{
		foreach ($this->champF as $curcle => $curchamp)
		{
			global ${$curcle};
			${$curcle}="";
		}
	}
	
/******************************************************************************/
// affichage d'un filtre
/******************************************************************************/
	function affFiltre($valeur, $nomchamp, $dataliste)
	{
		if ($this->filtre !== true) return;
		$curchamp = $this->champF[$nomchamp];
//		if ($curchamp['aff'] !== true) return;
		$libchamp = $curchamp['label'];
		if (isset($curchamp['type'])) $typechamp = $curchamp['type'];
		else $typechamp = 'Texte';
		if (isset($curchamp['largeur_champ'])) $largeur_champ = $curchamp['largeur_champ'];
		else $largeur_champ = '';
		if ($largeur_champ == '') $largeur_champ = '110';
		if (isset($curchamp['taille'])) $taille = $curchamp['taille'];
		else $taille = 100;

                $class = '';
                if ($curchamp['cacher-mobile'] === true) {
                    $class .= "hidden-xs ";
                }                   
                
		$retour = '';
		switch ($typechamp)
		{
                    // champ Texte
                    case 'Texte' :
                            $retour .= "<span class='".$class."'><label>" . $libchamp . " : </label>\n";
                            $retour .= "<INPUT class='form-control input-sm' style='width:" . $largeur_champ . "px;' TYPE=text NAME='" . $nomchamp . "' MAXLENGTH=" . $taille . " VALUE=\"" . $valeur . "\"></span>\n"; 
                            break; // fin

                    // Champ libre (en principe select)
                    case 'Libre' :
                            if ($this->change) $event = " onChange=\"SubmitForm('" . $this->nomform . "','filtre')\" ";
                            else $event = '';
                            $retour .= "<span><label>" . $libchamp . " :</label>\n";
                            $retour .= "<SELECT class='form-control input-sm' style='width:" . $largeur_champ . "px;' NAME='" . $nomchamp . "' " .$event . ">\n";
                            $retour .= $dataliste;
                            $retour .= "</SELECT></span>\n";
                            break; // fin

                    // champ liste (select)
                    case 'Liste' :
                            $retour .= "<span><label>" . $libchamp . " :</label>\n";
                            $retour .= "<SELECT class='form-control input-sm' style='width:" . $largeur_champ . "px;' NAME='" . $nomchamp . "' >\n";
    //			$tab = explode(';', $dataliste);
                            $retour .= "<OPTION >\n";
                            foreach ($dataliste as $cle => $element)
                            {
                                    if ($valeur == $element || $valeur == $cle) {$opt = " selected ";} else {$opt = "";}
                                    $retour .= "<OPTION value=\"" . $cle . "\"" . $opt . ">" . $element . "\n";
                            }
                            $retour .= "</SELECT></span>\n";
                            break; // fin

                    // champ Date
                    case 'Date' :
                            $retour .= "<span><label>" . $libchamp . " :</label>\n";
                            $retour .= "<INPUT class='form-control input-sm datepicker' style='margin-right:2px; width:" . $largeur_champ . "px;' TYPE=text NAME='" . $nomchamp . "' MAXLENGTH=20 VALUE=\"" . $valeur . "\"></span>";
                            break; // fin
		}	
		$this->tampon .= $retour;
	}

/******************************************************************************/
// fin des filtres
/******************************************************************************/
	function finFiltre()
	{
		if ($this->filtre !== true) return;
//		echo '<input type="image" class="imgfiltre" src="images/error32.gif" onClick="SubmitForm(\'razfiltre\')">';
		//echo "</div>";
		$this->tampon .= "</FORM>\n";
		$this->etat = true;
	}
	// fin de la classe filtre
}
