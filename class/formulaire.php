<?php
/* 
	version 1.01 le 24/06/2014
	
	classe de gestion des formulaires
	
	Gestion des formulaires à partir de la définition déclarée dans le fichier de configuration
	Définition de la configuration globale du formulaire
        largeur       (int)    	: largeur du tableau, en unité de douzième de page. Doit être un nombre entre 1 et 12. 12 par défaut.
        largeur-decal (int)    	: décalage du tableau sur la droite, en unité de douzième de page. Doit être un nombre entre 0 et 12. 0 par défaut.
        titre         (string) 	: le titre du tableau. null par défaut.
        titresuite    (bool) 	: Affiche de données spécifiques (nom de l'élément) à la suite du titre.
        aligner    	  (string) 	: alignement des formulaires si plusieurs (gauche ou droite).

	Type de champs de formulaire possibles
		Libre					: champ select garni dynamiquement
		Montant					: champ montant avec €
		Nombre					: champ nombre
		Pourcentage				: champ pourcentage avec %
		Texte					: champ texte
		Date					: champ date (datepicker)
		Date/heure				: champ date et heure
		Autocomplete			: champ autocomplete avec option liste et scrollable
		Liste					: champ select avec les valeurs indiquées dans le fichier de config
		Liste case				: champ liste de cases à cocher
		Case à cocher			: champ case à cocher
		Radio bouton			: champ radio bouton
		Zone texte				: champ zone de texte
		Ficher					: champ sélection d'un fichier à télécharger
		Fichiers				: champ gestion de plusieurs fichiers téléchargés
		Password				: champ mot de passe
		Sortable				: champ passage d'une liste à une autre via drag and drop

	Définition des options pour chaque champ
	    aff     	  (bool)   	: la colonne est-elle visible ? Vrai par défaut.
        bdd      	  (string) 	: libellé de la colonne équivalente en base de données.
        label    	  (string) 	: libellé de la colonne. 
        taille   	  (int)    	: Taille de la zone de saisie des champs Texte.
        ctl   	 	  (string) 	: indique si la champ doit obligatoirement être saisie (o).
	    decimale      (int)   	: Nombre de décimales après la virgule pour les types Montant, Nombre et Poucentage.
*/
class formulaire 
{
	public $titre;				/* titre du formulaire */
	public $titresuite;			/* nom de la variable pour la suite du titre du formulaire */
	public $titre2;				/* valeur de la suite du titre du formulaire */
	public $erreur='';			/* id du champ en erreur */
	public $libelle='';			/* message d'erreur ou de validation */
	public $numform;			/* numéro du formulaire */
	public $nomform;			/* nom du formulaire */
	public $idform;				/* id (html) du formulaire */
	public $largeur;			/* largeur du formulaire */
	public $aligner;			/* alignement des formulaires si plusieurs */
	public $typeform;       	/* type de formulaire (page ou boite de dialogue) */
	public $inline;         	/* le champ courant est inline */
	public $debinline;			/* indique le premier champ en ligne */
	public $fininline;			/* indique le dernier champ en ligne */
	public $flaginline;			/* indique des champs en ligne en cours */
	public $fieldset;			/* indique si un groupe de champs (fieldset) est ouvert */
	public $lec=false;     	 	/* indique si le formulaire est en lecture seule */
	public $bouton;				/* boutons d'enchainement  */
	public $numchamp;			/* numéro du champ si type 'Fichiers' */
	public $dischamp='';		/* liste des champs disabled */
	private $errorbdd;			/* Erreur bdd retournée par PDO */ 

	public $champF;				/* liste des champs du formulaire */

	private $tampon;			/* buffer tampon d'affichage du formulaire */

/******************************************************************************/
// initialisation du formulaire lors de la construction de la classe
/******************************************************************************/
	public function __construct($numform='1')
	{
		$objPage = page::getPage();
		// Le tampon d'affichage pointe vers le tampon de l'objet page 
		$this->tampon = &$objPage->tampon;
		
		$this->nomform = "nomform" . $_SESSION['curform']; // name de la balise <FORM> par défaut
		$this->idform = "idform" . $_SESSION['curform']; // id de la balise <FORM> par défaut
		$_SESSION['curform']++; // numéro du prochain formulaire
		$this->numform = $numform;
		$varform="descF".$numform;
		global $$varform;
		$descformulaire=&$$varform;
		$this->titre = $descformulaire['titre'];
		if (isset($descformulaire['titresuite'])) $this->titresuite = $descformulaire['titresuite'];
		else $this->titresuite = false;
		if (isset($descformulaire['largeur'])) $this->largeur = $descformulaire['largeur'];
		else $this->largeur = '';
		if (isset($descformulaire['aligner'])) $this->aligner = $descformulaire['aligner'];
		else $this->aligner = '';
		$this->bouton = '';
		
		$varform="champF".$this->numform;
		global $$varform;
		$this->champF = &$$varform; // on pointe vers les descriptions du fichier config
	}
	
/******************************************************************************/
// Gestion des attributs (appelé depuis les pages php)
/******************************************************************************/

	// modification de l'option d'affichage
	public function setAff($nomchamp, $aff)
	{
		if ($aff === true || $aff === false)
		$this->champF[$nomchamp]['aff'] = $aff;
	}
	
	// modification de l'option d'affichage
	public function setlecForm()
	{
		$this->lec = true;
	}
	
	// modification du champ option
	public function setOpt($nomchamp, $opt)
	{
		if ($opt != '')
		{
			// si readonly l'attibut value reste valide et est transmis 
			// si disabled l'attibut value n'est pas renseigné et donc pas transmis
			if ($opt == 'disabled') 
			{
				$opt = " disabled=\"disabled\" ";
				$this->champF[$nomchamp]['aff'] = 'disabled';
			}
			else if ($opt == 'readonly') $opt = " readonly ";
			else if ($opt == 'checked') $opt = " checked ";
			if (isset($this->champF[$nomchamp]['opt'])) $this->champF[$nomchamp]['opt'] .= $opt;
			else $this->champF[$nomchamp]['opt'] = $opt;
		}
	}
	
	// modification du champ deb
	public function setDeb($nomchamp, $opt)
	{
		$this->champF[$nomchamp]['deb'] = $opt;
	}
	
	// modification du champ fin
	public function setFin($nomchamp, $opt)
	{
		$this->champF[$nomchamp]['fin'] = $opt;
	}
	
	// modification du champ dataliste
	public function setDataliste($nomchamp, $dataliste)
	{
		$this->champF[$nomchamp]['dataliste'] = $dataliste;
	}

	// gestion repertoire des fichiers attachés
	public function setRepertoire($nomchamp, $repertoire)
	{
		$this->champF[$nomchamp]['repertoire'] = $repertoire;
	}
	
/******************************************************************************/
// création d'un champ SELECT dynamique
// $nomchamp = nom du champ indiqué dans le fichier config
// $valchamp = valeur du champ
// $requete = requete à exécuter pour récuperer les champs OPTIOn de la SELECT
// $cle = nom du champ CLE de la table
// $texte = nom du TEXTE à afficher dans la SELECT
//
// Exemple
// $objForm->setSelect('idprojet', $idprojet, $requete, 'PRJ_CLE', 'PRJ_NOMPROJET','getSelectOption=idbudget=BUDL_IDPROJET');
//
/******************************************************************************/
	public function setSelect($nomchamp, $valchamp, $requete, $cle, $texte, $change='')
	{
		$conn = database::getIntance();
		$dataliste = '';
		if ($change != '') // option à traiter
		{
			$tab = explode('=', $change);
			// la valeur courante de la SELECT détermine le contenu de la SELECT suivante (cible)
			if ($tab[0] == 'getSelectOption')
			{
				// tab[1] correspond au nom du champ cible
				// tab[2] correspond 
				if (isset($tab[1]) && isset($tab[2])) // le champ cible et le champ de la clause WHERE doivent être renseignés
				{
					// ajout des options where et whereval dans le champ cible
					if (isset($this->champF[$tab[1]]))  
					{
						$this->champF[$tab[1]]['where'] = $tab[2]; // nom du champ
						$this->champF[$tab[1]]['whereval'] = $valchamp; // valeur du champ
					}	
					// ajout de l'évènement onChange sur le champ courant
					$change="onChange=\"getSelectOption('idbudget',this.value)\"";
				}
			}
		}
		if (isset($this->champF[$nomchamp]['opt'])) $opt = $this->champF[$nomchamp]['opt'];
		else $opt = '';
		$dataliste .= "<span>\n"; // bug IE
		$dataliste .= "<SELECT id='" . $nomchamp . "' class='form-control input-sm' NAME='" . $nomchamp . "' " . $change . " " . $opt . ">\n";
		$dataliste .= "<OPTION >";
		// ajout d'une clause where si SELECT liée à une autre SELECT
		if (isset($this->champF[$nomchamp]['where'])) 
		{
			$tab = explode('where', $requete); // y a t-il une clause where
			if (isset($tab[1])) 
			{
				$requete = $tab[0] . " where " . $this->champF[$nomchamp]['where'] . "=\"" . $this->champF[$nomchamp]['whereval'] . "\" and " . $tab[1];
			}
			else // si non on regarde s'il y a une clause order by
			{
				$tab = explode('order by', $requete);
				$requete = $tab[0] . " where " . $this->champF[$nomchamp]['where'] . "=\"" . $this->champF[$nomchamp]['whereval'] . "\"";
				if (isset($tab[1])) $requete .= ' order by ' . $tab[1];
			}	
		}	
		$statement = $conn->query($requete);
		while ($row = $statement->fetch(PDO::FETCH_ASSOC))
		{
			if ($valchamp == $row[$cle]) {$opt = " selected ";} else {$opt = "";}
			$dataliste .= "<OPTION value=\"" . $row[$cle] . "\"" . $opt . ">" . $row[$texte] . "\n";
		}
		$dataliste .= "</SELECT>\n";
		$dataliste .= "</span>\n";
		$this->setDataliste($nomchamp, $dataliste);
	}
	
/******************************************************************************/
// initialisation des variables php pour les formulaires
/******************************************************************************/
	public function initChamp()
	{
		foreach ($this->champF as $curcle => $curchamp)
		{
			if (isset($curchamp['type']) && $curchamp['type'] == 'Groupe') continue;
			global $$curcle; 
//			$$curcle='';
		}
	}
	
/******************************************************************************/
// mapping des champs bdd vers les variables php des formulaires
/******************************************************************************/
	public function mapChamp($requete)
	{
		$conn = database::getIntance();
		$statement = $conn->query($requete);
		$resultat = $statement->fetch(PDO::FETCH_ASSOC);

		foreach ($this->champF as $curcle => $curchamp) // on boucle sur tous les champs
		{
			if (isset($curchamp['bdd'])) // s'il y a une correspondance avec un champ bdd on créé la variable
			{
				// on créé la variable
				global $$curcle; 
				// on récupère la valeur depuis la bdd
				if (isset($curchamp['taille']) && $curchamp['taille'] > 50) 
//					$$curcle = stripslashes($resultat[$curchamp['bdd']]); // stripslashes si besoin
					$$curcle = $resultat[$curchamp['bdd']]; // stripslashes si besoin
				else $$curcle = $resultat[$curchamp['bdd']];
				// test si champs Date vide
				if ($curchamp['type'] == 'Date' && ($$curcle == '00-00-0000' || $$curcle == '0000-00-00')) $$curcle = ''; 
				// test si champs Date/heure vide
				if ($curchamp['type'] == 'Date/heure' && ($$curcle == '00-00-0000 00:00' || $$curcle == '0000-00-00 00:00')) $$curcle = ''; 
			}
		}
		return $resultat;
	}

/******************************************************************************/
// Ajout d'un champ dans le tableau des champs pour mapper une variable
// avec un champ bbd avant mise à jour via majBdd
/******************************************************************************/
	public function addChamp($nomchamp, $nombdd)
	{
		$this->champF[$nomchamp] = ["aff"=>false,"label"=>"","bdd"=>$nombdd,"type"=>"Texte"];
	}
	
/******************************************************************************/
// suppression de l'attribut bdd pour éviter la mise à jour de la bse de données via majBdd
/******************************************************************************/
	public function delChampBdd($nomchamp)
	{
		if (isset($this->champF[$nomchamp]['bdd'])) $this->champF[$nomchamp]['bdd'] = '';
	}
	
/******************************************************************************/
// mise à jour de la bdd à partir des variables 
/******************************************************************************/
	public function majBdd($table, $clenom, $cle)
	{
		$conn = database::getIntance();
		$listeNom = '';
		$listVal = '';
		if ($cle == '') // création
		{
			foreach ($this->champF as $curcle => $curchamp) // on boucle sur tous les champs
			{
				if (isset($curchamp['bdd']) && $curchamp['bdd'] != '') // s'il y a une correspondance avec un champ bdd on créé la variable
				{	
					if ($curchamp['type'] == 'Date' || $curchamp['type'] == 'Date/heure') $curcle = 'MYSQL' . $curcle;
					global $$curcle;
					if ($$curcle != '') // création seulement si champ renseigné 
					{	
						if ($listeNom != '') 
						{
							$listeNom .= ",";
							$listVal .= ",";
						}
						$listeNom .= $curchamp['bdd'];
						if ($curchamp['type'] == 'Texte' || $curchamp['type'] == 'Zone texte') $listVal .= "\"" . addslashes($$curcle) . "\"";
						else $listVal .= "\"" . $$curcle . "\"";
					}
				}
			}
			$requete = "insert into " . $table . " (" . $listeNom . ") values (" . $listVal . ")";
		}	
		else // mise à jour
		{
			foreach ($this->champF as $curcle => $curchamp) // on boucle sur tous les champs
			{
				if (isset($curchamp['bdd']) && $curchamp['bdd'] != '') // s'il y a une correspondance avec un champ bdd on créé la variable
				{
					if ($curchamp['type'] == 'Date' || $curchamp['type'] == 'Date/heure') $curcle = 'MYSQL' . $curcle;
					global $$curcle; 
					if ($listeNom == '') $listeNom = $curchamp['bdd'];
					else $listeNom .= "," . $curchamp['bdd'];
					if ($$curcle != '') // mise à jour si champ renseigné sinon DEFAULT
					{
						if ($curchamp['type'] == 'Texte' || $curchamp['type'] == 'Zone texte') $listeNom .= "=\"" . addslashes($$curcle) . "\"";
						else $listeNom .= "=\"" . $$curcle . "\"";
					}
					else $listeNom .= "=DEFAULT";
				}
			}
			$requete = "update " . $table . " set " . $listeNom . " where " . $clenom . "=\"" . $cle . "\"";
		}
		$statement = $conn->exec($requete);
		// mise à jour OK
		if ($statement !== false) 
		{
			$this->libelle="Mise à jour effectuée";
			return true; 
		}
		// mise à jour KO
		else 
		{
			$this->errorbdd = $conn->errorInfo();
			if ($this->errorbdd[1] == 1062) $this->libelle = $this->errorbdd[1];
			else $this->libelle = $this->errorbdd[1] . " " . $this->errorbdd[2];
			return false;
		}
	}

	public function getErrBdd()
	{
		if ($this->errorbdd[1] == 1062) return $this->errorbdd[1];
		else return $this->errorbdd[1] . " " . $this->errorbdd[2];
	}
	
/******************************************************************************/
// début du formulaire
/******************************************************************************/
	public function debFormulaire()
	{
		$objProfil = profil::getProfil();
		$objPage = page::getPage();

		// constitution de la balise FORM
		$url = $_SERVER['SCRIPT_NAME'] . "?typeaction=reception";
		if ($objPage->param != '') $url .= "&" . $objPage->param;
		$this->tampon .= "<FORM role='form' class='form-horizontal' id='" . $this->idform . "' name='" . $this->nomform . "' ACTION=\"" . $url . "\"  METHOD=post enctype=\"multipart/form-data\" onsubmit=\"return checkSize('reservation', 8388608)\" >";
		
        if (basename($_SERVER['SCRIPT_NAME']) == 'log_connexion.php') $this->lec = false; // détermine si le formulaire est en readonly
		
		if ($this->erreur != "") 
		{
			$this->tampon .= "<script type=\"text/javascript\">";
			$this->tampon .= "$(document).ready(function (){document.getElementById('" . $this->erreur . "').focus();})";
			$this->tampon .= "</script>";
		}
		
		$style = '';
		if ($this->largeur != "") // largeur du formulaire
		{
			if(!strpos($this->largeur, "px") && !strpos($this->largeur, "%")) $this->largeur .= "px";
//			if ($this->largeur < 200) $this->largeur = "200px";
//			if ($this->largeur > 2000) $this->largeur = "2000px";
			$style .= "width:" . $this->largeur . ";";
		}
		if ($this->aligner != "") // alignement des formulaires
		{
			if ($this->aligner == 'gauche' || $this->aligner == 'left')	$style .= "float:left;";
			if ($this->aligner == 'droite' || $this->aligner == 'right')	$style .= "float:right;";
		}
		if ($style != '') $style = " style='" . $style . "' ";
//		$this->tampon .= "<div class='container thumbnail'" . $style . ">";
		$this->tampon .= "<div class='container'" . $style . ">";
		// affichage du titre
		if ($this->titresuite === true && isset($_SESSION['titresuite'])) $this->titre2 = $_SESSION['titresuite'];
		else if ($this->titresuite !== false)
		{
			// on récupère la variable
			global ${$this->titresuite}; 
			// et on la sauvegarde en session si présente
			if (${$this->titresuite} != '') 
			{
				$_SESSION['titresuite'] = ${$this->titresuite};
				$this->titre2 = ${$this->titresuite};
			}	
			else $this->titre2 = "";
		}
		else $this->titre2 = "";
		$separateur = $this->titre?" / ":null;
		$this->tampon .= "<h3>". $this->titre . $separateur . $this->titre2 . " </h3>\n";
//		if ($this->typeform == 'dialog' && $this->libelle != "") $this->tampon .= "<div id='msgerr' style='font-size: 1.4em; font-weight: bold; color:red;'>" . $this->libelle . "</div>\n";
		// gestion du messages d'erreur ou de validation
		if ($this->erreur != "") $couleurmess = 'red';
		else $couleurmess = 'blue';
		if ($this->libelle != "") 
		{
			$this->tampon .= "<div id='msgerr' style='font-size: 1.2em; font-weight: bold; color:" . $couleurmess . ";'>" . $this->libelle . "</div>\n";
			$this->tampon .= "<script type=\"text/javascript\">";
			$this->tampon .= "$(document).ready(function (){openModalMessage('" .$couleurmess. "',\"" .$this->libelle. "\");})";
			$this->tampon .= "</script>";
		}
		else $this->tampon .= "<div id='msgerr'>Tous les champs avec * doivent &ecirc;tre renseign&eacute;s</div>\n";
//		$this->tampon .= "<div id='msgerr'>Tous les champs avec * doivent &ecirc;tre renseign&eacute;s</div>\n";
		$this->tampon .= "<br/>\n";
	}

/******************************************************************************/
//  affichage de l'ensemble d'un formulaire en une seule fois  
/******************************************************************************/
	public function affFormulaire()
	{
		$this->fieldset = false; // pas de groupe ouvert
		foreach ($this->champF as $curcle => $curchamp)
		{
			if (isset($curchamp['dataliste'])) $dataliste = $curchamp['dataliste'];
			else $dataliste = '';
			if (isset($curchamp['opt'])) $opt = $curchamp['opt'];
			else $opt = '';
			global $$curcle; // on récupère la valeur du champ
			// formatage du formulaire en fonction du type de champ
			// les champs non affichables deviennent des champs cachés
			// si type 'Fichiers' on appelle deux fois la fonction
/*			
			if ($curchamp['type'] == 'Fichiers')
			{
				$this->numchamp = 1;
				$this->tampon .= $this->affAllChamp($$curcle, $curcle, $dataliste, $opt);
				$this->numchamp = 2;
				$this->tampon .= $this->affAllChamp($$curcle, $curcle, $dataliste, $opt);
			}
			else $this->tampon .= $this->affAllChamp($$curcle, $curcle, $dataliste, $opt);
*/			
			$this->tampon .= $this->affAllChamp($$curcle, $curcle, $dataliste, $opt);
		}
		if ($this->fieldset === true) $this->tampon .= "</fieldset>"; // fermeture du dernier groupe (fieldset) si besoin
	}
	
/******************************************************************************/
// sérialisation du formulaire
/******************************************************************************/
	// sérialisation du formulaire
	public function serFormulaire($champ)
	{
//		return serialize($champ);
		return base64_encode(serialize($champ));
	}
	
	// desérialisation du formulaire
	static function unserFormulaire($dbchamp)
	{
//		return unserialize($dbchamp);
		return unserialize(base64_decode($dbchamp));
	}
	
/******************************************************************************/
//  affichage d'un champ spécifique du formulaire (pas utilisé)
/******************************************************************************/
	public function affChamp($valeur, $nomchamp, $dataliste, $opt)
	{
		$curchamp = $this->champF[$nomchamp];
//		les champs non affichables deviennent des champs cachés
//		if ($curchamp['aff'] !== true) return; 
		$this->tampon .= $this->affAllChamp($valeur, $nomchamp, $dataliste, $opt);
		// on ferme le dernier inline si besoin
		if ($this->inline === true) 
		{
			$retour .= "</div></div>"; 
			$this->inline = false;
		}	

	}
		
/******************************************************************************/
//  affichage d'un champ du formulaire via l'affichage global 
/******************************************************************************/
	public function affAllChamp($valeur, $nomchamp, $dataliste, $opt)
	{
		$curchamp = $this->champF[$nomchamp];
		$libchamp = $curchamp['label'];
		if (isset($curchamp['type'])) $typechamp = $curchamp['type'];
		else $typechamp = 'Texte';
		// initialisation du buffer
		$retour = ""; 
		// Test si formulaire dans une boite de dialogue (pas utilisé)
		$this->typeform = 'mono'; // TEST
		if ($this->typeform == 'dialog') $class= "row";
		else if ($this->typeform == 'mono') $class= "row";
		else $class= "row";
		// traitement des groupes de formulaire (fieldset)
		if ($typechamp == 'Groupe') 
		{
			if ($this->fieldset === true) $this->tampon .= "</fieldset>"; // fermeture du dernier groupe (fieldset) si besoin
			if ($this->lec) $retour .= "<fieldset disabled='disabled'>\n"; 
			else $retour .= "<fieldset>\n";
			$retour .= "<legend class='form-legend'><span>" . $libchamp . " </span></legend>\n";
			$this->fieldset = true;
			return $retour; // fin
		}
		// Les champs qui ne doivent pas s'afficher seront des champs cachés
		// on trouvera des variables à passer et les ID des champs select
		if ($curchamp['aff'] === false)
		{
			$retour .= '<input type="hidden" ID="' . $nomchamp . '" value="'. $valeur .'" name="'. $nomchamp . '">';
			return $retour; // fin
		}
		// gestion des champs disabled
		if ($curchamp['aff'] === 'disabled')
		{
			// traitement particulier pour les cas avec dataliste
			$valeurhidden = $valeur;
			if ($typechamp == 'Case à cocher' && $valeur == '') $valeurhidden = 'o';
			if ($typechamp == 'Radio bouton')
			{
				if(isset($curchamp['val'])) $listval=explode(";", $curchamp['val']);
				else $listval=array(1,2,3,4,5,6,7,8,9,10);
				if (isset($listval[0])) $valeurhidden = $listval[0];
				for ($cpt=0;$cpt<count($dataliste);$cpt++)
				{
					if ($valeur != '' && $valeur == $listval[$cpt]) $valeurhidden = $listval[$cpt];
				}
			}
			$this->dischamp .= 'dis_' . $nomchamp . ',';
			$retour .= '<input type="hidden" value="'. $valeurhidden .'" name="dis_'. $nomchamp . '">';
		}
		// gestion des champs inline
		if (isset($curchamp['inline'])) $this->inline = $curchamp['inline'];
		else $this->inline = false;
		if (isset($curchamp['debinline'])) $this->debinline = $curchamp['debinline'];
		else $this->debinline = false;
		if (isset($curchamp['fininline'])) $this->fininline = $curchamp['fininline'];
		else $this->fininline = false;
		if ($this->debinline === true) $this->inline = true;
		if ($this->fininline === true) $this->inline = true;
		// traitement des autres champs de formulaire
		if (isset($curchamp['ctl'])) $saisie = $curchamp['ctl'];
		else $saisie = '';
		if (isset($curchamp['taille'])) $taille = $curchamp['taille'];
			else $taille = 100;
		if ($libchamp != '' && $saisie == 'o') $libchamp .= " <b style='text-color:red;'>*</b>";
		if ($libchamp != '') $libchamp .= " :";
		if (isset($curchamp['largeur'])) $largeurcomp = $curchamp['largeur'];
		else $largeurcomp = '';
		if (isset($curchamp['labelinline'])) $libinline = $curchamp['labelinline'];
		else $libinline = '';
		if ($this->erreur == $nomchamp)	$err = "style=\"color:red\" ";
		else $err = "";

		// traitement de début du changement de ligne
		// autres champs que Case à cocher, Sortable et Fichiers
		if ($typechamp != 'Sortable' && $typechamp != 'Fichiers') // si Sortable ou Fichiers --> on ne fait rien
		{	
			// si pas en ligne ou début de en ligne
			if (!$this->inline || $this->debinline || $typechamp == 'Radio bouton') 
			{	
				$retour .= "<div class='form-group " . $class . "'>";
				if ($typechamp == 'Fichier')
				{
					if ($valeur == '') $libchamp = "Nom du fichier à attacher (8M maxi)";
					else $libchamp = "Fichier attaché";
				}
	//			if ($libchamp != '') $retour .= "<label class='control-label col-sm-4' for='" . $nomchamp . "' " . $err . "><B>" . $libchamp . "</B></label>\n";
				$retour .= "<label class='control-label col-sm-4' for='" . $nomchamp . "' " . $err . "><B>" . $libchamp . "</B></label>\n";
			}
			// largeur du champ en fonction de l'affichage en ligne ou non (sauf case à cocher)
			if (!$this->inline || ($typechamp == 'Radio bouton' && $this->inline) || ($typechamp == 'Case à cocher' && $this->debinline) ) 
				$retour .= "<div class='col-sm-6'>";
			else if ($typechamp != 'Case à cocher' && ($this->debinline || $this->fininline)) $retour .= "<div class='col-sm-3'>";
		}	
		
		// traitement particulier en fonction du type de champ
		switch ($typechamp)
		{
		// Champ libre (en principe select)
		case 'Libre':
			$retour .= $dataliste;
			break; // fin
			
		// champ montant, nombre et pourcentage deviennent texte
		case 'Montant':
		case 'Nombre':
		case 'Pourcentage':
			if (isset($curchamp['decimale'])) $decimale = $curchamp['decimale'];
			else $decimale = 2;
			if (is_numeric($valeur)) $valeur = number_format($valeur, $decimale, ',', ' ');
			if ($valeur == 0) $valeur = '';
			$typechamp = 'Texte';
		case 'Texte':
			$retour .= "<INPUT class='form-control input-sm' ID='" . $nomchamp . "' TYPE=text NAME='" . $nomchamp . "' MAXLENGTH=" . $taille . " VALUE=\"" .  htmlspecialchars($valeur) ."\" " . $opt;
			if ($largeurcomp != '') $retour .= " style=\"width: " . $largeurcomp . "px;\" ";
			$retour .=  ">"; 
			break; // fin

		// champ date
		case 'Date':
			if (strstr($opt, 'readonly') != '')
				$retour .= "<INPUT class='form-control input-sm' ID='" . $nomchamp . "' " . $err . " TYPE=text NAME='" . $nomchamp . "' MAXLENGTH=20 VALUE='" . $valeur ."'" . $opt . ">";
			else
				$retour .= "<INPUT class='form-control input-sm datepicker' ID='" . $nomchamp . "' " . $err . " TYPE=text NAME='" . $nomchamp . "' MAXLENGTH=20 VALUE='" . $valeur ."'" . $opt . ">";
			break; // fin

		// champ date et heure
		case 'Date/heure':
			$retour .= "<INPUT class='form-control input-sm' ID='" . $nomchamp . "' " . $err . "TYPE=text NAME='" . $nomchamp . "' MAXLENGTH=20 VALUE='" . $valeur ."'" . $opt . ">";
			break; // fin

		// champ liste (select)
		case 'Liste':
			$retour .= "<SELECT class='form-control input-sm' ID='" . $nomchamp . "' NAME='" . $nomchamp . "' " . $opt . " >\n";
//			$tab = explode(';', $dataliste);
			$retour .= "<OPTION >\n";
			foreach ($dataliste as $cle => $element)
			{
				if ($valeur == $cle) {$opt = " selected ";} else {$opt = "";}
				$retour .= "<OPTION value=\"" . $cle . "\"" . $opt . ">" . $element . "\n";
			}
			$retour .= "</SELECT>";
			break; // fin

		// champ autocomplete
		case 'Autocomplete':
			$retour .= "<INPUT class='form-control input-sm liste-ac' ID='" . $nomchamp . "' TYPE=text NAME='" . $nomchamp . "' MAXLENGTH=" . $taille . " VALUE=\"" .  htmlspecialchars($valeur) ."\" " . $opt;
			if ($largeurcomp != '') $retour .= " style=\"width: " . $largeurcomp . "px;\" ";
			$retour .=  ">"; 
			break; // fin

		// champ liste de case à cocher
		case 'Liste case':
			if (isset($curchamp['nbinline'])) $nbinline = $curchamp['nbinline'];
			else $nbinline = 0;
			if (isset($curchamp['nbline'])) $nbline = $curchamp['nbline']*25; // 25 pixels par ligne
			else $nbline = 0;
			if ($nbinline > 0) $largeur = 100/$nbinline;
			else $largeur = 100;
			if ($valeur == '') $valeur = 'o';
			if ($nbline > 0) $retour .= "<div class='divboxfic' style='height:" . $nbline . "px;'>";
			else $retour .= "<div class='divboxfic'>";
			$cptcase = 0;
			foreach ($dataliste as $element)
			{
				$tab=explode ('=', $element);
				if (isset($tab[0]) && $tab[0] == 'checked') {$opt = " checked ";} else {$opt = "";}
				if (isset($tab[1])) {$valeur = $tab[1];} else {$valeur = 'o';}
				if (isset($tab[2])) {$libchamp = $tab[2];} else {$libchamp = '';}
				if ($cptcase == 0) $retour .= "<div class='checkbox' style='padding-top:0; margin-bottom:-10px; font-size:13px;'>";
				$retour .= "<label style='width: " . $largeur . "%;' ><INPUT TYPE='checkbox' NAME='" . $nomchamp . "[]' VALUE='" . $valeur . "' " . $opt . " />" . $libchamp . "</label>";
				$cptcase++;
				if ($cptcase >= $nbinline) {$retour .= "</div>";	$cptcase = 0;}
			}
			if ($cptcase > 0) $retour .= "</div>";
			$retour .= "</div>";
			break; // fin

		// champ case à cocher
		case 'Case à cocher':
			if ($valeur == '') $valeur = 'o';
			if (!$this->inline) // pas en ligne
			{	
				$retour .= "<div class='checkbox'><label>";
				$retour .= "<INPUT TYPE='checkbox' NAME='" . $nomchamp . "' VALUE='" . $valeur . "' " . $opt . " />" . $libinline . "</label>";
				$retour .= "</div>";
			}
			else // en ligne
			{	
				$retour .= "<label class='checkbox-inline'>";
				$retour .= "<INPUT TYPE='checkbox' NAME='" . $nomchamp . "' VALUE='" . $valeur . "' " . $opt . " />" . $libinline . "</label>";
			}
			break; // fin

		// champ radio bouton
		case 'Radio bouton':
			if(isset($curchamp['val'])) $listval=explode(";", $curchamp['val']);
			else $listval=array(1,2,3,4,5,6,7,8,9,10);
			$cpt=0;
			if ($valeur == '') {$optcase = " checked ";}
			foreach ($dataliste as $element)
			{
				if ($valeur != '') {if ($valeur == $listval[$cpt]) {$optcase = " checked ";} else {$optcase = "";}}
				if ($this->inline === true) $retour .= "<label class='radio-inline'>";
				else $retour .= "<div class='radio'><label>";
				$retour .= "<INPUT TYPE='radio' NAME='" . $nomchamp . "' VALUE='" . $listval[$cpt] . "' " . $optcase . $opt . " />" . $element . "</label>";
				if ($this->inline !== true) $retour .= "</div>";
				$optcase = '';
				$cpt++;
			}
			break; // fin

		// champ zone de texte
		case 'Zone texte':
			if ($taille < 50 || $taille > 3000) $taille = 500;
			$nbrow = round($taille/100);
			if ($nbrow > 5) $nbrow = 5;
			if ($nbrow < 2) $nbrow = 2;
			$retour .= "<TEXTAREA class='form-control input-sm' ID='" . $nomchamp . "' ROWS=" . $nbrow . " COLS=78 MAXLENGTH=" . $taille . " onKeyUp=\"limite(this," . $taille . ");\" NAME='" . $nomchamp . "' " . $opt . ">" . $valeur . "</TEXTAREA>";
			break; // fin

		// champ téléchargement d'un fichier
		case 'Fichier':
			if ($valeur == '')
			{
				$retour .= "<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"8388608\">\n";   
				$retour .= "<span><input class='form-control-file' type='file' name='nomfic[]'></span>"; 
			}
			else
			{	
				$opt = 'readonly';
				$retour .= "<INPUT class='form-control input-sm' ID='" . $nomchamp . "' TYPE=text NAME='" . $nomchamp . "' VALUE=\"" .  htmlspecialchars($valeur) ."\" " . $opt;
				$retour .=  ">"; 
			}
			break; // fin

		// champ téléchargement de plusieurs fichiers
		case 'Fichiers':
			// partie sélection de fichier
			if (!$this->lec)
			{
				$retour .= "<div class='form-group " . $class . "'>";
				$retour .= "<label class='control-label col-sm-4'><B>Nom du fichier à attacher (8M maxi) :</B></label>";
				$retour .= "<div class='col-sm-6'>";
				$retour .= "<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"8388608\">\n";   
				$retour .= "<span style='display:none;' id='myinput'>";
				$retour .= "<span><input class='form-control-file' type='file' name='nomfic[]'></span>"; 
				$retour .= "</span>";
				$retour .= "<input class='form-control-file' id='uploadid' type='button' name='add' style='width:75px;' value='Parcourir' onclick='addUpload()' />";
				$retour .= "</div></div>";
			}
			// partie liste des fichiers sélectionnés
			$retour .= "<div class='form-group'>";
			$retour .= "<label class='control-label col-sm-4'><B>Fichiers attachés :</B></label>";
			$retour .= "<div class='col-sm-6'>";
			$retour .= "<div id='mylist' class='divboxfic'>";
			$cptfic = 0;
			if ($dataliste != '')
			{
				$tabnomfic = explode(';', $dataliste);
				foreach ($tabnomfic as $nomfic)
				{
					$retour .= "<div>";
					if ($this->lec)
					{
						$retour .= "<div class='col-sm-11 form-lib-file'>" . $nomfic . "</div>";
					}
					else
					{
						$retour .= "<div id='delupload" . $cptfic . "' class='col-sm-1 form-poub-file'><a onClick=\"delUpload('" . $nomfic . "','" . $cptfic . "')\" ><img class='imgfiltre' src='images/corbeille.gif' ></a></div>";
						$retour .= "<div class='col-sm-11 form-lib-file'><a href='/" . $curchamp['repertoire'] . "/" . $nomfic . "' target='_blank'>" . $nomfic . "</a></div>";
					}
					$retour .= "</div>";
					$cptfic++;
				}
			}
			$retour .= "</div></div></div>";
			break; // fin

		// champ mot de passe
		case 'Password':
			$retour .= "<INPUT class='form-control input-sm' ID='" . $nomchamp . "' " . $err . " TYPE=password NAME='" . $nomchamp . "' MAXLENGTH=" . $taille . " VALUE='" . $valeur ."' " . $opt;
			$retour .=  ">"; 
			break; // fin

		// champ de deux listes sélectionnables en drag and drop
		case 'Sortable':
			if (isset($curchamp['largeur'])) $largeur = $curchamp['largeur'];
			else $largeur = '';
			$retour .= "<div class='form-group " . $class . "'>";
			if ($largeur == 'form') 
			{
				$retour .= "<label class='control-label col-sm-4'><B>" . $libchamp . "</B></label>\n";
				$retour .= "<div class=' row col-sm-7'>";
			}
			if (isset($curchamp['dataliste1']) || isset($curchamp['dataliste2']))
			{
				$id1 = 'box1';
				$id2 = 'box2';
				$retour .= $this->getLigne($curchamp['dataliste1'], $id1, 1);
				$retour .= $this->getLigne($curchamp['dataliste2'], $id2, 2);
				if (!$this->lec)
				{
					$retour .= "\n<script type=\"text/javascript\">\n";
					$retour .= "$(function(){";
					$retour .= "$('.sortable').mouseover(function() {\$(this).css('cursor','move');});";
					$retour .= "$('.sortable').sortable({";
					$retour .= "scroll: true,
								items: 'tr',
								connectWith: '.sortable'
								}).disableSelection();";
					$retour .= "});";
					$retour .= "function majFormSortable(type, cle) {
								var result = '';
								$('#" . $id1 . "').find('tr').each(function() { 
								if ($(this).is('[id]')) result += this.id + ';';});
								postFormSortable(type, cle, result);
								}";
					$retour .= "function majFormSortableDiff(type, cle) {
								var result = '';
								$('#" . $id1 . "').find('tr').each(function() { 
									if ($(this).is('[box2]')) result += 'A*-*' + this.id + ';';
								});
								$('#" . $id2 . "').find('tr').each(function() { 
									if ($(this).is('[box1]')) result += 'S*-*' + this.id + ';';
								});
								postFormSortable(type, cle, result);
								}";
					$retour .= "function postFormSortable(type, cle, result) {
								$.ajax({
									type: 'POST',
									url: 'maj_formsortable.php',
									data: { 'type': type,'cle': cle,'list': result },
									async: false,
									success: function(data){}
								});	}";
					$retour .= "\n</script>";
				}
			}
			$retour .= "</div>";
			if ($largeur == 'form') $retour .= "</div>";
			break; // fin
			
		default:
			$retour .= "<label class='control-label' ID='" . $nomchamp . "' " . $opt;
			if ($largeurcomp != '') $retour .= " style=\"text-align: left; font-weight:normal; width: " . $largeurcomp . "px;\" ";
			else $retour .= " style=\"text-align: left; font-weight:normal\" ";
			$retour .=  ">" . htmlspecialchars($valeur);"</label>\n"; 
			break; // fin
		}

		// possibilité d'ajouter des balises HTML en début et en fin de ligne
		if (isset($curchamp['deb']) && isset($curchamp['fin'])) 
		{
			$retour = $curchamp['deb'] . $retour . $curchamp['fin']; 
			unset($curchamp['deb']);
			unset($curchamp['fin']);
		}
		
		// Fermeture du form-group pour autres champs Sortable et Fichiers
		if ($typechamp != 'Sortable' && $typechamp != 'Fichiers') // si Sortable ou Fichiers --> on ne fait rien
		{	
			if ($typechamp == 'Case à cocher' && ($this->debinline || $this->inline)) $retour .= ''; 
			else $retour .= "</div>"; // fin partie champ
			if (!$this->inline || $this->fininline || $typechamp == 'Radio bouton') // si pas en fin ou fin de champs en ligne
				$retour .= "</div>"; // fin form-group
		}
		
		// retour du champ
		return $retour; // fin
	}
	
/******************************************************************************/
// récupération des informations pour les champs sortable
/******************************************************************************/
	private function getLigne(&$dataliste, $id, $box)
	{
		if ($box == 1) $retour = "<div class='divbox sortable col-sort-l'>";
		else $retour = "<div class='divbox sortable col-sort-r'>";
		if ($box == 1) $retour .= "<p style='color:gray;font-size:1.3em;font-weight:bold;'> Eléments selectionnés</p><table id='" . $id . "'>";
		else $retour .= "<p style='color:gray;font-size:1.3em;font-weight:bold;'> Eléments selectionnables</p><table id='" . $id . "'>";
		if ($dataliste != '') 
		{
			$tab = explode('*;*', $dataliste);
			$tabcle = explode(';', $tab[0]);
			if (isset($tab[1])) $tabval = explode(';', $tab[1]);
			else $tabval = array();
			$nbcle = count($tabcle);
			for($i=0; $i<$nbcle; $i++)
			{
				$retour .= "<tr id='" . $tabcle[$i] . "' " . $id . "='" . $box . "'>";
				$tabcol = explode('*-*', $tabval[$i]);
				$nbcol = count($tabcol);
				for($j=0; $j<$nbcol; $j++)
				{
					$retour .= "<td>&nbsp; " . $tabcol[$j] . " &nbsp;</td>";
				}
				$retour .= "</tr>";
			}
		}
		$retour .= "</table></div>"; 
		return $retour;
	}

/******************************************************************************/
//  reception des paramètres des formulaires  
/******************************************************************************/
	public function recChamp()
	{
		// gestion des champs standards
		foreach ($this->champF as $curcle => $curchamp)
		{
			global ${$curcle};
			if(isset($_REQUEST[$curcle])) ${$curcle}=$_REQUEST[$curcle];
			else ${$curcle}="";
		}
		// gestion des champs disabled non passés dans le formulaire
		if(isset($_REQUEST['dis_listechamps']))
		{
			$dislistechamps=$_REQUEST['dis_listechamps'];
			$tabchamps=explode(",",$dislistechamps);
			foreach ($tabchamps as $curchamp)
			{
				$tab=explode("dis_",$curchamp);
				$nomchamp = $tab[1];
				global ${$nomchamp};
				if(isset($_REQUEST[$curchamp])) ${$nomchamp}=$_REQUEST[$curchamp];
				else ${$nomchamp}="";
//				echo $nomchamp . '=' . ${$nomchamp} . '   ';
			}
		}
		return $this->ctlChamp($this->champF);
	}	

/******************************************************************************/
//  controle des paramètres des formulaires (appelé par recChamp)
/******************************************************************************/
	private function ctlChamp(&$champ)
	{
		$this->erreur = '';
		$this->libelle = '';
		foreach ($champ as $curcle => $curchamp)
		{
			global ${$curcle};
			$nomchampF = $curcle;
			$libchampF = $curchamp['label'];
			if (isset($curchamp['type'])) $typechampF = $curchamp['type'];
			else $typechampF = '';
			if($typechampF == 'Groupe') continue; // on ne traite pas les champs groupe de formulaire
			if (isset($curchamp['ctl'])) $flagctlF = $curchamp['ctl'];
			else $flagctlF = '';
//			echo 'champ2 ' . $curcle . "=" . ${$curcle};
			if ($flagctlF == "o" && ${$curcle} == '') {$this->erreur=$curcle; $this->libelle="Le champ doit être renseigné (" . $libchampF . ")"; break;}
			if (($typechampF == 'Montant' || $typechampF == 'Nombre' || $typechampF == 'Pourcentage') && ${$curcle} != '' )
			{
				${$curcle} = str_replace(" ", "", ${$curcle});
				${$curcle} = str_replace("€", "", ${$curcle});
				${$curcle} = str_replace("$", "", ${$curcle});
				${$curcle} = str_replace("%", "", ${$curcle});
				${$curcle} = str_replace(",", ".", ${$curcle});
				${$curcle} = preg_replace('/\s+/', '', ${$curcle});
				if (!is_numeric(${$curcle})) 
				{
					$this->erreur=$curcle; 
					if ($typechampF == 'Montant') $this->libelle="Montant incorrect"; 
					if ($typechampF == 'Nombre') $this->libelle="Nombre incorrect"; 
					if ($typechampF == 'Pourcentage') $this->libelle="Pourcentage incorrect"; 
					break;
				}
			}
			if ($typechampF == "Date")
			{
				if (${$curcle} != '' && !ctlDate(${$curcle})) {$this->erreur=$curcle; $this->libelle="Date incorrecte"; break;}
				$tempvar = 'MYSQL' . $curcle;
				global ${$tempvar};
				if (${$curcle} != "") ${$tempvar} = invDate(${$curcle});
				else ${$tempvar} = '';
				if (${$tempvar} == '0000-00-00') ${$tempvar} = '';
			}
			if ($typechampF == "Date/heure")
			{
				if (${$curcle} != '' && !ctlDateheure(${$curcle})) {$this->erreur=$curcle; $this->libelle="Date incorrecte"; break;}
				$tempvar = 'MYSQL' . $curcle;
				global ${$tempvar};
				if (${$curcle} != "") ${$tempvar} = invDateHeure(${$curcle});
				else ${$tempvar} = '';
				if (${$tempvar} == '0000-00-00 00:00:00') ${$tempvar} = '';
			}
			if ($typechampF == 'Fichier' || $typechampF == 'Fichiers')
			{
				if (isset($_FILES['nomfic']))
				{
					foreach ($_FILES['nomfic']['name'] AS $key => $value )
					{ 
		//				echo "key=" . $key . " value=" . $value;
		//				exit();
						if (isset($_FILES['nomfic']['error'][$key])) 
						{   
							switch ($_FILES['nomfic']['error'][$key])
							{   
								case 0: // OK   
									break;			 
								case 1: // UPLOAD_ERR_INI_SIZE   
									$this->erreur=$curcle;
									$this->libelle="Le fichier dépasse la limite autorisée"; 
									break;			 
								case 2: // UPLOAD_ERR_FORM_SIZE   
									$this->erreur=$curcle;
									$this->libelle="Le fichier dépasse la limite autorisée"; 
									break;			 
								case 3: // UPLOAD_ERR_PARTIAL   
									$this->erreur=$curcle;
									$this->libelle="Problème pendant le transfert, ressayez"; 
									break;			 
								case 4: // UPLOAD_ERR_NO_FILE   
									break;			 
								default: // UPLOAD_ERR_NO_FILE   
									$this->erreur=$curcle;
									$this->libelle="Erreur téléchargement fichier " . $_FILES['nomfic']['error'][$key]; 
									break;			 
							} 
						}   
					}
				}
			}	
		}
		if ($this->erreur != '') return false;
	}
	
/******************************************************************************/
// traitement des fichiers attachés
/******************************************************************************/
	public function trtFicAttache($rename, $cle, $repertoire, $bdd_nomfic, $bdd_cle, $bdd_table)	
	{
		global $listnomfic;
		$conn = database::getIntance();
		
		if ($cle != '')
		{	
			// recherche des pièces jointes existantes
			$requete = "select " . $bdd_nomfic . " from " . $bdd_table . " where " . $bdd_cle . "=\"" . $cle . "\"";
			$statement = $conn->query($requete);
			$resultat = $statement->fetch(PDO::FETCH_ASSOC);
			$tabnomfic = explode(';', $resultat[$bdd_nomfic]);
		}
		else $tabnomfic = array(); // les noms des pièces jointes ne sont stockés en bdd
		// suppression de pièces jointes existantes
		if (isset($_REQUEST['delnomfic']))
		{
			foreach ($_REQUEST['delnomfic'] AS $key => $value)
			{
				$tab = explode('*;*', $value); // numéro du fichier et nom du fichier séparé par *;*
				$cptfic=$tab[0];
				$nomfic=$tab[1];
				if (isset($tabnomfic[$cptfic]))
				{
					if ($tabnomfic[$cptfic] == $nomfic) unset($tabnomfic[$cptfic]); // suppression du fichier dans la liste
				}
				unlink($repertoire . "/" . $nomfic); // suppression physique du fichier
			}
			$listnomfic = implode(';', $tabnomfic);
		}
		// ajout des nouvelles pièces jointes
		if (isset($_FILES['nomfic']))
		{
		    foreach ($_FILES['nomfic']['name'] AS $key => $value )
			{ 
//				echo "key=" . $key . " value=" . $value;
//				exit();
//				$namefic = ctlNomFichier($_FILES['nomfic']['name'][$key]);
				$namefic = $_FILES['nomfic']['name'][$key];
				if ($_FILES['nomfic']['error'][$key] != UPLOAD_ERR_OK) continue;
				$chemin_destination = $repertoire . '/' . $namefic;   
				// test si doublon avant le move
				if ($rename == 'rename')
				{	
					if (file_exists($chemin_destination))
					{
						$path_parts = pathinfo($chemin_destination);
						if (isset($path_parts["extension"]) && $path_parts["extension"] != '')
						{
							$tab = explode(".", $path_parts["basename"]);
							$newnom = '';
							$nb=count($tab)-1;
							for($i=0; $i<$nb; $i++)
							{
								if ($newnom == '') $newnom = $tab[$i];
								else $newnom .= "." . $tab[$i];
							}
							$extension = "." . $path_parts["extension"];
						}
						else 
						{
							$newnom = $path_parts["basename"];
							$extension = '';
						}
						for ($i=1; $i<500; $i++)
						{
							$namefic = $newnom . "(" . $i . ")" . $extension;
							$newchemin = $path_parts["dirname"] . "/" . $namefic;
							if (!file_exists($newchemin)) break;
						}
						$chemin_destination = $newchemin;
					}
				}	
				move_uploaded_file($_FILES['nomfic']['tmp_name'][$key], $chemin_destination); 
				if ($listnomfic != "") $listnomfic .= ";" . $namefic;
				else $listnomfic = $namefic;
			}
		}	
	}

/******************************************************************************/
// traitement des boutons
// type de bouton 'button' 'reset' 'submit'
/******************************************************************************/
	// Ajout d'un bouton d'enchainement
	// le champ lien est une URL sauf si submit ou c'est typeaction
	public function addBouton($type, $libelle, $lien='')
	{
		$objPage = page::getPage();
		$bouton = "<input type=\"" . $type . "\" value=\"" . $libelle . "\" class=\"espace-btn btn btn-default btn-sm\"";
		if ($type == 'submit') $bouton .= " onclick=\"SubmitForm('" . $this->nomform . "','" . $lien . "')\" />";
		else 
		{
			if (strpos($lien, "?") === false) $url = $lien . "?" . $objPage->param; // ajout des paramètres d'URL 
			else $url = $lien . "&" . $objPage->param;
			// cas particulier du retour ou il faut sortir du mode onglet
			if (strpos($lien, "?menu") === false) $bouton .= " onclick=\"VersURL('" . $url . "')\" />";
			else $bouton .= " onclick=\"document.location.href='" . $url . "'\" />";
		}
		$this->bouton .= $bouton;
	}

	// Ajout d'un bouton d'enchainement spécifique
	// le champ lien est une clause onclick 
	public function addBoutonSpe($type, $libelle, $lien='')
	{
		$bouton = "<input type=\"" . $type . "\" value=\"" . $libelle . "\" class=\"espace-btn btn btn-default btn-sm\" " . $lien . " />";
		$this->bouton .= $bouton;
	}

	// affichage des boutons
	public function affBouton()
	{
		$this->tampon .= "<div class='col-md-6 col-md-offset-4'>" . $this->bouton . "</div>";
	}

/******************************************************************************/
// fin du formulaire
/******************************************************************************/
	public function finFormulaire()
	{
		if ($this->bouton != '') $this->affBouton();
		$this->tampon .= "</div>\n";
		// ajout du champ de gestion des champs disabled
		if ($this->dischamp != '')
		{
			$this->dischamp = rtrim($this->dischamp, ","); // suppression de la dernère virgule
			$this->tampon .= '<input type="hidden" value="'. $this->dischamp .'" name="dis_listechamps">';
			
		}
		$this->tampon .= "</FORM>\n";
	}
	// fin de la classe formulaire
}
