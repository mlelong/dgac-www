<?php
/* 
	class de gestion des droits d'accès et de la visibilité
	version 1.01 le 24/06/2014
*/	

class profil 
{
	public $codefonc;		/* code de la fonction, évite le repositionnement si même code (private) */
	public $idfonc;         /* id du code fonction */

//  Variables profil, visibilité et droit mis à jour par index.php
//  profil de l'utilisateur 
	public $identifiant;	/* identifiant de connexion */
	public $idnom;			/* id du nom de l'utilisateur */
	public $nom;			/* nom de l'utilisateur */
	public $prenom;			/* prénom de l'utilisateur */
	public $groupe;			/* nom du groupe principal de l'utilisateur */
	public $idgroupe;		/* id du groupe principal de l'utilisateur */
	public $idgroupes;		/* liste des groupes auxquels appartient l'utilisateur */
	public $profil;			/* type de profil */ 

//  totalité des droits de l'utilisateur
	public $droit;			/* liste des droits de l'utilisateur */
	public $listfonc;		/* liste des fonctions (commune à tous les utilisateurs) */
//listfonc="reg;dep;dom;typ;eve;con;equ;uti;rol;sta";
	
//  droits spécifiques à la fonction courante mis à jour par setDroit (private)
	public $lec;			/* autorisation de lire (true/false) */
	public $cre;			/* autorisation de créer (true/false) */
	public $maj;			/* autorisation de mise à jour (true/false) */
	public $sup;			/* autorisation de supprimer (true/false) */
	public $exe;			/* autorisation d'exécuter (true/false) */
	public $vis;			/* autorisation lec sur les autres groupes (true/false) */
	public $sui;			/* autorisation lec et maj sur les autres groupes (true/false) */
	public $adm;			/* autorisation totale sur les autres groupes (true/false) */
	
// objet sur le profil en cours
	private static $objProfil = null;
	
/******************************************************************************/
// initialisation du profil lors de la construction de la classe
/******************************************************************************/
	public function __construct()
	{
	}
	
// récupération de l'objet sur le profil courant
	public static function getProfil()
	{
		if (self::$objProfil == null) self::$objProfil = new profil();
		return self::$objProfil;		
	}

// sauvegarde de l'objet profil suite à la restauration de la session
	public static function getSession()
	{
		self::$objProfil = $_SESSION['s_profil'];
		return self::$objProfil;		
	}

// appelé lors de la connexion pour positionner les droits administrateurs 	
	public function setProfilAdmin($pwd)
	{
		$conn = database::getIntance();
		// récupération du mot de passe admin
		$requete = "select CFG_ADMIN from config where CFG_CLE=1";
		$statement = $conn->query($requete);
		$resultat = $statement->fetch(PDO::FETCH_ASSOC);
		$pwdadmin = $resultat['CFG_ADMIN'];
		if ($pwdadmin == '') $pwdadmin = 'admin'; // mot de passe par défaut
		if ($pwdadmin != $pwd) return false;
		// initialisation du profil
		$this->identifiant = $identifiant;
		$this->nom = $identifiant;
		$this->prenom = '';
		$this->adm = true;
		// sauvegarde du profil en session
		$_SESSION['s_profil'] = $this;
	}
	
	// appelé dans la page droit.php pour positionner les droits en fonction du code fonction 	
	public function setProfil($resultat)
	{
		$conn = database::getIntance();
		// information de l'utilisateur
		$this->identifiant = $resultat['UTI_IDENTIFIANT'];
		$this->idnom = $resultat['UTI_CLE'];
		$this->nom = $resultat['UTI_NOM'];
		$this->prenom = $resultat['UTI_PRENOM'];
		$this->droit = '';  
		// recalcul des droits de l'utilisateur si besoin
		if ($this->droit == '')
		{
			$requete = "select PRO_NOMPROFIL, PRO_LISTDROIT from profil, utilisateur_profil 
						where RUP_IDPROFIL=PRO_CLE and RUP_IDUTILISATEUR=" . $this->idnom . " union " .
						"select PRO_NOMPROFIL, PRO_LISTDROIT from profil, groupe_profil 
						where RGP_IDPROFIL=PRO_CLE and RGP_IDGROUPE in 
						(select RGU_IDGROUPE from groupe_utilisateur where RGU_IDUTILISATEUR=" . $this->idnom . ")";
			$statement = $conn->query($requete);
			$listdroit='';
			while ($row = $statement->fetch(PDO::FETCH_ASSOC))
			{
				if ($row['PRO_LISTDROIT'] != '') $listdroit = $row['PRO_LISTDROIT'] | $listdroit; 
			}
			$this->droit = $listdroit; 
		}
		// liste des fonctions
		$requete = "select CFG_DEFDROIT, CFG_LISTFONC from config where CFG_CLE=1";
		$statement = $conn->query($requete);
		$resultat = $statement->fetch(PDO::FETCH_ASSOC);
		$this->listfonc = $resultat['CFG_LISTFONC'];
		// on ajoute le profil par défaut
		if ($resultat['CFG_DEFDROIT'] != '') $this->droit = $resultat['CFG_DEFDROIT'] | $this->droit; 
		// liste des équipes de l'utilisteur
		$cpt = 0;
		$requete = "select GRU_CLE from groupe, groupe_utilisateur
					where GRU_CLE=RGU_IDGROUPE and RGU_RELATION='a' and RGU_IDUTILISATEUR=\"" . $this->idnom . "\" order by 1";
		$statement = $conn->query($requete);
		$tabgroupemaj = array();
		while ($row = $statement->fetch(PDO::FETCH_ASSOC))
		{
			$tabgroupemaj[$cpt] = $row['GRU_CLE'];
			$cpt++;
		}
		$this->idgroupes = implode(';', $tabgroupemaj);
		// on sauvegarde le profil dans la session
		$_SESSION['s_profil'] = $this; 
	}
	
// appelé par ctlFonc pour positionner les droits en fonction du code fonction 	
	private function setDroit($codefonc, $typeaction)
	{
		$conn = database::getIntance();
		// cas particulier admin
		if ($this->identifiant == 'admin')
		{
			$this->lec=true;$this->cre=true;$this->maj=true;$this->sup=true;$this->exe=true;$this->vis=true;$this->sui=true;$this->adm=true;
			return true;

		}	
//		if ($this->codefonc == $codefonc) return true;
		$this->codefonc = $codefonc;
		$tabfonc=explode(';', $this->listfonc);
		$numpage=array_search($codefonc, $tabfonc);
		// la fonction n'est pas dans la liste des fonctions en session
		// on recharge au cas où une fonction aurait été ajoutée lors d'une mise à jour de l'application
		if ($numpage === false) 
		{
			// on recharge la liste des fonctions
			$requete = "select CFG_DEFDROIT, CFG_LISTFONC from config where CFG_CLE=1";
			$statement = $conn->query($requete);
			$resultat = $statement->fetch(PDO::FETCH_ASSOC);
			$this->listfonc = $resultat['CFG_LISTFONC'];
			// on vérifie si la fonction existe
			$tabfonc=explode(';', $this->listfonc);
			$numpage=array_search($codefonc, $tabfonc);
			// fonction connue -> on sauvegarde le profil dans la session
			if ($numpage === true) $_SESSION['s_profil'] = $this; 
			else return false;
		}
		$this->idfonc = $numpage+1; // l'id de la fonction en bdd est dans l'ordre 
		$sui=0x01; $exe=0x02; $sup=0x04; $maj=0x08;	$cre=0x10; $lec=0x20; $vis=0x40; $adm=0x80;
		$curdroit=ord($this->droit[$numpage]);
		if ($curdroit & $lec) $this->lec=true; else $this->lec=false;
		if ($curdroit & $cre) $this->cre=true; else $this->cre=false;
		if ($curdroit & $maj) $this->maj=true; else $this->maj=false;
		if ($curdroit & $sup) $this->sup=true; else $this->sup=false;
		if ($curdroit & $exe) $this->exe=true; else $this->exe=false;
		if ($curdroit & $vis) $this->vis=true; else $this->vis=false;
		if ($curdroit & $sui) $this->sui=true; else $this->sui=false;
		if ($curdroit & $adm) $this->adm=true; else $this->adm=false;
		if ($this->cre === true && $this->maj === false) // il faut pouvoir afficher le bouton ENVOYER si cre mais pas maj
		{
			if(isset($typeaction) && ($typeaction='' || $typeaction="creation")) $this->maj=true; 
		}
		return true;
	}
	
// appelé par ctlFonc si le code fonction n'existe pas	
	private function resetDroit()
	{
		$this->lec=false;
		$this->cre=false;
		$this->maj=false;
		$this->sup=false;
		$this->exe=false;
		$this->vis=false;
		$this->sui=false;
		$this->adm=false;
		$this->codefonc='';
	}
	
// Appelé dans les pages qui nécessitent un contrôle d'autorisation de lecture , création, modification ou suppression
	public function ctlDroit($typeaction, $codeaction, $cle='')
	{
		$errctl = false;
		switch ($codeaction)
		{
			// Traitement des pages avec une liste
			case 'tableau':
				if ($typeaction == 'suppression' && !$this->sup) $errctl = true;
				else if (!$this->lec) $errctl = true;
				break; 
				
			// Traitement des pages avec un formulaire 
			case 'formulaire':
				if ($typeaction == 'creation' && !$this->cre) $errctl = true;
				// si on peut lire c'est OK car pas modifiable dans l'interface
				if ($typeaction == 'modification' && !$this->lec) $errctl = true; 
				if ($typeaction == 'reception')
				{
					if ($cle == '' && !$this->cre) $errctl = true;
					else if (!$this->maj) $errctl = true;
				}
				break; 
				
			// Traitement par défaut
			default:
				break; 
		}
		if ($errctl === true) $this->errDroit();
		else return true;
	}

// // test pour savoir si la fonction est autorisée pour ce profil 
	public function ctlFonc($codefonc, $typeaction)
	{
		if (isset($codefonc) && $codefonc != '*') 
		{
			if (!$this->setDroit($codefonc, $typeaction)) $this->errFonc();
			if ($this->lec === false) $this->errDroit();
		}
		else if ($codefonc == '*') 
		{
			$this->lec=true;
			$this->cre=true;
			$this->maj=true;
			$this->sup=true;
			$this->exe=false;
			$this->vis=false;
			$this->sui=false;
			$this->adm=false;
		}
		else $this->resetDroit();
	}

// problème de droit en lecture
	private function errDroit()
	{
		$objPage = page::getPage();
		$objPage->debPage('erreur');
		$objPage->tampon .= "<h4>Vous n'avez pas les droits suffisants. Veuillez contacter votre administrateur</h4>";
		$objPage->finPage();
		exit();
	}

// fonction inconnue 
	private function errFonc()
	{
		$objPage = page::getPage();
		$objPage->debPage('erreur');
		$objPage->tampon .= "<h4>Problème sur une fonction. Veuillez contacter votre administrateur</h4>";
		$objPage->finPage();
		exit();
	}
}
