<?php
/* 
	classe de mise à jour des budgets pour une commande (commande et paiement)
	
	mise à jour du budget global et du budget du projet
	si l'année ou le programme LOLF change -> déplacer tous les montants vers le nouveau budget
	si l'EJ passe de renseigné à non renseigné -> déplacer les AE consommés vers prévisonnels
	si l'EJ passe de non renseigné à renseigné -> déplacer les AE prévisonnels vers consommés
	si la DP passe de renseignée à non renseignée -> déplacer les CP consommés vers prévisonnels
	si la DP passe de non renseignée à renseignée -> déplacer les CP prévisonnels vers consommés

	Fonctions public de gestion des commandes, des lignes de commande et des paiements
	**********************************************************************************
	cmd_maj_commande 	-> majCommande(numerosif, tva)
	cmd_list_commande 	-> supCommand()
	cmd_maj_ligne 		-> majLigneCommande(totalht, flagimputation)
	cmd_list_ligne 		-> supLigneCommande()
*/	

class commande 
{
	private $debug=3;   	    /* niveau du mode debug */
	private $conn;     	    	/* connexion à la base de données */
	private $typeaction;       	/* type d'action cre=création maj=mise à jour sup=suppression */
	private $typedemande;       /* type de demande commande ou paiement */
	private $cmd_cle;			/* cle de la commande */
	private $lcmd_cle;			/* cle de la ligne de commande */
	private $coefrev;       	/* coefficient de révision */
// informations sur la commande	avant mise à jour
	private $av_idmarche;   	/* clé du marché */
	private $av_idprojet;   	/* clé du projet */
	private $av_idbudget;   	/* clé du budget */
	private $av_idlignebudget;  /* clé de la ligne budgétaire */
	private $av_numerosif;  	/* numéro EJ ou de DP dans le SIF */
	private $av_programme;  	/* programme lolf 612 613 614 */
	private $av_coefrev;    	/* coefficient de révision */
	private $av_dateej;     	/* date de l'EJ */
	private $av_datecre;    	/* date de création de la demande */
	private $av_datepre;    	/* date prévisionnelle de passage de la demande */
	private $av_datecmd;    	/* date réelle de passage de la commande */
	private $av_datepaie;   	/* date réelle de passage de la demande de paiement */
	private $av_datereel;   	/* date réelle commande ou paiement */
	private $av_tva;    		/* taux de TVA */
	private $av_totalht;  		/* montant total HT */
	private $av_invesht;  		/* montant investissement HT */
	private $av_foncht;    		/* montant fonctionnement HT */
	private $av_totalttc;  		/* montant total TTC */
	private $av_investtc;  		/* montant investissement TTC */
	private $av_foncttc;    	/* montant fonctionnement TTC */
	private $av_p_totalht;  	/* montant total HT */
	private $av_p_invesht;  	/* montant investissement HT */
	private $av_p_foncht;    	/* montant fonctionnement HT */
	private $av_p_totalttc;  	/* montant total TTC */
	private $av_p_investtc;  	/* montant investissement TTC */
	private $av_p_foncttc;    	/* montant fonctionnement TTC */
// informations sur la commande	après mise à jour
	private $ap_idmarche;   	/* clé du marché */
	private $ap_idprojet;   	/* clé du projet */
	private $ap_idbudget;   	/* clé du budget */
	private $ap_idlignebudget;  /* clé de la ligne budgétaire */
	private $ap_numerosif;  	/* numéro EJ ou de DP dans le SIF */
	private $ap_programme;  	/* programme lolf 612 613 614 */
	private $ap_coefrev;    	/* coefficient de révision */
	private $ap_datecre;    	/* date de création de la demande */
	private $ap_datepre;    	/* date prévisionnelle de passage de la demande */
	private $ap_datecmd;    	/* date réelle de passage de la commande */
	private $ap_datepaie;   	/* date réelle de passage de la demande de paiement */
	private $ap_datereel;   	/* date réelle commande ou paiement */
	private $ap_tva;    		/* taux de TVA */
	private $ap_totalht;  		/* montant total HT */
	private $ap_invesht;  		/* montant investissement HT */
	private $ap_foncht;    		/* montant fonctionnement HT */
	private $ap_totalttc;  		/* montant total TTC */
	private $ap_investtc;  		/* montant investissement TTC */
	private $ap_foncttc;    	/* montant fonctionnement TTC */
	private $ap_p_totalht;  	/* montant total HT */
	private $ap_p_invesht;  	/* montant investissement HT */
	private $ap_p_foncht;   	/* montant fonctionnement HT */
	private $ap_p_totalttc; 	/* montant total TTC */
	private $ap_p_investtc; 	/* montant investissement TTC */
	private $ap_p_foncttc;    	/* montant fonctionnement TTC */
// informations sur la ligne de commande avant mise à jour
	private $lav_flagimputation; /* Type d'imputation (F/I) */
	private $lav_totalht;    	/* montant HT */
// informations sur la ligne de commande après mise à jour
	private $lap_flagimputation; /* Type d'imputation (F/I) */
	private $lap_totalht;    	/* montant HT */
// montant budget 
	private $previnves=0;		/* AE ou CP prévisionnels investissement */
	private $prevfonct=0;		/* AE ou CP prévisionnels fonctionnement */
	private $consinves=0;		/* AE ou CP consommés investissement */
	private $consfonct=0;		/* AE ou CP consommés fonctionnement */
	
	public  $erreur; 			/* erreur lors de la construction */
	public  $libelle; 			/* libellé si erreur */
	public  $errbdd; 			/* erreur bdd */

/******************************************************************************/
// initialisation de la commande lors de la construction de la classe
// récupération en bdd des informations sur la commande ou le paiement
/******************************************************************************/
	public function __construct($conn, $typeaction, $typedemande, $cmd_cle='', $xcmd_cle='')
	{
		$this->traceCommande(1,"**********************************************************************");
		$this->traceCommande(1,"initialisation objet commande typeaction=".$typeaction." typedemande=".$typedemande." cmd_cle=".$cmd_cle." xcmd_cle=".$xcmd_cle);
		$this->traceCommande(1,"**********************************************************************");
		$this->erreur = false;
		$this->conn = $conn;
		if ($typeaction != 'cre' && $typeaction != 'maj' && $typeaction != 'sup') 
		{
			$this->libelle = "Typeaction incorrect " . $typeaction;
			$this->erreur = true;
		}	
		$this->typeaction = $typeaction;
		if ($typedemande != 'commande' && $typedemande != 'ligne commande')
		{
			$this->libelle = "Typedemande incorrect " . $typedemande;
			$this->erreur = true;
		}	
		$this->typedemande = $typedemande;
		// récupération des informations  en bdd en fonction du type de demande et du type d'action
		//
		// gestion des commandes
		//
		if ($typedemande == 'commande')
		{
			$this->cmd_cle = $cmd_cle;
			$this->lcmd_cle = '';
			// si clé non renseignée c'est une création
			if ($this->cmd_cle == '') $this->typeaction = 'cre';
			// sinon récupération des informations de la commande
			else
			{
				if (!$this->getCommande())
				{
					$this->libelle = "Erreur sur récupération de la commande avant mise à jour";
					$this->erreur = true;
				}
			}
		}
		
		//
		// gestion des ligne de commande
		//
		else if ($typedemande == 'ligne commande')
		{
			$this->cmd_cle = $cmd_cle;
			$this->lcmd_cle = $xcmd_cle;
			// récupération des informations de la commande
			if (!$this->getCommande())
			{
				$this->libelle = "Erreur sur récupération de la commande avant mise à jour";
				$this->erreur = true;
			}
			// si clé non renseignée c'est une création
			if ($this->lcmd_cle == '') $this->typeaction = 'cre';
			// sinon récupération des informations de la ligne de commande
			else
			{
				if (!$this->getLigneCommande())
				{
					$this->libelle = "Erreur sur récupération de la ligne commande avant mise à jour";
					$this->erreur = true;
				}
			}
		}
		
		//
		// ni commande, ni ligne de commande
		//
		else $this->erreur = true;

		if ($this->libelle != '') $this->traceCommande(1,$this->libelle);
		$this->coefrev = 0; // pas calculé
	}
	
/******************************************************************************/
// Récupération des informations de la commande 
// stockage des informations dans les zones avant et après 
/******************************************************************************/
	private function getCommande()
	{
		// informations sur la commande
		$requete = "select CMD_IDMARCHE, CMD_IDPROJET, CMD_IDBUDGET, CMD_IDLIGNEBUDGET,
					CMD_TVA, CMD_NUMEROSIF, CMD_PROGRAMME, CMD_COEFREV, 
					CMD_TOTALHT, CMD_INVESHT, CMD_FONCHT, 
					CMD_TOTALTTC, CMD_INVESTTC, CMD_FONCTTC,
					CMD_P_TOTALHT, CMD_P_INVESHT, CMD_P_FONCHT, 
					CMD_P_TOTALTTC, CMD_P_INVESTTC, CMD_P_FONCTTC,
					DATE_FORMAT(CMD_DATECMD, '%Y-%m-%d') AS CMD_DATEEJ,
					DATE_FORMAT(CMD_DATECRE, '%Y-%m-%d') AS CMD_DATECRE, 
					DATE_FORMAT(CMD_DATEPRE, '%Y-%m-%d') AS CMD_DATEPRE, 
					DATE_FORMAT(CMD_DATECMD, '%Y-%m-%d') AS CMD_DATECMD 
					from commande where CMD_CLE=\"" . $this->cmd_cle . "\"";
		$statement = $this->conn->query($requete);
		if (!$statement) {$this->traceErrBdd(); return false;} 
		if ($statement->rowCount() == 0) return false;
		$resultat = $statement->fetch(PDO::FETCH_ASSOC);
		// stockage des informations avant mise à jour
		$this->av_idmarche = $resultat['CMD_IDMARCHE'];
		$this->av_idprojet = $resultat['CMD_IDPROJET'];
		$this->av_idbudget = $resultat['CMD_IDBUDGET'];
		$this->av_idlignebudget = $resultat['CMD_IDLIGNEBUDGET'];
		$this->av_numerosif = $resultat['CMD_NUMEROSIF'];
		$this->av_programme = $resultat['CMD_PROGRAMME'];
		$this->av_coefrev = $resultat['CMD_COEFREV'];
		$this->av_dateej = $resultat['CMD_DATEEJ'];
		$this->av_datecre = $resultat['CMD_DATECRE'];
		$this->av_datepre = $resultat['CMD_DATEPRE'];
		$this->av_datecmd = $resultat['CMD_DATECMD'];
		$this->av_datereel = $this->av_datecmd;
		$this->av_tva = $resultat['CMD_TVA'];
		$this->av_totalht = $resultat['CMD_TOTALHT'];
		$this->av_invesht = $resultat['CMD_INVESHT'];
		$this->av_foncht = $resultat['CMD_FONCHT'];
		$this->av_totalttc = $resultat['CMD_TOTALTTC'];
		$this->av_investtc = $resultat['CMD_INVESTTC'];
		$this->av_foncttc = $resultat['CMD_FONCTTC'];
		$this->av_p_totalht = $resultat['CMD_P_TOTALHT'];
		$this->av_p_invesht = $resultat['CMD_P_INVESHT'];
		$this->av_p_foncht = $resultat['CMD_P_FONCHT'];
		$this->av_p_totalttc = $resultat['CMD_P_TOTALTTC'];
		$this->av_p_investtc = $resultat['CMD_P_INVESTTC'];
		$this->av_p_foncttc = $resultat['CMD_P_FONCTTC'];
		// stockage des mêmes informations pour comparaison après mise à jour
		// sauf dateej
		$this->ap_idmarche = $resultat['CMD_IDMARCHE'];
		$this->ap_idprojet = $resultat['CMD_IDPROJET'];
		$this->ap_idbudget = $resultat['CMD_IDBUDGET'];
		$this->ap_idlignebudget = $resultat['CMD_IDLIGNEBUDGET'];
		$this->ap_numerosif = $resultat['CMD_NUMEROSIF'];
		$this->ap_programme = $resultat['CMD_PROGRAMME'];
		$this->ap_coefrev = $resultat['CMD_COEFREV'];
		$this->ap_datecre = $resultat['CMD_DATECRE'];
		$this->ap_datepre = $resultat['CMD_DATEPRE'];
		$this->ap_datecmd = $resultat['CMD_DATECMD'];
		$this->ap_datereel = $this->ap_datecmd;
		$this->ap_tva = $resultat['CMD_TVA'];
		$this->ap_totalht = $resultat['CMD_TOTALHT'];
		$this->ap_invesht = $resultat['CMD_INVESHT'];
		$this->ap_foncht = $resultat['CMD_FONCHT'];
		$this->ap_totalttc = $resultat['CMD_TOTALTTC'];
		$this->ap_investtc = $resultat['CMD_INVESTTC'];
		$this->ap_foncttc = $resultat['CMD_FONCTTC'];
		$this->ap_p_totalht = $resultat['CMD_P_TOTALHT'];
		$this->ap_p_invesht = $resultat['CMD_P_INVESHT'];
		$this->ap_p_foncht = $resultat['CMD_P_FONCHT'];
		$this->ap_p_totalttc = $resultat['CMD_P_TOTALTTC'];
		$this->ap_p_investtc = $resultat['CMD_P_INVESTTC'];
		$this->ap_p_foncttc = $resultat['CMD_P_FONCTTC'];
		// trace
		$this->traceCommande(1,"Récupération des informations via getCommande");
		$this->traceCommande(1,"     av_idmarche=".$this->av_idmarche." av_idprojet=".$this->av_idprojet." av_programme=".$this->av_programme." av_tva=".$this->av_tva);
		$this->traceCommande(2,"     av_idbudget=".$this->av_idbudget." av_idlignebudget=".$this->av_idlignebudget." av_numerosif=".$this->av_numerosif
							." av_datecre=".$this->av_datecre." av_datepre=".$this->av_datepre." av_datereel=".$this->av_datereel);
		$this->traceCommande(3,"     av_totalht=".$this->av_totalht." av_invesht=".$this->av_invesht." av_foncht=".$this->av_foncht
							." av_totalttc=".$this->av_totalttc." av_investtc=".$this->av_investtc." av_foncttc=".$this->av_foncttc);
		// retour
		return true;
	}

/******************************************************************************/
// Récupération des informations de la ligne de commande 
// stockage des informations dans la zone avant 
/******************************************************************************/
	private function getLigneCommande()
	{
		$requete = "select LCMD_FLAGIMPUTATION, LCMD_TOTALHT from commande_ligne where LCMD_CLE=\"" . $this->lcmd_cle . "\"";
		$statement = $this->conn->query($requete);
		$resultat = $statement->fetch(PDO::FETCH_ASSOC);
		// stockage des informations avant mise à jour
		$this->lav_flagimputation = $resultat['LCMD_FLAGIMPUTATION'];
		$this->lav_totalht = $resultat['LCMD_TOTALHT'];
		// stockage des mêmes informations pour comparaison après mise à jour
		$this->lap_flagimputation = $resultat['LCMD_FLAGIMPUTATION'];
		$this->lap_totalht = $resultat['LCMD_TOTALHT'];
		// trace
		$this->traceCommande(1,"Récupération des informations via getLigneCommande lav_flagimputation=".$this->lav_flagimputation." lav_totalht=".$this->lav_totalht);
		// retour
		return true;
	}
	
/******************************************************************************/
// calcul du coefficient de révision d'un marche en fonction de la date
// stockage pour calcul et retour de la valeur
/******************************************************************************/
	public function getRevision($date='',$force='')
	{
		if ($this->coefrev != 0 && $force != 'force') return $this->coefrev; // déjà calculé
		if ($force != 'force') return $this->ap_coefrev; 
		
		if ($date != '')
		{
			$tab = explode('-', $date);
			if (count($tab) != 3)
			{
				$tab = explode('/', $date);
				if (count($tab) != 3) return false;
			}
			if (!checkdate($tab[1],$tab[2],$tab[0])) return false; // check sur mois, jour, annee
			$curdate = $date;
		}
		else
		{
			if ($this->av_dateej != '' && $this->av_dateej != '0000-00-00') $curdate = $this->av_dateej;
			else if ($this->av_datecmd != '' && $this->av_datecmd != '0000-00-00') $curdate = $this->av_datecmd;
			else if ($this->av_datepre != '' && $this->av_datepre != '0000-00-00') $curdate = $this->av_datepre;
			else if ($this->av_datecre != '' && $this->av_datecre != '0000-00-00') $curdate = $this->av_datecre;
			else $curdate = date("Y-m-d");
		}	
		$requete = "select MREV_COEFREV, MREV_DATEREV from marche_revision 
					where MREV_IDMARCHE=\"" . $this->av_idmarche . "\" and MREV_DATEREV <= \"" . $curdate . "\"
					order by MREV_DATEREV desc";
		$statement = $this->conn->query($requete);
		$resultat = $statement->fetch(PDO::FETCH_ASSOC);
		$mrev_coefrev = $resultat['MREV_COEFREV'];
		if ($mrev_coefrev == '' || $mrev_coefrev == 0) $mrev_coefrev = 1;
		// sauvegarde du coefficient
		$this->ap_coefrev = $mrev_coefrev;
		$this->coefrev = $mrev_coefrev;
		$this->traceCommande(1,"getRevision mrev_coefrev=".$mrev_coefrev." av_idmarche=".$this->av_idmarche." curdate=".$curdate);
		return $mrev_coefrev;
	}
	
/******************************************************************************/
// Récupération de la tva courante
/******************************************************************************/
	public function getTva()
	{
		return $this->av_tva; 
	}
	
/******************************************************************************/
// Récupération de l'ID du budget global (appelé par cmd_maj_commande)
// Stockage des informations dans la zone après pour comparaison des changements
/******************************************************************************/
	public function getIdBudget($datecre, $datepre, $datesif, $programme)
	{
		// stockage des informations 
		if ($datecre == '') $this->ap_datecre = '0000-00-00';
		else $this->ap_datecre = invDate($datecre);
		if ($datepre == '') $this->ap_datepre = '0000-00-00';
		else $this->ap_datepre = invDate($datepre);
		if ($datesif == '') $this->ap_datereel = '0000-00-00';
		else $this->ap_datereel = invDate($datesif);
		$this->ap_datecmd = $this->ap_datereel;
		$this->ap_programme = $programme;
		// calcul de l'année budgétaire 
		if ($this->ap_datereel != '0000-00-00') $curdate = $this->ap_datereel;
		else if ($this->ap_datepre != '0000-00-00') $curdate = $this->ap_datepre;
		else if ($this->ap_datecre != '0000-00-00') $curdate = $this->ap_datecre;
		else $curdate = date("Y-m-d");
		$tab = explode('-', $curdate); 
		$curdate = $tab[0];
		// sélection du budget global pour l'année budgétaire
		$requete = "select BUD_CLE from budget where BUD_ANNEE=\"" . $curdate . "\" and BUD_PROGRAMME=\"" . $programme . "\"";
		$this->traceCommande(1,"Récupération du budget via getIdBudget datecre=".$datecre." datepre=". $datepre." datesif=". $datesif." programme=".$programme." curdate=".$curdate);
		$statement = $this->conn->query($requete);
		if (!$statement) return false; 
		$resultat = $statement->fetch(PDO::FETCH_ASSOC);
		if ($statement->rowCount() == 0) return false;
		else 
		{
			$this->ap_idbudget = $resultat['BUD_CLE'];
			$this->traceCommande(1,"Retour budget via getIdBudget ap_idbudget=".$this->ap_idbudget);
			return $this->ap_idbudget;
		}
	}
	
/******************************************************************************/
// Récupération de l'ID de la ligne budgétaire (appelé par cmd_maj_commande)
// Stockage des informations dans la zone après pour comparaison des changements
/******************************************************************************/
	public function getIdLigneBudget($idbudget, $idprojet)
	{
		$this->ap_idprojet = $idprojet;
		// sélection du budget du projet pour le budget global correspondant
		$requete = "select BUDL_CLE	from budget_ligne where BUDL_IDBUDGET=\"" . $idbudget . "\" and BUDL_IDPROJET=\"" . $idprojet . "\"";
		$this->traceCommande(1,"Récupération du budget projet via getIdLigneBudget idbudget=".$idbudget." idprojet=".$idprojet);
		$statement = $this->conn->query($requete);
		if (!$statement) return false; 
		$resultat = $statement->fetch(PDO::FETCH_ASSOC);
		if ($statement->rowCount() == 0) 
		{
			$this->ap_idlignebudget = 0;
			$this->traceCommande(1,"Retour budget projet via getIdLigneBudget (pas de ligne budgétaire) ap_idlignebudget=0");
			return false;
		}	
		else 
		{
			$this->ap_idlignebudget = $resultat['BUDL_CLE'];
			$this->traceCommande(1,"Retour budget projet via getIdLigneBudget ap_idlignebudget=".$this->ap_idlignebudget);
			return $this->ap_idlignebudget;
		}
	}

/******************************************************************************/
// Récupération du programme courant
/******************************************************************************/
	public function getProgramme()
	{
		return $this->av_programme; 
	}
	
/******************************************************************************/
// Récupération de l'ID courant du projet
/******************************************************************************/
	public function getIdProjet()
	{
		return $this->av_idprojet; 
	}
	
/******************************************************************************/
// Récupération du libellé d'erreur
/******************************************************************************/
	public function getLibelle()
	{
		return $this->libelle; 
	}
	
/******************************************************************************/
// positionnement de l'id du marché
/******************************************************************************/
	public function setMarche($idmarche)
	{
		$this->av_idmarche = $idmarche; 
		$this->ap_idmarche = $idmarche; 
	}
	
/******************************************************************************/
// positionnement de l'id du budget
/******************************************************************************/
	public function setIdBudget($idbudget)
	{
		$this->ap_idbudget = $idbudget;
	}
	
/******************************************************************************/
// positionnement de l'id de la ligne budgétaire
/******************************************************************************/
	public function setIdLigneBudget($idlignebudget)
	{
		$this->ap_idlignebudget = $idlignebudget;
	}
	
/******************************************************************************/
// mise à jour des budgets suite à une création ou une mise à jour d'une commande
// pas utile d'aligner les budgets lors d'une création de commande
// les montants ne bougent pas au niveau de la commande
// getIdBudget($datecre, $datepre, $datesif, $programme) 
// getIdLigneBudget($idbudget, $idprojet)
// la TVA reste à positionner dans la zone après
/******************************************************************************/
	public function majCommande($numerosif, $tva)
	{
		$this->traceCommande(1,"majBudgetCommande numerosif=".$numerosif." tva=".$tva);
		// pas de budget lors de la création de la commande
		if ($this->cmd_cle == '') return true;

		// récupération des données 
		$this->ap_numerosif = $numerosif;
		$this->ap_tva = $tva;

		// contrôle sur la révision et la tva
		$this->ap_coefrev = $this->getRevision();
		if ($this->av_coefrev != $this->ap_coefrev || $this->av_tva != $this->ap_tva)
		{
			if (!$this->ctlCommande()) return false;
		}
		// mise à jour des montants du budget global et du budget projet
		else
		{
			if (!$this->majBudget()) return false;
		}	
		return true;
	}
	
/******************************************************************************/
// Suppression d'une commande et alignement des budgets
/******************************************************************************/
	public function supCommande()
	{
		// Mise à 0 des montants pour suppression dans les budgets
		$this->ap_numerosif = $this->av_numerosif;
		$this->ap_totalttc = 0;
		$this->ap_investtc = 0;
		$this->ap_foncttc = 0;
		$this->ap_p_totalttc = 0;
		$this->ap_p_investtc = 0;
		$this->ap_p_foncttc = 0;
		
		// mise à jour des montants du budget global et du budget projet
		$this->traceCommande(1,"supCommande");
		if (!$this->majBudget()) return false;

		// suppression de la commande
		$requete = "delete from commande where CMD_CLE=\"" . $this->cmd_cle . "\"";
		$statement = $this->conn->query($requete);
		if (!$statement) return false; 

		// suppression des paiements de la commande
		$requete = "delete from commande_paiement where PCMD_IDCMD=\"" . $this->cmd_cle . "\"";
		$statement = $this->conn->query($requete);
		if (!$statement) return false; 

		return true;
	}
	
/******************************************************************************/
// appelé par cmd_maj_ligne
// mise à jour de la commande en BDD si le montant HT ou l'imputation de la ligne ont changé
// mise à jour des budgets suite à une création ou une mise à jour d'une ligne de commande
/******************************************************************************/
	public function majLigneCommande($totalht, $flagimputation)
	{
		$this->lap_totalht = $totalht;
		$this->lap_flagimputation = $flagimputation;;
		$this->traceCommande(1,"majLigneCommande totalht=".$totalht." flagimputation=".$flagimputation);
		// création d'une ligne de commande, on ajoute la totalité du montant de la ligne
		if ($this->typeaction == 'cre')
		{
			if ($flagimputation == 'F') $this->ap_foncht += $totalht;
			else $this->ap_invesht += $totalht;
			$this->ap_totalht += $totalht;
			$this->traceCommande(1,"     majLigneCommande création imputation ap_invesht=".$this->ap_invesht." ap_foncht=".$this->ap_foncht." ap_totalht=".$this->ap_totalht);
		}
		
		// modification d'une ligne de commande
		else
		{
			// imputation modifiée
			if ($this->lav_flagimputation != $flagimputation)
			{
				// investissement devient fonctionnement
				$this->traceCommande(1,"     majLigneCommande modification imputation ap_invesht=".$this->ap_invesht." ap_foncht=".$this->ap_foncht." lav_totalht=".$this->lav_totalht);
				if ($flagimputation == 'F') 
				{
					$this->ap_invesht -= $this->lav_totalht; 
					$this->ap_foncht += $this->lav_totalht; 
				}
				// fonctionnement devient investissement 
				else
				{
					$this->ap_invesht += $this->lav_totalht; 
					$this->ap_foncht -= $this->lav_totalht; 
				}
			}
			// calcul de la différence et cumul sur les montants HT
			$diffht = $totalht - $this->lav_totalht;
			if ($flagimputation == 'F') $this->ap_foncht += $diffht;
			else $this->ap_invesht += $diffht;
			$this->ap_totalht += $diffht; 
			$this->traceCommande(1,"     majLigneCommande nouveaux montants ap_foncht=".$this->ap_foncht." ap_invesht=".$this->ap_invesht." ap_totalht=".$this->ap_totalht);
		}
		
		// calcul des montants TTC
		$this->calculTva();
		
		// mise à jour des montants de la commande
		if (!$this->majMontantCommande()) return false;
		
		// mise à jour des montants du budget global et du budget projet
		if (!$this->majBudget()) return false;
		return true; 
	}
	
/******************************************************************************/
// Suppression d'une ligne de commande et alignement de la commande et des budgets
/******************************************************************************/
	public function supLigneCommande()
	{
		$this->traceCommande(1,"supLigneCommande  avant maj ap_foncht=".$this->ap_foncht." ap_invesht=".$this->ap_invesht." lav_totalht=".$this->lav_totalht);
		// réduction du montant de fonctionnement ou d'investissement de la commande
		if ($this->lav_flagimputation == 'F') $this->ap_foncht -= $this->lav_totalht;
		else $this->ap_invesht -= $this->lav_totalht; 
		
		// réduction du total de la commande
		$this->ap_totalht -= $this->lav_totalht;
		
		// calcul des montants TTC de la commande après suppression
		$this->calculTva();
		
		// mise à jour des montants de la commande
		if (!$this->majMontantCommande()) return false;
		
		// mise à jour des montants du budget global et du budget projet
		if (!$this->majBudget()) return false;

		// suppression de la ligne de commande
		$requete = "delete from commande_ligne where LCMD_CLE=\"" . $this->lcmd_cle . "\"";
		$statement = $this->conn->query($requete);
		if (!$statement) return false; 
		return true; 
	}
	
/******************************************************************************/
// Contrôle de cohérence de toutes les lignes d'une commande
// si une erreur a été positionnée lors du batch de contrôle
/******************************************************************************/
	public function ctlCommande()
	{
		$flagmaj = false;
		$this->ap_totalht = 0;
		$this->ap_invesht = 0;
		$this->ap_foncht = 0;
		//
		// récupération de la dernière révision du marché
		// si la date de l'EJ est renseignée on cherche la révision par rapport à cette date
		//
		if ($this->av_dateej != '' && $this->av_dateej != '0000-00-00') $mrev_coefrev = $this->getRevision($this->av_dateej,'force');
		else $mrev_coefrev = $this->getRevision('','force');
		if ($mrev_coefrev === false) return false;
		//
		// recherche des informations de la commande
		//
		$requete = "select MAR_TYPEUO from commande 
					left join marche on CMD_IDMARCHE=MAR_CLE
					where CMD_CLE=\"" . $this->cmd_cle . "\"";
		$statement = $this->conn->query($requete);
		$resultat = $statement->fetch(PDO::FETCH_ASSOC);
		$mar_typeuo = $resultat['MAR_TYPEUO'];
		// recherche de toutes les lignes d'une commande
		//
		$requete = "select LCMD_CLE, LCMD_IDUO, LCMD_FLAGIMPUTATION, LCMD_COEFREDU,
					LCMD_MONTANTREV, LCMD_QUANTITE, LCMD_TOTALHT, LCMD_TYPEUO,
					MUO_FLAGIMPUTATION, MUO_FLAGREVISION, MUO_MONTANT, MUO_MCOSLA
					from commande_ligne 
					left join marche_uo on LCMD_IDUO=MUO_CLE
					where LCMD_IDCMD=\"" . $this->cmd_cle . "\"";
		$statement = $this->conn->query($requete);
		while ($row = $statement->fetch(PDO::FETCH_ASSOC))
		{
			// récupération des informations de la ligne de commande
			$lcmd_coefredu = $row['LCMD_COEFREDU'];
			$lcmd_quantite = $row['LCMD_QUANTITE'];
			$lcmd_totalht = $row['LCMD_TOTALHT'];
			if ($lcmd_coefredu == '' || $lcmd_coefredu == 0) $lcmd_coefredu = 1;
			else $lcmd_coefredu = (100-$lcmd_coefredu)/100; // convertir % en coefficient de réduction (25% -> 0,75)
			if ($lcmd_quantite == '' || $lcmd_quantite == 0) $lcmd_quantite = 1;
			if ($lcmd_totalht == '' || $lcmd_totalht == 0) $lcmd_totalht = 0;
			// MAR_TYPEUO ('UO' 'Sans UO' 'BPU sans SLA' 'BPU avec SLA') 
			// marché sans uo 
			if ($mar_typeuo == 'Sans UO')
			{
				if ($row['LCMD_MONTANTREV'] != '' || $row['LCMD_MONTANTREV'] != 0)
					$muo_montant = $row['LCMD_MONTANTREV'];
				else if($lcmd_coefredu == 1) 
					$muo_montant = $lcmd_totalht/$lcmd_quantite;	
			}	
			// sinon marché avec UO -> montant venant de l'UO
			else
			{
				// montant de l'UO hors SLA 
				$muo_montant = $row['MUO_MONTANT'];
				// montant de l'UO en tenant compte des SLA si présents
				// si LCMD_TYPEUO = S1 S2 S3 S4
				// alors MUO_MCOSLA contient une liste des 4 montants SLA séparés par des ;
				$typeuo = $row['LCMD_TYPEUO'];
				if ($typeuo == 'S1' || $typeuo == 'S2' || $typeuo == 'S3' || $typeuo == 'S4')
				{
					$tabmcosla = explode(';',$row['MUO_MCOSLA']);
					if ($typeuo == 'S1' && isset($tabmcosla[0])) $muo_montant = $tabmcosla[0];
					else if ($typeuo == 'S2' && isset($tabmcosla[1])) $muo_montant = $tabmcosla[1];
					else if ($typeuo == 'S3' && isset($tabmcosla[2])) $muo_montant = $tabmcosla[2];
					else if ($typeuo == 'S4' && isset($tabmcosla[3])) $muo_montant = $tabmcosla[3];
				}
			}
			// on prend l'imputation en priorité au niveau de la ligne de commande
			if ($row['LCMD_FLAGIMPUTATION'] == '') $muo_flagimputation = $row['MUO_FLAGIMPUTATION'];
			else $muo_flagimputation = $row['LCMD_FLAGIMPUTATION'];
			if ($muo_flagimputation != 'F' && $muo_flagimputation != 'I') $muo_flagimputation = 'F'; // on met fonctionnement par défaut
			
			// calcul du montant HT révisé et remisé de la ligne en fonction de la quantité
			$montantrev = ($muo_montant * $mrev_coefrev);
			$montantHT = ($montantrev * $lcmd_quantite);
			$montantHT = ($montantHT * $lcmd_coefredu);
			$this->traceCommande(1,"ctlCommande actuel montantHT=".$row['LCMD_TOTALHT']." montantrev=".$row['LCMD_MONTANTREV']." montant unitaire=".$muo_montant." coefrev=".$this->av_coefrev." quantite=".$lcmd_quantite);
			$this->traceCommande(1,"ctlCommande recalculé montantHT=".$montantHT." montantrev=".$montantrev." coefrev=".$mrev_coefrev);
			
			// vérification du montant calculé avec le montant enregistré pour la ligne de commande
			// si différence -> mise à jour du montant de la ligne de commande
			$montantrev = round($montantrev,2);
			$montantHT = round($montantHT,2);
			if ($montantHT != $row['LCMD_TOTALHT'] || $montantrev != $row['LCMD_MONTANTREV'])
			{
				$requete = "update commande_ligne set 
							LCMD_MONTANTREV=\"" . $montantrev . "\",
							LCMD_TOTALHT=\"" . $montantHT . "\"
							where LCMD_CLE=\"" . $row['LCMD_CLE'] . "\""; 
				$statement1 = $this->conn->query($requete);
				if (!$statement1) return false; 
				$this->traceCommande(1,"ctlCommande différence LCMD_TOTALHT=".$row['LCMD_TOTALHT']." montantHT=".$montantHT);
				$flagmaj = true;
			}
			
			// cumul des montants pour la commande
			$this->ap_totalht += $montantHT;
			if ($muo_flagimputation == 'I') $this->ap_invesht += $montantHT;
			else $this->ap_foncht += $montantHT;
		}
		//
		// comparaison des montants (total, investissement et fonctionnement) de la commande
		// en base de données il y a un arrondi à deux chiffres après la virgule
		//
		if (round($this->ap_totalht,2) != $this->av_totalht 
			or round($this->ap_invesht,2) != $this->av_invesht 
			or round($this->ap_foncht,2) != $this->av_foncht) $flagmaj = true;
		//
		// mise à jour de la commande et des budgets si montants différents
		//
		if ($flagmaj || $this->av_coefrev != $this->ap_coefrev || $this->av_tva != $this->ap_tva)
		{	
			// calcul des montants TTC
			$this->calculTva();
			
			// mise à jour des montants de la commande
			if (!$this->majMontantCommande()) return false;
			
			// mise à jour des montants du budget global et du budget projet
			if (!$this->majBudget()) return false;
		}
		return true; 
	}
	
/******************************************************************************/
// mise à jour de la commande en bdd 
// lors de la création , la mise à jour ou la suppression d'une ligne de commande
/******************************************************************************/
	private function majMontantCommande()
	{
		$requete = "update commande set 
					CMD_COEFREV=\"" . $this->ap_coefrev . "\", 
					CMD_TOTALHT=\"" . $this->ap_totalht . "\", 
					CMD_INVESHT=\"" . $this->ap_invesht . "\",
					CMD_FONCHT=\"" . $this->ap_foncht . "\",
					CMD_TOTALTTC=\"" . $this->ap_totalttc . "\", 
					CMD_INVESTTC=\"" . $this->ap_investtc . "\",
					CMD_FONCTTC=\"" . $this->ap_foncttc . "\"
					where CMD_CLE=\"" . $this->cmd_cle . "\""; 
		$statement = $this->conn->query($requete);
		$this->traceCommande(1,"majMontantCommande ".$requete);
		if (!$statement) return false; 
		return true;
	}
	
/******************************************************************************/
// Calcul de la tva 
/******************************************************************************/
	private function calculTva()
	{
		$this->ap_totalttc = round($this->ap_totalht + (($this->ap_totalht * $this->ap_tva) / 100),2);
		$this->ap_investtc = round($this->ap_invesht + (($this->ap_invesht * $this->ap_tva) / 100),2);
		$this->ap_foncttc = round($this->ap_foncht + (($this->ap_foncht * $this->ap_tva) / 100),2);
	}
	
/******************************************************************************/
// mise à jour des budgets d'une commande ou d'une demande de paiement
// on compare les montants avant la mise à jour avec ceux après la mise à jour
/******************************************************************************/
	public function majBudget()
	{
		//
		// si erreur lors de la construction de l'objet -> pas de mise à jour
		//
		if ($this->erreur === true) 
		{
			if ($this->libelle == '') $this->libelle = "Erreur lors de l'initialisation " . $this->errbdd;
			return false;
		}
		
		//
		// pour le traitement des lignes de commande ou la création ou suppression en général 
		// -> pas de changement de budget (année ou programme) donc pas nécessaire de contrôler
		// en mise à jour av_numerosif et ap_numerosif doivent être renseignés
		//
		if ($this->typedemande != 'ligne commande' && $this->typeaction != "cre" && $this->typeaction != "sup")
		{
			// contrôle si changement de budget (année ou programme #) 
			if (!$this->ctlBudget())
			{	
				$this->libelle = "Erreur lors du contrôle du budget";
				return false;
			}
		}		

		//
		// calcul des montants à ajuster sur les budgets
		//
		if (!$this->cptMontantBudget())
		{	
			$this->libelle = "Erreur lors du calcul des montants à répercuter";
			return false;
		}	

		//
		// mise à jour des montants sur les budgets
		//
		if (!$this->majMontantBudget())
		{	
			$this->libelle = "Erreur lors de la mise à jour des montants à répercuter";
			return false;
		}	
		return true;
	}

/******************************************************************************/
// Contrôle si modification affectation budgétaire 
// si l'année ou le programme LOLF change -> déplacer tous les montants vers le nouveau budget
// appelé par majBudget uniquement en cas de mise à jour d'une commande 
/******************************************************************************/
	private function ctlBudget()
	{
		//
		// si année ou programme changé -> déplacement vers le nouveau budget
		//
		if ($this->av_idbudget != $this->ap_idbudget) 
		{
			$this->traceCommande(1,"ctlBudget réaffection budget av_idbudget=".$this->av_idbudget." ap_idbudget=".$this->ap_idbudget);
			if ($this->typedemande == 'commande')
			{	
				// suppression des AE de l'ancien budget si traitement d'une commande
				$requete = "update budget set ";
				if ($this->av_numerosif == '') 
					$requete .= "BUD_AEPREVINVES=BUD_AEPREVINVES-\"" . $this->av_investtc . "\",
								 BUD_AEPREVFONCT=BUD_AEPREVFONCT-\"" . $this->av_foncttc . "\""; 
				else 
					$requete .= "BUD_AECONSINVES=BUD_AECONSINVES-\"" . $this->av_investtc . "\",
								 BUD_AECONSFONCT=BUD_AECONSFONCT-\"" . $this->av_foncttc . "\"";
				$requete .= " where BUD_CLE=\"" . $this->av_idbudget . "\""; 
				$this->traceCommande(1,"ctlBudget diff budget supp commande ".$requete);
				$statement = $this->conn->query($requete);
				if (!$statement) return false; 
				
				// ajout des AE au nouveau budget si traitement d'une commande
				$requete = "update budget set ";
				if ($this->av_numerosif == '') 
					$requete .= "BUD_AEPREVINVES=BUD_AEPREVINVES+\"" . $this->av_investtc . "\",
								 BUD_AEPREVFONCT=BUD_AEPREVFONCT+\"" . $this->av_foncttc . "\""; 
				else 
					$requete .= "BUD_AECONSINVES=BUD_AECONSINVES+\"" . $this->av_investtc . "\",
								 BUD_AECONSFONCT=BUD_AECONSFONCT+\"" . $this->av_foncttc . "\"";
				$requete .= " where BUD_CLE=\"" . $this->ap_idbudget . "\""; 
				$this->traceCommande(1,"ctlBudget diff budget ajout commande ".$requete);
				$statement = $this->conn->query($requete);
				if (!$statement) return false; 
			}	
		}
		
		//
		// si année ou programme changé ou projet changé -> déplacement vers la nouvelle ligne budgétaire
		//
		if ($this->av_idbudget != $this->ap_idbudget || $this->av_idlignebudget != $this->ap_idlignebudget) 
		{
			$this->traceCommande(1,"ctlBudget réaffection budget projet av_idbudget=".$this->av_idbudget." ap_idbudget=".$this->ap_idbudget." av_idlignebudget=".$this->av_idlignebudget." ap_idlignebudget=".$this->ap_idlignebudget);
			if ($this->typedemande == 'commande')
			{	
				// suppression des AE de l'ancien budget si traitement d'une commande
				$requete = "update budget_ligne set ";
				if ($this->av_numerosif == '') 
					$requete .= "BUDL_AEPREVINVES=BUDL_AEPREVINVES-\"" . $this->av_investtc . "\",
								 BUDL_AEPREVFONCT=BUDL_AEPREVFONCT-\"" . $this->av_foncttc . "\""; 
				else 
					$requete .= "BUDL_AECONSINVES=BUDL_AECONSINVES-\"" . $this->av_investtc . "\",
								 BUDL_AECONSFONCT=BUDL_AECONSFONCT-\"" . $this->av_foncttc . "\"";
				$requete .= " where BUDL_CLE=\"" . $this->av_idlignebudget . "\""; 
				$this->traceCommande(1,"ctlBudget diff ligne budget supp commande ".$requete);
				$statement = $this->conn->query($requete);
				if (!$statement) return false; 

				// ajout des AE au nouveau budget si traitement d'une commande
				$requete = "update budget_ligne set ";
				if ($this->av_numerosif == '') 
					$requete .= "BUDL_AEPREVINVES=BUDL_AEPREVINVES+\"" . $this->av_investtc . "\",
								 BUDL_AEPREVFONCT=BUDL_AEPREVFONCT+\"" . $this->av_foncttc . "\""; 
				else 
					$requete .= "BUDL_AECONSINVES=BUDL_AECONSINVES+\"" . $this->av_investtc . "\",
								 BUDL_AECONSFONCT=BUDL_AECONSFONCT+\"" . $this->av_foncttc . "\"";
				$requete .= " where BUDL_CLE=\"" . $this->ap_idlignebudget . "\""; 
				$this->traceCommande(1,"ctlBudget diff ligne budget ajout commande ".$requete);
				$statement = $this->conn->query($requete);
				if (!$statement) return false; 
			}	
		}
		return true;
	}
	
/******************************************************************************/
// Calcul des montants du budget global et du budget du projet
// données utiles
//		montants -> av_investtc av_foncttc ap_investtc ap_foncttc
//		prévisionnel ou consommé -> av_numerosif ap_numerosif
// les montants calculés sont des différentiels à ajouter ou enlever des budgets
/******************************************************************************/
	private function cptMontantBudget()
	{
		(double)$this->previnves = 0;
		(double)$this->prevfonct = 0;
		(double)$this->consinves = 0;
		(double)$this->consfonct = 0;
		$this->traceCommande(1,'cptMontantBudget av_numerosif='.$this->av_numerosif.' ap_numerosif='.$this->ap_numerosif);
		$this->traceCommande(1,'cptMontantBudget av_investtc='.$this->av_investtc.' av_foncttc='.$this->av_foncttc.' ap_investtc='.$this->ap_investtc.' ap_foncttc='.$this->ap_foncttc);
		//
		// déplacement si besoin des AE entre prévisionnel et consommé
		// uniquement en cas de mise à jour
		//
		if ($this->typeaction == 'maj')
		{	
			// si l'EJ ou la DP passe de renseigné à non renseigné -> déplacer consommé vers prévisonnel
			if ($this->av_numerosif != '' && $this->ap_numerosif == '')
			{
				$this->previnves = $this->av_investtc;
				$this->prevfonct = $this->av_foncttc;
				$this->consinves = -$this->av_investtc;
				$this->consfonct = -$this->av_foncttc;
			}
			else if ($this->av_numerosif == '' && $this->ap_numerosif != '')
			// si l'EJ ou la DP passe de non renseigné à renseigné -> déplacer prévisonnel vers consommé
			{
				$this->previnves = -$this->av_investtc;
				$this->prevfonct = -$this->av_foncttc;
				$this->consinves = $this->av_investtc;
				$this->consfonct = $this->av_foncttc;
			}
		}
		
		//
		// calcul de la différence du montant 
		//
		(double)$diffinvest = $this->ap_investtc-$this->av_investtc;
		(double)$difffonct = $this->ap_foncttc-$this->av_foncttc;
		
		//
		// ajustement des montants prévisionnels ou consommés
		//
		if ($this->ap_numerosif == '')
		{
			$this->previnves = $this->previnves+$diffinvest;
			$this->prevfonct = $this->prevfonct+$difffonct;
		}
		else 
		{
			$this->consinves = $this->consinves+$diffinvest;
			$this->consfonct = $this->consfonct+$difffonct;
		}
		$this->traceCommande(1,'cptMontantBudget previnves=' . $this->previnves . ' prevfonct=' . $this->prevfonct . ' consinves=' . $this->consinves . ' consfonct=' . $this->consfonct);
		return true;
	}
	
/******************************************************************************/
// Mise à jour du budget global et du budget du projet 
/******************************************************************************/
	private function majMontantBudget()
	{
		// si les différences de montant sont = 0 --> pas de modification des budgets
		if ($this->previnves == 0 && $this->prevfonct == 0 && $this->consinves == 0 && $this->consfonct == 0)
		{
			$this->traceCommande(1,"majMontantBudget pas de changement de montant");
			return true;
		}
		// mise à jour du budget global
		$requete = 'update budget set ';
		// sur une commande ou sur une ligne de commande
		$requete .= "BUD_AEPREVINVES=BUD_AEPREVINVES+\"" . $this->previnves . "\",
					 BUD_AEPREVFONCT=BUD_AEPREVFONCT+\"" . $this->prevfonct . "\",
					 BUD_AECONSINVES=BUD_AECONSINVES+\"" . $this->consinves . "\",
					 BUD_AECONSFONCT=BUD_AECONSFONCT+\"" . $this->consfonct . "\" 
		             where BUD_CLE=\"" . $this->ap_idbudget . "\""; 
		$this->traceCommande(1,"majMontantBudget requete budget=" . $requete);
		$statement = $this->conn->query($requete);
		if (!$statement) return false; 

		// mise à jour du budget projet si budget pour le projet
		if ($this->ap_idlignebudget > 0)
		{
			$requete = 'update budget_ligne set ';
			// sur une commande
			$requete .= "BUDL_AEPREVINVES=BUDL_AEPREVINVES+\"" . $this->previnves . "\",
						 BUDL_AEPREVFONCT=BUDL_AEPREVFONCT+\"" . $this->prevfonct . "\",
						 BUDL_AECONSINVES=BUDL_AECONSINVES+\"" . $this->consinves . "\",
						 BUDL_AECONSFONCT=BUDL_AECONSFONCT+\"" . $this->consfonct . "\"
			             where BUDL_CLE=\"" . $this->ap_idlignebudget . "\""; 
			$this->traceCommande(1,"majMontantBudget requete budget_ligne=" . $requete);
			$statement = $this->conn->query($requete);
			if (!$statement) return false; 
		}	
		return true;
	}
	
/******************************************************************************/
// Trace les erreurs base de données
/******************************************************************************/
	private function traceErrBdd()
	{
		$this->errbdd = $$this->conn->errorInfo(); 
	}

/******************************************************************************/
// Trace les mises à jour
/******************************************************************************/
	private function traceCommande($niveau, $message)
	{
		if($niveau <= $this->debug) Trace($message);
	}

/******************************************************************************/
// fin de la classe commande
/******************************************************************************/
}
