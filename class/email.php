<?php
/* 
	class de gestion d'envoi des mails
	version 1.01 le 24/06/2014
	
	$sujet = '=?UTF-8?B?'.base64_encode($sujet).'?=';
	
	envoi de mail avec phpmailer sans serveur SMTP
	$mail = new PHPMailer;
	//$mail->IsSMTP();  	   // telling the class to use SMTP
	//$mail->Host = '';
	//$mail->SMTPAuth = false; // true - false : Enable SMTP authentication
	$mail->IsMail();  	       // => Sets Mailer to send message using PHP mail() function
*/
class email 
{
	public $objet;			/* sujet du mail */
	public $corps;			/* corps du mail */
	public $destinataires;	/* destinataires du mail */
	public $lien;			/* lien */
	public $origine='SSIM <noreply@ssim.com>';	/* origine du mail */
	private $charset="utf-8"; 
	
	function initEmail()
	{
		$this->corps = "<html><meta http-equiv='Content-Type' content='text/html' charset='utf-8' /><body>";
	}
	function addObjet($objet)
	{
		$this->objet = "[THEMIS]" . $objet;
	}
	function addTitre($titre)
	{
		$this->corps .= "<p><strong>" . $titre . "</strong></p>";
		$this->corps .= "<table style='border: 1px solid #808080; border-collapse: collapse; width: 650px;'>";
		$this->corps .= "<tr style='padding:4px; border: 1px dotted #808080; background-color:#93d1f0;' ><th style='text-align:left; width:30%;padding:4px;'>Libellé </th><th style='text-align:left;width:70%;padding:4px; border-left: 1px solid #808080;'>Description</th></tr>";
	}
	function addInfos($lib, $infos)
	{
		$this->corps .=  "<tr style='padding:4px; border: 1px dotted #808080;' ><td style='padding:4px;background-color:#DEF1FA;'>" . $lib . "</td><td style='padding:4px; border-left: 1px solid #808080;'>" . $infos . "</td></tr>\n";
	}
	function addLien($lien)
	{
		$this->lien = $lien;
	}
	function addDestinataires($destinataires)
	{
		$this->destinataires = $destinataires;
		return true;
	}
	function sendEmail()
	{
		global $codefonc;
		$conn = database::getIntance();
		
		if (strpos("cmd",$codefonc) !== false && $this->lien != '')
		{
			$lien = "<a style='color:blue;' href=\"" . $this->lien . "\">Lien vers Themis</a>";
			$this->addInfos("Lien vers la commande", $lien);
		}
		$this->corps .= "</table></body></html>";
		$entete = "Content-transfer-encoding: 8bit\nMIME-Version: 1.0\nFrom: " . $this->origine . "\nContent-Type: text/html; charset=\"UTF-8\"\n";
		set_error_handler("errTrap", E_WARNING);
		
		$this->objet = '=?UTF-8?B?'.base64_encode($this->objet).'?=';
		mail($this->destinataires, $this->objet, $this->corps, $entete);
/*		
		$requete = "select 	CFG_FLAGMAIL, CFG_MAIL_OBJET, CFG_MAIL_CORPS from config where CFG_CLE='1'";
		$statement = $conn->query($requete);
		$resultat = $statement->fetch(PDO::FETCH_ASSOC);
		
		if ($resultat['CFG_FLAGMAIL'] == 'o')
		{
			if ($resultat['CFG_MAIL_OBJET'] != 'o' && $resultat['CFG_MAIL_CORPS'] != 'o')
				mail($this->destinataires, utf8_decode($this->objet), utf8_decode($this->corps), $entete);
			else if ($resultat['CFG_MAIL_OBJET'] == 'o' && $resultat['CFG_MAIL_CORPS'] != 'o')
				mail($this->destinataires, $this->objet, utf8_decode($this->corps), $entete);
			else if ($resultat['CFG_MAIL_OBJET'] != 'o' && $resultat['CFG_MAIL_CORPS'] == 'o')
				mail($this->destinataires, utf8_decode($this->objet), $this->corps, $entete);
			else if ($resultat['CFG_MAIL_OBJET'] == 'o' && $resultat['CFG_MAIL_CORPS'] == 'o')
				mail($this->destinataires, $this->objet, $this->corps, $entete);
		}
*/		
		restore_error_handler(); 
	}
}
