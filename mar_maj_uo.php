<?php
include('config/mar_maj_uo.php');
$codefonc='mar';
require_once('prepage.php');

$objForm = new formulaire('1');

// création
if($typeaction == "creation") $objForm->initChamp();

// modification
if($typeaction == "modification")
{
	$requete = "select MUO_CLE, MUO_NOMUO, MUO_REFEXTERNE, MUO_IDPRESTATION, MUO_IDMARCHE, MUO_FREQUENCE, 
				MUO_LIBPOSTE, MUO_MONTANT, MUO_MCOSLA, MUO_MARCHANDISE, MUO_FLAGIMPUTATION, MUO_FLAGREVISION, MUO_FLAGDSI,
				MUO_DESCRIPTION
				from marche_uo where MUO_CLE=\"" . $cle . "\"";
	$resultat = $objForm->mapChamp($requete);
	if ($resultat['MUO_MCOSLA'] != '') $mcosla = explode(';',$resultat['MUO_MCOSLA']);
	else $mcosla = array('','','','');
	if (isset($mcosla[0])) $mcosla1 = $mcosla[0];
	if (isset($mcosla[1])) $mcosla2 = $mcosla[1];
	if (isset($mcosla[2])) $mcosla3 = $mcosla[2];
	if (isset($mcosla[3])) $mcosla4 = $mcosla[3];
}

// réception des paramètres
if($typeaction == "reception")
{
// contrôle des paramètres
	$objForm->recChamp();
	while($objForm->erreur == '')
	{
		break;
	}

	if ($objForm->erreur == '')
	{
		if ($mcosla1 != '' && $mcosla2 != '' && $mcosla3 != '' && $mcosla4 != '')
			$mcosla=$mcosla1.";".$mcosla2.";".$mcosla3.";".$mcosla4;
		else $mcosla='';
		$objForm->addChamp('mcosla', 'MUO_MCOSLA');
		$objForm->majBdd('marche_uo', 'MUO_CLE', $cle);
		RedirURL("mar_list_uo.php");
		exit();
	}
}

// Affichage du début de la page
if ($cle != '') $objPage->addParam('cle', $cle);
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();

$requete = "select MPRE_CLE, MPRE_NOMPRESTATION from marche_prestation where MPRE_IDMARCHE=\"" . $cleparent . "\" order by 2";
$objForm->setSelect('idprestation', $idprestation, $requete, 'MPRE_CLE', 'MPRE_NOMPRESTATION');
	
if ($flagrevision == "o") $opt = " checked "; else $opt = "";
$objForm->setOpt('flagrevision', $opt);
$flagrevision = "o";
	
if ($flagdsi == "o") $opt = " checked "; else $opt = "";
$objForm->setOpt('flagdsi', $opt);
$flagdsi = "o";
	
// affichage des boutons d'enchainement
$objForm->addBouton("reset","RETOUR","mar_list_uo.php");
if (($cle == '' && $objProfil->cre) || ($cle != '' && $objProfil->maj))
{
	$objForm->addBouton("submit","ENVOYER");
}
else $objForm->setLecForm();

$objForm->affFormulaire();
$objForm->finFormulaire();

// fin de page
$objPage->finPage();
