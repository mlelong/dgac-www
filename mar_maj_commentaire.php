<?php
include('config/mar_maj_commentaire.php');
$codefonc='mar';
require_once('prepage.php');

$objForm = new formulaire('1');

// création
if($typeaction == "creation") $objForm->initChamp();

// réception des paramètres
if($typeaction == "reception")
{
// contrôle des paramètres
	$objForm->recChamp();
	while($objForm->erreur == '')
	{
		break;
	}

	if ($objForm->erreur == '')
	{
		if ($commentaire == '') 
		{	
			RedirURL("mar_list_commentaire.php");
			exit();
		}
		$datecom = date("Y-m-d h:i:s");
		$requete = "insert into commentaire (COM_NOMTABLE, COM_IDTABLE, COM_IDNOM, COM_DATECOM, COM_DESCRIPTION) 
					values (\"marche\",\"" . $cleparent .	"\",\"" . $objProfil->idnom . 
							"\",\"" . $datecom .	"\",\"" . addslashes($commentaire) . "\")";
		$statement = $conn->query($requete);
		RedirURL("mar_list_commentaire.php");
		exit();
	}
}

// Affichage du début de la page
if ($cle != '') $objPage->addParam('cle', $cle);
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();
	
// affichage des boutons d'enchainement
$objForm->addBouton("reset","RETOUR","mar_list_commentaire.php");
if (($cle == '' && $objProfil->cre) || ($cle != '' && $objProfil->maj))
{
	$objForm->addBouton("submit","ENVOYER");
}
else $objForm->setLecForm();

$objForm->affFormulaire();
$objForm->finFormulaire();

// fin de page
$objPage->finPage();
?>
<script type="text/javascript" src="js/summernote/summernote.min.js"></script>
<script type="text/javascript" src="js/summernote/summernote-fr-FR.min.js"></script>
<script>
	$(document).ready(function() {
//	console.log('summernote');
		$('#commentaire').summernote({
			height: 200,        // paramétrage de la hauteur de l éditeur
			minHeight: null,    // hauteur minimale de l éditeur
			maxHeight: null,    // hauteur maximale de l éditeur
			lang: 'fr-FR'       // defaut: 'en-US'
		});
	});
</script>
