<?php
$codefonc='cmd';
include('prepage.php');

// récupération des informations de la commande
$requete = "select CMD_DEMANDEUR, CMD_INTERVENANT, CMD_PROGRAMME, 
			CMD_NUMERO, CMD_NUMEROSIF, CMD_TVA, CMD_LIEU, CMD_TYPEIMPUTATION, 
			DATE_FORMAT(CMD_DATECRE, '%d-%m-%Y') AS CMD_DATECRE,
			DATE_FORMAT(CMD_DATESOU, '%d-%m-%Y') AS CMD_DATESOU,
			DATE_FORMAT(CMD_DATECMD, '%d-%m-%Y') AS CMD_DATECMD,
			DATE_FORMAT(CMD_DATELIVA, '%d-%m-%Y') AS CMD_DATELIVA,
			CMD_IDPOLE, CMD_IDMARCHE, CMD_IDPROJET, CMD_COMMENTAIRE,  
			POL_NOMPOLE, MAR_NOMMARCHE, MAR_NUMERODGR, MAR_NUMEROSIF, PRJ_NOMPROJET
			from commande 
			left join pole on CMD_IDPOLE=POL_CLE
			left join marche on CMD_IDMARCHE=MAR_CLE
			left join projet on CMD_IDPROJET=PRJ_CLE
			where CMD_CLE=\"" . $cleparent . "\" ";
$cmd_statement = $conn->query($requete);
$cmd_resultat = $cmd_statement->fetch(PDO::FETCH_ASSOC);
if ($cmd_resultat['CMD_DATELIVA'] == '00-00-0000') $dateliva = '';
else $dateliva = $cmd_resultat['CMD_DATELIVA'];
if ($cmd_resultat['CMD_DATESOU'] == '00-00-0000') $datesou = '';
else $datesou = $cmd_resultat['CMD_DATESOU'];
if ($cmd_resultat['CMD_DATECMD'] == '00-00-0000')
{
	if ($cmd_resultat['CMD_DATECRE'] == '00-00-0000') $datecmd = '';
	else $datecmd = $cmd_resultat['CMD_DATECRE'];
}
else $datecmd = $cmd_resultat['CMD_DATECMD'];

// récupération de la révision
$objCommande = new commande($conn, 'maj', 'commande', $cleparent);
$revision = $objCommande->getRevision();

// Constitution de l'entête de la commande 
$entete = array(
    'lieu' => $cmd_resultat['CMD_LIEU'],
    'date_demande' => $datecmd,
    'demandeur' => $cmd_resultat['CMD_DEMANDEUR'],
    'service' => 'SSIM',
    'bureau' => $cmd_resultat['POL_NOMPOLE'],
    'budget' => 'OUI',
    'programme' => $cmd_resultat['CMD_PROGRAMME'],
    'projet' => $cmd_resultat['PRJ_NOMPROJET'],
    'marche' => $cmd_resultat['MAR_NOMMARCHE'],
    'revision' => $revision,
    'imputation' => $cmd_resultat['CMD_TYPEIMPUTATION'], 
    'lieu_livraison' => $cmd_resultat['CMD_LIEU'],
    'responsable_reception' => $cmd_resultat['CMD_DEMANDEUR'],
    'date_livraison' => $dateliva,
    'commentaire' => $cmd_resultat['CMD_COMMENTAIRE'],
);

// récupération des lignes de commande
$requete = "select LCMD_CLE, LCMD_POSTE, GCMD_LIBELLE, MUO_NOMUO, MUO_MONTANT, LCMD_FLAGIMPUTATION,
			LCMD_MARCHANDISE, LCMD_TOTALHT, LCMD_LIBELLE, LCMD_MONTANTREV, LCMD_COEFREDU, LCMD_QUANTITE,
			DATE_FORMAT(LCMD_DATELIVA, '%d-%m-%Y') AS LCMD_DATELIVA,
			DATE_FORMAT(LCMD_DATEVA, '%d-%m-%Y') AS LCMD_DATEVA,
			DATE_FORMAT(GCMD_DATELIVA, '%d-%m-%Y') AS GCMD_DATELIVA,
			DATE_FORMAT(GCMD_DATEVA, '%d-%m-%Y') AS GCMD_DATEVA
			from commande_ligne 
			left join marche_uo on LCMD_IDUO=MUO_CLE
			left join commande_groupe_ligne on LCMD_IDGCMD=GCMD_CLE
			where LCMD_IDCMD=\"" . $cleparent . "\" order by GCMD_CLE, LCMD_CLE";
$lcmd_statement = $conn->query($requete);

// constitution des lignes de la commande
$totalHT = 0;
$lignes = array();
$refgroupe = '';
$cpt=0;
while ($row = $lcmd_statement->fetch(PDO::FETCH_ASSOC))
{
	$lignes[$cpt] = array();
	// Affichage des groupes de poste si presents
	if ($refgroupe != $row['GCMD_LIBELLE'])
	{
		$lignes[$cpt]['libellé'] = "<B>" . $row['GCMD_LIBELLE'] . "</B>";
		$lignes[$cpt]['quantité'] = "";
		$lignes[$cpt]['remise'] = "";
		$lignes[$cpt]['prix unitaire'] = "";
		$lignes[$cpt]['prix total'] = "";
		$refgroupe = $row['GCMD_LIBELLE'];
		$cpt++;
	}
	// affichage de la ligne	
	if ($row['MUO_NOMUO'] != '')
	    $lignes[$cpt]['libellé'] = "<B>" . $row['MUO_NOMUO'] . "</B>";
	else if ($row['LCMD_POSTE'] != '')	
	    $lignes[$cpt]['libellé'] = "<B>" . $row['LCMD_POSTE'] . "</B>";
	else 
		$lignes[$cpt]['libellé'] = "<B>" . $row['GCMD_LIBELLE'] . "</B>";
	$lignes[$cpt]['quantité'] = $row['LCMD_QUANTITE'];
	$lignes[$cpt]['remise'] = $row['LCMD_COEFREDU'];
	$lignes[$cpt]['prix unitaire'] = number_format($row['LCMD_MONTANTREV'],2,',',' ');
	$lignes[$cpt]['prix total'] = number_format($row['LCMD_TOTALHT'],2,',',' ');
	// incrémentation du total
	$totalHT += $row['LCMD_TOTALHT'];
	// affiche du libellé si présent
	if ($row['LCMD_LIBELLE'] != '') $lignes[$cpt]['libellé'] .= "<BR>" . $row['LCMD_LIBELLE'];
	$cpt++;
}

// Constitution du total de la commande
$totalTVA=(($totalHT*$cmd_resultat['CMD_TVA'])/100);
$totalTTC=$totalHT+(($totalHT*$cmd_resultat['CMD_TVA'])/100);
$tva = array(
    'taux' => $cmd_resultat['CMD_TVA'],
    'montant' => number_format($totalTVA,2,',',' ') . ' €',
);
$total_ht = number_format($totalHT,2,',',' ') . ' €';
$total_ttc =  number_format($totalTTC,2,',',' ') . ' €';

//
// affichage à l'écran en format PDF de la page
//
require_once('vendor/tecnickcom/tcpdf/config/tcpdf_config.php');
require_once('vendor/tecnickcom/tcpdf/tcpdf.php');

function addFooterSSIM(TCPDF $pdf)
{
    // Footer
    $pdf->setX(0);
    $pdf->SetY(260);
    $pdf->SetFont('helvetica', '', 8);
    $pdf->SetLineWidth(0.6);
    $pdf->Cell(
        200,
        10,
        "SSIM - 1, rue Georges Pelletier d'Oisy - 91200 ATHIS-MONS - Téléphone 01 69 84 60 01 - Fax 01 69 84 60 03",
        1,
        0,
        'C' // Centré
    );
}

function addTableauProjet(TCPDF $pdf, array $entete)
{
    $table = <<<EOT
<table cellspacing="0" cellpadding="5" border="1">
    <tr style="background-color:#b0b7c5;" >
        <td colspan="5" style="font-size:150%; text-align:center;">Demande de produits / services informatiques</td>
    </tr>
    <tr>
        <td rowspan="2">Identification du demandeur :<br><b>##DEMANDEUR##</b></td>
        <td rowspan="2">Lieu :<br><b>##LIEU##</b></td>
        <td rowspan="2">Date de la demande :<br><b>##DATE_DEMANDE##</b></td>
        <td>Prévision budget :<br><b>##BUDGET##</b></td>
        <td>Programme :<br><b>##PROGRAMME##</b></td>
    </tr>
    <tr>
        <td colspan="2">Type d'imputation de la commande :<br><b>##IMPUTATION##</b></td>  
    </tr>
    <tr style="background-color:#ffb8a9;" >
        <td colspan="5" style="font-size:200%; text-align:center;">##PROJET## (marché ##MARCHE## révision ##REVISION##)</td>
    </tr>
    <tr>
        <td colspan="5" style="height:50px;">Commentaire: ##COMMENTAIRE##</td>
    </tr>
</table>
EOT;
    $table = str_replace('##DEMANDEUR##', $entete['demandeur'], $table);
    $table = str_replace('##LIEU##', $entete['lieu'], $table);
    $table = str_replace('##DATE_DEMANDE##', $entete['date_demande'], $table);
    $table = str_replace('##BUDGET##', $entete['budget'], $table);
    $table = str_replace('##IMPUTATION##', $entete['imputation'], $table);
    $table = str_replace('##PROGRAMME##', $entete['programme'], $table);
    $table = str_replace('##PROJET##', $entete['projet'], $table);
    $table = str_replace('##MARCHE##', $entete['marche'], $table);
    $table = str_replace('##REVISION##', $entete['revision'], $table);
    $table = str_replace('##COMMENTAIRE##', $entete['commentaire'], $table);
    $pdf->writeHTML($table, true, false, false, false, '');
}

$pdf = new TCPDF();

// Informations (métadonnées)
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('DGAC/SSIM');
$pdf->SetTitle($entete['projet']);
$pdf->SetSubject('Demande de produits ou services informatiques');

// Réglages du document
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
$pdf->SetMargins(5, 5, 5);
$pdf->SetHeaderMargin(0);
$pdf->SetFooterMargin(0);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// Ajout d'une page
$pdf->AddPage();

// Entête DGAC/SSIM
$pdf->ImageSVG(dirname(__FILE__) . '/images/logo_dgac.svg', 5, 5, 20, 20);
$pdf->ImageSVG(dirname(__FILE__) . '/images/logo_republique_francaise.svg', 100, 9, 20);
$pdf->setX(27, true);
$pdf->SetY(7, false, true);
$pdf->SetTextColor(128);
$pdf->SetFont('helvetica', 'B', 7);
$pdf->SetFillColor(230);
$pdf->MultiCell(75, 5,
    "Ministère de la Transition écologique et solidaire\n\n" .
    "Direction générale de l'aviation civile\n\n" .
    "Service des systèmes d'information et de la modernisation",
    0, 'L', false
);
$pdf->SetTextColor(0);

// Cadre Numéro et date de la demande
$pdf->setX(135, true);
$pdf->SetY(0, false, true);
$pdf->SetFont('helvetica', '', 5);
$pdf->Cell(80, 30, "Ce document à compléter est à envoyer à SSIM", 0, 0, 'C');
$pdf->setX(135, true);
$pdf->SetY(2, false, true);
$pdf->Cell(80, 30, "Domaine Gestion des Ressources - Pôle Finances", 0, 0, 'C');
$pdf->setX(148, true);
$pdf->SetY(20, false, true);
$pdf->SetFont('helvetica', 'B', 7);
$pdf->SetFillColor(230);
$pdf->MultiCell(35, 10, "NUMÉRO DE LA DEMANDE", 0, 'R');
$pdf->setX(183, true);
$pdf->SetY(19, false, true);
$pdf->Cell(15, 5, $cmd_resultat['CMD_NUMERO'], 1, 0, 'C');
$pdf->setX(155, true);
$pdf->SetY(24, false, true);
$pdf->MultiCell(28, 10, "Date de réception au Pôle Finances", 0, 'R');
$pdf->setX(183, true);
$pdf->SetY(25, false, true);
$pdf->Cell(15, 5, $datesou, 1, 0, 'C');

// Tableau caractéristiques du projet
$pdf->SetFont('helvetica', '', 8);
$pdf->setX(5, true);
$pdf->SetY(40, false, true);
addTableauProjet($pdf, $entete);

// Tableau des articles et montants
$table = <<<EOT
<table cols="14" cellspacing="0" cellpadding="5" border="1">
    <tr style="background-color:#b0b7c5;">
        <td colspan="7" style="font-size:10pt;">Libellé des articles</td>
        <td colspan="2" style="font-size:10pt;">Quantité</td>
        <td colspan="2" style="font-size:10pt;">Remise</td>
        <td colspan="3" style="font-size:10pt;">Prix unitaire HT</td>
        <td colspan="3" style="font-size:10pt;">Prix total HT</td>
    </tr>
EOT;

// Ajout des lignes
foreach ($lignes as $article) {
    $ligne = <<<EOT
    <tr>
        <td colspan="7" style="text-align:left;">##LIBELLE##</td>
        <td colspan="2">##QUANTITE##</td>
        <td colspan="2">##REMISE##</td>
        <td colspan="3" style="text-align:right;">##PRIX_UNITAIRE##</td>
        <td colspan="3" style="text-align:right;">##PRIX_TOTAL##</td>
    </tr>
EOT;
    $ligne = str_replace('##LIBELLE##', $article['libellé'], $ligne);
    $ligne = str_replace('##QUANTITE##', $article['quantité'], $ligne);
    $ligne = str_replace('##REMISE##', $article['remise'], $ligne);
    $ligne = str_replace('##PRIX_UNITAIRE##', $article['prix unitaire'], $ligne);
    $ligne = str_replace('##PRIX_TOTAL##', $article['prix total'], $ligne);

    $table .= $ligne;
}

// Total HT
$ligne = <<<EOT
    <tr>
        <td colspan="14" style="text-align:right;"><b>TOTAL HT</b></td>
        <td colspan="3" style="text-align:right; font-size:125%;">##TOTAL_HT##</td>
    </tr>
EOT;
$table .= str_replace('##TOTAL_HT##', $total_ht, $ligne);

// TVA
$ligne = <<<EOT
    <tr>
        <td colspan="14" style="text-align:right;"><b>TVA ##TAUX_TVA##</b></td>
        <td colspan="3" style="text-align:right;">##TOTAL_TVA##</td>
    </tr>
EOT;
$ligne = str_replace('##TAUX_TVA##', $tva['taux'], $ligne);
$ligne = str_replace('##TOTAL_TVA##', $tva['montant'], $ligne);
$table .= $ligne;

// Total TTC
$ligne = <<<EOT
    <tr>
        <td colspan="11" style="text-align:right; font-size:125%;"><b>TOTAL TTC</b></td>
        <td colspan="3" style="text-align:right; font-size:150%;"><b>##TOTAL_TTTC##</b></td>
    </tr>
EOT;
$ligne = str_replace('##TOTAL_TTTC##', $total_ttc, $ligne);
$table .= $ligne;
$table .= '</table>';
$pdf->writeHTML($table, true, false, false, false, 'C');

// Livraison
$pdf->SetY($pdf->GetY() + 10, false, true);
// Tableau des articles et montants
$table = <<<EOT
<table cellspacing="0" cellpadding="5" border="1">
    <tr>
        <td>Lieu de livraison :<br><b>##LIEU_LIVRAISON##</b></td>
        <td>Nom du responsable à la livraison :<br><b>##RESPONSABLE_RECEPTION##</b></td>
        <td> Date de la livraison :<br><b>##DATE_LIVRAISON##</b></td>
        <td colspan="1"></td>
    </tr>
</table>
EOT;
$table = str_replace('##LIEU_LIVRAISON##', $entete['lieu_livraison'], $table);
$table = str_replace('##RESPONSABLE_RECEPTION##', $entete['responsable_reception'], $table);
$table = str_replace('##DATE_LIVRAISON##', $entete['date_livraison'], $table);
$pdf->writeHTML($table, true, false, false, false, 'C');

// Pied de page
addFooterSSIM($pdf);

// Sortie du PDF dans le navigateur
$pdf->Output('doc.pdf', 'I');
