<?php
include('config/adm_maj_phase.php');
$codefonc='adm';
require_once('prepage.php');

$objForm = new formulaire('1');

// création
if($typeaction == "creation") $objForm->initChamp();

// modification
if($typeaction == "modification")
{
	$requete = "select PHA_IDFONC, PHA_ORDRE, PHA_CODEPHASE, PHA_NOMPHASE from fonc_phase where PHA_CLE=\"" . $cle . "\"";
	$resultat = $objForm->mapChamp($requete);
}

// réception des paramètres
if($typeaction == "reception")
{
// contrôle des paramètres
	$objForm->recChamp();
	while($objForm->erreur == '')
	{
		break;
	}

	if ($objForm->erreur == '')
	{
		if ($cle == '') 
		{
			$objForm->setAff('ordre', true); // pour prise en compte dans la requête
			$requete = "select MAX(PHA_ORDRE) from fonc_phase where PHA_IDFONC=\"" . $cleparent . "\"";
			$statement = $conn->query($requete);
			$resultat = $statement->fetch(PDO::FETCH_ASSOC);
			if ($resultat['MAX(PHA_ORDRE)'] != '') $ordre = ($resultat['MAX(PHA_ORDRE)']+1);
			else $ordre = 1;
		}	
		$objForm->majBdd('fonc_phase', 'PHA_CLE', $cle);
		RedirURL("adm_list_phase.php");
		exit();
	}
}

// Affichage du début de la page
if ($cle != '') $objPage->addParam('cle', $cle);
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();

//if ($cle != "") $objForm->setOpt('codephase','readonly');

// affichage des boutons d'enchainement
$objForm->addBouton("reset","RETOUR","adm_list_phase.php");
if (($cle == '' && $objProfil->cre) || ($cle != '' && $objProfil->maj))
{
	$objForm->addBouton("submit","ENVOYER");
}
else $objForm->setLecForm();

$objForm->affFormulaire();
$objForm->finFormulaire();

// fin de page
$objPage->finPage();
