<?php
include('config/adm_maj_utilisateur.php');
$codefonc='uti';
require_once('prepage.php');

$objForm = new formulaire('1');

// création
if($typeaction == "creation") $objForm->initChamp();

// modification
if($typeaction == "modification")
{
	$requete = "select UTI_CLE, UTI_IDENTIFIANT, UTI_NOM, UTI_PRENOM, UTI_CIVILITE, UTI_PWD,
				UTI_ADRESSE, UTI_COMMUNE, UTI_CODEPOSTAL, UTI_DEPARTEMENT, UTI_TELEPHONE,
				UTI_PORTABLE, UTI_FAX, UTI_MAIL, UTI_ETAT, 
				DATE_FORMAT(CMD_DATESOU, '%Y') as CMD_DATESOU, 
				DATE_FORMAT(UTI_DATESUSPENSION, '%d-%m-%Y') as UTI_DATESUSPENSION, 
				DATE_FORMAT(UTI_DATEFIN, '%d-%m-%Y') as UTI_DATEFIN, 
				UTI_DESCRIPTION
				from utilisateur where UTI_CLE=\"" . $cle . "\"";
	$resultat = $objForm->mapChamp($requete);
}

// réception des paramètres
if($typeaction == "reception")
{
// contrôle des paramètres
	$objForm->recChamp();
	while($objForm->erreur == '')
	{
		break;
	}

	if ($objForm->erreur == '')
	{
		$objForm->majBdd('utilisateur', 'UTI_CLE', $cle);
		RedirURL("adm_list_utilisateur.php");
		exit();
	}
}

// Affichage du début de la page
if ($cle != '') $objPage->addParam('cle', $cle);
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();
	
// affichage des boutons d'enchainement
$objForm->addBouton("reset","RETOUR","adm_list_utilisateur.php");
if (($cle == '' && $objProfil->cre) || ($cle != '' && $objProfil->maj))
{
	$objForm->addBouton("submit","ENVOYER");
}
else $objForm->setLecForm();

$objForm->affFormulaire();
$objForm->finFormulaire();

// fin de page
$objPage->finPage();
