<?php
include('config/adm_list_fonctionnalite.php');
$codefonc='adm';
include('prepage.php');
if (!$objProfil->ctlDroit($typeaction, 'tableau')) exit();

$objPage->debPage('center');

// suppression d'une entrée
if($typeaction == "suppression")
{
	if(isset($cle))	
	{
		$requete = "delete from fonctionnalite where FON_CLE=\"" . $cle . "\"";
		$statement = $conn->query($requete);
	}
}

// préparation du tableau
$objTab = new tableau('1');

// requête d'accès à la base 
$requete = "select FON_CLE, FON_CODEFONC, FON_NOMFONC  from fonctionnalite";
$requete .= $objTab->majRequete('order by FON_CLE'); // ajout tri et pagination si besoin

// affichage des boutons d'enchainement
if ($objProfil->cre) $objTab->addBouton("button","AJOUTER","adm_maj_fonctionnalite.php");

// gestion des paramètres de lien
$objTab->setLien('codefonc','adm_tab_fonctionnalite.php',"?typeaction=modification&cle=#FON_CLE#");
$objTab->setLien('supp','adm_list_fonctionnalite.php',"?typeaction=suppression&cle=#FON_CLE#","Supprimer");

// affichage du tableau
$objTab->affTableau($requete);

// fin de page
$objPage->finPage();
