<?php
$codefonc='mar';
include('prepage.php');

// début de page
$objPage->debPage('center');

$objOnglet = new onglet();

// début de l'onglet
$objOnglet->debOnglet();

$requete = "select MAR_TYPEUO from marche where MAR_CLE=\"" . $cle . "\"";
$statement = $conn->query($requete);
$resultat = $statement->fetch(PDO::FETCH_ASSOC);
$typeuo = $resultat['MAR_TYPEUO'];

// creation des liens
$objOnglet->debLienOnglet();
$objOnglet->addLienOnglet('car','url','Caractéristiques','mar_maj_marche.php?typeaction=' . $typeaction . '&cle=' . $cle);
$objOnglet->addLienOnglet('pla','url','Planification','mar_maj_planification.php?typeaction=' . $typeaction . '&cle=' . $cle);
if ($typeuo != 'Sans UO')
{
	$objOnglet->addLienOnglet('rev','url','Révision','mar_list_revision.php?cleparent=' . $cle);
	$objOnglet->addLienOnglet('pre','url','Prestation','mar_list_prestation.php?cleparent=' . $cle);
	$objOnglet->addLienOnglet('uoe','url','Unités d\'oeuvre','mar_list_uo.php?cleparent=' . $cle);
}
$objOnglet->addLienOnglet('pce','url','Pièces jointes','mar_list_piece.php?cleparent=' . $cle);
$objOnglet->addLienOnglet('fou','url','Fournisseurs','mar_list_fournisseur.php?cleparent=' . $cle);
//$objOnglet->addLienOnglet('com','url','Commentaires','mar_list_commentaire.php?cleparent=' . $cle);
$objOnglet->addLienOnglet('com','url','Commentaires','mar_com_commentaire.php?cleparent=' . $cle);
$objOnglet->finLienOnglet();

// fin de l'onglet
$objOnglet->finOnglet();

// trace d'accès aux marchés
TraceAcces($conn, $objProfil->idnom, "marche", $cle);

// fin de page
$objPage->finPage();
