<?php
require_once('class/database.php');
$conn = database::getIntance();

header('Content-Type: text/csv'); 
header('Content-Disposition: attachment;filename=commande.csv'); 

$requete = "select CMD_CLE, CMD_NUMERO, CMD_NUMEROSIF, POL_NOMPOLE, MAR_NOMMARCHE, 
			PRJ_NOMPROJET, PRG_NOMPROGRAMME, 
			DATE_FORMAT(CMD_DATECMD, '%d-%m-%Y') AS CMD_DATECMD,
			DATE_FORMAT(CMD_DATELIVA, '%d-%m-%Y') AS CMD_DATELIVA,
			DATE_FORMAT(CMD_DATEVA, '%d-%m-%Y') AS CMD_DATEVA,
			DATE_FORMAT(CMD_DATESERV, '%d-%m-%Y') AS CMD_DATESERV,
			CMD_PROGRAMME, CMD_IDDEMANDEUR, CMD_DEMANDEUR, CMD_INTERVENANT, CMD_OBJET,
			ETA_NOMETAT, CMD_TOTALTTC, CMD_TOTALTTCSERV, CMD_TOTALTTCFACT, CMD_TYPEIMPUTATION
			from commande 
			left join pole on CMD_IDPOLE=POL_CLE
			left join marche on CMD_IDMARCHE=MAR_CLE
			left join projet on CMD_IDPROJET=PRJ_CLE
			left join programme on PRJ_IDPROGRAMME=PRG_CLE
			left join fonc_etat on CMD_IDETAT=ETA_CLE
			order by 2 desc";
$statement = $conn->query($requete);
$sortie = fopen('PHP://output', 'w');
//add BOM to fix UTF-8 in Excel
//fputs($sortie, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
fputcsv($sortie, array_map("utf8_decode", array("NUMERO","OBJET","PROGRAMME","NOM POLE","DEMANDEUR","NOM MARCHE",
					"NOM PROGRAMME","NOM PROJET","GESTIONNAIRE","ETAT",
					"NATURE DEPENSE","NUMERO EJ","DATE COMMANDE","DATE LIVRAISON","DATE VA","SERVICE FAIT","TOTAL TTC",
					"LIVRE TTC","FACTURE TTC")),";");
while ($row = $statement->fetch(PDO::FETCH_ASSOC))
{
    fputcsv($sortie, array_map("utf8_decode", array(
    	$row['CMD_NUMERO'],
    	stripslashes($row['CMD_OBJET']),
     	$row['CMD_PROGRAMME'],
		stripslashes($row['POL_NOMPOLE']),
    	$row['CMD_DEMANDEUR'],
    	stripslashes($row['MAR_NOMMARCHE']),
    	stripslashes($row['PRG_NOMPROGRAMME']),
    	stripslashes($row['PRJ_NOMPROJET']),
    	$row['CMD_INTERVENANT'],
    	$row['ETA_NOMETAT'],
    	$row['CMD_TYPEIMPUTATION'],
     	$row['CMD_NUMEROSIF'],
    	$row['CMD_DATECMD'],
    	$row['CMD_DATELIVA'],
    	$row['CMD_DATEVA'],
    	$row['CMD_DATESERV'],
    	str_replace(".", ",", $row['CMD_TOTALTTC']),
    	str_replace(".", ",", $row['CMD_TOTALTTCSERV']),
    	str_replace(".", ",", $row['CMD_TOTALTTCFACT'])
		)),";");
}
fclose($sortie);
