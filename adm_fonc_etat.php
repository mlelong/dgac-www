<?php
class fctetat {
	public $cle;
	public $codeetat;
	public $nometat;
}

class etat {
	private $tabetat = array();
	private $cptetat = 0;

	// creation des objets etat
	public function creEtat($cle)
	{
		global $champT1;
		$conn = database::getIntance();
		$requete   = "select ETA_CLE, ETA_CODEETAT, ETA_NOMETAT 
					FROM fonc_etat where ETA_IDFONC=\"" . $cle . "\" order by ETA_ORDRE";
		$statement = $conn->query($requete);
		while ($row = $statement->fetch(PDO::FETCH_ASSOC))
		{
			$curetat = new fctetat;
			$curetat->cle = $row['ETA_CLE'];
			$curetat->codeetat = $row['ETA_CODEETAT'];
			$curetat->nometat = $row['ETA_NOMETAT'];
			$this->tabetat[$this->cptetat] = $curetat;
			$this->cptetat++;
			$champ = ["aff"=>true,"label"=>$row['ETA_NOMETAT'],"taille"=>40,"tri"=>false,"trinum"=>0];
			array_push($champT1, $champ);
		}
	}

	// test d'existence d'un etat dans la table des fonctions
	private function existEtat($etat)
	{
		foreach ($this->tabetat as $curetat)
		{
			if ($curetat->etat == $etat) return $curetat;
		}
		return false;
	}

	// boucle sur reception des états
	public function majEtat($fonc, $profil)
	{
		$conn = database::getIntance();
		$requete = "delete from workflow where WOR_IDPROFIL=\"" .$profil . "\"";
		$statement = $conn->query($requete);
		$nbetat = count($this->tabetat)*count($this->tabetat);
		// mise à jour des état en découpant la requête pat tranche à cause de la taille maxi d'une requête
		$requete = '';
		$cpt = 0;
		for ($i=1; $i<=$nbetat; $i++)
		{
			$nom = "nom" . $i;
			if (isset($_REQUEST[$nom])) 
			{
				$tab=explode('=', $_REQUEST[$nom]);
				if ($requete == '') $requete = "(NULL,\"" . $fonc . "\",\"" . $profil . "\",\"" . $tab[0] . "\",\"" . $tab[1] . "\")";
				else $requete .= ",(NULL,\"" . $fonc . "\",\"" . $profil . "\",\"" . $tab[0] . "\",\"" . $tab[1] . "\")";
			}
			$cpt++;
			if ($cpt > 50)
			{
				$requete = "insert into workflow values " . $requete; 
				$statement = $conn->query($requete);
				$requete = '';
				$cpt = 0;
			}
		}
		if ($requete != '')
		{
			$requete = "insert into workflow values " . $requete; 
			$statement = $conn->query($requete);
		}
	}

	// Affichage de la liste des états
	public function affEtat($objTab, $fonc, $profil)
	{
		$conn = database::getIntance();
		// recherche des états pour un rôle
		$tabcuretat = array();
		if ($profil != '')
		{
			$requete = "select WOR_IDCURETAT, WOR_IDNEWETAT 
					from workflow where WOR_IDFONC=\"" . $fonc . "\" and WOR_IDPROFIL=\"" . $profil . "\" order by 1, 2";
			$statement = $conn->query($requete);
			$cptetat=0;
			while ($row = $statement->fetch(PDO::FETCH_ASSOC))
			{
				$tabcuretat[$row['WOR_IDCURETAT']][$row['WOR_IDNEWETAT']] = true;
				$cptetat++;
			}
		}
		// affiche des états pour un rôle
		$cptetat=1;
		$ligne = array();
		foreach ($this->tabetat as $ligetat)
		{
			$ligne[0] = $ligetat->nometat;
			$cptcol=1;
			foreach ($this->tabetat as $coletat)
			{
				if (isset($tabcuretat[$ligetat->cle][$coletat->cle])) $opt=" checked='checked' "; 
				else $opt = ''; 
				$ligne[$cptcol] = "<input type='checkbox' name='nom" . $cptetat . "' value='" . $ligetat->cle . "=" . $coletat->cle . "'" . $opt . ">";
				$cptcol++;
				$cptetat++;
			}
			$objTab->affLigne($ligne);
		}
	}
}