<?php
$codefonc='rpm';
include('prepage.php');

$objPage->addParam('rapport', $_REQUEST['rapport']);

if ($cle != '') $objPage->addParam('cle', $cle);
$objPage->debPage('center');

// requête d'accès à la base 
	$requete = "select RAP_CLE, RAP_NOMRAPPORT, RAP_NOMEXE, RAP_CFGFILTER, RAP_CFGTABLEAU,
				RAP_CFGDESC, RAP_REQUETE, RAP_LIEN, RAP_TYPERAPPORT, RAP_DESCRIPTION
				from rapport where RAP_CLE=\"" . $cle . "\"";
$statement = $conn->query($requete);
$resultat = $statement->fetch(PDO::FETCH_ASSOC);

$nomrap = $resultat['RAP_NOMRAPPORT'];
if ($resultat['RAP_CFGFILTER'] != '') eval ("\$champFL1 = " . $resultat['RAP_CFGFILTER'] . ";");
eval ("\$champT1 = " . $resultat['RAP_CFGTABLEAU'] . ";");
eval ("\$descT1 = " . $resultat['RAP_CFGDESC'] . ";");
$requete = $resultat['RAP_REQUETE'];
if ($resultat['RAP_LIEN'] != '') eval ("\$lienT1 = " . $resultat['RAP_LIEN'] . ";");
else $lienT1=array();

// préparation du tableau
$objTab = new tableau('1');

$objTab->addBouton("reset","RETOUR","sui_list_rapport.php");

// traitement des cas particuliers d'accès à la base
if ($nomrap == "Budget par programme" || $nomrap == "Budget par projet")
{
	if (!isset($f_annee) || $f_annee == '') $f_annee = date("Y");
	if (!isset($f_programme) || $f_programme == '') $f_programme = "P613";
}
// traitement standard d'accès à la base
$order = '';
if(stristr($requete, 'order') !== FALSE)
{
	$tab = explode("order by", $requete);
	$requete = $tab[0];
	if (isset($tab[1])) $order = " order by " . $tab[1];
}
if(stristr($requete, 'where') === FALSE) $requete .= " where 1 ";
$requete .= $objTab->majRequete($order); // ajout tri et pagination si besoin

// affichage des boutons d'enchainement
//$objTab->addBouton("button","EXPORTER","mar_csv_marche.php");

// gestion des paramètres de lien
if (is_array($lienT1 ))
{
	foreach ($lienT1 as $lien)
	{
		$objTab->setLien($lien[0],$lien[1],$lien[2],$lien[3]);
	}
}	

// affichage du tableau
$objTab->affTableau($requete);

// fin de page
$objPage->finPage();
