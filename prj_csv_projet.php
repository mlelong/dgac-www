<?php
require_once('class/database.php');
$conn = database::getIntance();

header('Content-Type: text/csv'); 
header('Content-Disposition: attachment;filename=projet.csv'); 

$requete = "select PRJ_CLE, PRJ_TYPE, PRJ_CODE, PRJ_NOMPROJET, PRJ_LIBELLE, 
			PRJ_IDPOLE, POL_NOMPOLE, PRJ_IDPROGRAMME, PRG_NOMPROGRAMME
			from projet	
			left join pole on PRJ_IDPOLE=POL_CLE
			left join programme on PRJ_IDPROGRAMME=PRG_CLE order by 3";
$statement = $conn->query($requete);
$sortie = fopen('PHP://output', 'w');
//add BOM to fix UTF-8 in Excel
fputs($sortie, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
fputcsv($sortie, array("CLE PRJ","TYPE","CODE","NOM PROJET","LIBELLE PROJET","CLE POLE","NOM POLE","CLE PROGRAMME","NOM PROGRAMME"),";");
while ($row = $statement->fetch(PDO::FETCH_ASSOC))
{
    fputcsv($sortie, array(
     	$row['PRJ_CLE'],
     	$row['PRJ_TYPE'],
    	$row['PRJ_CODE'],
		stripslashes($row['PRJ_NOMPROJET']),
		stripslashes($row['PRJ_LIBELLE']),
     	$row['PRJ_IDPOLE'],
     	$row['POL_NOMPOLE'],
     	$row['PRJ_IDPROGRAMME'],
		stripslashes($row['PRG_NOMPROGRAMME'])
		),";");
}
fclose($sortie);
