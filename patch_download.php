<?php
if(isset($_REQUEST['fic'])) 
{
	$fic=$_REQUEST['fic'];
	$nomfic=basename($fic);
	header('Content-Description: File Transfer');
	header('Content-Type: application/octet-stream');
	header("Content-Disposition: attachment; filename=\"".$nomfic."\"");
	header('Content-Transfer-Encoding: binary');
	header('Expires: 0');
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	header('Pragma: public');
	header('Content-Length: ' . filesize($fic));
	ob_clean();
	flush();
	readfile($fic);
	exit;
}	
