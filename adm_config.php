<?php
include('config/adm_config.php');
require_once('prepage.php');
$objPage->debPage('center');

// vidage des tables
$requete = 'truncate table fonctionnalite';
$statement = $conn->query($requete);
$requete = 'truncate table etat';
$statement = $conn->query($requete);
$requete = 'truncate table permission';
$statement = $conn->query($requete);

$listdroit='';
$listfonc='';
$cptfonc = 0;
foreach ($champC1 as $curchamp)
{
	// récupération des champs
	if ($curchamp['aff'] === false) continue; // on ne traite pas
	if (isset($curchamp['type'])) $type=$curchamp['type']; else $type = "";
	if (isset($curchamp['code'])) $code=$curchamp['code']; else $code = "";
	if (isset($curchamp['ordre'])) $ordre=$curchamp['ordre']; else $ordre = "";
	if (isset($curchamp['nom'])) $nom=$curchamp['nom']; else $nom = "";
	if (isset($curchamp['modifiable'])) $modifiable=$curchamp['modifiable']; else $modifiable = "";
	
	if (isset($curchamp['droit'])) $droit=$curchamp['droit']; else $droit = "";
	if (isset($curchamp['table'])) $table=$curchamp['table']; else $table = "";
	// test de cohérence
	if ($type == 'fonctionnalite')
	{
		if ($code == '' || $ordre == '' || $nom == '') continue;
		if (isset($table['nom'])) $nomtable=$table['nom']; else $nomtable = "";
		if (isset($table['cle'])) $cle=$table['cle']; else $cle = "";
		if (isset($table['visibilite'])) $visibilite=$table['visibilite']; else $visibilite = "";
		if (isset($table['etat'])) $etat=$table['etat']; else $etat = "";
		
		$requete = "insert into fonctionnalite (FON_ORDRE,FON_CODEFONC,FON_NOMFONC,
					FON_NOMTAB,FON_CLETAB,FON_VISIBLETAB,FON_ETATTAB)
					values (\"" . $ordre . "\",\"" . $code . 
					"\",\"" . addslashes($nom) . "\",\"" . $nomtable . "\",\"" . $cle .
					"\",\"" . $visibilite . "\",\"" . $etat . "\")";
		$statement = $conn->query($requete);
		$cptfonc++;
		if ($droit != '')
		{
			if (isset($droit['lec'])) $lec=0x20; else $lec=0x00;
			if (isset($droit['cre'])) $cre=0x10; else $cre=0x00;
			if (isset($droit['maj'])) $maj=0x08; else $maj=0x00;
			if (isset($droit['sup'])) $sup=0x04; else $sup=0x00;
			if (isset($droit['exe'])) $exe=0x02; else $exe=0x00;
			if (isset($droit['vis'])) $vis=0x40; else $vis=0x00;
			if (isset($droit['sui'])) $sui=0x01; else $sui=0x00;
			if (isset($droit['adm'])) $adm=0x80; else $adm=0x00;
			$listdroit .= chr($lec | $cre | $maj | $sup | $exe | $vis | $sui| $adm);
		}
		else
		{
			$listdroit .= chr(0x00);
		}
		if ($listfonc=='') $listfonc=$code; else $listfonc .= ';' . $code;
	}
}
if ($listdroit != '')
{
	for ($i=$cptfonc; $i<50; $i++)
	{
		$listdroit .= chr(0x00);
	}
	$requete = "update config set CFG_MASQDROIT=\"" . $listdroit . "\", CFG_LISTFONC=\"" . $listfonc . "\" where CFG_CLE=1";
	$statement = $conn->query($requete);
}
	
$objPage->tampon .= "<div style='text-align:center;'><h2>Configuration prise en compte</h2></div>";

// fin de page
$objPage->finPage();
