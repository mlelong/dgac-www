<?php
include('config/adm_maj_notification.php');
$codefonc='adm';
$codemenu='not';
require_once('prepage.php');

$objForm = new formulaire('1');

// création
if($typeaction == "creation") $objForm->initChamp();

// modification
if($typeaction == "modification")
{
	$requete = "select NOT_CLE, NOT_NOMNOTIF, NOT_DESCRIPTION	
			from notification where NOT_CLE=\"" . $cle . "\"";
	$resultat = $objForm->mapChamp($requete);
}

// réception des paramètres
if($typeaction == "reception")
{
// contrôle des paramètres
	$objForm->recChamp();
	while($objForm->erreur == '')
	{
		break;
	}

	if ($objForm->erreur == '')
	{
		$objForm->majBdd('notification', 'NOT_CLE', $cle);
		RedirURL("adm_list_notification.php");
		exit();
	}
}

// Affichage du début de la page
if ($cle != '') $objPage->addParam('cle', $cle);
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();
	
// affichage des boutons d'enchainement
$objForm->addBouton("reset","RETOUR","adm_list_notification.php");
if (($cle == '' && $objProfil->cre) || ($cle != '' && $objProfil->maj))
{
	$objForm->addBouton("submit","ENVOYER");
}
else $objForm->setLecForm();

$objForm->affFormulaire();
$objForm->finFormulaire();

// fin de page
$objPage->finPage();
