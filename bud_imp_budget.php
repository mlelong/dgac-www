<?php
include('config/bud_imp_ligne.php');
$codefonc='bud';
require_once('prepage.php');

$objForm = new formulaire('1');

// création
if (!isset($typeaction) || $typeaction == '') $typeaction = 'creation';
if($typeaction == "creation") $objForm->initChamp();

// réception des paramètres
if($typeaction == "reception")
{
// contrôle des paramètres
	$objForm->recChamp();
	while ($objForm->erreur == '')
	{
		break;
	}
	if ($objForm->erreur == '')
	{
		$rapport = "";
		// récupération du fichier
		if ((isset($_FILES['nomfic']['name'][0])&&($_FILES['nomfic']['error'][0] == UPLOAD_ERR_OK)))
		{
			$namefic = ctlNomFichier($_FILES['nomfic']['name'][0]);
			$chemin_destination = "document/" . $namefic;   
			move_uploaded_file($_FILES['nomfic']['tmp_name'][0], $chemin_destination); 
		}
		if (!file_exists($chemin_destination)) // vérification pour savoir si le fichier existe
			{$objForm->erreur="nomfic"; $objForm->libelle="Le fichier " . $namefic . " n'a pas pu être chargé"; break;}
		//
		// création ou ajout des lignes pour le budget
		//
		$conn->setAttribute(PDO::ATTR_AUTOCOMMIT,0); 
		$conn->beginTransaction();
		$flag = false;
		while(true)
		{
			$fichier = "document/" . $namefic;
//			Trace("Traitement du fichier " . $fichier);
			$handle = fopen ($fichier,"r");
			// entete
			$ligne = fgetcsv($handle, 1000, ";");
			// test si utf8
//			echo 'encoding = ' . mb_detect_encoding($ligne[0]);
			if (mb_detect_encoding($ligne[0] != 'UTF-8')) $flagutf8 = true;
			else $flagutf8 = false;
//			if ($flagutf8) echo "pas UTF-8";
			if ($flagutf8) $ligne = array_map("utf8_encode",$ligne);
			$idbudget = $cleparent;
			// remplace
			if ($flagsup == 'o')
			{
//				Trace("Suppression des lignes existantes");
				$requete = "TRUNCATE TABLE programme";
				$statement = $conn->query($requete);
				$requete = "TRUNCATE TABLE budget_ligne";
				$statement = $conn->query($requete);
				$requete = "delete from projet where PRJ_TYPE != \"Projet\"";
				$statement = $conn->query($requete);
			}
			// 
			// traitement du fichier
			//
			$libelle = '';
			// lignes des uos
//			Trace("Début de la boucle des lignes");
			while ($ligne = fgetcsv($handle, 1000, ";")) 
			{
		//		print_r($ligne);
				if ($flagutf8) $ligne = array_map("utf8_encode",$ligne);
				// informations lignes
				$cleligne = $ligne[0];
				$idpap = $ligne[1];
				$lignebud = $ligne[2];
				if ($lignebud == '') continue;
				$codeprojet = $ligne[3];
				$typeprojet = $ligne[4];
				$nomprojet = $ligne[5];
				$nompole = $ligne[6];
				$imputation = $ligne[7];
				if ($imputation == "O") $imputation = "Obligatoire";
				else if ($imputation == "I") $imputation = "Inéluctable";
				else if ($imputation == "A") $imputation = "Autre";
				else $imputation = "";
				$naturedep = $ligne[8];
				$ae = str_replace(" ", "", $ligne[9]);
				$cp = str_replace(" ", "", $ligne[10]);
				// Contrôle de l'existence du pôle
				$requete = "select POL_CLE, POL_NOMPOLE from pole where POL_NOMPOLE=\"" . $nompole . "\"";
				$statement = $conn->query($requete);
				if ($statement->rowCount() == 0) $idpole = 0;
				else
				{
					$resultat = $statement->fetch(PDO::FETCH_ASSOC);
					$idpole = $resultat['POL_CLE'];
				}
/*				
				// Contrôle de l'existence du programme
				$requete = "select PRG_CLE, PRG_NOMPROGRAMME from programme where PRG_NOMPROGRAMME=\"" . $nomprogramme . "\"";
				$statement = $conn->query($requete);
				if ($statement->rowCount() == 0) 
				{
					$requete = "insert into programme (PRG_CLE, PRG_NOMPROGRAMME, PRG_LIBELLE)
								values (\"" . $nomprogramme . "\",\"" . $libelle . "\")";
					$statement = $conn->query($requete);
					$idprogramme = $conn->lastInsertId();
				}
				else
				{
					$resultat = $statement->fetch(PDO::FETCH_ASSOC);
					$idprogramme = $resultat['PRG_CLE'];
				}
*/				
				// Contrôle de l'existence du projet
				$requete = "select PRJ_CLE, PRJ_CODE, PRJ_NOMPROJET from projet where PRJ_CODE=\"" . $codeprojet . "\"";
				$statement = $conn->query($requete);
				$libelle = '';
				if ($statement->rowCount() == 0) 
				{
					$requete = "insert into projet (PRJ_TYPE, PRJ_CODE, PRJ_NOMPROJET, PRJ_IDPOLE, PRJ_LIBELLE)
								values (\"" . $typeprojet . "\",\"" . $codeprojet . "\",\"" . $nomprojet . "\",\"" . $idpole . "\",\"" . $libelle . "\")";
					$statement = $conn->query($requete);
					$idprojet = $conn->lastInsertId();
				}
				else
				{
					$resultat = $statement->fetch(PDO::FETCH_ASSOC);
					$idprojet = $resultat['PRJ_CLE'];
					$requete = "update projet set 
								PRJ_TYPE=\"" . $typeprojet . "\",
								PRJ_CODE=\"" . $codeprojet . "\",
								PRJ_NOMPROJET=\"" . $nomprojet . "\"
								where PRJ_CLE=\"" . $resultat['PRJ_CLE'] . "\"";
					$statement = $conn->query($requete);
				}
				// insertion ou modification de la ligne budgétaire	
				$requete = "select BUDL_CLE from budget_ligne where BUDL_IDBUDGET=\"" . $idbudget . "\" and BUDL_LIGNE=\"" . $lignebud . "\" and BUDL_IDPROJET=\"" . $idprojet . "\"";
				$statement = $conn->query($requete);
				if ($naturedep == 'F') 
				{
					$aedotafonct = $ae;
					$cpdotafonct = $cp;
					$aedeblfonct = $ae;
					$cpdeblfonct = $cp;
					$aedotainves = 0;
					$cpdotainves = 0;
					$aedeblinves = 0;
					$cpdeblinves = 0;
				}
				else 
				{
					$aedotafonct = 0;
					$cpdotafonct = 0;
					$aedeblfonct = 0;
					$cpdeblfonct = 0;
					$aedotainves = $ae;
					$cpdotainves = $cp;
					$aedeblinves = $ae;
					$cpdeblinves = $cp;
				}
				if ($statement->rowCount() == 0) 
				{
					$requete = "insert into budget_ligne (BUDL_LIGNE, BUDL_IDBUDGET, BUDL_IDPAP, BUDL_IDPROJET, BUDL_TYPELIG, BUDL_LIBELLE, 
														  BUDL_AEDOTAFONCT, BUDL_AEDOTAINVES, BUDL_CPDOTAFONCT, BUDL_CPDOTAINVES,
														  BUDL_AEDEBLFONCT, BUDL_AEDEBLINVES, BUDL_CPDEBLFONCT, BUDL_CPDEBLINVES)
								values (\"" . $lignebud . "\",\"" . $idbudget . "\",\"" . $idpap . "\",\"" . $idprojet . "\",\"" . $imputation . "\",\"" . $libelle . "\",\"" 
											. $aedotafonct . "\",\"" . $aedotainves . "\",\"" . $cpdotafonct . "\",\"" . $cpdotainves . "\",\""
											. $aedeblfonct . "\",\"" . $aedeblinves . "\",\"" . $cpdeblfonct . "\",\"" . $cpdeblinves . "\")";
					$statement = $conn->query($requete);
				}	
				else
				{
					$resultat = $statement->fetch(PDO::FETCH_ASSOC);
					$budl_cle = $resultat['BUDL_CLE'];
					$requete = "update budget_ligne set 
								BUDL_TYPELIG=\"" . $imputation . "\", 
								BUDL_AEDOTAFONCT=BUDL_AEDOTAFONCT+\"" . $aedotafonct . "\", 
								BUDL_AEDOTAINVES=BUDL_AEDOTAINVES+\"" . $aedotainves . "\", 
								BUDL_CPDOTAFONCT=BUDL_CPDOTAFONCT+\"" . $cpdotafonct . "\", 
								BUDL_CPDOTAINVES=BUDL_CPDOTAINVES+\"" . $cpdotainves . "\" 
								BUDL_AEDEBLFONCT=BUDL_AEDEBLFONCT+\"" . $aedeblfonct . "\", 
								BUDL_AEDEBLINVES=BUDL_AEDEBLINVES+\"" . $aedeblinves . "\", 
								BUDL_CPDEBLFONCT=BUDL_CPDEBLFONCT+\"" . $cpdeblfonct . "\", 
								BUDL_CPDEBLINVES=BUDL_CPDEBLINVES+\"" . $cpdeblinves . "\" 
								where BUDL_CLE=\"" . $budl_cle . "\"";
					$statement = $conn->query($requete);
				}
			}
			fclose ($handle);
			$conn->commit(); 
			$flag = true;
//			Trace("COMMIT");
			break;
		}
		if ($flag == false) 
		{
//			Trace("ROLLBACK");
			$conn->rollBack();
			$objForm->erreur="nomfic";
			if ($objForm->libelle == '') $objForm->libelle = "Erreur sur chargement des lignes budgétaires"; 
		}
	}
	$result= unlink($fichier); // suppression du fichier
	if ($objForm->libelle == '')
	{
		RedirURL("bud_list_ligne.php");
		exit();
	}
}

// Affichage du début de la page
if ($cle != '') $objPage->addParam('cle', $cle);
$objPage->debPage('center');

// Affichage du formulaire
$objForm->debFormulaire();

// affichage des boutons d'enchainement
$objForm->addBouton("reset","RETOUR","bud_list_ligne.php");
if (($cle == '' && $objProfil->cre) || ($cle != '' && $objProfil->maj))
{
	$objForm->addBouton("submit","ENVOYER");
}
else $objForm->setLecForm();

$objForm->affFormulaire();
$objForm->finFormulaire();

// fin de page
$objPage->finPage();
